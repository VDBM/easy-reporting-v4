
    -   








-------------------------------------------------------------------------------------------------------

    -   We could refactor the router to extend the mediator class, but this doesn't give any advantage.
        So the router is basically a specific, more complex mediator.

    -   The mediator adds the `this.events` property to store the events, and the event methods `this.on()` and `this.off()`.
    -   The mediator supports named events, further divided into actions for that event: `change`, `click`, `init`, `render`, `remove`
        These actions get triggered by modules that subclass, implement or use the mediator:
            1)
            component.js uses a mediator to store events that manage its own internal state.
            component.js implements `render` when the components'- HTML gets added to the DOM.
            component.js implements `remove` when the components' HTML gets removed from the DOM.
            2)
            application_easyreporting.js uses a mediator to store application-level events. Think print, export, etc.
            application_easyreporting.js implements it's own version of the `this.on()` method to redirect to either the router or the mediator.
            application_easyreporting.js uses the `remove()` method on any active components when the router notifies us of a hashchange.
            So if we use `app.on( '/', ... )`, both our own handler and a routechange handler will be registered for that event inside the router.
            Since we have a single page application, chnaging route implies we navigate to a different state, so we `.remove()` any active components.
            We could make it even more complex by not removing any components that will stay active, but swince we sahre alot of components between states, it's the safest to reset all components.
            This makes sure any internal remove logic is handled before the new page gets rendered.
            3)
            Specific components, like the datatable, will register their own handlers upon component `init()`.
            
    -   We readded the `.bind()` method to the component class so we can manually bind the evnts handlers as well.
        This was needed for components that don't actually render anything, but still want to use the component event binding service.
        We might want to split component into an evvnt binding and a mediator part as well then so we can inherit from that subclass and avoid having a useless `render()` method on a renderless component.
        
    -   Added the this.event_types proeprty to the base component class. Most extensions will overwrite this. Stems from the fact that we wanted to use the 'keypress' event binding isnide the popover extension.
        Sicne this wasn't natively supported, we either changed the class syntax or we had to bidn this manually due to a call to `_bind()`, which would also require us to rewrite thr remove method.
        So we opted for this solution.
        We could change the mediator to push non change/click events into the this.event_types array, but that would imply differentiating between native events and custom events.
    -   Removed the state.lenth check from the datatable components. We ahve to be able to init the components without a state specified. ELse we're forced to wait with creating those components until we actually ajax the data.  
        
        
        
        
    -   MEDIATOR is an object that stores event handlers by name and type.
    -   COMPONENT is a MEDIATOR that renders html.
    -   DATATABLE is a COMPONENT that renders a html table.
    -   DATATABLE_SORT is a DATATABLE that can sort the html table rows by clicking on their header.
    -   DATATABLE_EDIT is a DATATABLE_SORT that can change the value of a cell by clicking on that cell.
    -   