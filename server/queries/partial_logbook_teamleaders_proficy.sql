SELECT
TED.TEDet_id AS 'id',
PL.PL_Id AS 'line',
TED.Start_Time AS 'starttime',
dbo.fnLocal_GetWedgeRowDumpDowntimeCorrection( TED.Duration, TED.TEFault_Id ) AS 'value',
dbo.fnLocal_getReasonDesc( TED.Reason_Level1 ) AS 'data_area',
dbo.fnLocal_getReasonDesc( TED.Reason_Level2 ) AS 'data_location',
dbo.fnLocal_getReasonDesc( TED.Reason_Level3 ) AS 'data_failure',
dbo.fnLocal_getReasonDesc( TED.Reason_Level4 ) AS 'data_cause',
TEF.TEFault_Name AS 'data_reason',
CM.Comment_Text AS 'data_comments'
FROM [dbo].[Timed_Event_Details] AS TED WITH ( NOLOCK )
LEFT OUTER JOIN Comments AS CM WITH (NOLOCK) ON TED.Cause_Comment_Id = CM.Comment_Id
LEFT OUTER JOIN Timed_Event_Fault AS TEF WITH (NOLOCK) ON TED.TEFault_Id = TEF.TEFault_Id
LEFT OUTER JOIN Prod_Units AS PU WITH (NOLOCK) ON TED.PU_id = PU.PU_ID
LEFT OUTER JOIN Prod_Lines AS PL WITH (NOLOCK) ON PU.PL_id = PL.PL_id
WHERE
TED.TEDet_id IN ( ${ dependencies.partial_logbook_teamleaders_easyreporting.map( entry => entry.data_reference ).filter( reference => reference !== 'NULL' ).join( ', ' ) || null } )
ORDER BY [line], [starttime];