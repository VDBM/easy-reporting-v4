CREATE TABLE [${ database }].[dbo].[${name_table}] (
    [id]        [INT]               IDENTITY( 1, 1 ) NOT NULL,
    [line]      [NVARCHAR]( 10 )
);