SELECT
[ID] AS 'entry_id',
[Type] AS 'entry_type',
[Line] AS 'entry_line',
[Date] AS 'starttime',
[Created] AS 'entry_created',
[User] AS 'entry_user',
[Team] AS 'entry_team',
[Area] AS 'entry_area',
[Content] AS 'entry_content',
[Replies] AS 'entry_replies',
[Binds] AS 'data_reference',
[Used] AS 'entry_used',
[Breakdown] AS 'entry_breakdown',
[PM] AS 'entry_pm',
[Extension_1] AS 'entry_extension_1', 
[Extension_2] AS 'entry_extension_2',
[Extension_3] AS 'entry_extension_3'
FROM
dbo.LOGBOOK_MNP WITH (NOLOCK)
WHERE
[Line] IN ( ${ resources.line } )
AND [Date] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
${ resources.filters }