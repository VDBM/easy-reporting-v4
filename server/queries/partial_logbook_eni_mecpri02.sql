SELECT
EVEN.[EventID] AS 'id',
EVEN.[Time] AS 'starttime',
NAME.[Name] AS 'user',
LINE.[Line] AS 'line',
AREA.[Area] AS 'area',
EQUI.[Equipment] AS 'equipment',
EVEN.[Action] AS 'action'
/*,
PART.[classification] AS [classification],
PART.[duration] AS [duration],
RESP.[name] AS [responsibility],
PART.[priority] AS [priority],
PART.[source] AS [source],
PART.[type] AS [type]
*/
FROM [logbook].[dbo].[tblEvents] AS EVEN
LEFT OUTER JOIN [logbook].[dbo].[tblNames] AS NAME ON EVEN.[NameID] = NAME.[NameID]
LEFT OUTER JOIN [logbook].[dbo].[tblLines] AS LINE ON EVEN.[LineID] = LINE.[LineID]
LEFT OUTER JOIN [logbook].[dbo].[tblAreas] AS AREA ON EVEN.[AreaID] = AREA.[AreaID]
LEFT OUTER JOIN [logbook].[dbo].[tblEquip] AS EQUI ON EVEN.[EquipID] = EQUI.[EquipID]
/*
LEFT OUTER JOIN [EasyReporting_MNP].[dbo].[Logbook_ENI_Partials] AS PART ON EVEN.[EventID] = PART.[reference]
LEFT OUTER JOIN [logbook].[dbo].[tblNames] AS RESP ON PART.[responsibility] = RESP.[NameID]
*/
WHERE [Line] IN ( ${ resources.line } )
AND [Time] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
/*ORDER BY [Line], [Time] ASC*/