SELECT
[Test_Id] AS 'id',
CASE [PL_Id]
	WHEN  2 THEN 'line1'
	WHEN 15 THEN 'line2'
	WHEN 16 THEN 'line3'
    WHEN 17 THEN 'line4'
    WHEN 18 THEN 'line5'
    WHEN 19 THEN 'line6'
	ELSE 'not mnp'
END AS 'line',
'mnp' AS 'department',
[Result_On] AS 'starttime',
CAST( 
    CASE
        WHEN [PSEUDOVIEW].[shiftgroup_time] < 615 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date] - 1, '22' )
        WHEN [PSEUDOVIEW].[shiftgroup_time] > 614 AND [PSEUDOVIEW].[shiftgroup_time] < 1415 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date], '06' )
        WHEN [PSEUDOVIEW].[shiftgroup_time] > 1414 AND [PSEUDOVIEW].[shiftgroup_time] < 2215 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date], '14' )
        WHEN [PSEUDOVIEW].[shiftgroup_time] > 2214 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date], '22' )
    END
    AS INTEGER
) AS 'shift',
COALESCE( [Var_Desc], [Test_name] ) AS 'text',
[PU_Desc] AS 'location',
[Result] AS 'result',
[Sampling_Interval] AS 'interval',
[Sampling_Offset] AS 'offset',
[Var_Id] AS 'variable'
FROM (
    SELECT
    TEST.[Test_Id],
    LINE.[PL_Id],
    TEST.[Result_On],
    CONVERT( NVARCHAR, TEST.[Result_On], 12 ) AS 'shiftgroup_date',
	DATEPART( HOUR, TEST.[Result_On] ) * 100 + DATEPART( MINUTE, TEST.[Result_On] ) AS 'shiftgroup_time',
    VARS.[Var_Desc],
    VARS.[Test_name],
    UNIT.[PU_Desc],
    TEST.[Result],
    VARS.[Sampling_Interval],
    VARS.[Sampling_Offset],
    VARS.[Var_Id]
    FROM [dbo].[Variables] AS VARS WITH (NOLOCK)
    LEFT OUTER JOIN [dbo].[Prod_Units] AS UNIT WITH (NOLOCK) ON VARS.[PU_Id] = UNIT.[PU_Id]
    LEFT OUTER JOIN [dbo].[Prod_Lines] AS LINE WITH (NOLOCK) ON UNIT.[PL_Id] = LINE.[PL_Id]
    LEFT OUTER JOIN [dbo].[Specifications] AS SPEC WITH (NOLOCK) ON VARS.[Spec_Id] = SPEC.[Spec_Id]
    LEFT OUTER JOIN [dbo].[Tests] AS TEST WITH (NOLOCK) ON VARS.[Var_Id] = TEST.[Var_Id]
    WHERE
    VARS.[Var_Id] IN ( 28, 4278, 4542, 4190, 4366, 4454 )
    AND TEST.[Result_On] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
    ${ resources.filters }
) AS PSEUDOVIEW
ORDER BY [line], [starttime]