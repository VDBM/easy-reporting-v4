DECLARE @line VARCHAR(10) = '${ resources.line_normalized }';
DECLARE @starttime VARCHAR(30) = '${ resources.starttime }';
DECLARE @endtime VARCHAR(30) = '${ resources.endtime }';
DECLARE @id_weight VARCHAR(5);
IF @line LIKE 'line4%'
BEGIN
	DECLARE @id_accepted VARCHAR(5) = (
		CASE @line
			WHEN 'line4'  THEN 6432
			WHEN 'line4L' THEN 6454
			WHEN 'line4R' THEN 6395
		END
	);
	DECLARE @id_total VARCHAR(5) = (
		CASE @line
			WHEN 'line4'  THEN 6437
			WHEN 'line4L' THEN 6459
			WHEN 'line4R' THEN 6400
		END
	);
	SET @id_weight = (
		CASE @line
			WHEN 'line4'  THEN 6435
			WHEN 'line4L' THEN 6457
			WHEN 'line4R' THEN 6398
		END
	);
	SELECT
	[line] AS 'line',
    'packing' AS 'department',
	[date] AS 'starttime',
    CAST(
        CASE
            WHEN [shiftgroup_time] < 615 THEN CONVERT( NVARCHAR, [date] - 1, 12 )
            ELSE [shiftgroup_date]
        END
        AS INTEGER
    ) AS 'day',
    CAST(
        CASE
            WHEN [shiftgroup_time] < 615 THEN CONVERT( INTEGER, CONCAT( CONVERT( NVARCHAR, [date] - 1, 12 ), '22' ) )
            WHEN [shiftgroup_time] > 614 AND [shiftgroup_time] < 1415 THEN CONVERT( INTEGER, CONCAT( [shiftgroup_date], '06' ) )
            WHEN [shiftgroup_time] > 1414 AND [shiftgroup_time] < 2215 THEN CONVERT( INTEGER, CONCAT( [shiftgroup_date], '14' ) )
            WHEN [shiftgroup_time] > 2214 THEN CONVERT( INTEGER, CONCAT( [shiftgroup_date], '22' ) )
        END
        AS INTEGER
    ) AS 'shift',
    NULL AS 'user',
    NULL AS 'team',
    NULL AS 'text',
	CAST( [cans_accepted] AS INTEGER ) AS 'data_cans_accepted',
	CAST( [weight] AS REAL ) AS 'data_cans_weight',
	CASE
		WHEN CAST( [weight] AS REAL ) > 0 THEN
           ( ROUND( CONVERT( REAL, [weight] ) * 100, 0 ) - ROUND( CONVERT( REAL, [target] ) * 100, 0 ) ) / 100
		ELSE NULL
	END AS 'data_cans_overpack',
	ROUND( ( CONVERT( REAL, [cans_total] ) - CONVERT( REAL, [cans_accepted] ) ) / CONVERT( REAL, [cans_total] ) * 100.00, 2 )  AS 'data_cans_rejects',
/*ROUND ( CONVERT( float, 100.00 - CONVERT( float, [cans_accepted] ) / ( CONVERT( float, [cans_total] ) / 100.00 ) ), 2 ) AS 'rejects_alt',*/
	CAST( [target] AS REAL ) AS 'data_cans_target'
	FROM (
		SELECT
		@line AS 'line',
		[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'cans_accepted' AS 'type',
		[Result] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		WHERE
		TESTS.[Var_Id] = @id_accepted
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	UNION
		SELECT
		@line AS 'line',
		[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'cans_total' AS 'type',
		[Result] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		WHERE
		TESTS.[Var_Id] = @id_total
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	UNION
		SELECT
		@line AS 'line',
		[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'weight' AS 'type',
		[Result] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		WHERE
		TESTS.[Var_Id] = @id_weight
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	UNION
		SELECT
		@line AS 'line',
		[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'target' AS 'type',
		SPECS.[L_Reject] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		LEFT OUTER JOIN dbo.Variables AS VARS WITH (NOLOCK) ON TESTS.[Var_Id] = VARS.[Var_Id]
		LEFT OUTER JOIN dbo.Production_Starts AS PRODS WITH (NOLOCK) ON (
			VARS.[PU_Id] = PRODS.[PU_Id]
			AND (
				TESTS.[Result_On] BETWEEN PRODS.[Start_Time] AND PRODS.[End_Time]
				OR
				PRODS.[End_Time] IS NULL AND TESTS.[Result_On] > PRODS.[Start_Time]
			)
		)
		LEFT OUTER JOIN dbo.Var_Specs AS SPECS WITH (NOLOCK) ON (
           TESTS.[Var_Id] = SPECS.[Var_Id]
           AND PRODS.[Prod_Id] = SPECS.[Prod_Id]
           AND (
               TESTS.[Result_On] BETWEEN SPECS.[Effective_Date] AND SPECS.[Expiration_Date]
               OR (
                   TESTS.[Result_On] > SPECS.[Effective_Date]
                   AND SPECS.[Expiration_Date] IS NULL
               )
           )
       )
		WHERE
		TESTS.[Var_Id] = @id_weight
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	) AS NETWEIGHTS
	PIVOT (
		MAX( [value] )
		FOR [type] IN ( [cans_accepted], [cans_total], [weight], [target] )
	) AS AGGREGATION
	ORDER BY [date] ASC
END
ELSE
BEGIN
	DECLARE @id_cans VARCHAR(5) = (
		CASE @line
			WHEN 'line1'  THEN 13727
			WHEN 'line2'  THEN 13852
			WHEN 'line3'  THEN 13982
            /*
			--WHEN 'line4'  THEN NULL  -- old school
			--WHEN 'line4L' THEN 14110 -- broken
			--WHEN 'line4R' THEN 14134 -- broken
            */
			WHEN 'line5'  THEN 14358
			WHEN 'line6'  THEN 14482
		END
	);
	DECLARE @id_rejects VARCHAR(5) = (
		CASE @line
			WHEN 'line1'  THEN 13712
			WHEN 'line2'  THEN 13837
			WHEN 'line3'  THEN 13967
            /*
			--WHEN 'line4'  THEN NULL  -- old school
			--WHEN 'line4L' THEN 14095 -- broken
			--WHEN 'line4R' THEN 14119 -- broken
            */
			WHEN 'line5'  THEN 14343
			WHEN 'line6'  THEN 14467
		END
	);
	SET @id_weight = (
		CASE @line
			WHEN 'line1'  THEN 13729
			WHEN 'line2'  THEN 13854
			WHEN 'line3'  THEN 13984
            /*
			--WHEN 'line4'  THEN NULL  -- old school
			--WHEN 'line41' THEN 14112 -- broken -- worked on 2017-05-26 and 2017-05-27 only 
			--WHEN 'line42' THEN 14136 -- broken -- worked on 2017-05-17, 2017-05-26 and 2017-05-27 only 
            */
			WHEN 'line5'  THEN 14360
			WHEN 'line6'  THEN 14484
		END
	);
	SELECT
	[line] AS 'line',
    'packing' AS 'department',
	[date] AS 'starttime',
    CAST(
        CASE
            WHEN [shiftgroup_time] < 615 THEN CONVERT( NVARCHAR, [date] - 1, 12 )
            ELSE [shiftgroup_date]
        END
        AS INTEGER
    ) AS 'day',
    CAST(
        CASE
            WHEN [shiftgroup_time] < 615 THEN CONVERT( INTEGER, CONCAT( CONVERT( NVARCHAR, [date] - 1, 12 ), '22' ) )
            WHEN [shiftgroup_time] > 614 AND [shiftgroup_time] < 1415 THEN CONVERT( INTEGER, CONCAT( [shiftgroup_date], '06' ) )
            WHEN [shiftgroup_time] > 1414 AND [shiftgroup_time] < 2215 THEN CONVERT( INTEGER, CONCAT( [shiftgroup_date], '14' ) )
            WHEN [shiftgroup_time] > 2214 THEN CONVERT( INTEGER, CONCAT( [shiftgroup_date], '22' ) )
        END
        AS INTEGER
    ) AS 'shift',
    NULL AS 'user',
    NULL AS 'team',
    NULL AS 'text',
	CAST( [cans] AS INTEGER ) AS 'data_cans_accepted',
	CAST( [weight] AS REAL ) AS 'data_cans_weight',   
	CASE
		WHEN CAST( [weight] AS REAL ) > 0 THEN
           ( ROUND( CONVERT( REAL, [weight] ) * 100, 0 ) - ROUND( CONVERT( REAL, [target] ) * 100, 0 ) ) / 100
		ELSE NULL
	END AS 'data_cans_overpack',
	CAST( [rejects] AS REAL ) AS 'data_cans_rejects',
	CAST( [target] AS REAL ) AS 'data_cans_target'
	FROM (
		SELECT
		@line AS 'line',
		TESTS.[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'cans' AS 'type',
		TESTS.[Result] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		WHERE
		TESTS.[Var_Id] = @id_cans
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	UNION
		SELECT
		@line AS 'line',
		TESTS.[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'rejects' AS 'type',
		TESTS.[Result] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		WHERE
		TESTS.[Var_Id] = @id_rejects
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	UNION
		SELECT
		@line AS 'line',
		TESTS.[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'weight' AS 'type',
		TESTS.[Result] AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		WHERE
		TESTS.[Var_Id] = @id_weight
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	UNION
		SELECT
		@line AS 'line',
		TESTS.[Result_On] AS 'date',
        CONVERT( NVARCHAR, [Result_On], 12 ) AS 'shiftgroup_date',
        DATEPART( HOUR, [Result_On] ) * 100 + DATEPART( MINUTE, [Result_On] ) AS 'shiftgroup_time',
		'target' AS 'type',
		COALESCE( SPECS.[Target], SPECS.[L_Reject] ) AS 'value'
		FROM
		dbo.Tests AS TESTS WITH (NOLOCK)
		LEFT OUTER JOIN dbo.Variables AS VARS WITH (NOLOCK) ON TESTS.[Var_Id] = VARS.[Var_Id]
		LEFT OUTER JOIN dbo.Production_Starts AS PRODS WITH (NOLOCK) ON (
			VARS.[PU_Id] = PRODS.[PU_Id]
			AND (
				TESTS.[Result_On] BETWEEN PRODS.[Start_Time] AND PRODS.[End_Time]
				OR
				PRODS.[End_Time] IS NULL AND TESTS.[Result_On] > PRODS.[Start_Time]
			)
		)
		LEFT OUTER JOIN dbo.Var_Specs AS SPECS WITH (NOLOCK) ON (
           TESTS.[Var_Id] = SPECS.[Var_Id]
           AND
           PRODS.[Prod_Id] = SPECS.[Prod_Id]
           AND (
				TESTS.[Result_On] BETWEEN SPECS.[Effective_Date] AND SPECS.[Expiration_Date]
               OR
               SPECS.[Expiration_Date] IS NULL AND TESTS.[Result_On] > SPECS.[Effective_Date]
           )
       )
		WHERE
		TESTS.[Var_Id] = @id_weight
		AND TESTS.[Result_On] BETWEEN @starttime AND @endtime
	) AS NETWEIGHTS
	PIVOT (
		MAX( [value] )
		FOR [type] IN ( [cans], [weight], [rejects], [target] )
	) AS AGGREGATION
	ORDER BY [date] ASC
END