SELECT TOP 1
[id],
[line],
[department],
[starttime],
[shift],
[user],
[team],
[text],
[update],
[user_update]
FROM
[EasyReporting_MNP].[dbo].[signs_cf_banner]
WHERE
[line] = ${ resources.line }
ORDER BY [starttime] DESC