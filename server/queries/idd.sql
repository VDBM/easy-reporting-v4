SELECT
[id],
[starttime],
[endtime],
[duetime],
[edittime],
[duration],
[plant],
[department],
[organisation],
[line],
[area],
[location],
[equipment],
[part],
[text],
[notes],
[type],
[type_organisation],
[priority],
[condition],
[id_order],
[id_defect],
[staging],
[creator],
[creator_team],
[assignee],
[actor],
[editor],
[editor_team],
[reviewer],
[reviewer_team]
FROM [EasyReporting_MNP].[dbo].[idd]
WHERE (
	'${ resources.search_type }' = 'all'
	AND (
		[organisation] = '${ resources.organisation }'
		OR
		'${ resources.organisation }' = 'null'
	)
)
OR (
    '${ resources.search_type }' = 'open'
    AND (
		[organisation] = '${ resources.organisation }'
		OR
		'${ resources.organisation }' = 'null'
	)
	AND
    [starttime] >= '${ resources.starttime }'
    AND
    [endtime] IS NULL
)
OR (
    '${ resources.search_type }' = 'open_timeframe'
    AND (
		[organisation] = '${ resources.organisation }'
		OR
		'${ resources.organisation }' = 'null'
	)
    AND
    [starttime] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
    AND (
        [endtime] IS NULL
        OR
        [endtime] >= '${ resources.endtime }'
    )
)
OR (
    '${ resources.search_type }' = 'timeframe'
    AND (
		[organisation] = '${ resources.organisation }'
		OR
		'${ resources.organisation }' = 'null'
	)
    AND (
        [starttime] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
        OR (
            [endtime] IS NULL
            AND
            [starttime] > '${ resources.starttime }'
        )
    )
)
${ resources.filters }