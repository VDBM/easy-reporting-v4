SELECT
PSEUDOVIEW.id,
PSEUDOVIEW.line,
NULL AS 'department',
PSEUDOVIEW.entry_date AS 'starttime',
CASE
	WHEN PSEUDOVIEW.shiftgroup_time < 615 THEN CONCAT( PSEUDOVIEW.shiftgroup_date - 1, '22' )
	WHEN PSEUDOVIEW.shiftgroup_time > 614 AND PSEUDOVIEW.shiftgroup_time < 1415 THEN CONCAT( PSEUDOVIEW.shiftgroup_date, '06' )
	WHEN PSEUDOVIEW.shiftgroup_time > 1414 AND PSEUDOVIEW.shiftgroup_time < 2215 THEN CONCAT( PSEUDOVIEW.shiftgroup_date, '14' )
	WHEN PSEUDOVIEW.shiftgroup_time > 2214 THEN CONCAT( PSEUDOVIEW.shiftgroup_date, '22' )
END AS 'entry_shift',
PSEUDOVIEW.entry_user,
PSEUDOVIEW.entry_team,
PSEUDOVIEW.data_question AS 'text',
PSEUDOVIEW.value AS 'data_value',
PSEUDOVIEW.comment AS 'data_comment',
PSEUDOVIEW.area AS 'data_area'
FROM (
	SELECT
    SOURCE.id,
    SOURCE.line,
	SOURCE.entry_date,
    SOURCE.entry_user,
    SOURCE.entry_team,
    SOURCE.value,
    SOURCE.comment,
    SOURCE.area,
    SOURCE.data_question,
	DATE_FORMAT( SOURCE.entry_date, '%y%m%d') AS 'shiftgroup_date',
	HOUR( SOURCE.entry_date ) * 100 + MINUTE( SOURCE.entry_date ) AS 'shiftgroup_time'
	FROM (
		SELECT
        BOS.UniqueID AS 'id',
        '${ resources.line_normalized }' AS 'line',
		BOS.Inputtime AS 'entry_date',
        USERS.Naam AS 'entry_user',
        CASE USERS.Team
            WHEN 0 THEN 'dag'
            ELSE CONCAT( 'team', USERS.Team )
        END AS 'entry_team',
        CASE BOS.answer
            WHEN 2 THEN 'vergeten'
            WHEN 3 THEN 'kennis'
            WHEN 4 THEN 'uitvoerbaar'
            WHEN 5 THEN 'ontwerp'
            WHEN 6 THEN 'andere'
            ELSE 'niet opgegeven'
        END AS 'value',
        BOS.Comments AS 'comment',
        FLOC.Location AS 'area',
        QST.Question AS 'data_question'
		FROM ${ resources.line }.bos AS BOS
        LEFT OUTER JOIN ${ resources.line }.gebruikers AS USERS ON BOS.userID = USERS.ID
        LEFT OUTER JOIN ${ resources.line }.boslocation AS FLOC ON BOS.Location = FLOC.ID
        LEFT OUTER JOIN ${ resources.line }.bosquestion AS QST ON BOS.Question = QST.ID
        WHERE
        InputTime BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
	) AS SOURCE
) AS PSEUDOVIEW