SELECT
[id],
[division],
[line],
[group],
[created] AS 'starttime',
[user],
[golden6]
FROM [EasyReporting_MNP].[dbo].[MNP_STO_Inputs]
WHERE [line] IN ( ${ resources.line } )
AND [created] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
ORDER BY [group], [line]