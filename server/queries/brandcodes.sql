SELECT
Production_Plan.Prod_Id,
NULL AS 'id',
'${ resources.line_normalized }' AS 'line',
'mnp' AS 'department',
NULL AS 'starttime',
NULL AS 'entry_shift',
NULL AS 'entry_user',
NULL AS 'entry_team',
Products_Base.Prod_Desc AS 'text',
Products_Base.Prod_Code AS 'data_code',
PP_Status_Desc AS 'data_status',
Production_Plan.Actual_Start_Time AS 'data_starttime',
Production_Plan.Actual_End_Time AS 'data_endtime',
Production_Plan.Forecast_Start_Date AS 'data_estimatedStart',
Production_Plan.Forecast_End_Date AS 'data_estimatedEnd',
Production_Plan.Forecast_Quantity AS 'data_cases_target',
Production_Plan.Predicted_Remaining_Quantity AS 'data_cases_remaining',
Production_Plan.Actual_Good_Quantity AS 'data_cases_produced',
Var_Specs.[Target] AS 'data_casesize',
( CASE
	WHEN Products_Base.Prod_Desc LIKE '%X40G%' THEN 'smallsize'
	ELSE 'mediumsize'
END ) AS 'data_cansize'
FROM Production_Plan WITH (NOLOCK)
LEFT OUTER JOIN Products_Base WITH (NOLOCK) ON Production_Plan.Prod_Id = Products_Base.Prod_Id
LEFT OUTER JOIN Production_Plan_Statuses WITH (NOLOCK) ON Production_Plan.PP_Status_Id = Production_Plan_Statuses.PP_Status_Id
LEFT OUTER JOIN Prdexec_Paths WITH (NOLOCK) ON Production_Plan.Path_Id = Prdexec_Paths.Path_Id
LEFT OUTER JOIN Products WITH (NOLOCK) ON Production_Plan.Prod_Id = Products.Prod_Id
LEFT OUTER JOIN Product_Family WITH (NOLOCK) ON Products.Product_Family_Id  = Product_Family.Product_Family_Id
LEFT OUTER JOIN Var_Specs WITH (NOLOCK) ON (
    Production_Plan.Prod_Id = Var_Specs.Prod_Id
    AND Var_Specs.Expiration_Date IS NULL
    AND Var_Id = (
        CASE '${ resources.line_normalized }'
            WHEN 'line1' THEN 10008
            WHEN 'line2' THEN 9933
            WHEN 'line3' THEN 9968
            WHEN 'line4' THEN 10193
            WHEN 'line5' THEN 9897
            WHEN 'line6' THEN 9853
        END
    )
)
WHERE [PL_Id] IN ( ${ resources.line } )
AND (
	 (
		Production_Plan.PP_Status_Id IN ( 1, 11 )
		AND
		Production_Plan.Forecast_Start_Date > CURRENT_TIMESTAMP
	 )
	 OR Production_Plan.PP_Status_Id IN ( 2,3 )
	 OR (
		Production_Plan.PP_Status_Id = 4
		AND (
			Production_Plan.Actual_Start_Time BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
           OR
		    Production_Plan.Actual_End_Time BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
       )
	 )
)
${ resources.filters }