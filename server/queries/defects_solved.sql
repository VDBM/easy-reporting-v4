SELECT
DEF.ID AS 'id',
'${ resources.line_normalized }' AS 'line',
NULL AS 'department',
DEF.Datum AS 'starttime', 
null AS 'entry_shift',
USERS.Naam AS 'entry_user',
null AS 'entry_team',
DEF.Uitleg AS 'text',
DEF.Status AS 'data_status',
DEF.Solved AS 'data_solved', 
DEF.LastUpdated AS 'data_updated',
REPLACE( DEF.FlocationNaam, REPLACE( '${ resources.line }', 'lijn', 'lijn ' ), '' ) AS 'data_location', 
DEF.Flocation AS 'data_FLOC',
DEF.Quality AS 'data_quality',
DEF.Safety AS 'data_safety',
DEF.Critical AS 'data_critical',
DEF.SAP AS 'data_type',
DEF.SapNumbers As 'data_SAP',
DEF.OrderNr As 'data_order',
DEF.Team AS 'data_assigned',
DEF.Job AS 'data_task'
FROM ${ resources.line }.defecten AS DEF
LEFT OUTER JOIN ${ resources.line }.gebruikers AS USERS ON DEF.Persoon = Users.ID
WHERE Solved BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
${ resources.filters }
ORDER BY DEF.Datum ASC