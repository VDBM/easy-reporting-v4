SELECT
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]

)AS 'total_entries',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL
) AS 'total_solved',
-- Teams
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'dag'
) AS 'total_solved_day',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'dag' AND [assigned] = 'team_day'
) AS 'total_solved_assigned_day',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [assigned] = 'team_day'
) AS 'total_assigned_day',


( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team1'
) AS 'total_solved_team1',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team1' AND [assigned] = 'team1'
) AS 'total_solved_assigned_team1',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [assigned] = 'team1'
) AS 'total_assigned_team1',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team2'
) AS 'total_solved_team2',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team2' AND [assigned] = 'team2'
) AS 'total_solved_assigned_team2',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [assigned] = 'team2'
) AS 'total_assigned_team2',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team3'
) AS 'total_solved_team3',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team3' AND [assigned] = 'team3'
) AS 'total_solved_assigned_team3',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [assigned] = 'team3'
) AS 'total_assigned_team3',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team4'
) AS 'total_solved_team4',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team4' AND [assigned] = 'team4'
) AS 'total_solved_assigned_team4',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [assigned] = 'team4'
) AS 'total_assigned_team4',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team5'
) AS 'total_solved_team5',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [reporter_team] = 'team5' AND [assigned] = 'team5'
) AS 'total_solved_assigned_team5',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [assigned] = 'team5'
) AS 'total_assigned_team5',

-- Lines
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [line] = 'lijn 1'
) AS 'total_entries_line1',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [line] = 'lijn 1'
) AS 'total_solved_line1',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [line] = 'lijn 2'
) AS 'total_entries_line2',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [line] = 'lijn 2'
) AS 'total_solved_line2',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [line] = 'lijn 3'
) AS 'total_entries_line3',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [line] = 'lijn 3'
) AS 'total_solved_line3',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [line] = 'lijn 4'
) AS 'total_entries_line4',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [line] = 'lijn 4'
) AS 'total_solved_line4',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [line] = 'lijn 5'
) AS 'total_entries_line5',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [line] = 'lijn 5'
) AS 'total_solved_line5',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [line] = 'lijn 6'
) AS 'total_entries_line6',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [line] = 'lijn 6'
) AS 'total_solved_line6',

-- Departments
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [department] = 'packing'
) AS 'total_entries_packing',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [department] = 'packing'
) AS 'total_solved_packing',

( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [department] = 'process'
) AS 'total_entries_process',
( SELECT COUNT( * ) FROM [EasyReporting_MNP].[dbo].[idd]
WHERE [endtime] IS NOT NULL AND [department] = 'process'
) AS 'total_solved_process'
