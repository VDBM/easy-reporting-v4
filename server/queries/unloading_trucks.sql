SELECT 
T.TruckOrderID,
CONVERT( DATETIME, LEFT( T.ArrivalTimeStamp, 19 ), 126 ) AS 'starttime',
T.ASNnumber,
T.RMSnumber,
T.Batchnumber,
T.AmountDelivered AS 'AmountDelivered',
T.QWreleaseStatus,
T.TruckStatus, 
CASE T.CheckList
    WHEN 0 Then 'NOK'
    WHEN 1 Then 'OK' 
END AS 'CheckList',
T.SupplierQW AS 'Supplier',
TRANS.[SPOT] AS 'Spot',
TRANS.[bin] AS 'Bin',
UNL.[start] AS 'Start',
UNL.[InitialsStart] AS 'InitialsStart',
UNL.[Stop] AS 'Stop',
UNL.[InitialsStop] AS 'InitialsStop',
UNL.[Finished] AS 'Finished',
UNL.[DestinLevelStart] AS 'DestinLevelStart',
UNL.[DestinLevelStop] AS 'DestinLevelStop'
FROM dbo.TruckOrder AS T
LEFT OUTER JOIN [dbo].[unload] AS UNL WITH ( NOLOCK ) ON (
    UNL.[TruckOrderID] = T.TruckOrderID
)
LEFT OUTER JOIN [dbo].[TransActies] AS TRANS WITH ( NOLOCK ) ON (
    TRANS.[TransActie] = UNL.[TransActieNr]
)
WHERE
T.ArrivalTimeStamp BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
AND T.ASNnumber <> '0'
ORDER BY
T.QWtimeStamp DESC