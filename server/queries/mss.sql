SELECT
[id],
[division],
[line],
[group],
[created] AS 'starttime',
[user],
[mss]
FROM [EasyReporting_MNP].[dbo].[MNP_STO_Inputs]
WHERE [line] IN ( ${ resources.line } )
AND [created] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
ORDER BY [group], [line]