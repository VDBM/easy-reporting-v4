SELECT
[TEDet_Id] AS 'id',
'${ resources.line_normalized }' AS 'line',
'mnp' As 'department',
[Start_Time] AS 'starttime',
CAST( 
    CASE
        WHEN [PSEUDOVIEW].[shiftgroup_time] < 615 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date] - 1, '22' )
        WHEN [PSEUDOVIEW].[shiftgroup_time] > 614 AND [PSEUDOVIEW].[shiftgroup_time] < 1415 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date], '06' )
        WHEN [PSEUDOVIEW].[shiftgroup_time] > 1414 AND [PSEUDOVIEW].[shiftgroup_time] < 2215 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date], '14' )
        WHEN [PSEUDOVIEW].[shiftgroup_time] > 2214 THEN CONCAT( [PSEUDOVIEW].[shiftgroup_date], '22' )
    END
    AS INTEGER
) AS 'shift',
NULL AS 'user',
NULL AS 'team',
[PU_Desc] AS 'text',
[End_Time] AS 'endtime',
CASE
	WHEN [PSEUDOVIEW].[PU_Desc] LIKE '%Row Dump' THEN [dbo].[fnLocal_GetWedgeRowDumpDowntimeCorrection]( [Duration], [TEFault_Id] )
    ELSE [Duration]
END AS 'duration',
CASE
    WHEN [PSEUDOVIEW].[PU_Desc] LIKE '%QA Canfiller%' THEN 'CF'
    WHEN [PSEUDOVIEW].[PU_Desc] LIKE '%QA%packe%' THEN 'CP'
    WHEN [PSEUDOVIEW].[PU_Desc] LIKE '%QA Gasser-Seamer%' THEN 'GS'
    WHEN [PSEUDOVIEW].[PU_Desc] LIKE '%Shingling Guide' THEN 'shingling'
    WHEN [PSEUDOVIEW].[PU_Desc] LIKE '%Row Dump'  THEN 'scrap'
    ELSE 'no_type_defined'
END AS 'type',
dbo.fnLocal_GetReasonDesc( REASON_LEVEL1 ) AS 'area',
dbo.fnLocal_GetReasonDesc( REASON_LEVEL2 ) AS 'location',
dbo.fnLocal_GetReasonDesc( REASON_LEVEL3 ) AS 'failure',
dbo.fnLocal_GetReasonDesc( REASON_LEVEL4 ) AS 'cause',
[TEFault_Name_Local] AS 'explanation'
FROM (
    SELECT
    [TED].[TEDet_Id],
    [TED].[Start_Time],
    CONVERT( NVARCHAR, [TED].[Start_Time], 12 ) AS 'shiftgroup_date',
	DATEPART( HOUR, [TED].[Start_Time] ) * 100 + DATEPART( MINUTE, [TED].[Start_Time] ) AS 'shiftgroup_time',
    [PU].[PU_Desc],
    [TED].[End_Time],
    [TED].[Duration],
    [TED].[REASON_LEVEL1],
    [TED].[REASON_LEVEL2],
    [TED].[REASON_LEVEL3],
    [TED].[REASON_LEVEL4],
    [TED].[TEFault_Id],
    [TEF].[TEFault_Name_Local]
    FROM [dbo].[Timed_Event_Details] AS TED WITH (NOLOCK)
    LEFT OUTER JOIN [dbo].[Prod_Units] AS PU WITH (NOLOCK) ON (
        TED.[PU_Id] = PU.[PU_Id]
    )
    LEFT OUTER JOIN [dbo].[Timed_Event_Fault] AS TEF WITH (NOLOCK) ON (
        TED.[PU_Id] = TEF.[PU_Id]
        AND
        TED.[TEFault_Id] = TEF.[TEFault_Id]                    
    )
    WHERE [PL_Id] IN ( ${ resources.line } )
    AND TED.[Start_Time] BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
    AND dbo.fnLocal_GetReasonDesc(ted.Reason_Level1) <> 'Waiting'
    AND dbo.fnLocal_GetReasonDesc(ted.Reason_Level1) <> 'Idle State'
    AND dbo.fnLocal_GetReasonDesc(ted.Reason_Level3) <> 'Waiting'
    ${ resources.filters }
) AS PSEUDOVIEW