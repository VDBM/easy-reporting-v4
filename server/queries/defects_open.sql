SELECT
ID AS 'id',
'${ resources.line_normalized }' AS 'line',
NULL AS 'department',
Datum AS 'starttime',
CASE
	WHEN PSEUDOVIEW.shiftgroup_time < 615 THEN CONCAT( PSEUDOVIEW.shiftgroup_date - 1, '22' )
	WHEN PSEUDOVIEW.shiftgroup_time > 614 AND PSEUDOVIEW.shiftgroup_time < 1415 THEN CONCAT( PSEUDOVIEW.shiftgroup_date, '06' )
	WHEN PSEUDOVIEW.shiftgroup_time > 1414 AND PSEUDOVIEW.shiftgroup_time < 2215 THEN CONCAT( PSEUDOVIEW.shiftgroup_date, '14' )
	WHEN PSEUDOVIEW.shiftgroup_time > 2214 THEN CONCAT( PSEUDOVIEW.shiftgroup_date, '22' )
END AS 'shift',
Naam AS 'user',
NULL AS 'team',
Uitleg AS 'text',
Status AS 'status',
Solved AS 'solved',
LastUpdated AS 'updated',
REPLACE( FlocationNaam, REPLACE( '${ resources.line }', 'lijn', 'lijn ' ), '' ) AS 'location',
Flocation AS 'FLOC',
FlocationNaam AS 'area',
Quality AS 'quality',
Safety AS 'safety',
Critical AS 'critical',
SAP AS 'type',
SapNumbers As 'SAP',
OrderNr As 'order',
Team AS 'assigned',
Job AS 'task',
IF ( status < 2, DATEDIFF( NOW(), Datum ), DATEDIFF( solved, Datum ) ) AS 'days_open'
FROM (
    SELECT
    DEF.ID,
    DEF.Datum,
    DATE_FORMAT( DEF.Datum, '%y%m%d') AS 'shiftgroup_date',
    HOUR( DEF.Datum ) * 100 + MINUTE( DEF.Datum ) AS 'shiftgroup_time',
    USERS.Naam,
    DEF.Uitleg,
    DEF.Status,
    DEF.Solved,
    DEF.LastUpdated,
    DEF.FlocationNaam,
    DEF.Flocation,
    DEF.Quality,
    DEF.Safety,
    DEF.Critical,
    DEF.SAP,
    DEF.SapNumbers,
    DEF.OrderNr,
    DEF.Team,
    DEF.Job
    FROM ${ resources.line }.defecten AS DEF
    LEFT OUTER JOIN ${ resources.line }.gebruikers AS USERS ON DEF.Persoon = Users.ID
    WHERE DEF.Status != 2
    ${ resources.filters }
) AS PSEUDOVIEW
ORDER BY line, starttime ASC