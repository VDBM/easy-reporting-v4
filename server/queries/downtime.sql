SELECT
DOWNTIME.[id],
'${ resources.line_normalized }' AS 'line',
NULL AS 'department',
DOWNTIME.[start] AS 'starttime',
CAST( 
    CASE
        WHEN [DOWNTIME].[shiftgroup_time] < 615 THEN CONVERT( INTEGER, CONCAT( [DOWNTIME].[shiftgroup_date] - 1, '22' ) )
        WHEN [DOWNTIME].[shiftgroup_time] > 614 AND [DOWNTIME].[shiftgroup_time] < 1415 THEN CONVERT( INTEGER, CONCAT( [DOWNTIME].[shiftgroup_date], '06' ) )
        WHEN [DOWNTIME].[shiftgroup_time] > 1414 AND [DOWNTIME].[shiftgroup_time] < 2215 THEN CONVERT( INTEGER, CONCAT( [DOWNTIME].[shiftgroup_date], '14' ) )
        WHEN [DOWNTIME].[shiftgroup_time] > 2214 THEN CONVERT( INTEGER, CONCAT( [DOWNTIME].[shiftgroup_date], '22' ) )
    END
    AS INTEGER
) AS 'shift',
NULL AS 'user',
NULL AS 'team',
DOWNTIME.[duration] AS 'duration',
DOWNTIME.[type] AS 'type',
DOWNTIME.[end] AS 'endtime',
DOWNTIME.[area] AS 'area',
DOWNTIME.[location] AS 'location',
DOWNTIME.[failure] AS 'failure',
DOWNTIME.[cause] AS 'cause',
DOWNTIME.[reason] AS 'reason',
DOWNTIME.[comments] AS 'comments'
FROM (
	SELECT
	(
	CASE
        WHEN TED.end_time IS NULL THEN 'ongoing'
		WHEN [dbo].[fnLocal_getReasonDesc]( TED.REASON_LEVEL1 ) = 'Planned Downtime' THEN 'planned'
		ELSE 'unplanned'
	END
	) AS 'type',
	TED.TEDet_id AS 'id',
	--CAST( LTRIM(Str( TED.Duration, 16, 2 ) ) AS REAL ) AS 'duration',
    TED.Duration AS 'duration',
    CONVERT( NVARCHAR, TED.Start_Time, 12 ) AS 'shiftgroup_date',
	DATEPART( HOUR, TED.Start_Time ) * 100 + DATEPART( MINUTE, TED.Start_Time ) AS 'shiftgroup_time',
	TED.Start_Time AS 'start',
	TED.end_time AS 'end',
	[dbo].[fnLocal_getReasonDesc]( TED.REASON_LEVEL1 ) AS 'area',
	[dbo].[fnLocal_getReasonDesc]( TED.REASON_LEVEL2 )AS 'location',
	[dbo].[fnLocal_getReasonDesc]( TED.REASON_LEVEL3 ) AS 'failure',
	[dbo].[fnLocal_getReasonDesc]( TED.REASON_LEVEL4 ) AS 'cause',
	TEF.TEFault_Name AS 'reason',
	[dbo].[fnLocal_getCommentText]( TED.cause_comment_id ) AS 'comments'
	FROM [dbo].[Timed_Event_Details] AS TED WITH (NOLOCK)
	LEFT OUTER JOIN Comments AS CM WITH (NOLOCK) ON TED.Cause_Comment_Id = CM.Comment_Id
	LEFT OUTER JOIN Timed_Event_Fault AS TEF WITH (NOLOCK) ON TED.TEFault_Id = TEF.TEFault_Id
	LEFT OUTER JOIN Prod_Units AS PU WITH (NOLOCK) ON TED.PU_id = PU.PU_ID
	LEFT OUTER JOIN Prod_Lines AS PL WITH (NOLOCK) ON PU.PL_id = PL.PL_id
	WHERE PL.[PL_Id] IN ( ${ resources.line } )
    AND TEF.TEFault_Name = 'Line Down'
    AND (
        TED.Start_Time BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
        OR
        Ted.Duration IS NULL AND TED.Start_Time < '${ resources.endtime }'
    )
    ${ resources.filters }
) AS DOWNTIME
ORDER BY [line], [start] ASC;