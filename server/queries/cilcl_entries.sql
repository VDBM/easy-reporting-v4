SELECT
LIST.ID AS 'id',
'${ resources.line_normalized }' AS 'line',
'mnp' AS 'department',
LIST.Tijd AS 'starttime',
LIST.ShiftGroep AS 'shift',
USER.Naam AS 'user',
CASE Team
	WHEN 0 THEN 'dag'
	ELSE Team
END AS 'team',
DATA.Text AS 'text',
CASE CheckKISD(DATA.opt2,16,DATA.opt3)
	WHEN '0' THEN 'cil'
	WHEN '1' THEN 'cl'
END AS 'type',
CASE
	WHEN AREA.Naam LIKE '%process' THEN 'process'
	WHEN AREA.Naam LIKE '%process%C&T%' THEN 'process'
	WHEN AREA.Naam LIKE '%process%HO%SaCo%' THEN 'process'
	WHEN AREA.Naam LIKE '%process%DM%' THEN 'packing'
	WHEN AREA.Naam LIKE '%packing%' THEN 'packing'
	ELSE 'other'
END AS 'subdepartment',
AREA.naam AS 'area',
SYS.Naam AS 'location',
ACT.Actie AS 'action',
DATA.KISO AS 'explanation',
LIST.opmerkingen AS 'notes',
LIST.OOL AS 'ool',
BLIM.Naam AS 'result'
FROM 
${ resources.line }.checklist AS LIST
LEFT OUTER JOIN ${ resources.line }.checklistdata AS DATA ON LIST.CheckListItem = DATA.ID
LEFT OUTER JOIN ${ resources.line }.systeem AS SYS ON DATA.Sys = SYS.ID
LEFT OUTER JOIN ${ resources.line }.gebruikers AS USER ON LIST.Persoon = USER.ID
LEFT OUTER JOIN ${ resources.line }.area AS AREA ON DATA.Area = AREA.ID
LEFT OUTER JOIN ${ resources.line }.acties AS ACT ON LIST.Actie = ACT.ID
LEFT OUTER JOIN ${ resources.line }.buitenlimiet AS BLIM ON LIST.OOL = BLIM.ID
WHERE LIST.Tijd BETWEEN '${ resources.starttime }' AND '${ resources.endtime }'
${ resources.filters }
ORDER BY starttime