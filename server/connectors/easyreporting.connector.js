/*  connector_easyreporting.mjs   => Query EASYREPORTING database
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-06
*/
////////////////////
///
const FS = require( 'fs' );
// import MSSQL from 'tedious';
const MSSQL = require( 'tedious' );
// import { NOOP, templatePopulate as populate, propHash, propTransform, propUpdate } from '../modules/util.mjs';
const { NOOP, templatePopulate, propTransform } = require( '../modules/util.js' );
// import { read } from '../util_node.mjs';
const { promisify } = require( 'util' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
// SERVER
/*
*/
const EASYREPORTING_NORMALIZED = {
    'username' : 'EasyTrackingUser',
    'password' : 'EasyUser123',
    'server' : 'mecvm22', // = '10.144.9.93'
    'options' : {
        'connectTimeout' : 90000,
        'database' : 'EasyReporting_MNP',
        'instanceName' : 'mecsql02',
        'requestTimeout' : 120000
    }
};
/*
*/
const EASYREPORTING = {
    'userName' : EASYREPORTING_NORMALIZED.username,
    'password' : EASYREPORTING_NORMALIZED.password,
    'server' : EASYREPORTING_NORMALIZED.server,
    'options' : Object.assign( {}, EASYREPORTING_NORMALIZED.options )
};
/*
*/
const transformations = {
    'filters' : filters => filters.length
        ? `AND ( ${ filters.join( ' ) AND ( ' ) } )`
        : '',
    'line' : line => Array.isArray( line )
        ? line.map( line => `'${ line }'` ).join( ', ' )
        : `'${ line }'`
};
/*
*/
const types = {
    'boolean' : '[BIT]',
    'datetime' : '[DATETIME]',
    'float' : '[REAL]',
    'integer' : '[INT]',
    'primary' : '[INT] IDENTITY(1,1)',
    'string' : '[NVARCHAR](max)'
};
////////////////////
// UTILS
/*
*/
const queryDatabase = ( sql, http_status_code ) => {
    return new Promise( ( resolve, reject ) => {
        const resultSet = [];
        const connection = new MSSQL.Connection( EASYREPORTING );
        const query = new MSSQL.Request( sql, ( error, rowCount ) => {
            connection.close();
            if ( error ) reject( { 'code' : 500, 'value' : 'request creation error', 'error' : JSON.stringify( error ) } );
            else {
                resolve( {
                    "data" : resultSet,
                    "headers" : {},
                    "http_status_code" : http_status_code
                } );
            }
        } );
        connection.on( 'connect', error  => error ? reject( { 'code' : 500, 'value' : 'connection refused', 'error' : JSON.stringify( error ) } ) : connection.execSql( query ) );
        /* Each infoMessage contains the following keys: Keep this clean, since every request can generate messages. Might be useful to log these somewhere in the future.
        *    {
        *      'number' : '5701/5703',
        *      'state' :  '1/2',
        *      'class' : '0',
        *      'message' : 'Changed database context to master/Changed language settings to us_english',
        *      'servername' : 'MEC_PRI_02\MEC_PRI_02',
        *      'procname' : '',
        *      'linenumber' : '1/12',
        *      'name' : 'INFO',
        *      'event' : 'infoMessage'
        *   }
        */
        connection.on( 'infoMessage', NOOP );
        connection.on( 'error', error => reject( { 'code' : 500, 'value' : 'internal error', 'error' : JSON.stringify( error ) } ) );
        connection.on( 'errorMessage', error => reject( { 'code' : 500, 'value' : `invalid query syntax:\n${ sql }`, 'error' : JSON.stringify( error ) } ) );
        /* columnMetaData will be sent once for each query before row events occur.
        *       Might nto work for all databses
        *       Properties: colName, typeName, precision, scale, dataLength
        */
        query.on( 'columnMetaData', NOOP );
        /*	row format
            {
                'value' :
                'metadata' : {
                    'userType' : 
                    'flags' : 
                    'type' : 
                    'colName' :
                    'collation' :
                    'precision' :
                    'scale' :
                    'udtInfo' :
                    'dataLength' :
                    'tableName' :
                }
            }
        */
        query.on( 'row', row => {
            // For efficiency, we just write the full transform into one loop
            // We could split this into propX chaining but end up with more code.
            const record = row.reduce( ( record, row ) => { record[ row.metadata.colName ] = row.value; return record; }, {} );
            // TODO: NULL check, decide if we want to parse the string NULL to the value NULL if the database doesn't contain this automatically.
            resultSet.push( record );
        } );
    } );
};
////////////////////
// TEMPLATE FETCH / TEMPLATE POPULATE
/*
*/
const fetchTemplate = resourceName => readFile( `./queries/${ resourceName }.sql` ).then( buffer => buffer.toString() );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => templatePopulate( sqlTemplate, propTransform( parameters, transformations, false ), dependencies );
////////////////////
// CRUD
/*
*/
const create = ( model, buffer ) => {
    if ( model.tables.length === 1 ) {
        const body = JSON.parse( buffer.toString() );
        const properties = model
            .properties
            .filter( property => property.type !== 'primary' ); // remove all the fields that are inserted automatically, like primary keys.
        const property_list = properties
            .map(({ field }) => `[${ field }]` )
            .join( ', ' );
        const values = properties
            .map(({ field, type }) => {
                const value = body[ field ];
                if ( !value ) return 'NULL';
                else {
                    if ( [ 'integer', 'float' ].includes( type ) ) return value;
                    else if ( type === 'string' ) return `'${ value.replace(/\'/g, "''") }'`;
                    else return `'${ value }'`;
                }
            })
            .join( ', ' );
        const sql = `INSERT INTO [dbo].[${ model.tables[ 0 ] }] ( ${ property_list } ) OUTPUT INSERTED.* VALUES ( ${ values } )`;
        return queryDatabase( sql, 201 );
    }
    else return Promise.reject( new Error( `${ model.name }: cant find correct table to generate insert command.` ) );
};
/*
*/
const read = sql => queryDatabase( sql, null );
/*  200 if we send content, else 204
*/
const update = ( model, id, buffer ) => {
    if ( model.tables.length === 1 ) {
        const body = JSON.parse( buffer.toString() );
        const values = model
            .properties
            .reduce(( expressions, { field, type } ) => {
                if ( body.hasOwnProperty( field ) ) {
                    const value = body[ field ];
                    if ( !value ) expressions.push( `[${ field }] = NULL` );
                    else {
                        if ( [ 'integer', 'float' ].includes( type ) ) expressions.push( `[${ field }] = ${ value }` );
                        else expressions.push( `[${ field }] = '${ value }'` );
                    }
                }
                return expressions;
            }, [] )
            .join( ', ' );
        const sql = `UPDATE [dbo].[${ model.tables[ 0 ] }] SET ${ values } OUTPUT INSERTED.* WHERE [id] = ${ id }`; // SET x OUTPUT INSERTED.* WHERE ==> If we provide output, we send code 200, if we don't provide output, we send code 204
        if ( sql ) return queryDatabase( sql, 200 );
        else return Promise.resolve( { "data" : [], "headers" : {}, "http_status_code" : 200 } );
    }
    else return Promise.reject( new Error( `${ model.name }: cant find correct table to generate update command.` ) );
};
/*
*/
const deleteResource = NOOP;
////////////////////
// export default { create, parse, read, 'server' : EASYREPORTING_NORMALIZED, template };
module.exports = { create, 'delete' : deleteResource, fetchTemplate, populateTemplate, read, 'server' : EASYREPORTING_NORMALIZED, types, update };