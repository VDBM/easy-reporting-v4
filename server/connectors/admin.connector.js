/*  admin.connector.mjs   => ADMIN Tools
*   @data_language: ES6
*   @data_version:  v1.0.1
*   @entry_user:    VDBM
*   @update_date:   2018-05-22
*/
////////////////////
//
const connector_easyreporting = require( './easyreporting.connector.js' );
// import FS from 'fs';
const FS = require( 'fs' );
//
const MSSQL = require( 'tedious' );
// import
const { promisify } = require( 'util' );
const { capitalize, dateLocal } = require( '../modules/util.js' );
//
const PATH_TEMPLATE_COMPONENT = './projects/admin/templates/admin_template_component.txt';
const PATH_TEMPLATE_MODEL = './projects/admin/templates/admin_template_model.txt';
////////////////////
const fileExists = path => readFile( path ).then( file => true ).catch( err => false );
const readFile = promisify( FS.readFile );
const writeFile = promisify( FS.writeFile ); 
////////////////////
// SERVER
/*
*/
const ADMIN_NORMALIZED = {
    'username' : null,
    'password' : null,
    'server' : null,
    'options' : {}
};
/*
*/
const ADMIN = {};
/*
*/
const transformations = {};
////////////////////
// UTILS
/*  admin_create_component  =>
*/
const admin_create_component = ( model, body ) => {
    return readFile( PATH_TEMPLATE_COMPONENT )
        .then( template => {
            const { resource, author, event_type } = body;
            const file_content = template
                .toString()
                .replace( /\$\{ name_class \}/g, capitalize( resource ) )
                .replace( /\$\{ name_component \}/g, resource )
                .replace( '${ starttime }', dateLocal( new Date() ).slice( 0, 10 ) )
                .replace( '${ event_type }', event_type )
                .replace( '${ creator }', author );
            const path = `./modules/component_${ resource }.js`;    
            return fileExists( path )
                .then( exists => exists
                    ? false
                    : writeFile( path, file_content ).then( () => true )
                )
                .then( written => written
                    ? { "data" : `succesfully created ${ path }`, "headers" : {}, "http_status_code" : 201 } // 201 Created
                    : { "data" : `${ path } already exists`, "headers" : {}, "http_status_code" : 200 } // 409 Conflict
                );
        } );
};
/*
*/
const admin_create_connector = ( model, body ) => {
    return Promise.reject( { "data": "method not implemented", "headers": {}, "http_status_code": 500 } );
};
/*  admin_create_model      => {"resource":"string","server":"string"}
*   @notes
*               - The properties array will contains objects { "field": "",            "type": "" }
*/
const admin_create_model = ( model, body ) => {
    return readFile( PATH_TEMPLATE_MODEL )
        .then( template => {
            const { resource, server } = body;
            const template_model = JSON.parse( template );
            const structure = Object
                .entries( template_model )
                .reduce( ( structure, [ property, type ] ) => {
                    if ( type === 'array' ) structure[ property ] = [];
                    else if ( type === 'string' ) structure[ property ] = null;
                    return structure;
                }, {} );
            // 500 Internal server error  
            if ( !structure || !structure.hasOwnProperty( 'name' ) || !structure.hasOwnProperty( 'servers' ) ) {
                throw new Error( {
                    "data" : "Failed creating model. Structure does not have a name or server field.",
                    "headers" : {},
                    "http_status_code" : 500
                } );
            }
            else {
                structure.name = resource;
                structure.servers.push( server.toUpperCase() );
                const path = `./models/${ structure.name }.model.json`;
                const file_content = JSON.stringify( structure, null, 4 );
                return fileExists( path )
                    .then( exists => exists
                        ? false
                        : writeFile( path, file_content ).then( () => true ) // Since writeFile returns undefined or throws, our only check for success is the error handler .
                    )
                    .then( written => written
                        ? { "data" : `succesfully created ${ path }`, "headers" : {}, "http_status_code" : 201 } // 201 Created
                        : { "data" : `${ path } already exists`, "headers" : {}, "http_status_code" : 200 } // 409 Conflict
                    );
            }
        } );
};
/*  admin_create_table      => {"resource":"string"}
*/
const admin_create_table = ( model, body ) => {
    // model property reference: primary / datetime / string / integer / boolean / float
    const resource = body.resource;
    const path = `./models/${ resource }.model.json`;
    return readFile( path )
        .then( buffer => buffer.toString() )
        .then( JSON.parse )
        .then( model => {
            const server = model.servers[ 0 ];
            const properties = model
                .properties
                .map( ( { field, required, type } ) => `[${ field }] ${ connector_easyreporting.types[ type ] } ${ required ? 'NOT NULL' : 'NULL' }` )
                .join( ', ' );
            const properties_insert = model
                .properties
                .filter( entry => entry.field !== 'id' )
                .map( ( { field, required, type } ) => {
                    let value;
                    switch( type ) {
                        case 'boolean' :
                            value = 0;
                        break;
                        case 'datetime' :
                            value = '2018-04-25T12:00:00.000Z';
                        break;
                        case 'float' :
                            value = 0.0;
                        break;
                        case 'integer' :
                            value = 42;
                        break;
                        case 'string' :
                            value = "'default string'";
                        break;
                        default:
                            value = 'NULL';
                        break;
                    }
                    return { field, value };
                } );
            const string_properties = properties_insert.map( entry => `[${ entry.field }]` ).join( ', ' )
            const string_values = properties_insert.map( entry => entry.value ).join( ', ' )
            const sql_insert = body.defaults
                ? `INSERT INTO [EasyReporting_MNP].[dbo].[${ resource }] ( ${ string_properties } ) VALUES( ${ string_values } )`
                : '';
            const sql_create = `CREATE TABLE [EasyReporting_MNP].[dbo].[${ resource }] (${ properties }) ${ sql_insert } SELECT COLUMN_NAME AS 'field', DATA_TYPE AS 'type' FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '${ resource }' AND TABLE_SCHEMA='dbo'`;
            const sql_read = `SELECT * FROM [EasyReporting_MNP].[dbo].[${ resource }]`;
            // rest.resourceCreate() will still apply transformations .
            // So we want to extract the data returned by the resourceRead() which was already transformed .
            // This way the transformer can still apply header logic
            if ( server === 'EASYREPORTING' && !model.tables.length ) {
                
                console.log( sql_create );
                
                model.tables.push( resource );
                return connector_easyreporting
                    .read( sql_create )
                    .then( pack => Promise.all( [
                        Promise.resolve( pack.data ),
                        writeFile( path, JSON.stringify( model, null, 4 ) ).catch( error => { throw error; } ),
                        Promise.resolve( 'query not written' )//writeFile( `./queries/${ resource }.sql`, sql_read ).catch( error => { throw error; } )
                    ] ) )
                    .then( ( [ data, updated_model, updated_query ] ) => {
                        console.log( data );
                        console.log( updated_model );
                        console.log( updated_query );
                        return data;
                    } );
            }
            else throw new Error( `${ resource }: table already exists` );
        } )
        .then( data => ( { data, "headers" : {}, "http_status_code" : null } ) )
        .catch( error => ( { "data" : [ error.message ], "headers" : {}, "http_status_code" : 400 } ) ); // 400 Bad Request
};
/*  admin_update_model      => {"resource":"string","property":"string","value":any}
*/
const admin_update_model = ( model, body ) => {
    return readFile( TEMPLATE_MODEL )
        .then( template => {
            const { resource, property, value } = body;
            const template_model = JSON.parse( template );
            const casted_value = template_model[ property ] === 'array' && typeof value === 'string'
                ? value.split( ',' ).filter( value => value.length )
                : value;
            const path = `./models/${ resource }.model.json`;
            return readFile( path )
                .then( buffer => buffer.toString() )
                .then( JSON.parse )
                .then( model => Object.assign( model, { [ property ] : casted_value } ) )
                .then( model => writeFile( path, JSON.stringify( model, null, 4 ) ) )
                .then( () => ( { "data" : `succesfully updated ${ path }`, "headers" : {}, "http_status_code" : 204 } ) ) // 200 OK + content || 204 No Content
                .catch( err => ( { "data" : `cant update, ${ path } does not exist`, "headers" : {}, "http_status_code" : 400 } ) ); // Bad Request
        } );
};
/*  admin_methods
*/
const ADMIN_METHODS = { admin_create_component, admin_create_connector, admin_create_model, admin_create_table, admin_update_model };
////////////////////
// FETCH / POPULATE
/*
*/
const fetchTemplate = resource => Promise.resolve( './models/${ resource }.model.json' );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => sqlTemplate.replace( '${ resource }', parameters.resource );
////////////////////
// CRUD
/*
*   body = { resourceName, headers, output } = parameters instead of actual body ???
*/
const create = ( model, chunks_body ) => {
    return new Promise( ( resolve, reject ) => {
        const admin_method = model.name;
        const body = JSON.parse( chunks_body.join( '' ) );
        if ( ADMIN_METHODS.hasOwnProperty( admin_method ) ) ADMIN_METHODS[ admin_method ]( model, body ).then( resolve );
        else reject( `${ admin_method } - error - ${ body }` );
    } );
};
/*  read === admin_read_model
*   If we parse the file to an object ehre, we can stringify it again. TODO: repalce this bya ctually detecting the return type before casting .
*/
const read = path => readFile( path ).then( buffer => buffer.toString() ).then( file => ( { "data" : JSON.parse( file ), "headers" : {}, "http_status_code" : null } ) );
/*
*   dev => body={\"resourceName\":\"admin_update_model\",\"property\":\"output\",\"value\":[\"json\",\"table\",\"text\"]}
*/
const update = ( model, chunks_body ) => {
    return new Promise( ( resolve, reject ) => {
        const admin_method = model.name;
        const body = JSON.parse( chunks_body.join( '' ) );
        if ( ADMIN_METHODS.hasOwnProperty( admin_method ) ) ADMIN_METHODS[ admin_method ]( model, body ).then( resolve );
        else reject( `${ admin_method } - error - ${ body }` );
    } );
};
////////////////////
// export default { parse, read, 'server' : ADMIN_NORMALIZED, template };
module.exports = { create, fetchTemplate, populateTemplate, read, 'server' : ADMIN_NORMALIZED, update };