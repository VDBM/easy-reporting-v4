/*  connector_randomizer.mjs   => Query EASYREPORTING database
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-21
*/
////////////////////
////////////////////
/*
*/
const RANDOMIZER_NORMALIZED = {
    'username' : null,
    'password' : null,
    'server' : null,
    'options' : {
        'connectTimeout' : null,
        'database' : null,
        'instanceName' : null,
        'requestTimeout' : null
    }
};
/*
*/
const RANDOMIZER = {
    'userName' : RANDOMIZER_NORMALIZED.username,
    'password' : RANDOMIZER_NORMALIZED.password,
    'server' : RANDOMIZER_NORMALIZED.server,
    'options' : Object.assign( {}, RANDOMIZER_NORMALIZED.options )
};
/*
*/
const transformations = {};
////////////////////
////////////////////
const create = ( model, resource ) => Promise.resolve( '' );
/*
*/
const fetchTemplate = resourceName => Promise.resolve( '' );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => null;
/*
*/
const read = sql => Promise.resolve( {
    "data" : [ { "endtime" : new Date().toJSON(), "id" : 1, 'text' : "random value", "starttime" : new Date( new Date().getTime() - 3600000 ).toJSON() } ],
    "headers" : {},
    "http_status_code" : null
} );
////////////////////
// export default { create, parse, read, 'server' : RANDOMIZER_NORMALIZED, template };
module.exports = { create, fetchTemplate, populateTemplate, read, 'server' : RANDOMIZER_NORMALIZED };