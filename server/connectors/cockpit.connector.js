/*  connector_cockpit.mjs   => Query COCKPIT database
*   @data_language: ES6
*   @data_version:  v1.0.1
*   @entry_user:    VDBM
*   @update_date:   2018-01-12
*/
////////////////////
//
const FS = require( 'fs' );
// import MSSQL from 'tedious';
const MSSQL = require( 'tedious' );
// import { NOOP, templatePopulate as populate, propHash, propTransform, propUpdate } from '../modules/util.mjs';
const { NOOP, templatePopulate, propTransform, propUpdate } = require( '../modules/util.js' );
// import 
const { promisify } = require( 'util' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
/*
*/
const COCKPIT_NORMALIZED = {
    'username' : 'Illuminator',
    'password' : 'Illuminat0r',
    'server' : '10.144.9.164',
    'options' : {
        'instanceName' : 'COCKPIT'
    }
};
/*
*/
const COCKPIT = {
    'userName' : COCKPIT_NORMALIZED.username,
    'password' : COCKPIT_NORMALIZED.password,
    'server' : COCKPIT_NORMALIZED.server,
    'options' : Object.assign( {}, COCKPIT_NORMALIZED.options )
};
/*
*/
const link_evaluate = `http://mechelen.tactics.eu.kellogg.com/training/exam/result/evaluate/${ 'number_exam' }`;
/*
*/
const transformations = {
    'filters' : filters => filters.length
        ? `AND ( ${ filters.join( ' ) AND ( ' ) } )`
        : ''
};
/*
*/
const generateColumnName = propUpdate( 'columnName', record => record.metadata.colName );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => templatePopulate( sqlTemplate, propTransform( parameters, transformations, false ), dependencies );
/*
*/
const read = ( sql ) => {
    return new Promise( ( resolve, reject ) => {
        const resultSet = [];
        const connection = new MSSQL.Connection( COCKPIT );
        const query = new MSSQL.Request( sql, ( error, rowCount ) => {
            connection.close();
            if ( error ) reject( { 'code' : 500, 'value' : 'request creation error', 'error' : JSON.stringify( error ) } );
            else {
                resolve( {
                    "data" : resultSet,
                    "headers" : {},
                    "http_status_code" : null
                } );
            }
        } );
        connection.on( 'connect', error  => error ? reject( { 'code' : 500, 'value' : 'connection refused', 'error' : JSON.stringify( error ) } ) : connection.execSql( query ) );
        /* Each infoMessage contains the following keys: Keep this clean, since every request can generate messages. Might be useful to log these somewhere in the future.
        *    {
        *      'number' : '5701/5703',
        *      'state' :  '1/2',
        *      'class' : '0',
        *      'message' : 'Changed database context to master/Changed language settings to us_english',
        *      'servername' : 'MEC_PRI_02\MEC_PRI_02',
        *      'procname' : '',
        *      'linenumber' : '1/12',
        *      'name' : 'INFO',
        *      'event' : 'infoMessage'
        *   }
        */
        connection.on( 'infoMessage', NOOP );
        connection.on( 'error', error => reject( { 'code' : 500, 'value' : 'internal error', 'error' : JSON.stringify( error ) } ) );
        connection.on( 'errorMessage', error => reject( { 'code' : 500, 'value' : `invalid query syntax:\n${ sql }`, 'error' : JSON.stringify( error ) } ) );
        /* columnMetaData will be sent once for each query before row events occur.
        *       Might nto work for all databses
        *       Properties: colName, typeName, precision, scale, dataLength
        */
        query.on( 'columnMetaData', NOOP );
        /*	row format
            {
                'value' :
                'metadata' : {
                    'userType' : 
                    'flags' : 
                    'type' : 
                    'colName' :
                    'collation' :
                    'precision' :
                    'scale' :
                    'udtInfo' :
                    'dataLength' :
                    'tableName' :
                }
            }
        */
        query.on( 'row', row => {
            // For efficiency, we just write the full transform into one loop
            // We could split this into propX chaining but end up with more code.
            const record = row.reduce( ( record, row ) => { record[ row.metadata.colName ] = row.value; return record; }, {} );
            // TODO: NULL check, decide if we want to parse the string NULL to the value NULL if the database doesn't contain this automatically.
            resultSet.push( record );
        } );
    } );
};
/*
*/
const fetchTemplate = resourceName => readFile( `./queries/${ resourceName }.sql` ).then( buffer => buffer.toString() );
////////////////////
// export default { parse, read, 'server' : COCKPIT_NORMALIZED, template };
module.exports = { fetchTemplate, populateTemplate, read, 'server' : COCKPIT_NORMALIZED };