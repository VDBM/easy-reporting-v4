/*  connector_mecpri02.mjs   => Query database
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-06
*/
////////////////////
//
const FS = require( 'fs' );
// import MSSQL from 'tedious';
const MSSQL = require( 'tedious' );
// import { NOOP, templatePopulate as populate, propHash, propTransform, propUpdate } from '../modules/util.mjs';
const { NOOP, templatePopulate, propTransform, propUpdate } = require( '../modules/util.js' );
// import { read } from '../util_node.mjs';
const { promisify } = require( 'util' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
/*
*/
const DEPAL_NORMALIZED = {
    'username' : 'contec',
    'password' : 'katrien',
    'server' : '143.26.66.39',
    'options' : {
        'connectTimeout' : 90000,
        'database' : 'depal',
        'instanceName' : 'mec_pri_02',
        'requestTimeout' : 120000
    }
};
/*
*/
const DEPAL = {
    'userName' : DEPAL_NORMALIZED.username,
    'password' : DEPAL_NORMALIZED.password,
    'server' : DEPAL_NORMALIZED.server,
    'options' : Object.assign( {}, DEPAL_NORMALIZED.options )
};
/*
*/
const transformations = {
    'filters' : filters => filters.length
        ? `AND ( ${ filters.join( ' ) AND ( ' ) } )`
        : '',
    'line' : line => Array.isArray( line )
        ? line.map( line => `'${ line }'` ).join( ', ' )
        : `'${ line }'`
};
////////////////////
////////////////////
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => templatePopulate( sqlTemplate, propTransform( parameters, transformations, false ), dependencies );
/*
*/
const read = sql => {
    return new Promise( ( resolve, reject ) => {
        const resultSet = [];
        const connection = new MSSQL.Connection( DEPAL );
        const query = new MSSQL.Request( sql, ( error, rowCount ) => {
            if ( error ) reject( { 'code' : 500, 'value' : 'request creation error', 'error' : JSON.stringify( error ) } );
            else {
                resolve( {
                    "data" : resultSet,
                    "headers" : {},
                    "http_status_code" : null
                } );
            }
        } );
        connection.on( 'connect', error  => error ? reject( { 'code' : 500, 'value' : 'connection refused', 'error' : JSON.stringify( error ) } ) : connection.execSql( query ) );
        /* Each infoMessage contains the following keys: Keep this clean, since every request can generate messages. Might be useful to log these somewhere in the future.
        *    {
        *      'number' : '5701/5703',
        *      'state' :  '1/2',
        *      'class' : '0',
        *      'message' : 'Changed database context to master/Changed language settings to us_english',
        *      'servername' : 'MEC_PRI_02\MEC_PRI_02',
        *      'procname' : '',
        *      'linenumber' : '1/12',
        *      'name' : 'INFO',
        *      'event' : 'infoMessage'
        *   }
        */
        connection.on( 'infoMessage', NOOP );
        connection.on( 'error', error => reject( { 'code' : 500, 'value' : 'internal error', 'error' : JSON.stringify( error ) } ) );
        connection.on( 'errorMessage', error => reject( { 'code' : 500, 'value' : `invalid query syntax:\n${ sql }`, 'error' : JSON.stringify( error ) } ) );
        /* columnMetaData will be sent once for each query before row events occur.
        *       Might nto work for all databses
        *       Properties: colName, typeName, precision, scale, dataLength
        */
        query.on( 'columnMetaData', NOOP );
        /*	row format
            {
                'value' :
                'metadata' : {
                    'userType' : 
                    'flags' : 
                    'type' : 
                    'colName' :
                    'collation' :
                    'precision' :
                    'scale' :
                    'udtInfo' :
                    'dataLength' :
                    'tableName' :
                }
            }
        */
        query.on( 'row', row => {
            // For efficiency, we just write the full transform into one loop
            // We could split this into propX chaining but end up with more code.
            const record = row.reduce( ( record, row ) => { record[ row.metadata.colName ] = row.value; return record; }, {} );
            // TODO: NULL check, decide if we want to parse the string NULL to the value NULL if the database doesn't contain this automatically.
            resultSet.push( record );
        } );
    } );
};
/*
*/
const fetchTemplate = resourceName => readFile( `./queries/${ resourceName }.sql` ).then( buffer => buffer.toString() );
////////////////////
// export default { create, parse, read, 'server' : MECPRI02_NORMALIZED, template };
module.exports = { fetchTemplate, populateTemplate, read, 'server' : DEPAL_NORMALIZED };