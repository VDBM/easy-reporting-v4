/*  connector_phd.mjs   => Query PHD database through relaying Illuminatior XML
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-28
*/
/*  NOTES
*   - We should reread all the docum,entatino in the uniformance help file, opened with citrix.
*   - Uniformance installation files + docs: Y:\Software\C&IS\Honeywell\Kellogg PHD\Uniformance Process Studio R300.1 - 51157950
*   - Current implementation still uses Illuminator redirection to get PHD data .
*   - Once we fix the direct PHD through the PHD interface itsself, we can refactor this connector .
*   - We thought about giving Illuminator its own connector, but since Easy Reporting only uses it to connect to PHD, this seemed a waste of resources to implement .
*   - The Illuminator interface only accepts dates in basic format, not UTC format .
*   - Added the casting of the local time to UTC so we get a ISO 8601 representation of the resource .
*   - If we want to use the PHD AVG aggregation, after the MUX has chunked the dates, we still have to chunk the dates further inside this connector so we can ask for an average of that specific timeframe .
*
*   - Value @ specific time:    StartDate=${ starttime } & EndDate=${ starttime } & Mode=History    & Method=False
*   - Value @ timeseries:       StartDate=${ starttime } & EndDate=${ endtime }   & Mode=History    & Method=False & Resolution=${ secondsPerChunk } & RowCount=${ >= amountOfChunks || max=99999 }
*   - Average @ timeseries:     StartDate=${ starttime } & EndDate=${ endtime }   & Mode=Statistics & Method=AVG
*/
/*  MAIL 2016 - Luk Versavel - Steven Van Sant
*
*    (1)    Voor web toepassingen hebben we een nieuwe applicatie Uniformance Insight, waarvan je op onze website informatie terugvindt:
*
*    https://www.honeywellprocess.com/en-US/explore/products/advanced-applications/uniformance/Pages/uniformance-insight.aspx 
*    https://www.honeywellprocess.com/library/marketing/webinars/Get-More-Out-of-Your-PHD-Data%E2%80%93Introducing-Honeywells-Uniformance-Insight-webinar-datasheet.pdf
*
*    (2)    In het algemeen, om data uit PHD op te halen, kan je onze .NET API gebruiken, die je vanuit verschillende programmeertalen eenvoudig kan aanroepen.
*
*    (3)    Voor snelle eenvoudige stukken van webpagina’s kan je ook gebruik maken van de Protocol Adapter, die data in HTML of XML formaat geeft.
*
*    phd://localhost/GetData?Disconnect="yes",TagName="TEST0001",StartTime="today",EndTime="HOUR",SampleInterval=3600000,ResampleMethod="Around" 
*    phd://localhost/GetData?Disconnect="yes",TagName="L1_PROCSPD2_L01",StartTime="today",EndTime="HOUR",SampleInterval=3600000,ResampleMethod="Around"
*
*    phd://localhost/GetData?Disconnect="yes",Output="XML",TagName="TEST0001",StartTime="today",EndTime="HOUR",SampleInterval=3600000,ResampleMethod="Around" 
*
*/

// http://mecvm21/api/linespeed?line=line3&starttime=2018-04-05T12:15:00.000Z&endtime=2018-04-06T12:14:59.000Z&aggregation=avg

////////////////////
const { fetch } = require( '../modules/fetch.js' );
//
const FS = require( 'fs' )
// import 
const { promisify } = require( 'util' );
//
const UTIL_ER = require( '../modules/util.js' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////

/*
*/
const ILLUMINATOR = {
    'username' : 'easytracking',
    'password' : 'easytracking',
    'server' : '10.144.9.75', // mecvm12
    'options' : {}
};
/*
*/
const PHD_NORMALIZED = {
    'username' : null,
    'password' : null,
    'server' : null,
    'options' : {
        'connectTimeout' : null,
        'database' : null,
        'instanceName' : null,
        'requestTimeout' : null
    }
};
/*
*/
const PHD = {
    'userName' : PHD_NORMALIZED.username,
    'password' : PHD_NORMALIZED.password,
    'server' : PHD_NORMALIZED.server,
    'options' : Object.assign( {}, PHD_NORMALIZED.options )
};
/*
*/
const transformations = {};
////////////////////
/*
*   notes:.
*               - Current interval = 300 seconds = 5mins between each check .
*               - The Illuminator xml seems to be limited to about 6 days of records on a 5min interval.
*               - This as a template string would be so long that its probably easier to follow since alll parameters are listed seperately .
*/
const chunksize = 518400000;
const createURI = ( [ tags_phd, parameters ] ) => {
    const starttime = new Date( parameters.starttime );
    const endtime = new Date( parameters.endtime );
    return [
        'http://',
        ILLUMINATOR.server,
        '/Lighthammer/Illuminator',
        '?IllumLoginName=',
        ILLUMINATOR.username,
        '&IllumLoginPassword=',
        ILLUMINATOR.password,
        '&QueryTemplate=',
        'Pringles-DEV/Mathias/Qry-GEN-PHD',
        '&Content-Type=',
        'text/xml',
        '&mask=',
        tags_phd[ parameters.resourceName ][ parameters.line ],
        '&StartDate=',
        UTIL_ER.dateLocal( starttime ).replace( 'T', ' ' ).replace( 'Z', '' ),
        '&EndDate=',
        UTIL_ER.dateLocal( endtime ).replace( 'T', ' ' ).replace( 'Z', '' ),
        '&Mode=',
        'History',
        '&Method=',
        'false',
        '&Resolution=',
        300,
        '&RowCount=',
        9999
    ].join( '' );
};
/*
*/
const parseIlluminatorXML = xml => {
    // An xml response has to exist .
    if ( !xml ) throw new Error( 'connector_phd: parseIlluminatorXML: no xml provided.' );
    // The response can't include the FatalError tag . Eg, server or connector crashed during the request .
    const error = xml.match( /(?:<FatalError>)(.+)(?:<\/FatalError>)/ );
    if ( error ) throw new Error( error[ 1 ] );
    // The response can't include the Message tag . Eg, max number of rows exceeded .
    const message = xml.match( /(?:<Message>)(.+)(?:<\/Message>)/ );
    if ( message ) throw new Error( message[ 1 ] );
    // The resourceName has to appead as a SourceColumn tag for the xml to be valid .
    const matchingResource = xml.match( /(?:\sSourceColumn="DateTime"\/>.+\sName=")(.+)(?:" SQLDataType.+\/><\/Columns>)/ );
    const resourceName = matchingResource
        ? matchingResource[ 1 ]
        : null;
    if ( !resourceName ) {
        console.error( xml );
        throw new Error( 'connector_phd: parseIlluminatorXML: malformed XML.' );
    }
    else {
        // We''l have two identical rowsets, so we only need the first rowset to get the full data .
        const rowset = xml.substring( xml.indexOf( '<Rowset>' ) + 8, xml.indexOf( '</Rowset>' ) );
        const rows = rowset.match( /(?:<Row>)(.+?)(?:<\/Row>)/gi );
        const re_tag = new RegExp( `(?:<${ resourceName }>)(.+)(?:</${ resourceName }>)` );
        const re_starttime = /(?:<DateTime>)(.+?)(?:<\/DateTime>)/;
        // Since we already only handle the first resultset, we don't have to doublecheck the dates anymore as in the previous implementation .
        return rows.map( row => ( { "tag" : resourceName, "starttime" : new Date( row.match( re_starttime )[ 1 ] ).toJSON(), "starttime_local" : row.match( re_starttime )[ 1 ].replace( 'T', ' ' ), "text" : parseFloat( row.match( re_tag )[ 1 ], 10 ) } ) );
    }
};
////////////////////
const create = ( model, resource ) => Promise.resolve( { "data" : null, "headers" : {}, "http_sttaus_code" : null } );
/*
*/
const fetchTemplate = resourceName => Promise.resolve( '' );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => parameters;
/*
*       - Due to populateTemplate, sql === parameters
*       - Should return array of objects
*/
const read = parameters => {
    if ( parameters.resourceName && parameters.line /*&& parameters.starttime && parameters.endtime*/ ) {
        return readFile( 'C:/easyreporting/resources/tags_phd.connector.json' )
            .then( buffer => buffer.toString() )
            .then( JSON.parse )
            .then( tags_phd => [ tags_phd, parameters ] )
            .then( createURI )
            .then( uri => { console.log( '--- URI ---' ); console.log( uri ); console.log( '--------------' ); return uri; } )
            .then( fetch )
            .then( parseIlluminatorXML )
            .then( data => ( {
                data,
                "headers" : {},
                "http_status_code" : null
            } ) );
    }
    else return Promise.reject( `resource not found: ${ JSON.stringify( parameters ) }` );
};
////////////////////
// export default { create, parse, read, 'server' : PHD_NORMALIZED, template };
module.exports = { chunksize, create, fetchTemplate, populateTemplate, read, 'server' : PHD_NORMALIZED };