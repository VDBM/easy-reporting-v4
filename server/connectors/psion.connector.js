/*  connector_psion.mjs   => Query PSION database
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-03
*/
// WATCHOUT: MySQL doesn't use brackets [] around table and field names.
////////////////////
//
const FS = require( 'fs' );
// import MYSQL from 'mysql';
const MYSQL = require( 'mysql' );
//import { NOOP } from '../modules/util.mjs';
const { NOOP, templatePopulate, propTransform } = require( '../modules/util.js' );
// import 
const { promisify } = require( 'util' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
/* we want uniform names for all servers, then will cast these to server specific names, depending on the connector used ( MYSQL in the case of PSION 
*/
const PSION_NORMALIZED = {
    'username' : 'all',
    'password' : 'all',
    'server' : 'mecvm27', // === '10.144.9.161',
    'options' : {
        'port' : '3306'
    }
};
/*
*/
const PSION = {  
    'host' : PSION_NORMALIZED.server,
    'user' : PSION_NORMALIZED.username,
    'password' : PSION_NORMALIZED.password,
    'database' : 'lijn1'
};
/*
*/
const transformations = {
    'filters' : filters => Array.isArray( filters )
        ? `AND ( ${ filters.join( ' ) AND ( ' ) } )`
        : filters,    
    'line' : {
        // 'crackers' : 2018??? defects in wrong database???
        'depal'     : 'depal',
        // 'engeneer' : null, never used
        'loge'      : 'loge',
        'line1'     : 'lijn1',
        'line2'     : 'lijn2',
        'line3'     : 'lijn3',
        'line4'     : 'lijn4',
        'line5'     : 'lijn5',
        'line6'     : 'lijn6',
        // 'mssdepal'  : null, never used
        // 'mssrm'     : null, never used
        // 'mssseasoning' : 2016
        // 'ontlss' : 2012
        'rm'        : 'rm',
        'seasoning' : 'seasoning',
        // 'slt' : null, never used
        'spot'      : 'lijn7',
        // 'tankveld' : 2011
        'ulf'       : 'ulf',
        'util'      : 'util',
        'whs'       : 'whs'
        // 'wwtp' : 2012
    }
};
/*
*   @notes:
*                   - We can propably write this control flow a little bit shoter, but left it this way atm for readability.
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => {
    if ( Array.isArray( parameters.line ) ) {
        return parameters.line
            .map( line => `( ${ populateTemplate( propTransform( Object.assign( {}, parameters, { 'line' : line } ), transformations, true ), dependencies, sqlTemplate ) } )` )
            .join( ' UNION ' );
    }
    else if ( !parameters.line && parameters.department === 'packing' ) {
        // If we want to use populateTemplate recursively, we need to ONLY update the line from 'all' to the normalized version, without using propTransform before that.
        // The proptransform will be trigegred once we loop populateTemplate over the array.
        // If we use propTransform before that, our lines and filters will be transformed twice, which elads to errors if we don't add the transformation result as valid inputs as well.
        const sql = [ 'line1', 'line2', 'line3', 'line4', 'line5', 'line6' ]
            .map( line => `( ${ populateTemplate( Object.assign( {}, parameters, { line } ), dependencies, sqlTemplate ) } )` )
            .join( ' UNION ' );
        // Wrap the result into another query so we can use ORDER BY to catch the entire resultset returned by the multiple queries.
        return `SELECT RESULTSET.* FROM ( ${ sql } ) AS RESULTSET ORDER BY line, starttime`;
    }
    else if ( parameters.line === 'all' ) {
        // If we want to use populateTemplate recursively, we need to ONLY update the line from 'all' to the normalized version, without using propTransform before that.
        // The proptransform will be trigegred once we loop populateTemplate over the array.
        // If we use propTransform before that, our lines and filters will be transformed twice, which elads to errors if we don't add the transformation result as valid inputs as well.
        const sql = Object.keys( transformations.line )
            .map( line => `( ${ populateTemplate( Object.assign( {}, parameters, { line } ), dependencies, sqlTemplate ) } )` )
            .join( ' UNION ' );
        // Wrap the result into another query so we can use ORDER BY to catch the entire resultset returned by the multiple queries.
        return `SELECT RESULTSET.* FROM ( ${ sql } ) AS RESULTSET ORDER BY line, starttime`; 
    }
    // Add the normalized name as well by using ' propTransform( ..., ..., true ) '
    else return templatePopulate( sqlTemplate, propTransform( parameters, transformations, true ), dependencies );
};
/*
*/
const read = ( sql ) => {
    return new Promise( ( resolve, reject ) => {
        const resultSet = [];
        const connection = MYSQL.createConnection( PSION );
        connection.connect( error => error ? reject( { 'code' : 500, 'value' : 'connection refused', 'error' : JSON.stringify( error ) } ) : void( 0 ) );
        const query = connection.query( sql );
        // Error executing the query: bad syntax, too many rows, server crashed, other errors, ... .
        // Should be extended absed on what the error from the server is.
        query.on( 'error', error => reject( { 'code' : 500, 'value' : 'server error', 'error' : JSON.stringify( error ) } ) );
        // Incoming field packets, if any, preceding the resultSet rows.
        query.on( 'fields', NOOP );
        // Incoming row
        query.on( 'result', row => resultSet.push( row ) );
        // All rows have been finished
        query.on( 'end', () => {
            resolve( {
                "data" : resultSet,
                "headers" : {},
                "http_status_code" : null
            } );
        });
        // Close the connection: MySQL module documentation: Closing the connection is done using end() which makes sure all remaining queries are executed before sending a quit packet to the mysql server.
        connection.end();
    } );
};
/*
*/
const fetchTemplate = resourceName => readFile( `./queries/${ resourceName }.sql` ).then( buffer => buffer.toString() );
////////////////////
//export default { parse, read, 'server' : PSION_NORMALIZED, template };
module.exports = { fetchTemplate, populateTemplate, read, 'server' : PSION_NORMALIZED };