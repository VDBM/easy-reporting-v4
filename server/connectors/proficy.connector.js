/*  connector_proficy.mjs   => Query PROFICY database
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-23
*/
// DATE FORMAT: database returns ISO format, but tables contain the local timestamp.
// => database  : STRING "2018-03-14 07:23:30.000"
// =>     node  :   JSON "2018-03-14T07:23:30.000Z"
// => displays  :  LOCAL "2018-03-14 07:23:30"
// WATCHOUT: We might be able to merge the EASYREPORTING, COCKPIT and PROFICY connetors into a seperate module connector_mssql and only define the transformations and connection object in these connectors themselves.
// WATCHOUT: we should refactor the line transformation so we can reuse it in transformation functions as well. Alternatively think about introducing a post query transformation? Or include it in the SQL itsself? Or extra join on lines table in proficy?
////////////////////
//
const FS = require( 'fs' );
// import MSSQL from 'tedious';
const MSSQL = require( 'tedious' );
// import { NOOP, templatePopulate as populate, propHash, propTransform, propUpdate } from '../modules/util.mjs';
const { NOOP, templatePopulate, propTransform, propUpdate } = require( '../modules/util.js' );
// import 
const { promisify } = require( 'util' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
/*
*/
const PROFICY_NORMALIZED = {
    'username' : 'contec',
    'password' : 'katrien',
    'server' : 'mecvm33', // === '10.144.9.166'
    'options' : {
        'database' : 'GBDB', // database must be defined after changing to contec/katrien credentials due to comxclient being removed
        'connectTimeout' : 90000, // The number of milliseconds before the attempt to connect is considered failed (default: 15000).
        'requestTimeout' : 120000 // The number of milliseconds before a request is considered failed, or 0 for no timeout (default: 15000).
    }
};
/*
*/
const PROFICY = {
    'userName' : PROFICY_NORMALIZED.username,
    'password' : PROFICY_NORMALIZED.password,
    'server' : PROFICY_NORMALIZED.server,
    'options' : Object.assign( {}, PROFICY_NORMALIZED.options )
};
/*
*/
const transformations = {
    'filters' : filters => filters.length
        ? `AND ( ${ filters.join( ' ) AND ( ' ) } )`
        : '',
    'line' : ( function() {
        const lineMap = {
            'line1' : 2,
            'line2' : 15,
            'line3' : 16,
            'line4' : 17,
            'line5' : 18,
            'line6' : 19
        };
        return line => Array.isArray( line )
            ? line.map( line => lineMap[ line ] ).join( ', ' )
            : lineMap[ line ];
    } () )
};
/*
*/
const generateColumnName = propUpdate( 'columnName', record => record.metadata.colName );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => templatePopulate( sqlTemplate, propTransform( parameters, transformations, true ), dependencies );
/*
*/
const read = ( sql ) => {
    return new Promise( ( resolve, reject ) => {
        const resultSet = [];
        const connection = new MSSQL.Connection( PROFICY );
        const query = new MSSQL.Request( sql, ( error, rowCount ) => {
            connection.close();
            if ( error ) reject( { 'code' : 500, 'value' : 'request creation error', 'error' : JSON.stringify( error ) } );
            else {
                resolve( {
                    "data" : resultSet,
                    "headers" : {},
                    "http_status_code" : null
                } );
            }
        } );
        connection.on( 'connect', error  => error ? reject( { 'code' : 500, 'value' : 'connection refused', 'error' : JSON.stringify( error ) } ) : connection.execSql( query ) );
        /* Each infoMessage contains the following keys: Keep this clean, since every request can generate messages. Might be useful to log these somewhere in the future.
        *    {
        *      'number' : '5701/5703',
        *      'state' :  '1/2',
        *      'class' : '0',
        *      'message' : 'Changed database context to master/Changed language settings to us_english',
        *      'servername' : 'MECVM33\GBDB',
        *      'procname' : '',
        *      'linenumber' : '1/12',
        *      'name' : 'INFO',
        *      'event' : 'infoMessage'
        *   }
        */
        connection.on( 'infoMessage', NOOP );
        connection.on( 'error', error => reject( { 'code' : 500, 'value' : 'internal error', 'error' : JSON.stringify( error ) } ) );
        connection.on( 'errorMessage', error => reject( { 'code' : 500, 'value' : `invalid query syntax:\n${ sql }`, 'error' : JSON.stringify( error ) } ) );
        /* columnMetaData will be sent once for each query before row events occur.
        *       Might nto work for all databses
        *       Properties: colName, typeName, precision, scale, dataLength
        */
        query.on( 'columnMetaData', NOOP );
        /*	row format
            {
                'value' :
                'metadata' : {
                    'userType' : 
                    'flags' : 
                    'type' : 
                    'colName' :
                    'collation' :
                    'precision' :
                    'scale' :
                    'udtInfo' :
                    'dataLength' :
                    'tableName' :
                }
            }
        */
        query.on( 'row', row => {
            // For efficiency, we just write the full transform into one loop
            // We could split this into propX chaining but end up with more code.
            const record = row.reduce( ( record, row ) => { record[ row.metadata.colName ] = row.value; return record; }, {} );
            // TODO: NULL check, decide if we want to parse the string NULL to the value NULL if the database doesn't contain this automatically.
            resultSet.push( record );
        } );
    } );
};
/*
*/
const fetchTemplate = resourceName => readFile( `./queries/${ resourceName }.sql` ).then( buffer => buffer.toString() );
////////////////////
// export default { parse, read, 'server' : PROFICY_NORMALIZED, template };
module.exports = { fetchTemplate, populateTemplate, read, 'server' : PROFICY_NORMALIZED };