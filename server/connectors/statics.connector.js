/*  statics.connector.mjs   => Static files & calculations
*   @data_language: ES6
*   @data_version:  v1.0.1
*   @entry_user:    VDBM
*   @update_date:   2018-05-22
*/
////////////////////
// import
////////////////////
// derivations
////////////////////
// SERVER
/*
*/
const STATICS_NORMALIZED = {
    'username' : null,
    'password' : null,
    'server' : null,
    'options' : {}
};
/*
*/
const STATICS = {};
/*
*/
const transformations = {};
////////////////////
// UTILS
////////////////////
// FETCH / POPULATE
/*
*/
const fetchTemplate = resourceName => Promise.resolve( null );
/*
*/
const populateTemplate = ( parameters, dependencies, sqlTemplate ) => parameters;
////////////////////
// CRUD
/*  create  
*/
const create = ( model, chunks_body ) => Promise.reject( { "data": "method not implemented", "headers": {}, "http_status_code": 500 } );
/*  read
*/
const read = input => {
	console.log( 'static request' );
	console.log( input );
	return Promise.resolve( {
		"data": [],
		"headers": {},
		"http_status_code": 200
	} );
};
/*  update
*/
const update = ( model, chunks_body ) => Promise.reject( { "data": "method not implemented", "headers": {}, "http_status_code": 500 } );
////////////////////
// export default { parse, read, 'server' : ADMIN_NORMALIZED, template };
module.exports = { create, fetchTemplate, populateTemplate, read, 'server' : STATICS_NORMALIZED, update };