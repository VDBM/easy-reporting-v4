/*  connector_active_directory.mjs   => Read active directory information for groups and users.
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-26
*/
/*  NOTES
*           - Username needs to ahve the domain appended to it to use inside a LDAP query => ${ bek_number }@eu.kellogg.com
*/
////////////////////
// import AD from 'activedirectory'';
const AD = require( 'activedirectory' );
// import { promisify } from '../modules/util.js'
const NODE_UTIL = require( 'util' ); // This is the default promisify implementation shipped with node.js 8.0.0 or higher. This is NOT our own util library.
////////////////////
/*  AD_NORMALIZED   =>
*   @type               private static Object
*   @name               AD_NORMALIZED
*   @notes
*                       -
*/
const DOMAIN_CONTROLLER_NORMALIZED = {
    'username' : 'bekmxv13@eu.kellogg.com', //'bessxa62',
    'password' : 'FreeLeague14', //'R3port!ng',
    'server' : 'ldap://BELVM02.eu.kellogg.com',
    'options' : {
        'domain' : 'DC=eu,DC=kellogg,DC=com'
    }
};
/*  AD              =>
*   @type               private static Object
*   @name               AD
*   @notes
*                       - 
*/
const DOMAIN_CONTROLLER = {
    'url' : DOMAIN_CONTROLLER_NORMALIZED.server,
    'baseDN' : DOMAIN_CONTROLLER_NORMALIZED.options.domain,
    'username' : DOMAIN_CONTROLLER_NORMALIZED.username,
    'password' : DOMAIN_CONTROLLER_NORMALIZED.password
};
/*  transformations     =>
*   @type               private static Object
*   @name               transformations
*   @notes
*                       -
*/
const transformations = {};
////////////////////
/*
*/
const DOMAIN_GROUPS = {
    'eni' : 'CN=MECEni,OU=Groups,OU=Mechelen factory,OU=BE,OU=Eu,dc=eu,dc=kellogg,dc=com'
};
/*
*/
const DOMAIN_SUFFIX = '@eu.kellogg.com';
////////////////////
/*  AD POST. Functionality disbled
*/
const create = () => null;
/*  parse               =>
*   @type               public Function
*   @name               parse
*   @param              
*   @return             String
*   @notes
*                       - dependencies will usually be null, but can be used
*                       - template will always be null, since we don't need a seperate SQL file. Technically we can consider the excel itsself as the template, but this might complicate the architecture too much.
*/
const populateTemplate = ( parameters, dependencies, template ) => parameters.user;
/*  query               =>
*   @type               public Function
*   @name               query
*   @param              
*   @return             String  
*   @notes
*                       -
*/
const read = sql => {
    const name_ad = `${ sql }${ DOMAIN_SUFFIX }`;
    const connection = new AD( DOMAIN_CONTROLLER );
    const findUser = NODE_UTIL.promisify( connection.findUser );
    const isUserMemberOf = NODE_UTIL.promisify( connection.isUserMemberOf );
    return findUser( name_ad )
        .then( user => ( { "exists" : true, "name_ad" : name_ad, "name_first" : user.givenName, "name_last" : user.sn } ) )
        .then( user => isUserMemberOf( name_ad, DOMAIN_GROUPS.eni ).then( isMember => Object.assign( user, { "group_eni" : isMember } ) ) )
        .then( data => ( {
            data,
            "headers" : {},
            "http_status_code" : null
        } ) )
        .catch( console.error );
};
/*  template            => We'll use the 'extending' field of the model to determine the path to the excel resource. Technically we can say the .model file extends the functionality of the excel.
*   @type               public Function
*   @name               template
*   @param              
*   @return             
*   @notes
*                       -
*/
const fetchTemplate = extending => Promise.resolve( 'result of template' );
////////////////////
// export default { create, parse, read, 'server' : null, template };
module.exports = { create, fetchTemplate, populateTemplate, read, 'server' : null };

// //read( 'bekmxv13' ).then( console.log );