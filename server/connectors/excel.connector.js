/*  connector_excel.mjs   => Read cells from excel file. Either from L-drive or from a uploaded document.
*   @data_language: ES6
*   @data_version:  v1.0.1
*   @entry_user:    VDBM
*   @update_date:   2017-12-12
*/
/*  NOTES
*   - Excel file paths have to be in escaped JSON, so \\
*   - Excel sheet date formatting: jjjj-mm-ddTuu:mm:ss,000Z
*   - We have two types of excel files: 1)  2) files uplaoded by the end suer that can remain static.
*       1) Files stored on the L-drive: these get read dynamically every time the data is requested. We'll figure out a way to cache the responses later, propably by saving and comparing the last editted date.
*       2) FIles stored on this server, uploaded by the end user. These files are stiatic until they get reuploaded, so we can permanently cache the response.
*/
////////////////////
// import 
const FS = require( 'fs' );
// import 
const SMB2 = require( 'smb2' );
// import XLSX from 'xlsx';
const XLSX = require( 'xlsx' );
////////////////////
/*  LDRIVE_NORMALIZED   =>
*   @type               private static Object
*   @name               LDRIVE_NORMALIZED
*   @notes
*                       -
*/
const LDRIVE_NORMALIZED = {
    'username' : 'bekmxv13', //'bessxa62',
    'password' : 'FreeLeague14', //'R3port!ng',
    'server' : '\\\\mecpublic\\mecpublic', // = '10.144.9.93'
    'options' : {
        'domain' : 'KEU'
    }
};
/*  LDRIVE              =>
*   @type               private static Object
*   @name               LDRIVE
*   @notes
*                       - 
*/
const LDRIVE = {
    'share' : LDRIVE_NORMALIZED.server,
    'domain' : LDRIVE_NORMALIZED.options.domain,
    'username' : LDRIVE_NORMALIZED.username,
    'password' : LDRIVE_NORMALIZED.password
};
/*  transformations     =>
*   @type               private static Object
*   @name               transformations
*   @notes
*                       -
*/
const transformations = {};
////////////////////
/*
*/
const create = ( model, chunks_body ) => {
    // const file = Buffer.concat( chunks_body );
    // Buffer.concat() and Buffer.from() will transform the array into a file again .
    /* EXCEL POST. MOVE THIS TO THE EXCEL CONNECTOR
    const FS = require( 'fs' );
    FS.writeFile( `./resources/${ resourceName }`, resource, 'binary', function( error ) {
        if ( error ) reject( 'cant write excel file.' );
        else {
            const json = `{"dependencies" : [],"extending" : "C:/easyreporting/resources/${ resourceName }","filters" : [],"name" : "${ resourceName }","notes" : [],"properties" : [],"servers" : ["EXCEL"],"transformation" : null}`;
            FS.writeFile( `./models/${ resourceName }.model`, json, function( error ) {
                if ( error ) reject( 'cant write model file.' );
                else resolve( `http://mecvm21/API/${ resourceName }?output=table` );
            } );
        }
    } );
    */  
};
/*  parse               => Return the URI of the excel file. Since the path is determined by the template method, we can just return the template to get the path as described in the model.extending property.
*   @type               public Function
*   @name               parse
*   @param              
*   @return             String
*   @notes
*                       - dependencies will usually be null, but can be used
*                       - template will always be null, since we don't need a seperate SQL file. Technically we can consider the excel itsself as the template, but this might complicate the architecture too much.
*/
const populateTemplate = ( parameters, dependencies, template ) => template;
/*  query               =>
*   @type               public Function
*   @name               query
*   @param              
*   @return             String  
*   @notes
*                       -
*/
const read = ( path ) => {
return new Promise( ( resolve, reject ) => {
    const from_ldrive = !path.match( /^[a-zA-Z]:\\/ );
    const connection = from_ldrive
        ? new SMB2( LDRIVE )
        : FS;
    connection.readFile( path, ( error, excel ) => {
        if ( error ) reject( error );
        else {
            const workbook = XLSX.read( excel );
            if ( !workbook ) reject( 'no workbook' );
            else {
                if ( from_ldrive ) {
                    connection.close();
                    resolve( workbook );
                }
                else {
                    let parsingSheets = true;
                    let sheetIndex = 0;
                    let json = [];
                    while ( parsingSheets ) {
                        const sheetName = workbook.SheetNames[ sheetIndex ];
                        if ( sheetName ) {
                            const worksheet = workbook.Sheets[ sheetName ];
                            if ( worksheet ) {
                                const rows = XLSX.utils.sheet_to_json( worksheet );
                                rows.forEach( row => json.push( row ) );
                            }
                            sheetIndex += 1;
                        }
                        else parsingSheets = false;
                    }
                    resolve( {
                        "data" : json,
                        "headers" : {},
                        "http_status_code" : null
                    } );
                }
            }
        }
    } );
} );
};
/*  template            => We'll use the 'extending' field of the model to determine the path to the excel resource. Technically we can say the .model file extends the functionality of the excel.
*   @type               public Function
*   @name               template
*   @param              
*   @return             
*   @notes
*                       -
*/
const fetchTemplate = extending => Promise.resolve( extending );
////////////////////
// export default { create, parse, read, 'server' : null, template };
module.exports = { create, fetchTemplate, populateTemplate, read, 'server' : null };