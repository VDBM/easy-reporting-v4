// Don't forget that the package needs to mention the babel compile version!
// Wrap our CommonJS modules into an AMD-style declaration so we can use the basic require.js library by leveraging the gulp-header and gulp-footer plugins.
// TODO: refactor the tempaltes and other resoruces files into its own resoruces call that will get called by every app . Add the grid css and such to it as well .
const babelify = require( 'babelify' );
const browserify = require( 'browserify' );
const buffer = require( 'vinyl-buffer' );
const gulp = require( 'gulp' );
const rename = require( 'gulp-rename' );
const source = require( 'vinyl-source-stream' );
const sourcemaps = require( 'gulp-sourcemaps' );
gulp.task( 'copy_statics', () => {
    gulp
        .src( [
            'projects/explorer/easyreporting_app_explorer.css',
            'resources/component_*.css',
            'resources/easyreporting.css',
            'resources/easyreporting_grid.css',
            'resources/*.config.json',
            'resources/polyfill.min.js',
            'resources/templates.html',
            'resources/faviconIE.ico'
        ] )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/resources' ) );
} );
gulp.task( 'checklist_compile', () => {
    browserify( 'projects/checklist/easyreporting_app_checklist.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ] } )
        .bundle()
        .pipe( source( 'projects/checklist/easyreporting_app_checklist.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'easyreporting_app_checklist.min.js' ) )
        .pipe( sourcemaps.init( { 'loadmaps' : true } ) )
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/checklist' ) );
} );
gulp.task( 'checklist_copy_statics', () => {
    gulp
        .src( [ 'projects/checklist/easyreporting_app_checklist.css', 'projects/checklist/easyreporting_app_checklist.html' ] )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/checklist' ) );
} );
gulp.task( 'decoupled', () => {
    browserify( 'projects/decoupled/easyreporting.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ] } )
        .bundle()
        .pipe( source( 'projects/decoupled/easyreporting.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'easyreporting.min.js' ) )
        .pipe( sourcemaps.init( { 'loadmaps' : true } ) )
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/decoupled' ) );
    gulp
        .src( [ 'projects/decoupled/index.html', 'projects/decoupled/*.css' ] )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/decoupled' ) );		
		
} );
gulp.task( 'signs_compile', function() {
    browserify( 'projects/signs/easyreporting_app_signs.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ] } )
        .bundle()
        .pipe( source( 'projects/signs/easyreporting_app_signs.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'easyreporting_app_signs.min.js' ) )
        .pipe( sourcemaps.init( { 'loadmaps' : true } ) )
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/signs' ) );       
} );
// We might want to split up the template file once it gets too large
gulp.task( 'signs_copy_statics', () => {
    gulp
        .src( [ 'projects/signs/easyreporting_app_signs.css', 'projects/signs/easyreporting_app_signs.html' ] )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/signs' ) );
} );
gulp.task( 'idd_compile', function() {
    browserify( 'projects/idd/easyreporting_app_idd.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ] } )
        .bundle()
        .pipe( source( 'projects/idd/easyreporting_app_idd.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'easyreporting_app_idd.min.js' ) )
        .pipe( sourcemaps.init( { 'loadmaps' : true } ) )
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/idd_dev' ) );
} );
gulp.task( 'idd_copy_statics', () => {
    gulp
        .src( [ 'projects/idd/easyreporting_app_idd.css', 'projects/idd/easyreporting_app_idd.html', 'projects/idd/idd.config.json' ] )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/idd_dev' ) );
} );
gulp.task( 'inputs_compile', () => {
    browserify( 'projects/inputs/inputs.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ] } )
        .bundle()
        .pipe( source( 'projects/inputs/inputs.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'inputs.min.js' ) )
        .pipe( sourcemaps.init( { 'loadmaps' : true } ) )
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/inputs' ) );
    gulp
        .src([ 'projects/inputs/index.html', 'projects/inputs/*.css' ])
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/inputs' ));	 
});
gulp.task( 'admin_compile', function() {
    browserify( 'projects/admin/easyreporting_app_admin.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ] } )
        .bundle()
        .pipe( source( 'projects/admin/easyreporting_app_admin.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'easyreporting_app_admin.min.js' ) )
        .pipe( sourcemaps.init( { 'loadmaps' : true } ) )
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/admin' ) );
} );
gulp.task( 'admin_copy_statics', () => {
    gulp
        .src( [ 'projects/admin/easyreporting_app_admin.css', 'projects/admin/easyreporting_app_admin.html' ] )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/admin' ) );
} );
gulp.task( 'admin', [ 'copy_statics', 'admin_compile', 'admin_copy_statics' ], () => {} );
gulp.task( 'checklist', [ 'copy_statics', 'checklist_compile', 'checklist_copy_statics' ], () => {} );
gulp.task( 'default', [ 'copy_statics', 'admin_compile', 'admin_copy_statics', 'explorer_compile', 'signs_compile', 'signs_copy_statics', 'idd_compile', 'idd_copy_statics' ], () => {} );
gulp.task( 'idd', [ 'copy_statics', 'idd_compile', 'idd_copy_statics' ], () => {} );
gulp.task( 'inputs', [ 'inputs_compile' ], () => {});
// gulp.task( 'redirect', [ 'redirect_compile', 'redirect_copy_statics' ], () => {} );
gulp.task( 'signs', [ 'copy_statics', 'signs_compile', 'signs_copy_statics' ], () => {} );