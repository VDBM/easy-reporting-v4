/*  server.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.5
*   @entry_user:    VDBM
*   @update_date:   2017-12-27
*/
////////////////////
//
const FS = require( 'fs' );
// import HTTP from 'http';
const HTTP = require( 'http' );
// import PARAMS from './modules/parameters.mjs';
const PARAMS = require( './modules/parameters.js' );
// import REST from './modules/rest.mjs';
const REST = require( './modules/rest.js' );
// import TMPL from './modules/templates.mjs';
const TMPL = require( './modules/class_template.js' );
//
const { promisify } = require( 'util' );
// import * as UTIL from './modules/util.mjs';
const UTIL_ER = require( './modules/util.js' );
// import ZLIB from 'zlib';
const ZLIB = require( 'zlib' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
/*
*/
const HTTP_HEADERS = [
    "Response",
    "Cache-Control",
    "Transfer-Encoding",
    "Content-Type",
    "Server",
    "Access-Control-Allow-Origin",
    "Access-Control-Allow-Credentials",
    "Access-Control-Allow-Headers",
    "X-Powered-By",
    "Date"
];
/*
*/
const sendError = ( request, response, error ) => {
    try {
        const errorPackage = {
            "date" : new Date(),
            "user" : request.headers[ 'x-iisnode-auth_user' ],
            "error" : error.message
                ? error.message.replace( /'/g, '' )
                : error
        };
        console.error( errorPackage );
        // Changed the settings of the IIS error pages to detailed errors for both local and remote requests .
        // This forces IIS to not use its custom error pages and forces us to always provide an error page in our apps .
        response.writeHead( 400, { 'Content-Type' : 'application/json', 'Cache-Control' : 'no-cache' } );
        response.end( JSON.stringify( errorPackage ) );
        //`<html><head><style>table{border-collapse:collapse;}th,td{border:1px solid black;padding:4px;}</style><title>easyreporting error</title></head><body><table><tr><th>date</th><th>user</th><th>error</th></tr><tr><td>${ errorPackage.date }</td><td>${ errorPackage.user }</td><td>${ errorPackage.error }</td></tr></body></html>`
    }
    catch( error ) {
        response.writeHead( 400, { 'Content-Type' : 'text/plain', 'Cache-Control' : 'no-cache' } );
        response.end( '{"error":"failed creating detailed error description"}' );
    }
};
/*
*   The output parameter will make sure that the resource always is in the { 'type'; x, 'data' : [] } format .
*/
const sendResource = ( request, response, resource ) => {
    ZLIB.gzip( resource.data, ( error, data ) => {
        if ( error ) sendError( request, response, error );
        else {
            if ( !resource.headers || !resource.headers[ 'Content-Type' ] ) sendError( request, response, `${ request.method.toUpperCase() } - ${ request.url.slice( 5 ) } - Response Content-Type header is undefined` );
            else {
                const headers = Object.assign( { "Content-Encoding" : "gzip", "Cache-Control" : "no-cache" }, resource.headers || {} );
                response.writeHead( resource.http_status_code || 200, headers );
                // HEAD requests should not return anything apart from status code 200 OK .
                if ( resource.data ) response.end( data );
                else response.end();
            }
        }
    } );
};
////////////////////
/* TODO: we can probably split this even better into dev / prod by refactoring the common parts .
*/
const server = {
    'deploy' : () => {
        HTTP.createServer( function( request, response ) {
            const { headers, method, url } = request;
            const resourceName = url.slice( 5 );
            // Watch out with excel discovery requests !
            // If no resource is requested, just send the explorer .
            // We have the same logic in our web.config file, but include it here as well explicitly .
            // Fake the URL so we can parse the resourceName out of it. Maybe we should just add the resourceName as a parameter as well .
            if ( !resourceName ) sendError( request, response, new Error( `resource does not exist: ${ url }` ) );
            else {    
                const parameters = PARAMS.parse( url );
                // CREATE - POST
                //  handles our excel file upload .
                // SOmethign else handles our json requests .
                if ( method === 'POST' ) {
                    const chunks = [];
                    request.on( 'error', error => sendError( request, response, error ) );
                    request.on( 'data', chunk => chunks.push( chunk ) );
                    request.on( 'end', () => {
                        REST
                            .post( parameters.resourceName, chunks )
                            .then( resource => sendResource( request, response, resource ) )
                            .catch( error => sendError( request, response, error ) );
                    } );
                }
                // READ - GET
                else if ( method === 'GET' ) {
                    REST
                        .get( parameters.resourceName, parameters )
                        .then( resource => sendResource( request, response, resource ) )
                        .catch( error => sendError( request, response, error ) );
                }
                // UPDATE - PUT
                else if ( method === 'PUT' ) {
                    const chunks = [];
                    request.on( 'error', error => sendError( request, response, error ) );
                    request.on( 'data', chunk => chunks.push( chunk ) );
                    request.on( 'end', () => {
                        REST
                            .put( parameters.resourceName, parameters, chunks )
                            .then( resource => sendResource( request, response, resource ) )
                            .catch( error => sendError( request, response, error ) );
                    } );
                    //sendError( request, response, 'The server does not support UPDATE/PUT requests.' );
                }
                // DELETE - DELETE
                else if ( method === 'DELETE' ) {
                    sendError( request, response, 'The server does not support DELETE/DELETE requests.' );
                }
                // HEAD request: preflight request often requested by excel imports.
                // A HEAD request has to answer with the http that would be sent if the same resource is requested with a GET request.
                else if ( method === 'HEAD' ) {
                    sendResource( request, response, { "data" : null , "headers" : { "Content-Type" : "text/plain" }, "http_status_code" : 200 } );
                }
                // Just for safety: Can people even make requests with non-existing methods? Typos GOT, PAST, PAT, DELIET ?
                // Not supported http verbs: OPTIONS TRACE CONNECT PATCH
                else sendError( request, response, `Mangled request. No correct METHOD: ${ method }` );
            }
        } ).listen( process.env.PORT );
    },
    'dev' : () => {
        // node server.js dev METHOD SLUG
        // Since dev requests don't actually have a body, we need to use the parameters from the URI as the body .
        const method = process.argv[ 3 ];
        const slug = process.argv[ 4 ];
        const parameters = PARAMS.parse( `/API/${ slug.replace( /\+/g, '&' ).replace( /'/g, '"' ) }` );
        // CREATE - POST
        if ( method === 'POST' ) {
            REST
                .post( parameters.resourceName, [ parameters.body ] )
                .then( resource => console.log( resource ) )
                .then( process.exit )
                .catch( error => { console.error( error ); process.exit(); } );
        }
        // READ - GET
        else if ( method === 'GET' ) {
            REST
                .get( parameters.resourceName, parameters )
                .then( resource => console.log( JSON.stringify( resource.data ) ) )
                .then( process.exit )
                .catch( error => { console.error( error ); process.exit(); } );
        }
        // UPDATE - PUT
        else if ( method === 'PUT' ) {
            REST
                .put( parameters.resourceName, [ parameters.body ] )
                .then( resource => console.log( resource ) )
                .then( process.exit )
                .catch( error => { console.error( error ); process.exit(); } );
        }
        // DELETE - DELETE
        else if ( method === 'DELETE' ) {
            console.error( 'NO DELETE' );
            process.exit();
        }
    },
    // The template engine module has to be initted before we can use it. So we provide the server run command as the callback to the template engine initting.
    'run' : () => TMPL.init( server[ process.argv[ 2 ] || 'deploy' ] )
};
server.run();