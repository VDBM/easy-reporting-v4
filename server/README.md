# Easy Reporting - REST Services

* Endpoint: http://mecvm21/api
* Physical location: \\\\mecvm21\C:\easyreporting_api

* Data compliance: 30% => 10&4 of 40
    - ![](resources/green.png) board_dev: OK - webpage
    - ![](resources/green.png) bos: OK
    - ![](resources/green.png) brandcodes: OK --> NOK, gives error
    - ![](resources/red.png) cil_cl_entries: TBI
    - ![](resources/green.png) cilcl_ool: OK
    - ![](resources/red.png) cil_cl_pecentage: TBI
    - ![](resources/orange.png) defects: OK - TODO: split lines
        - ![](resources/green.png) defects_quality: OK
        - ![](resources/green.png) defects_safety: OK
    - ![](resources/orange.png) defects_solved: OK - TODO - same as /defects
    - ![](resources/green.png) downtime: OK
        - ![](resources/green.png) downtime_ongoing: OK
        - ![](resources/green.png) downtime_packing: OK
        - ![](resources/green.png) downtime_planned: OK
        - ![](resources/green.png) downtime_process: OK
        - ![](resources/green.png) downtime_unplanned: OK
    - ![](resources/green.png) golden6: OK
    - ![](resources/red.png) incidents: TODO - update the parser_incidents transformation to support parameters, so we can actually 'search' and filter.
    - ![](resources/orange.png) logbook_eni: OK - old logbook. new not connected yet. => TODO: NOK yet
        - ![](resources/orange.png) partial_logbook_eni_easyreporting: TODO
        - ![](resources/green.png) partial_logbook_eni_mecpri02: OK
    - ![](resources/green.png) logbook_teamleaders: TODO - removed table_merged support.
        - ![](resources/green.png) partial_logbook_teamleaders_easyreporting: OK
            - ![](resources/red.png) partial_logbook_teamleaders_proficy: NOK
            - ![](resources/green.png) logbook_teamleaders_cards_pm: OK
            - ![](resources/green.png) logbook_teamleaders_breakdowns: OK
    - ![](resources/green.png) mss: OK
    
    
    /*
    + ![](resources/green.png) machine_stops: OK
    + ![](resources/green.png) machine_stops_cf: OK
    + ![](resources/green.png) machine_stops_cp: OK
    + ![](resources/green.png) machine_stops_gs: OK
    + ![](resources/orange.png) machine_stops_packing: Recheck if the filters are still correct.
    + ![](resources/red.png) machine_stops_process: TBI
    + ![](resources/green.png) machine_stops_shingling: OK
    + ![](resources/red.png) missed_tests: TBI
    + ![](resources/red.png) netweights: TBI, overpack + rejects in one entry?? = TODO: REDO transformation
    + ![](resources/red.png) PHD: TBI
    + ![](resources/red.png) pqm: TBI, calculations are wrong, meet with dimensys to resolve.
    + ![](resources/red.png) quality_alerts: TBI
    + ![](resources/red.png) scrap: TBI
    + ![](resources/red.png) scrap_packing: TBI
    + ![](resources/red.png) scrap_planned: TBI
    + ![](resources/red.png) scrap_process: TBI
    + ![](resources/red.png) triggers_packing: TBI
    + ![](resources/red.png) triggers_process: TBI
    */

    
* REST compliance: 47% => 5&2 of 13
    + ![](resources/red.png) DELETE: TBI
    + ![](resources/orange.png) GET: doublecheck implementation.
    + ![](resources/red.png) POST: TBI, only a select amount of POST requests are already implemented. ( the excel uploader )
    + ![](resources/red.png) PUT: TBI
    
    + ![](resources/green.png) Client / Server: OK. Usage of the split allows us to write front end reports that only have data transformations concerning the visualization. No seperate model files needed on the client.
    + ![](resources/green.png) Statelessness: OK. Our requests contain everything:
        - The method of the request.
        - The resource name in the URI after the servcie root.
        - The list of parameters in the URI after the resource name, followed by the query character. (?)
    + ![](resources/red.png) Cacheability: NOK. Have to do a full check of caching settings both front end and back end.
    + ![](resources/green.png) Layered system: OK
    + ![](resources/green.png) Code on demand: OK
    + ![](resources/orange.png) Uniform interface: We have a working version, but this has to be heavily updated. Important fields like department / area / location are not available for filtering yet.
        - ![](resources/green.png) Resource identification in requests: OK
        - ![](resources/red.png) Resource manipulation through representations: NOK. We don't include meta data inside the responses yet.
        - ![](resources/orange.png) Self-descriptive messages: Work in Progress. We already base most of the functionality on the combination of method/filetype, but this has to be extended further. Eg. change the excel POST functionality into a full POST infrastructure that can handle csv files etc as well.
        - ![](resources/red.png) Hypermedia as the engine of application state: NOK. No hyperlinks included inside the responses yet.
        
---


## DEV BACKLOG

*   **server.js**   :

    Excel Error ISSUE: Already added a HEAD request handler.
    Check the other excel import discovery requests and handle them properly.
    Raw Materials request generates following errors

    {"date":"2018-01-15T12:59:42.714Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T12:59:42.761Z","user":"KEU\\bekmxv13","error":"Mangled request. No correct METHOD: HEAD"}
    {"date":"2018-01-15T12:59:42.917Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T12:59:42.917Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T12:59:42.917Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T12:59:43.120Z","user":"KEU\\bekmxv13","error":"Mangled request. No correct METHOD: HEAD"}
    {"date":"2018-01-15T12:59:44.056Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T12:59:44.071Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}

    &&

    {"date":"2018-01-15T13:07:23.837Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T13:07:26.130Z","user":"KEU\\bekmxv13","error":"Mangled request. No correct METHOD: HEAD"}
    {"date":"2018-01-15T13:07:28.454Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T13:07:28.470Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T13:07:28.470Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T13:07:28.688Z","user":"KEU\\bekmxv13","error":"Mangled request. No correct METHOD: HEAD"}
    {"date":"2018-01-15T13:07:29.796Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    {"date":"2018-01-15T13:07:29.811Z","user":"KEU\\bekmxv13","error":"No valid resource requested."}
    
    We can't see the difference between a request originating from citrix or from laptop. So we'll include a parameter until we find a better solution.

    Direct link: ( laptop )

    {
        "connection":"keep-alive",
        "content-length":"0",
        "accept":"text/html, application/xhtml+xml,*-*",
        "accept-encoding":"gzip, deflate",
        "accept-language":"nl-BE",
        "host":"mecvm21",
        "user-agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
        "x-original-url":"/api/unloading_trucks?starttime=2018-01-01&endtime=2018-01-12",
        "x-iisnode-auth_type":"Negotiate",
        "x-iisnode-auth_user":"KEU\\bekmxv13"
    }


    Excel import: ( laptop )

    {
        "connection":"keep-alive",
        "content-length":"0",
        "accept":"text/html, text/plain, text/xml",
        "accept-encoding":"gzip, deflate",
        "authorization":"Negotiate - VERY LONG HASH",
        "host":"mecvm21",
        "user-agent":"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; ms-office)",
        "x-original-url":"/api/unloading_trucks?starttime=2018-01-01&endtime=2018-01-12",
        "x-iisnode-auth_type":"Negotiate",
        "x-iisnode-auth_user":"KEU\\bekmxv13"
    }


    Excel import: ( citrix )

    {
        "connection":"keep-alive",
        "content-length":"0",
        "accept":"text/html, text/plain, text/xml",
        "accept-encoding":"gzip, deflate",
        "authorization":"Negotiate - VERY LONG HASH",
        "host":"mecvm21",
        "user-agent":"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; InfoPath.3; .NET4.0E; .NET CLR 1.1.4322; ms-office)",
        "x-original-url":"/api/unloading_trucks?starttime=2018-01-01&endtime=2018-01-12",
        "x-iisnode-auth_type":"Negotiate",
        "x-iisnode-auth_user":"KEU\\bekmxv13"
    }
           
*   **server.js**       :
    This version that uses commonJS require() will cache all the required modules. So when debugging, watch out during testing, since if an edit was made inside a require'd module,
    that module might not have been reloaded inside IIS yet.
*   **server.js**       :
    We might want to move the outputTransformations into the transformations folder and just consider them a transformation like any other.
*   **server.js**       :
    Our URL rewrite rule configured inside the IIS also detects filenames that have the word 'demo' in them, so we can't use that word in any filename we try to upload until this is fixed.
*   **rest.js**         :
    We might want to move the entire .template() function into each connector so that we only need to expose create / read / update / delete in each connector. Came to this conclusion when implementiong POST and needing to pass both model and resources to the connector.
*   **rest.js**         :
    connector.query( sql ).then( require( queryModel.transformation ) ) inside 'resourceRead' will break once we switch back to ES6 syntax, since we don't have dynamic on the run import.
*   **rest.js**         :
    If we want to reuse the dependencies inside the transformation, we have to pass the dependencyObject into the transformation as well. Doublecheck if there's no other way to keep things seperated.
*   **rest.js**         :
    Does table_merged ordering logic inside tranformations make sense? See logbook_teamleaders.
*   **connector_x.js**  :
    I'm not really happy with the .generateInsert() implementation. We can propably move this to the rest.js module and keep it consistent across databases.
    The only watchout for this would be to transform things like dates and such to the correct format.
    So still under consideration next refactoring.
*   **connector_x**     :
    We should definately refactor the actual SQL execution into its own method since this is letter for leter the same in GET, POST and propably PUT as well.
*   **output_table.js** :
    We should refactor the HTML page wrapping into its own 'output_html' transformation and import this into output_table.js . This allows us to have connectors return full HTML pages without '?output=resource' hacks inside the connector.
*   **production_cans.sql** :
    Is this the right resource name? We have reworked cans as well, so we have to decide if we want to use
        production_cans,    production_cans_rework
        cans_produced,      cans_reworked
                            can_rework
        production_cases
*   **all queries** :
    Currently our entry_shift calculations don't think about DST. So we calculate entry_shift absed upon GMT time instead of GMT+1 time. Proven for PROFICY, but doublecheck the other servers as well.
*   **output_table_groups.js :
    This table can't be imported easily due to the inclusion of the statistics header. Find a better way, worst case, make a seperate model/transformation for it.
*   **.model files** :
    Add a way to set a server to null, so that we can declare models that are just aggregations of other queries. Eg, logbook_teamleaders is a transformation on the two partials, complicated by the fact that the proficy apartial needs the easyreporting partial to parse ids.
*   **.model files** :
    Update the tables array to incluide the actual servername as well. This will make it easier to merge data from multiple servers into one model description.
*   **logbook_teamleaders.model & transformations.js** :
    Defien the merge transformation that will merge the proficy and easyrorting models into one resultset.
*   **defects.model** :
    Implement all other departments as well so we can actually filter the defects.
*   **defects.model && connector_psion** :
    Find a better way to seperate the departments from eachother, so we have the logical following: mnp = line 1 to 6 . The other 'lines' should be departments.
*   **connector_x.js files** :
    Checkl if we shouldn't move the filters transformation back into the server or rest module, sicne we declare this filter on more or less all SQL servers.
*   **connector_psion** :
    Maybe we should include a replace inside the parse function that replaces [ and ] with nothing. This way we could write all our SQL tem,plates in exactly the same style, with [] around names, while this is invalid MySQl.
*   **partial_logbook_teamleaders_proficy** :
    We've permanently split the proficy and the easyreporting query. This way we no longer need linked servers to get the correct data.
    Check if the `downtime` query is fast enough to replace this partial by the downtime query. If so, update all the dependencies inside logbook_teamleaders and its dependants.
*   **downtime_packing.model & downtime_process.model** :
    Check query performance. If it's too slow, we can repalce the area lookup with its integers to eliminate the function call.
    Also doubelcheck again if CO and Planned should be included here.
*   **logbook_x & partial_logbook_x** :
    The logical resource name does not have SQL associated with it, since it just aggregates its depndenciesusing a tranformation. At the moment this forces us to use an empty .sql file to represent the resource.
    Find a better way to represent this. Either like a resourceType inside each model, or by detecting an empty filename somehow. Watch out making the difference between typo'ed resourceName and resoruces that are aggregations.
*   **templates.js** :
    Reenable transformation caching before live deployment.
*   **class_resource.js && transformations.js && templates.js** :
    See if we should chnge this into a transformation_template_interface_object and atatch all the templating functionality to it as well.
*   **count.js && other transformations** :
    We should check if we have to update the count.js and other transformations to support more than only the transofmration key + data property on their initial input.
    Since we have our template made that it should support this, but we dont have an actual transformation yet that return that format.
*   **server.js && rest.js** :
   {"code":500,"value":"connection refused","error":"{\"code\":\"ER_CON_COUNT_ERROR\",\"errno\":1040,\"sqlMessage\":\"Too many connections\",\"fatal\":true}"}
*   **multiplexer.js & rest.js** :
    Our MUX kinda ruins the ORDER BY clause in queries, since the resultset will be remerged after the SQL query has run.
*   **downtime_ongoing.sql** :
    This query is not date specific. So if we request all ongoing downtime of one month, we get one record. If we ask for ongoing of 2 months, we get 2 records. => Due to the MUX, we find the same record multiple times.
*   **incidents** :
    Query broke due to the new transformations implementation.
*   **group_x.js** :
    For shifts, days and possibly weeks, we can use fixed numbers to use with the dateSeries method of util.js, but we have to be carful trying the same with months and years. Doublecheck this if there's no edge cases near month and year borders. Leap years?
*   **statistics.js** :
    By extending the average calculations to check either property name or function, we could shorten the common syntax of resoource.map( extractProperty ).reduce( avg ) by making it part of the averages method.
*   **group_x.js** :
    After altering the group_x logic to include timechunks without any entries, we get empty lines when trying to ask for grouped raw data without an aggregation. So we need to check the existence of the aggregation to determine if we need to add the missing time chunks.
    We thought about 
*   **dev** :
    draggable_board ahs to be reimplemented as a model referring to webserver as the connector to actually render it again.
*   **parameters.js** :
    We might want to refactor the entire parsing into seperate transformations as well.
*   **util.js && group_day.js** :
    Watch out with the dateseries! If we create a series from (x)-01T00:00:00Z until (x+1)-01T00:00:00Z , the result will differ if we group on shift than if we group on day
    The series creation will just add miliseconds from the first day forward until it crosses the limit and keep the time intact.
    And since the last day of the month 00:00:00 is part of the previous day night shift, the last day of the month will not appear inside the series.
    So it's advised to always use shift start hours to create the series, since then the last day of the month 06:15:00 will appear as a seperate day.
*   **all classes** :
    ES6 classes throw a type error when not instantiated with the 'new' keyword, so we don't ned a sanity check for this.
*   **TODO** :
    Finish the group_x updates for week / month / year to also inclucde the missing timeframes.
*   **class_svg & dependants** :
    We could simplify the data handling by removing the 'property' that looks at data from all the components and write a seperate component that puts data into the right format.
*   **group_x** :
    Changed the check for dateSeries extraction from aggregation to aggregation & starttime & endtime. Else we can't group +aggregate queries without start and endtime.
    We cound extend this functionality into extracting the earliest and latest date gotten from the data.
*   **component_downtimebar.js && downtime.sql** :
    We should use the same type tag as in th query instead of 'uptime' and 'downtime' => running, planned & unplanned => these all correspond to a css class that determines the color.
*   **component_x** :
    Components should have both a render and a update method. The render function will render any static content and the update function will fill it with actual data.
    The end result will be that we can loop over all components and call the render() method on init, and the use the update() method from there on to update the data.
*   **component_x && class_x** :
    All components and classes need to have the 'type' property, either statically on their prototype chain or in their instance directly.
*   **class_application** :
    Watchout: diferentiate between application components and report conponents.
*   **application_easyreporting_signs** :
    We should think about defining fixed ranges for certain resources. But this might be report dependant.
*   **machine_stops.sql** :
    This is the format we want for all queries. Inner query to select the data, outer query to rename columns and do extra calculations. So recreate all queries like this.
*   **scrap.sql** :
    We have to divide the duration between the amount of rows => 12 rows = full duration, 6 rows = duration / 2 . There's a fucntion written by dimensys for this, but it's hardcoded. We also made a sql snippet detailing our own solution.
*   **downtime.sql && scrap.sql** :
    We need to find a better way to signify the duration and other floating point numbers. When importing them into excel, there's an issue with the number not being interpreted as a float, due to the . being used as the thousands seperator instead of the decimals behind seperator.
*   ** all projects ** :
    At the moment we use a workflow looking like this: Promise.then( fn() ).then( fn() ).then( fn() )
    This makes it a tiny bit more difficult to code, but is more readable in my opinion than using Promise.then( () => fn() ).then( () => fn() ).then( () => fn() )
    The advantage is that we can still have insatnces return a function bound to themselves upon initinstead of having to bind the function to its context again.
    We prefer .then( fn() ) over .then( () => fn() ) or .then( fn.bind( context ) )
*   **resources/phd_tags.json** :
    Does this belong in the query map or the resource map?
*   **transformations.js** :
    Temporarily added check to see if the transformation_exceptions property in the queryModel exists, containing an array of transformations to skip, so that specific connectors can implement these transformations themselves if needed.
    The logic behind this is that Illuminator already makes averages and sums for us, so we have to disable some of the transformations to keep our interface the same.
    We opted for this solution over adding an exception to the rest.js module, so that our REST model stays the same, even if we replace the transformer later.
    A good solution would be to specify in each model file which transformations can and can't be used with a resource.
*   **connector_phd.js** :
    The transformation implementation isn't complete yet. We'd need to find a better way to chunk the timeframes into parts corresponding with our grouping.
*   **util.js** :
    dateShift: changed the definition from 615 to 415 sicne we're trying to use UTC for everything. TRIPLECHECK that all queries return UTC time.
*   **server.js** :
    Updated the sendError code and editted the detailed errors in IIS error pages settings. Now any error thrown inside any module will bubble up to the sendError (live) or catch branch (dev)
    We added a basic html page that will send a 200 text/html to the client if something went wrong, so we can actually read the problem directly when we use a wrong hyperlink combination.
*   **connector_phd.js** :
    Added optional chunksize for the phd connection, since it can't handle 30 day resultsets.
*   **models && transformations.js** :
    removed the exceptions arry from the models. We will just use our standard avg function updated to also check proeprties instead of trying to reuse the PHD AVG function, since this complicates our code alot.
    If we want to use the PHD AVG and other statistical functions, we have to chunk the URI into the correct grouped pieces, which would imply we need to use our transformations BEFORE calling the data, compeltely reversing the current workflow.
    So it's probably better to use our own avg functions and keep the workflow simple.
*   ** config files && collections.json** :
    All collections should be arrays containing objects with a name property which is a primitive and a data property which is an array of  the actual collections contents, written in the same way.
    Differentiate between lists and collections! lists => array of primitives, collection => array of objects containing primitives, lists, or collections.
*   **component_x.js** :
    We could probably refactor the whole render() chunk that selects the root node or throws an error into its own class than we can have all other classes and components inherit from.
    Evaluate this later if such a class taxonometry will help us. If so, decide between using a base class or composition.
*   **server.js && rest.js** :
    While handling request bodies (POST, PUT ) wer need to use a Buffer for real files, and and array for JSON strings.
    So we moved the concatting to the actual connector.
    DEV requests need to use the 'body' uri parameter to send their payload.
*   **connector_x && transformation_x** :
    All connectors and transformations should return this format: { "data" : any, "headers" : {}, "http_status_code" : integer || null }
    TODO: update all the error codes as well so we can change the server.js' sendError functions to match the sendResponse.
*   **aggregation_sum**:
    TODO: fix the summing, sicne we used the 'data' proeprty again.
*   **component_x.js** :
    If we want to be able to use a component throught the component system of our templates, we need to split render() into a html and bindEvents method.
*   **component_x.js** :
    Components that save event handlers, need an event handler tat is a normal function instead of an arrow function, since the this context is important.
*   **component_dropdown_linked** :
    Check the viability of this component inheriting from component_dropdown.
*   **component_x.js** :
    Do we want to force the use of the templates class to create the HTML or do swe stick to the current implementation of lettign each component decide that itsself?
*   **component_x.js** :
    We shoudl remvoe the er_ prefix of the this.root string . This serves no purpose anymore now that our ids are sufficiently unique.
*   **x.config.json** :
    We should make an internationalization file so we can keep using the default english names and cast them later. Atm we have to decide between using english names as labels or adding another field for the l12n values.
*   **x.config.json** :
    All config files should follow the format used in the idd.config.json file.
*   **multiplexer.js** :
    chunkTimes fails for non timebound parameters. So we need a different mechanism, propably using 'TOP X' to chunk the resultSet if needed. FIgure this out later, sicne atm we don't really have databses that are 1) slow and 2) not timebound .
*   **component_x.js** :
    ALl components need render() and update() for the api engine. All components need x() for the view template engine to extract the html. This method will be registered upon creation of the template and will take in: ( data, operand, relation, expectation )
*   **connector_x.js** :
    Watch out with paths! All require()'d dependencies will be relative to the connector file. But all paths used during runtime will refer to the root of the app due to where server.js is located.
*   **component_x.js** :
    TODO: We need to standardize all our components and the reports we use them in:
        arrow_links         => NOK
        downtimebar         => NOK
        dropdown            => OK
        dropdown_linked     => NOK
        gauge               => NOK
        linechart           => NOK
        marquee             => NOK
        percentagebar       => NOK
*   **x.html** :
    CSS declaration order: GRID ER COMONENTS REPORT => elast important to most important.
    Worst case our report css can now overrule global styles or component styles.
*   **readme.md** :
    Move all the REST examples into the admin project as admin_example_x
*   **component_x** :
    Investigate if it's desirable to alsoa dd styling options for the labels. Or if we'll keep this inside the report-specific css.
*   **rest.js.resourceRead()** :
    SInce we throw new errors when detecting problems, we don't actually send back the first errors message. So when there's a typo in the model file, we get a 'failed finding connector' instead of 'typo in json?'.
*   **conponent_x** :
    We should rethink our id naming scheme. Atm, we have to use multiple selectors in our defaults to capture things like the dropdown_linked, which has a deviant id comapres to the others
    _application_name_property vs _appilcation_property
*   **component_dropdown_linked** :
    We need to provide a defaults method. Else we're stuck with replicating the population logic of the dropdowns template.
    We can't just set the values of all the selects, since they won't contain the correct options without triggering the change event for each dropdown in the chain.
*   **easyreporting_app_idd** :
    Once we change the defects explorer and STO reprots to our new structure, we can greatly simplify the format_defaults() method, sicne the defects wrappers will be the same.
    We can even just reference the correct defect with a link then, if needed.
*   **component_menu_list** :
    Why did we need the attribute parameter to begin with? (data-link)
*   **console.assert()** :
    Write the specification, test cases, preconditions, postconditions and assertions for a method before you write the body of the method!
*	**css icons** :
	All icons should have: 16x16, 32x32 and 128x128 pixel versions.
	Prefix JS hooks with js-*
	Don't use data- attributes as JS hooks.
	BEM: Blocks, Elements, Modifiers => .component-description => .component-description__element 	=> .component-description--modifier
									 => .stick-man			   => .stick-man__arms, .stick-man_legs => .stick-man--blue, .stick-man--red => .stick-man__legs--green
	<style>
	.form { }
	.form--theme-xmas { }
	.form--simple { }
	.form__input { }
	.form__submit { }
	.form__submit--disabled { }
	</style>
	<form class="form form--theme-xmas form--simple">
	  <input class="form__input" type="text" />
	  <input
		class="form__submit form__submit--disabled"
		type="submit" />
	</form>
*   **all.config.json** :
    formats: 'list' => array, 'hash' => object, 'collection' => object with "name", "format", "type", "index", "data". Each config file ahs an 'index' object as its first collection item.
*  **templates.html + foreach loop + component** :
    When we use a component inside a template loop that sin't related to the component, the html() method willd etect both a this.data state and a data parameters beign swent bythe tempalte engine.
    So we have to check somewhere if we actually ahve to use the scope of the loop, or if we can just use the parameter data directly.
    The problem being that the parameter data injected by the template loop will be an object instead of an array.
    ATM we just check for type and this.data existance as shown in the component_dropdown.js html() method, but we can ivnestigate if we can detect this inside the template engine.
    Update: We NEED to fix this inside the tempalte engine somehow, since the loop prevents us from reusing the same id for each component. So then we ened to filter the data sent by the tempalte, which implies that our components need specific knowledge about the implementation details of the tempalte engine, which violates seperation of concerns.
*   **components** :
    Some components use "data" to report their state, others use "state". Unify this into one system so we can read one of the two. Remove the other.
*   **idd** :
    1) Language is statefull atm, we ahve thwe string 'dutch' everywhere. Move this to the config file and reprot state ASAP.
    2) Fix the durations dropdown. It's not a component yet.
*   **util.translate** :
    Add the case where the word is not defined in the language.
    Might want to mvoe the whole logic into a i18n module asap. Update the IDD reprot accordingly.
*   **component_x** :
    Differentiatye better between update and updateState so the API become identical again.
*   **report IDD** :
    Clicking any button that makes ajax calls lags the inetrface animations due to the promsie chain attached to it.
*	**component_x && application.js**
	We have to add lifecycle management, so we can remove all the boilerplate that binds / unbinds handlers and such. oninit, onbind, onrender, onunbind
*	**places.config.json**
	We need to go to a system of 8 places so we can include plant and department.
	FLOC level 6 is not used anywhere! level 5 === level 6 in all cases so we can keep our naming scheme
*	**component_menu_list**
	updateLinks might not be the best way to do this, but rendering isn't either.
    
	
## Overview

* The Easy Reporting (ER) REST service unifies all data used in the ER project into a single endpoint that can be consumed by other apps.
* This allows us to build standardized reports without having to deal with data modelling on the front end.
* All generated URLs can be consumed by the 'from web' option in excel data services to quickly import any ER data to an excel sheet.
* The first report built in this new architecture is the data explorer, which basically is an URL generator.
* End users can upload excel files compatible with the structure to automagically plug them into the REST system and make them available for consumation. The service will save the file, create a .model file and link it to the saved resource.


## Module Dependencies

* connector_cockpit => tedious, util.js, util_node.js
* connector_easyreporting => tedious, util.js, util_node.js
* connector_excel => fs, smb2, xlsx
* connector_proficy => tedious, util.js, util_node.js
* connector_psion => mysql, util.js, util_node.js
* rest.js => connector_cockpit, connector_easyreporting, connector_excel, connector_proficy, connector_excel, fs, util.js, util_node.js, transformations.js
* server.js => http, rest.js, templates.js, transformations.js, util.js, zlib
* templates.js => util.js
* transformations.js => no dependencies
* util.js => no dependencies
* uti_node.js => fs


## Server Configuration

* IIS NODE configuration:
    - Runs on IIS node due to the web.config handler "<handlers><add name="iisnode" /></handlers>", which redirects any requests to http://mecvm21/api/ to the server.js module, through the iisnode module installed on IIS.
    - URL rewriting is also configured inside the web.config file. This is propagated into the URL rewrite module of IIS itsself. This excludes the iisnode and resources URLs.
    - http://mecvm21/api/iisnode is the log files root. By excluding it with the URL rewrite, we can still access this address to view the log files.
    - http://mecvm21/api/resources is the folder to save any files that get consumed by the servcie ittself. Template files, user uploads, etc.
      At the moment there's no security on this folder. In production this URL should be made unavailable for anything not the service or IT personnel logged into the server.
    - server.js has both the issnode handler and a URL rewrite. The URL rewrite allows any URLs that don't matchs the xceptions to be propagated further and the handler will redirect the traffic to server.js
    - Caching is explicitly enabled.

    
## CLI

- node server.js dev METHOD resourceName?parameters
- Installed postman to test POST requests.

## Workflow

*  1) Request is made by the client. The URI contains all the parameters to parse into a full parameters object. Any POST data is contained inside the body.
*  2) server.js slices the *resourceName* out of the URI.
*  3) server.js checks the HTTP verb to determine the _method_ ( GET, PUT, POST, DELETE ) 
*  4) server.js parses the *parameters* using *parseParameters()* into an object.
*  5) server.js calls the *method* named after the HTTP verb in rest.js, ( REST.get(), REST.post(), REST.put(), REST.delete() ) passing the resourceName and the parameters ( GET, PUT DELETE ) or the body ( POST ).
*  6) rest.js fetches the **_.model_** file ( json ) for the resourceName and extracts the name of the associated **_connector_**.
*  7) rest.js calls the *fetchTemplate()* method of the connector to get a template string. This can either be SQL, AD commands, a path, etc. depenending on the connector.
      If the *extending* property inside the .model file is null, then the **_template_** file of the resourceName will be fetched.
      Else the template of the resource defined in 'extending' will be fetched.
      This allows us to share templates between multiple model files and hence, define multiple model for the same resource.
*  8) If the .model file contains any **_dependencies_**, all these dependencies are fetched in advance using the same parameters as the original request. ( recursion of REST.get() )
*  9) If the .model file contains any **_filters_**, these filters are added to the *parameters* object under the key 'filters'. ( This makes filters a reserved keyword )
* 10) rest.js calls the *populateTemplate()* method of the connector, passing the *parameters* ( updated with any extra filters ), any fetched *dependencies* and the *template* string itsself.
* 11) rest.js splits the timeframe determined ( starttime and endtime difference ) inside the parameters object into a chunk for each month, using the multiplexer.js' *chunkTimes()* method.
* 12) connector_x.js populates the template for each chunk with the values contained in the parameters or the dependencies, as written inside the template, and returns the full code for the query.
* 13) rest.js calls the *read()* method of the connector for each chunk to produce a resultset from the query. This can be a database call, an api call, etc. depending on the connector.
* 14) rest.js combined the resultsets again into one array using the *merge()* method of multiplexer.js.
* 15) rest.js calls the *transform()* method of transformations.js passing the resultset, parameters, queryModel and dependencies to transform the resultset.
* 16) transformations.js applies the following transformations in order:
        **_transformation_** as described inside the .model file.
        *sort*, either from the parameters or by using the default sort_line_date.js
        *group* from the parameters to group the resultset into distinct pieces.
        *aggregation* from the parameters to reduce the groups or full resultset into one value.
        *output*, either from the parameters or by using the default table_html to create the requested output format.
* 17) server.js sends a response. Either the resultset returned from the rest.js script or an error message.


## Parameters


line                line1, line2, line3, line4, line5, line6, all
starttime           YYYY-MM-DDThh:mm:ss.000Z : ( mutually exclusive with timeframe )
startttime_shift    Integer : automatically calculated from starttime if not present
endtime             YYYY-MM-DDThh:mm:ss.000Z : ( mutually exclusive with timeframe )
endtime_shift       Integer : automatically calculated from endtime if not present
timeframe           shift, today, week, month, last_month, period : ( mutually exclusive with starttime && endtime )
sort                sort_line_date ( default )
group               shift, day, week, month, year
aggregation         avg, count, sum : ( avg always needs a group, sum always needs a property )
output              html, json, table ( default ), text
headers             true, false : ( used in combination with a non-json output parameter )
property            String : ( used in combination with sum and avg to determine the property to sum or avg on the resource entity )


## Kellogg Periods

- Always starts on a sunday 06:00:00 AM
- Is probably local time, but haven't confirmed this yet.
- The first period of 2018 started at 31 december 2017 06:00:00 , so this can be our reference point.
- The following values were confirmed by David and Peter to be the correct 2018 periods:

*        2018-P01    2017-12-31T06:00:00Z        
*        2018-P02    2018-01-28T06:00:00Z
*        2018-P03    2018-02-25T06:00:00Z
*        2018-P04    2018-04-01T06:00:00Z
*        2018-P05    2018-04-29T06:00:00Z
*        2018-P06    2018-05-27T06:00:00Z
*        2018-P07    2018-07-01T06:00:00Z
*        2018-P08    2018-07-29T06:00:00Z
*        2018-P09    2018-08-26T06:00:00Z
*        2018-P10    2018-09-30T06:00:00Z
*        2018-P11    2018-10-28T06:00:00Z
*        2018-P12    2018-11-25T06:00:00Z
*        2019-P01    2018-12-30T06:00:00Z


## Person Descriptors

creator     => person who ENTERS            ( create defect, incident, action item. enter checklist value. open ticket. )
approver    => person who APPROVES          ( workflows, change requests, ... )
editor      => person who EDITS             ( change existing defect, incident, action item, checklist value, ticket, ... )
assignee    => person who is ASSIGNED to    ( assigned to solve defect, followup on item, ... )
actor       => person who ACTED on          ( actually sovled defect, followed up, ... )
reviewer    => person who REPORTED end      ( closes defect, removes action item, ... )

## REST Structure

	GET			=> READ
	POST		=> CREATE
	PUT			=> UPDATE || CREATE
	DELETE		=> DELETE
	
	GET 	/plants										=> full list of all plants
	GET 	/plants/1									=> details of the plant with id 1
	DELETE 	/plants/1									=> delete plant 1
	GET		/plants/1/defects							=> list of all defects in plant 1
	GET		/plants/1/defects/12345						=> details of defect 12345 in plant 1
	DELETE	/plants/1/defects/12345						=> delete defect 12345 in plant 1
	POST	/plants										=> create a new plant and return its details
	
	GET		/plants?sort=rank_asc						=> Sorting: full list of all plants, sorted by rank, ascending
	GET		/plants?category=pringles&location=europe	=> Filtering: full list of all plants that produce pringles in europe
	GET		/plants?search=mechelen						=> Searching: details of the plant with the name 'mechelen'
	GET		/plants/1/defects?page=42					=> Pagination: smaller list of the 42th chunk of the defects list in plant 1. Eg if we have 1000 defects, with 10 defects per 'page', page=42 returns all defects between 421 and 430
	
	API versioning on breaking changes:					=> GET http://mecvm21/api/v1/plants	=> GET http://mecvm21/api/v2/plants
	
	const HTTP_RESPONSE_CODES = [
		{ "code": 200, "text": "OK",					"description": "GET success. PUT success, if resource does exist." },
		{ "code": 201, "text": "CREATED",				"description": "POST success. PUT success, if resource does not exist." },
		{ "code": 204, "text": "NO CONTENT",			"description": "DELETE success, without response content." },
		{ "code": 304, "text": "NOT MODIFIED",			"description": "ANY success. Response is already cached." },
		{ "code": 400, "text": "BAD REQUEST",			"description": "ANY failure. Request is invalid. Request URL cannot be parsed." },
		{ "code": 401, "text": "UNAUTHORIZED",			"description": "ANY failure. Client does not have correct authentication." },
		{ "code": 403, "text": "FORBIDDEN",				"description": "ANY failure. Client has correct authentication, but resource is not accesible." },
		{ "code": 410, "text": "GONE",					"description": "ANY failure. Resource was intentionally moved." },
		{ "code": 500, "text": "INTERNAL SERVER ERROR",	"description": "ANY failure. Valid request, but server cannot generate a correct response." },
		{ "code": 503, "text": "SERVICE UNAVAILABLE",	"description": "ANY failure. Service is down." }
	];
	// Web server running this service should return 503 SERVICE UNAVAILABLE if service cannot be started or crashes.
	// Intermediate devices should return 304 NOT MODIFIED if the requested resource is cached somewhere between the service and the client.
	// import ActiveDirectory from './connectors';
	// import FileSystem;
	const DATA_MODELS = [];
	class REST_service extends Module {
		constructor( name ) {
			super( name );
		}
		parseURI( uri ) {
			return {
				"resource": "string", // Canonical name of the requested resource. Eg defects.
				"path": "string", // Path to the resource model. Eg plant/mechelen/defects/1345/endtime = Endtime of defect 1345 in the Mechelen plant.
				"parameters": [ // Additional options to adjust the returned collection. Eg filters, sorts, outputs.
					{ "name": "string", "value": "any" },
					{ "name": "string", "value": "any" }
				]
			};
		}
		request( request ) {
			const valid_request = this.parseURI( request.url );
			const valid_authentication = ActiveDirectory.authenticate( process.enviroment.AUTH_USER );
			if ( !valid_authentication ) return 401; // UNAUTHORIZED
			else {
				if ( !valid_request ) return 400; // BAD REQUEST
				else {
					const model = DATA_MODELS.find( valid_request.path );
					if ( !model ) return 500; // INTERNAL SERVER ERROR
					else {
						if ( !model.is_public ) return 403; // FORBIDDEN
						else {
							const resource = FileSystem.get( model );
							if ( !resource ) {
								if ( valid_request.method === 'GET' && model.history.length > 1 ) return 410; // GONE
								else if ( valid_request.method === 'POST' || valid_request.method === 'PUT' ) {
									FileSystem.create( model.name, valid_request.body );
									return 201;
								}
							}
							else {
								if ( valid_request.method === 'DELETE' ) return 204;
								else return 200;
							}
						}
					}
				}
			}
		}
	}

## Reserved keywords inside query parameters object, template code, so we can't use them as variables when naming our fields.

* filters
* dependencies
* resources


## Template - SQL file

- WHERE clause end with ${ resources.filters } if the query needs to be inheritable.
- Line selection should use IN-clause or another mechanism to select mutiple lines.
- When a query inheritance chain exists of multiple resoruces combined, the main resource will have the most logical name.
  Any subqueries that need seperate model and SQL files, but aren't used directly, only inherited from, start with the prefix partial_
- SQL query for MySQL CANNOT wrap fieldnames and tablenames inside [].
- If you use a parameter inside a query, make sure it always produces valid SQL.
  For example, if you map the ids out of a dependency to be used inside an SQL IN() clause, we need to make sure that we return 'null' if the array is empty.
  [field] IN () => error, [field] IN ( null ) => empty resultset

## Template - connector file

```javascript
/*  connector_${ name }.mjs   => ${ description }
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          ${ user }
*   @date:          ${ entry_date || update_date }
*/
//// DEPENDENCIES ////
import ${ dependecy } from '${ path_dependency }';
const ${ dependecy } = require( '${ path_dependency }' );
//// SETTINGS     ////
/*
*/
const ${ name.toUpperCase() }_NORMALIZED = {
    'username' : ${ credentials_user },
    'password' : ${ credentials_password },
    'server' :  ${ server },
    'options' : {}
};
/*
*/
const ${ name.toUpperCase() } = {};
/*
*/
const transformations = {};
/*
*/
//// HELPERS      ////
//// API          ////
const create = ( model, resource ) => promise( array( resultSet ) );
const parse = ( resources, dependencies, sqlTemplate ) => string( template_populated );
const read = sql => promise( array( resultSet ) );
const template = resourceName => promise( string( template_raw ) );
//// EXPORT       ////
module.exports = { create, parse, read, 'server' : ${ name.toUpperCase() }_NORMALIZED, template };
```


## Template - .model file

- dependencies: Array of strings
        All other resources this resource requires to create its query or apply its transformation.
        Eg, we might need a full list of proficy id numbers to use as a filter on the teamleader logbook before we query easyreporting.
- extending: String or null
        A query template ( .sql ) from a different resource that has to be used.
        There won't be a seperate SQL file for this resource, but will reuse the SQL syntax of the model we're extending.
- filters: Array of strings
        Static conditions that have to be added to the WHERE clause of the .sql template.
        The template has to include a ${ resources.filters } variable somewhere to use this.
        This will mostly be used in combination with 'extending', but isn't tightly coupled to it.
        We might change this in the future if we confirm that only 'extending' models will use the 'filters' array.
        Eg:
        Use of filters when extending: breakdowns = production_bindings + filter "[Breakdown] = 1".
        Absence of filters when extending: unloading_trucks_citrix = unloading_trucks + transformation "unloading_trucks_citrix.js".
- name: String
        Required.
        Name of the resource.
- notes: Array of strings
        Information about the model.
        Since we can't include comments in JSON, we add a seperate array to include comments.
        No information inside this array is used in any way in the script.
- output: Array of strings
            html:
            json:
            table:
            table_groups:
            table_merged:
            text:
- parameters: Array of strings
        List of all the aprameters used inside the query template.
- properties: Array of objects
        List of all the properties the resource will contain.
        Unused at the moment, but in the future we'll want to specify all the properties so we can attach parsing rules to common property types.
        Eg:
        We might want to add a line number transformation for certain databases where we can't get the correct line number inside the SQL.
- servers: Array of strings
        Required.
        List of all the servers the resource can be found on. This determines which connector will be used to generate the resource.
        At the moment, only the first server in the list is used.
        But we already use an array to make it easier to update the rest.js script to support multiple servers.
- tables: Array of strings
        All tables the sql query constructed from the template uses.
- transformation: String || Object
        Name of the transformation script.
        Each resource can have One transformation associated with it.
        By creating multiple models etending a base model, we can create multiple versions of the same base resource, each with its own filters and transformation.

```javascript
{
    "dependencies" : [],
    "extending" : null,
    "filters" : [],
    "name" : "",
    "notes" : [],
    "output" : [],
    "parameters" : [],
    "properties" : [
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     },
        { "field" : "",             "type" : ""     }
    ],
    "servers" : [],
    "tables" : [],
    "transformation" : null,
    "transformation_exceptions" : []
}
```


## Template - module file

```javascript
/*  ${ name }.mjs   => ${ description }
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    ${ entry_user }
*   @update_date:   ${ entry_date || update_date }
*/
////////////////////
import ${ dependency } from '${ path_dependency }';
const ${ dependency } = require( '${ path_dependency }' );
////////////////////
/*  ${ name }   => ${ description }
*   @type           public || private ${ type }
*   @name           ${ name }
*   @param          ${ type } ${ name }                 - ${ description }
*   @return         ${ type }
*   @notes
*                   - ${ note_1 }
*                   - ${ note_2 }
*/
const Y = ...;
////////////////////
export Z;
module.exports = Z;
```

#   REPORTS


##  CF Sign

- { "output" : "json", "line" : "line1", "starttime" : "2018-01-28 06:15:00", "endtime" : "2018-01-29 06:14:59" }

- Downtime: Colored time tracking bar.
        => output, line, starttime, endtime
        /downtime
        => line, department, entry_date, entry_shift, entry_user, entry_team, text, data_duration, data_type, data_end, data_area, data_location, data_failure, data_cause, data_reason, data_comments
- Production: Cans produced graph.
        => output, line, starttime, endtime
        /production_cans?output=json
        => line, department, entry_date, entry_user, entry_team, text, data_cans_accepted, data_cans_weight, data_cans_overpack, data_cans_rejects, data_cans_target
- Can Rework: Cans reworked graph.
        TODO !!!
- Overpack: gauge
        => from Production?
- Netweight Rejects: gauge
        => from Production?
- Stops CF: gauge
- Scrap: gauge
- Line Speed: gauge
- Shingling: gauge
- Daily Message: marquee
