/*  easyreporting_app_signs.mjs
*   @data_language: ES6
*   @data_version:  1.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-22
*/

// Citrix stations: Demand Planning Request 864

// BUG: guage with min other than 0 renders a full circle red bar
// BUG: downtimebar between 14:15 and 15:15
// BUG: downtiembar between 14:15 - 15:15 also still shows a downtime event that has a number in it instead of just the color. => The width isn't calculated correctly . See pic bug_dtbar
// TODO: rewrite the production_cans / overpack / rejects transformation to use our group and aggregation mechanism
// TODO: replace the scrap and downtime logic ocne we have a sum( property ) transformation
// BUG: if a shift is not available, the wrong data gets written or we get errors. Eg, casepacker L1 was down, so we didn't get correct shiftly data since we didnt extrapolate the data yet for shifts without raw records.

////////////////////
const Application = require( '../../modules/class_application.js' );
const CONSTANTS = require( '../../modules/constants.js' );
const Downtimebar = require( '../../modules/component_downtimebar.js' );
const { fetch, root } = require( '../../modules/fetch.js' );
const Gauge = require( '../../modules/component_gauge.js' );
const Linechart = require( '../../modules/component_linechart.js' );
const Marquee = require( '../../modules/component_marquee.js' );
const Percentagebar = require( '../../modules/component_percentagebar.js' );
const Templates = require( '../../modules/class_template.js' );
const UTIL = require( '../../modules/util.js' ); 
////////////////////
/*  app
*/
const app = new Application( 'ER', [ Templates ] );
/*  app_resources_viewmodels

- Downtime: Colored time tracking bar.
        => output, line, starttime, endtime
        /downtime
        => line, department, entry_date, entry_shift, entry_user, entry_team, text, data_duration, data_type, data_end, data_area, data_location, data_failure, data_cause, data_reason, data_comments
- Production: Cans produced graph.
        => output, line, starttime, endtime
        /production_cans?output=json
        => line, department, entry_date, entry_user, entry_team, text, data_cans_accepted, data_cans_weight, data_cans_overpack, data_cans_rejects, data_cans_target
- Can Rework: Cans reworked graph.
        TODO !!!
- Overpack: gauge
        => from Production?
- Netweight Rejects: gauge
        => from Production?
- Stops CF: gauge
- Scrap: gauge
- Line Speed: gauge
- Shingling: gauge
- Daily Message: marquee

*/
const app_resources_viewmodels = [
    { // downtime
        'name' : 'downtime',
        'parameters' : '&group=shift',
        'query' : true,
        'settings' : {
            'label' : 'Downtime'
        },
        'type' : 'downtimebar'
    },
    { // linespeed previous shift 1
        'name' : 'linespeed_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // linespeed previous shift 2
        'name' : 'linespeed_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // linespeed current shift
        'name' : 'linespeed',
        'query' : true,
        'settings' : {
            'label' : 'Linespeed',
            'max' : 540,
            'min' : 520
        },
        'type' : 'gauge'
    },
    { // machine_stops_cf previous shift 1
        'name' : 'machine_stops_cf_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // machine_stops_cf previous shift 2
        'name' : 'machine_stops_cf_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // machine_stops_cf current shift
        'name' : 'machine_stops_cf',
        'parameters' : '&group=shift&aggregation=count',
        'query' : true,
        'settings' : {
            'label' : 'Stops',
            'max' : 10
        },
        'type' : 'gauge'
    },
    { // overpack previous shift 1
        'name' : 'overpack_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // overpack previous shift 2
        'name' : 'overpack_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // overpack current shift
        'name' : 'overpack',
        'query' : true,
        'settings' : {
            'label' : 'Overpack',
            'max' : 2.2,
            'precision' : 2
        },
        'type' : 'gauge'
    },
    { // production_cans previous shift 1
        'name' : 'production_cans_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // production_cans previous shift 2
        'name' : 'production_cans_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // production_cans current shift
        'name' : 'production_cans',
        'query' : true,
        'settings' : {
            'label' : 'Production'
        },
        'type' : 'linechart'
    },
    { // rejects previous shift 1
        'name' : 'rejects_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // rejects previous shift 2
        'name' : 'rejects_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // rejects current shift
        'name' : 'rejects',
        'query' : true,
        'settings' : {
            'label' : 'Rejects',
            'max' : 2.2,
            'precision' : 2
        },
        'type' : 'gauge'
    },
    { // rework_cans previous shift 1
        'name' : 'rework_cans_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // rework_cans previous shift 2
        'name' : 'rework_cans_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // rework_cans current shift
        'name' : 'rework_cans',
        'query' : true,
        'settings' : {
            'label' : 'Can rework'
        },
        'type' : 'linechart'
    },
    { // scrap previous shift 1
        'name' : 'scrap_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // scrap previous shift 2
        'name' : 'scrap_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // scrap current shift
        'name' : 'scrap',
        'parameters' : '&group=shift&aggregation=sum&property=duration',
        'query' : true,
        'settings' : {
            'label' : 'Scrap',
            'max' : 10,
            'precision' : 2
        },
        'type' : 'gauge'
    },
    { // shingling previous shift 1
        'name' : 'shingling_p1',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // shingling previous shift 2
        'name' : 'shingling_p2',
        'query' : false,
        'settings' : {},
        'type' : 'percentagebar'
    },
    { // shingling current shift
        'name' : 'shingling',
        'parameters' : '&group=shift&aggregation=count',
        'query' : true,
        'settings' : {
            'label' : 'Shingling',
            'max' : 100
        },
        'type' : 'gauge'
    },
    { // signs_cf_banner
        'name' : 'signs_cf_banner',
        'query' : true,
        'settings' : {
            'label' : null
        },
        'type' : 'marquee'
    }
];
/*  createComponents
*/
const createComponents = models => {
    const classes = {
        'downtimebar' : model => new Downtimebar( model.name ),
        'gauge' : model => new Gauge( { 'name' : model.name }, Object.assign( {}, model.settings ) ), // We use object.assign ehre so we can put the fixed values for all gauges into this object so we don't have to update these inside the model.
        'linechart' : model => new Linechart( {
            'height' : 302, /* 200px + 4px + 98px */
            'name' : model.name,
            'width' : 920
        }, {
            'axis_x' : {
                'label' : 'shift',
                'property' : 'starttime',
                'scale' : 'scaleTime'
            },
            'axis_y' : {
                'label' : model.settings.label,
                'property' : 'data',
                'scale' : 'scaleLinear'
            }
        } ),
        'marquee' : model => new Marquee( model.name ),
        'percentagebar' : model => new Percentagebar( model.name )
    };
    return () => models
        .map( model => classes[ model.type ]( model ) )
        .map( component => component.render() );
};
/*  createRequests
*   notes:
*                   - Since we haven't split overpack / rejects and production_cans yet, this is all the same query, so we can filter those models away.
*/
const createRequests = models => ( { endtime, line, starttime } = parameters ) => {
    const requests = models
        .filter( model => model.query )
        .map( model => `${ root }/${ model.name }?output=json&line=${ line }&starttime=${ starttime.toJSON() }&endtime=${ endtime.toJSON() }${ model.parameters || '' }` )
        .map( url => fetch( url ) );
    return Promise.all( requests );
};
/*  createParameters
*/
const createParameters = line => Promise.resolve( {
    'endtime' : new Date( UTIL.shiftCurrent().getTime() + CONSTANTS.MILISECONDS_SHIFT - 1000 ),
    line,
    'starttime' : new Date( UTIL.shiftCurrent().getTime() - ( CONSTANTS.MILISECONDS_SHIFT * 2 ) )
} );
/*  handleInitializations
*/
const handleInitializations = () => initializations => UTIL.NOOP( initializations );
/*  handleResponses
*/
const handleResponses = cache => resources_raw => {
    const { initializations, components_raw, parameters } = cache;
    const components = components_raw.reduce( UTIL.propHash( 'name', true ), {} );
    // Technically we already did this inside the resource fetching. We could add it to the cache before and map from there. Or we could as shown, just quickly filter out the fetched models again.
    const fetched_models = app_resources_viewmodels.filter( model => model.query );
    const resources = resources_raw.reduce( UTIL.propHash( null, true, ( entry, index, source ) => fetched_models[ index ].name ), {} );
    const shifts = UTIL
        .dateSeries( parameters.starttime, parameters.endtime, CONSTANTS.MILISECONDS_SHIFT )
        .map( UTIL.dateShift )
        .map( shift => parseInt( shift, 10 ) );
    const current_shift = shifts[ 2 ];
    //
    const banner_message = `${ UTIL.capitalize( CONSTANTS.L10N_DUTCH.line ) } ${ parameters.line.charAt( 4 ) }: ${ resources.signs_cf_banner[ 0 ].text || ''}`;
    const downtime = resources.downtime.find( group => group.shift === current_shift );
    //const machine_stops_cf = resources.machine_stops_cf.find( group => group.shift === current_shift );
    const overpack = resources.overpack.find( group => group.shift === current_shift );
    const rejects = resources.rejects.find( group => group.shift === current_shift );
    const scrap = resources.scrap.find( group => group.shift === current_shift );
    const shingling = resources.shingling.find( group => group.shift === current_shift );
    // Save our resources for debugging .
    app.cache.set( 'resources' )( resources );
    // OK => SIGNS_CF_BANNER - MARQUEE
    components.signs_cf_banner.update( banner_message );
    // OK => DOWNTIME - DOWNTIMEBAR
    components.downtime.update( downtime ? downtime.data : [] );
    // TODO => PRODUCTION_CANS - LINECHART
    //const parseTime = er_linechart_1.d3.utcParse( '%Y-%m-%dT%H-%M-%S.%LZ' );
    const parseTime = components.production_cans.d3.timeParse( '%d-%b-%y' );
    const linechart_data = [
            { 'date' : '01-Jan-18', 'data' : 1 },
            { 'date' : '02-Jan-18', 'data' : 15 },
            { 'date' : '02-Jan-18', 'data' : 23 },
            { 'date' : '03-Jan-18', 'data' : 9 },
            { 'date' : '03-Jan-18', 'data' : 7 },
            { 'date' : '03-Jan-18', 'data' : 3 },
            { 'date' : '04-Jan-18', 'data' : 21 },
            { 'date' : '05-Jan-18', 'data' : 23 },
            { 'date' : '05-Jan-18', 'data' : 23 },
            { 'date' : '06-Jan-18', 'data' : 18 },
        ].map( entry => Object.assign( entry, { 'starttime' : parseTime( entry.date ) } ) );
    components.production_cans.update( linechart_data );
    // TODO => REWORK_CANS - LINECHART
    components.rework_cans.update( linechart_data );
    // TODO => LINESPEED - GAUGE
    // TODO => LINESPEED percbars
    //components.linespeed.update( UTIL.intRandom( components.linespeed.settings.min, components.linespeed.settings.max ) );
    // OK => MACHINE_STOPS_CF - GAUGE => has total
    components.machine_stops_cf_p1.update( resources.machine_stops_cf[ 1 ].count, 0, 10, 10 );
    components.machine_stops_cf_p1.update( resources.machine_stops_cf[ 2 ].count, 0, 10, 10 );
    components.machine_stops_cf.update( resources.machine_stops_cf[ 3 ].count );
    // OK => OVERPACK - GAUGE => no total
   // components.overpack_p1.update( resources.overpack[ 0 ].avg_overpack_weighted, 0.00, 2.20, 2.00 );
   // components.overpack_p2.update( resources.overpack[ 1 ].avg_overpack_weighted, 0.00, 2.20, 2.00 );
   // components.overpack.update( resources.overpack[ 2 ].avg_overpack_weighted );
    // OK => REJECTS - GAUGE
  //  components.rejects_p1.update( resources.rejects[ 0 ].avg_rejects_weighted, 0.00, 2.20, 2.00 );
   // components.rejects_p2.update( resources.rejects[ 1 ].avg_rejects_weighted, 0.00, 2.20, 2.00 );
  //  components.rejects.update( resources.rejects[ 2 ].avg_rejects_weighted );
    // OK => SCRAP - GAUGE => has total
    components.scrap_p1.update( resources.scrap[ 1 ].sum, 0.00, 10.00, 10.00 );
    components.scrap_p2.update( resources.scrap[ 2 ].sum, 0.00, 10.00, 10.00 );
    components.scrap.update( resources.scrap[ 3 ].sum );
    // OK => SHINGLING - GAUGE => has total
    components.shingling_p1.update( resources.shingling[ 1 ].count, 0, 100, 100 );
    components.shingling_p2.update( resources.shingling[ 2 ].count, 0, 100, 100 );
    components.shingling.update( resources.shingling[ 3 ].count );
};
/*  workflow
*   notes:
*               - The template engine has to be initialized before any other functionality is triggered, so we include it as an application component .
*               - The init() callback needs to use a function key word instead of an arrow, since we'll use .call( app ) on it .
*                 Maybe there is pretttier syntax as well: app.router.register.bind( null, ':/line', lineChange )
*/
// Reusable update
const update = ( { line } = {} ) => createParameters( line )
    .then( app.cache.set( 'parameters' ) )
    .then( createRequests( app_resources_viewmodels ) )
    .then( responses => responses.map( JSON.parse ) )
    .catch( error => { console.log( 'failed parsing JSON file.' ); console.error( error ); } )
    .then( responses => responses.map( UTIL.updateTimeframe ) )
    .catch( error => { console.log( 'failed updating date objects.' ); console.error( error ); } )
    .then( handleResponses( app.cache.get() ) );
// Startup  
app
    .init( function() { this.router.register( '/:line', update ); } )
    .then( handleInitializations() )
    .then( app.cache.set( 'initializations' ) )
    .then( createComponents( app_resources_viewmodels ) )
    .then( app.cache.set( 'components_raw' ) )
    .then( app.router.checkInitial() )
    .catch( error => { console.log( error ); alert( error ); } );
////////////////////