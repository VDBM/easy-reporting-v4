# REQUIREMENTS

eventueel in 3 gesplits: menu, orimaire resultaten die in beled blijven, en 3e veld met de resulatten vd menu links.

Joeri wouters  (doughmaking vragen ) / kim stock ( technische vragen )


## Properties

Datum                   => starttime
Lijn                    => line
Ploeg                   => shiftgroup
Team                    => team
Naam                    => creator
Naam verv.              => ???successor???


## Resources

###Header Safety:
Safety incidents        => /api/events/incidents
BOS NOK                 => /api/checklists/bos
New safety defects      => /api/defects/new?group=type

###Header Quality:
Holds, maffus           => /api/events/quality
New quality defects     => /api/defects/new?group=type
5s plan volledig done   => /api/checklists/mss
Gemiste testen          => /api/tests/missed

###Header CIL / CL:
CIL completion          => /api/checklists/cilcl/percentages
CIL OOL                 => /api/checklists/cilcl/failures
CL completion           => /api/checklists/cilcl/percentages
CL OOL                  => /api/checklists/cilcl/failures

###Header Defect Handling:
Nieuwe defects          => /api/defects/new
Opgeloste defects       => /api/defects/solved

###Header Planning & Scheduling:
Breakdowns              => /api/logbooks/teamleaders/breakdowns
PM cards                => /api/logbooks/teamleaders/pms
# PM07 jobs done        => PM08, mag weg wordt opgevolgd door TSD
# R&F jobs done         => run & fix, mag weg, wordt opgebolgd door TSD

###Header Grootste machine stop CF, Overpack, Rejects
biggest stop CF         => /api/machine_stops/cf
overpack actie ool      => weg, in de tabel bij de andere hoofding
overpack average        => /api/production/netweights
nwr actie ool           => weg, in de tabel bij de andere hoofding
nwr average             => /api/production/netweights

###Header Downtimes / Scrap
downtimes               => /api/downtimes ??? /api/downtimes/packing
scrap                   => /api/scrap
acties ter bijsturing van het proces        => gewoon invulveld zoals id teamleader logboek.

###Header logsheet & Header L4 small size:
uurlijks gemiddelde     => /api//netweights
production
###Header 1x controleren:
greyzone                => proficy
average deviation       => proficy
trapdoors               => manuele controle ad lijn

###Header Bij begin shift:
brand, tarra, canlabel, C/W nr., Target Weight, Reject Low
=> /api/production/brandcodes
=> /api/production/pqm