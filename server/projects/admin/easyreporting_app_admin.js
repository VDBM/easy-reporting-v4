/*  easyreporting_app_admin.mjs
*   @language: ES6
*   @version:  1.0
*   @creator:    VDBM
*   @date:   2018-04-19
*/
////////////////////
// IMPORTS
import Application from '../../modules/class_application.js';
import Arrow_links from '../../modules/component_arrow_links.js';
import { fetch, root } from '../../modules/fetch.js';
import Picker_datetime from '../../modules/component_picker_datetime.js';
import templates from '../../modules/class_template.js';
////////////////////
////////////////////
// COMPONENTS
const arrow_list = new Arrow_links( 'arrow_links', 'color-bor-grey', [ 'color-bg-1', 'color-bg-2', 'color-bg-3', 'color-bg-4', 'color-bg-5' ] );
const picker_datetime_admin = new Picker_datetime( { "event": null, "name": "admin", "style": null } );
const transformations = {};
////////////////////
// APP
/*  app */
const app = new Application( 'ER', [ templates, picker_datetime_admin ], {} );
/**/
const createHandler = ( system_call, properties, method, suffix ) => {
    document.querySelector( `#button_${ system_call }` ).addEventListener( 'click', function( event ) {
        const data = Object.assign(
            {},
            ...properties.map( property => ( { [ property ] : readValue( `#${ system_call }_${ property }` ) } ) )
        );
        const body = transformations.hasOwnProperty( system_call )
            ? transformations[ system_call ]( data )
            : data;
        const query_parameters = suffix
            ? suffix( body )
            : '';
        const path = system_call === 'admin_read_resource'
            ? `${ root }/${ body.resource }${ query_parameters }`
            : `${ root }/${ system_call }${ query_parameters }`;
        const options = { 'body' : JSON.stringify( body ),  method };
        const request = method === 'GET'
            ? fetch( path )
            : fetch( path, options );
        request.then( renderData ).catch( error => renderData( `${ system_call } => ${ data.resource || 'unknown' } => ${ JSON.stringify( error || 'error' ) }` ) );
    } );
};
/**/
const readValue = selector => document.querySelector( selector ).value;
/**/
const renderData = data => {
    const content = Array.isArray( data )
        ? `<li>${ data.length } rows found</li>`.concat( data.map( entry => `<li>${ JSON.stringify( entry ) }</li>` ).join( '' ) )
        : `<li>${ data.length ? JSON.stringify( data ) : 'system call succeeded' }</li>`;
    document.querySelector( '#response_output' ).innerHTML = content;
};
/**/
const renderError = ( system_call, resource, error ) => renderData( `${ system_call } => ${ resource } => ${ JSON.stringify( error || 'error' ) }` );
////////////////////
// WORKFLOW
/**/
app
    .init()
    // create component
    .then( () => createHandler( 'admin_create_component', [ 'author', 'event_type', 'resource' ], 'POST', null ) )
    // create model
    .then( () => createHandler( 'admin_create_model', [ 'resource', 'server' ], 'POST', null ) )
    // create table
    .then( () => {
        createHandler( 'admin_create_table', [ 'resource', 'defaults' ], 'POST', null );
        transformations.admin_create_table = ( data ) => {
            data.defaults = ( data.defaults === 'true' );
            return data;
        };
    } )
    // read resource
    .then( () => createHandler( 'admin_read_resource', [ 'resource' ], 'GET', body => '?output=json&timeframe=today&line=line1' ) )
    // read model
    .then( () => createHandler( 'admin_read_model', [ 'resource' ], 'GET', body => `?output=json&resource=${ body.resource }` ) )
    // update model
    .then( () => {
        createHandler( 'admin_update_model', [ 'resource', 'property', 'value' ], 'PUT', null );
        transformations.admin_update_model = ( data ) => {
            if ( data.property == 'properties' ) {
                const value = data.value
                    .split( '|' )
                    .map( chunk => chunk.split( ',' ) )
                    .map( ( [ key, type, required ] ) => {
                        const definition = {'field' : key, type };
                        if ( required ) definition.required = ( required === 'true' );
                        return definition;
                    } );
                return Object.assign( data, { value } );
            }
            else return data;
        };
    } )
    // create picker datetime
    .then( () => {
        document.querySelector( '#hook_pdt_admin' ).innerHTML = app.picker_datetime_admin.html();
        app.picker_datetime_admin.bind();
    } )
	// Listen for the load event on the iframe so we can change the color to either green or red, depending on response status.
	.then( () => {
		document.querySelector( '#frame' ).addEventListener( 'load', event => {
			const node = document.querySelector( '#output' );
			node.classList.add( 'ready' );
			node.classList.remove( 'notready' );
		} );
	} )
	// Search button
	.then( () => {
		document.querySelector( '#button_search' ).addEventListener( 'click', event => {
			const group = document.querySelector( '#parameter_group' ).value;
			const resource = document.querySelector( '#parameter_resource' ).value;
			const aggregation = document.querySelector( '#parameter_aggregation' ).value;
			const starttime = document.querySelector( '#parameter_starttime' ).value;
			const endtime = document.querySelector( '#parameter_endtime' ).value;
			const line = document.querySelector( '#parameter_line' ).value;
			const timeframe = document.querySelector( '#parameter_timeframe' ).value;
			if ( resource && line && ( ( starttime && endtime ) || timeframe ) ) {
				let url = `${ root }/${ resource }?line=${ line }`;
				if ( timeframe ) url += `&timeframe=${ timeframe }`;
				else url += `&starttime=${ starttime }&endtime=${ endtime }`;
				if ( group ) url += `&group=${ group }`;
				if( aggregation ) url += `&aggregation=${ aggregation }`;
				const output = document.querySelector( '#output' );
				output.innerHTML = url;
				output.classList.remove( 'ready' );
				output.classList.add( 'notready' );
				document.querySelector( '#frame' ).src = url;
			}
		} );
	} )
	// Create arrow links
	.then( () => arrow_list.render() )
    .catch( console.error );