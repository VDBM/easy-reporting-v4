const colors = [
    { "name" : "black",     "type" : null,      "rgba" : "( 0, 0, 0, 1 )",          "code" :   0,   "source" : "standard",      "text" : null                               },
    { "name" : "white",     "type" : null,      "rgba" : "( 255, 255, 255, 1 )",    "code" : 765,   "source" : "standard",      "text" : null                               },
    { "name" : "white",     "type" : null,      "rgba" : "( 238, 238, 238, 1 )",    "code" : 714,   "source" : "er",            "text" : "current whiteish background"      },
    { "name" : "grey" ,     "type" : null,      "rgba" : "( 224, 224, 224, 1 )",    "code" : 672,   "source" : "kellogg",       "text" : "current light greyish background" },
    { "name" : "grey" ,     "type" : "old",     "rgba" : "( 201, 201, 201, 1 )",    "code" : 603,   "source" : "er",            "text" : null                               },
    { "name" : "blue",      "type" : "light",   "rgba" : "( 153, 153, 255, 1 )",    "code" : 561,   "source" : "legacy",        "text" : "easy tracking legacy blue"        },
    { "name" : "orange",    "type" : null,      "rgba" : "( 238, 175, 48, 1 )",     "code" : 509,   "source" : "er",            "text" : null                               },
    { "name" : "yellow",    "type" : null,      "rgba" : "( 252, 217, 1, 1 )",      "code" : 470,   "source" : "rainbow_1",     "text" : null                               },
    { "name" : "yellow",    "type" : "dark",    "rgba" : "( 238, 174, 48, 1 )",     "code" : 460,   "source" : "rainbow_1",     "text" : null                               },
    { "name" : "blue",      "type" : null,      "rgba" : "( 80, 153, 219, 1 )",     "code" : 452,   "source" : "er",            "text" : null                               },
    { "name" : "blue",      "type" : "fluo",    "rgba" : "( 0, 169, 244, 1 )",      "code" : 413,   "source" : "legacy",        "text" : "kellogg legacy blue fluo"         }, 
    { "name" : "green",     "type" : "fluo",    "rgba" : "( 190, 219, 0, 1 )",      "code" : 409,   "source" : "kellogg",       "text" : null                               },
    { "name" : "grey",      "type" : null,      "rgba" : "( 128, 128, 128, 1 )",    "code" : 384,   "source" : "standard",      "text" : null                               },
    { "name" : "blue",      "type" : "light",   "rgba" : "( 2, 153, 206, 1 )",      "code" : 361,   "source" : "er",            "text" : null                               },
    { "name" : "red",       "type" : null,      "rgba" : "( 238, 53, 35, 1 )",      "code" : 326,   "source" : "rainbow_1",     "text" : null                               },
    { "name" : "red",       "type" : null,      "rgba" : "( 211, 17, 69, 1 )",      "code" : 297,   "source" : "legacy",        "text" : "kellogg legacy red"               },
    { "name" : "indigo",    "type" : null,      "rgba" : "( 181, 26, 70, 1 )",      "code" : 277,   "source" : "rainbow_1",     "text" : null                               },
    { "name" : "red",       "type" : null,      "rgba" : "( 181, 24, 71 ,1 ",       "code" : 276,   "source" : "kellogg",       "text" : "kellogg current red"              },
    { "name" : "indigo",    "type" : "dark",    "rgba" : "( 151, 23, 46, 1 )",      "code" : 220,   "source" : "rainbow_1",     "text" : null                               },
    { "name" : "green",     "type" : "dark",    "rgba" : "( 0, 115, 99,1 )",        "code" : 214,   "source" : "kellogg",       "text" : null                               },
    { "name" : "blue",      "type" : "dark",    "rgba" : "( 0, 51, 102, 0.9 )",     "code" : 153,   "source" : "legacy",        "text" : "easy tracking legacy blue"        }
];
const css_rules = colors.map( ( definition, index ) => [
    `.color-${ index + 1 }-bg { /*    ${ definition.name } - ${ definition.source } - ${ definition.type || '' }    */`,
    `    background-color: rgba${ definition.rgba };`,
    `}`,
    `.color-${ index + 1 }-bor {`,
    `    border-color: rgba${ definition.rgba };`,
    `}`,
    `.color-${ index + 1 }-t {`,
    `    color: rgba${ definition.rgba };`,
    `}`
] );
css_rules.forEach( rule_lines => rule_lines.forEach( line => console.log( line ) ) );