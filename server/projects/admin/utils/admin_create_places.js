/* FORMAT NOTES

- Notepad++     =>  replace regex /\n +?"name"/ with /   "name"/


*/
////////////////////
// import 
const FS = require( 'fs' );
const { promisify } = require( 'util' );
const { propHash, unique } = require( '../../../modules/util.js' );
const XLSX = require( 'xlsx' );
////////////////////
// derivations
const readFile = promisify( FS.readFile );
const writeFile = promisify( FS.writeFile ); 
////////////////////
/*
*/
const read_excel = ( [ path, sheet ] ) => readFile( path )
    .then( file => { if ( file ) return XLSX.read( file ); else throw new Error( 'Cant find correct file. Parsing interrupted.' ); } )
    .then( workbook => { if ( workbook ) return workbook.Sheets[ sheet ]; else throw new Error( 'Cant parse workbook. Parsing interrupted.' ); } )
    .then( worksheet => { if ( worksheet ) return XLSX.utils.sheet_to_json( worksheet ); else throw new Error( 'Sheet1 not found. Parsing interrupted.' ); } )
    .then( resource => { if ( resource && resource.length ) return resource; else throw new Error( 'Failed creating array from rows. Parsing interrupted.' ); } );
/*
*/
const format_category = types => ( category, index = 0 ) => {
	return Object.keys( category ).map( key => {
		return { "name": key, "format": "collection", "floc": "something", "type": types[ index ], "data": format_category( types )( category[ key ], index + 1 ) }
	} );
};
/*
*/
const format_json = resource => {
    const types = [
		"plant",
        "department",
        "line",
        "area",
        "location",
        "equipment",
        "part"
		//,"subpart" not used anywhere
    ];
	const properties = [
		"area",
		"location",
		"equipment",
		"part"
	]
	return resource.reduce( propHash( 'Functional Location level ', true, null, entry => {
		const to_merge = [
			{ "plant": "mechelen" },
			{ "department": ( entry[ 'Departement' ] || '' ).toLowerCase() },
			{ "line": ( entry[ 'FunctLocDescrip. level 1' ] || '' ).toLowerCase().replace( /lijn /, 'line' ) }
		];
		let last = to_merge[ 2 ].line;
		properties.some( ( property, index ) => {
			const value = ( entry[ `FunctLocDescrip. level ${ index + 2 }` ] || '' ).toLowerCase().replace( /lijn \d/, '' );
			if ( last && last !== value ) {
				to_merge.push( { [ property ] : value } );
				last = value;
				return false;
			}
			else return true;
		} );
		return to_merge.reduce( ( acc, next ) => Object.assign( acc, next ), {} );
	} ), {} );
};
/*
*/
const write_json = path => resource => writeFile( path, JSON.stringify( resource, null, 4 ) );
////////////////////
// workflow: 
Promise
    .resolve( [ 'C:\\easyreporting_api\\projects\\idd\\planners_sap_floc.xlsx', 'Sheet1' ] )
    .then( read_excel )
    .then( format_json )
    .then( write_json( 'C:\\easyreporting_api\\resources\\places_hash.config.json' ) )
    .then( ready => console.log( 'Parsing finished.' ) )
    .catch( error => console.log( error ) );
/*
{   "name": "places",
    "type": "collection",
    "config": [
        "department",
        "area",
        "location",
        "equipment"
    ],
    "data": [
        {   "name": "packing",
            "type": "department",
            "data": [
                {   "name": "CF",
                    "type": "area",
                    "data": []
                },
                {   "name": "CP",
                    "type": "area",
                    "data": []
                },
                {   "name": "DEPAL",
                    "type": "area",
                    "data": []
                },
                {   "name": "DM",
                    "type": "area",
                    "data": []
                },
                {   "name": "GS",
                    "type": "area",
                    "data": []
                },
                {   "name": "SEAS",
                    "type": "area",
                    "data": []
                }
            ]
        },
        {   "name": "process",
            "type": "department",
            "data": [
                {   "name": "CNT",
                    "type": "area",
                    "data": [
                        {   "name": "dough_diverter",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "stuck_chips",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "hydraulic_unit",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "mill_rolls",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "sheet_conveyor",
                            "type": "location",
                            "data": [
                                {   "name": "cross",
                                    "type": "equipment",
                                    "data": []
                                },
                                {   "name": "infeed",
                                    "type": "equipment",
                                    "data": []
                                },
                                {   "name": "MTA",
                                    "type": "equipment",
                                    "data": []
                                },
                                {   "name": "recycle",
                                    "type": "equipment",
                                    "data": []
                                },
                                {   "name": "vertical",
                                    "type": "equipment",
                                    "data": []
                                }
                            ]
                        },
                        {   "name": "unit",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "unit_3roll",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "vacuum_fan",
                            "type": "location",
                            "data": []
                        }
                    ]
                },
                {   "name": "HO",
                    "type": "area",
                    "data": [
                        {   "name": "oil_system",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "oil_spray",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "poe",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "processor",
                            "type": "location",
                            "data": []
                        },
                        {   "name": "vpo",
                            "type": "lcoation",
                            "data": []
                        }
                    ]
                }
*/