
const { promisify } = require( 'util' );
const FS = require( 'fs' );
const readFile = promisify( FS.readFile );
const writeFile = promisify( FS.writeFile );
const XLSX = require( 'xlsx' );
//
const { fetch, root } = require( '../../../modules/fetch.js' );
const PSION = require( '../../../connectors/psion.connector.js' );
const { unique } = require( '../../../modules/util.js' );
//
/* NOTES
// Stork was the brand of the old ULFs. All the currently used ULFs are from the clevertech brand.
// ULF entries always have a line from 1 to 7
// ULF bodemvellenlegger is not used anymore
// So spot : palletizer is never used and hence we dont have to include it.
// line1 : palletizer : ulf clevertech : null : null
// line1 : palletizer : ulf stork 1.2 : null : null     
// line2 : palletizer : ulf clevertech : null : null
// line2 : palletizer : ulf stork 2.2 : null : null
// line3 : palletizer : ulf : null : null
// line4 : palletizers : palletizer 41 : ulf 41 : null
// line4 : palletizers : palletizer 42 : ulf 42 : null
// line5 : palletizer : ulf : null : null
// line6 : palletizer : ulf : null : null
// spot : palletiser : clevertech : null : null

// after casting fixed following issues:
//      changed condition Down into condition down
//      changed department cns into department c&s
*/
// UTILS
const months = {
    'Jan' : 1,
    'Feb' : 2,
    'Mar' : 3,
    'Apr' : 4,
    'May' : 5,
    'Jun' : 6,
    'Jul' : 7,
    'Aug' : 8,
    'Sep' : 9,
    'Oct' : 10,
    'Nov' : 11,
    'Dec' : 12
};
//
const lines = {
    'Lijn 1' : 'line1',
    'Lijn 2' : 'line2',
    'Lijn 3' : 'line3',
    'Lijn 4' : 'line4',
    'Lijn 4.1' : 'line4',
    'Lijn 4.2' : 'line4',
    'Lijn 5' : 'line5',
    'Lijn 6' : 'line6',
    'Lijn 7' : 'spot'
};
//
const createSQL = id => {
    return [
        "( SELECT 'crackers' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM crackers.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'depal' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM depal.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'line1' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn1.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'line2' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn2.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'line3' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn3.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'line4' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn4.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'line5' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn5.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'line6' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn6.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'spot' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM lijn7.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'loge' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM loge.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'mssseasoning' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM mssseasoning.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'unloading' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM ontlss.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'rm' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM rm.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'seasoning' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM seasoning.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'ulf' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM ulf.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'util' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM util.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'whs' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM whs.defecten WHERE ID = 6773 )",
        "UNION",
        "( SELECT 'wwtp' AS `db`, Datum, Flocation, ID, Job, Uitleg, FlocationNaam FROM wwtp.defecten WHERE ID = 6773 )"
    ]
        .join( ' ' )
        .replace(
            /WHERE ID = 6773/g,
            `WHERE ID = ${ id }`
        );
};
//
const padStart = number => number < 10 ? `0${ number }` : number.toString();
//
const read_json = name => readFile( `C:\\easyreporting_api\\projects\\idd\\${ name }.json` );
//
const write_json = name => resource => writeFile( `C:\\easyreporting_api\\projects\\idd\\${ name }.json` , JSON.stringify( resource, null, 4 ));
//
// TRANSFORMATIONS
// 1)
const cast_properties = resource => {
    const assignees = {
        'contr' : 'contractor',
        'core techn.' : 'operations_core',
        'dag' : 'team_day',
        'ops' : 'operations',
        'p&s' : 'pns', // planning_sheduling => de planners groep zelf
        'pe/pt' : 'pept',
        'shp' : 'shop',
        't1' : 'team1',
        't2' : 'team2',
        't3' : 'team3',
        't4' : 'team4',
        't5' : 'team5'
    };
    const formatted = resource.map( entry => {
        const format = {
            starttime: null,
            endtime: null,
            duetime: null,
            edittime: null,
            duration: null,
            plant: 'mechelen',
            organisation: 'cns',
            department: 'cns',
            line: null,
            area: null,
            location: null,
            equipment: null,
            part: null,
            text: null,
            notes: null,
            type: null,
            type_organisation: null,
            priority: null,
            condition: null,
            id_order: null,
            id_defect: null,
            staging: null,
            creator: null,
            creator_team: null,
            assignee: null,
            actor: null,
            editor: null,
            editor_team: null,
            reviewer: null,
            reviewer_team: null,
            place: {
                area: null,
                line: null,
                machine: null,
                FLOC: null
            }
        };
        try {
            if ( entry.starttime ) format.starttime = entry.starttime.split( /[\/-]/g ).reverse();
            if ( entry.endtime ) format.endtime =  entry.endtime.split( /[\/-]/g ).reverse();
            if ( entry.duetime ) format.duetime = entry.duetime.split( /[\/-]/g ).reverse();
            if ( entry.duration ) format.duration = parseInt( entry.duration.replace( /\D/g, '' ) );
            format.text = entry.text;
            format.notes = entry.notes;
            format.condition = entry.condition;
            format.id_order = entry.id_order;
            format.id_defect = entry.id_defect;
            format.staging = entry.staging;
            if ( entry.assignee ) format.assignee = assignees[ entry.assignee.toLowerCase() ];
            else {
                console.log( 'no assignee' );
                console.log( JSON.stringify( entry ) );
            }
            if ( entry.actor ) format.actor =  assignees[ entry.actor.toLowerCase() ];
            format.place.area = entry.place_area;
            format.place.line = entry.place_line;
            format.place.machine = entry.place_machine;
            //
            [
                'starttime',
                'endtime',
                'duetime',
                'edittime',
                'duration',
                'line',
                'area',
                'location',
                'equipment',
                'part',
                'text',
                'notes',
                'type',
                'type_organisation',
                'priority',
                'condition',
                'id_order',
                'id_defect',
                'staging',
                'creator',
                'creator_team',
                'assignee',
                'actor',
                'editor',
                'editor_team',
                'reviewer',
                'reviewer_team',
                'place'
            ].forEach( property => {
                if ( entry.hasOwnProperty( property ) && format.hasOwnProperty( property ) ) {
                    if ( ( entry[ property ] && !format[ property ] ) || ( !entry[ property ] && format[ property ] ) ) {
                        console.log( 'failed property: ' + property );
                        console.log( JSON.stringify( entry ) );
                        console.log( JSON.stringify( format ) );
                        console.log( '---' );
                    }
                }
            });
            return format;
        }
        catch( error ) {
            console.log( error );
            console.log( JSON.stringify( entry ) );
            console.log( '-----' );
            return entry;
        }
    });
    return formatted;
};
// 2)
const remove_NAs = resource => resource.map( entry => {
    Object.keys( entry ).forEach( key => {
        if ( entry[ key ] === 'N/A' ) entry [ key ] = null;
    });
    return entry;
});
// 3)
const create_starttimes = resource => resource.map( entry => {
    try {
        if ( !entry.starttime && entry.endtime ) {
            entry.starttime = entry.endtime.slice();
            entry.starttime[2] = ( parseInt( entry.starttime[2], 10 ) - 1 ).toString();
        }
    }
    catch( err ) {
        console.log( 'no starttime or endtime' );
        console.log( entry );
    }
    return entry; 
});
//
const format_dates = resource => {
    const formatted = resource.map(( entry, index ) => {
        if ( Array.isArray( entry.duetime )) {
            if ( isNaN( parseInt( entry.duetime[ 1 ], 10 ) ) ) {
                if ( months.hasOwnProperty( entry.duetime[1] ) ) entry.duetime[1] = months[ entry.duetime[1] ];
                else {
                    console.log( 'no month for duetime: ' + ( index + 3 ) );
                    console.log( JSON.stringify( entry ));
                    console.log( '---' );
                }
            }
            entry.duetime = '2018-' + padStart( parseInt( entry.duetime[1], 10 ) + 1 ) + '-01T02:00:00.000Z';
        }
        else {
            console.log( 'missing month for duetime' );
            console.log( entry );
            throw new Error();
        }
        // OK
        if ( !entry.endtime ) {
            // Create endtime from starttime
            if ( entry.actor ) {
                entry.endtime = entry.starttime.slice();
                entry.endtime[2] = ( parseInt( entry.endtime[2], 10 ) + 1 ).toString();
                if ( isNaN( parseInt( entry.endtime[ 1 ], 10 ) ) ) {
                    if ( months.hasOwnProperty( entry.endtime[1] ) ) entry.endtime[1] = months[ entry.endtime[1] ];
                    else {
                        console.log( 'no month for endtime: ' + ( index + 3 ) );
                        console.log( JSON.stringify( entry ));
                        console.log( '---' );
                    }
                }
                entry.endtime = [
                    '20' + entry.endtime[ 0 ].toString(),
                    padStart( parseInt( entry.endtime[1], 10 ) ).toString(),
                    padStart( parseInt( entry.endtime[2], 10 ) ).toString()
                ].join('-') + 'T02:00:00.000Z';
            }
            else entry.endtime = null;
        }
        else {
            if ( isNaN( parseInt( entry.endtime[ 1 ], 10 ) ) ) {
                if ( months.hasOwnProperty( entry.endtime[1] ) ) entry.endtime[1] = months[ entry.endtime[1] ];
                else {
                    console.log( 'no month for endtime: ' + ( index + 3 ) );
                    console.log( JSON.stringify( entry ));
                    console.log( '---' );
                }
            }
            entry.endtime = [
                '20' + entry.endtime[ 0 ].toString(),
                padStart( parseInt( entry.endtime[1], 10 ) ).toString(),
                padStart( parseInt( entry.endtime[2], 10 ) ).toString()
            ].join('-') + 'T02:00:00.000Z';
        }
        // OK, but AFTER endtime so we can use starttime to cast endtime.
        if ( isNaN( parseInt( entry.starttime[ 1 ], 10 ) ) ) {
            if ( months.hasOwnProperty( entry.starttime[1] ) ) entry.starttime[1] = months[ entry.starttime[1] ];
            else {
                console.log( 'no month for starttime: ' + ( index + 3 ) );
                console.log( JSON.stringify( entry ));
                console.log( '---' );
            }
        }
        entry.starttime = [
            '20' + entry.starttime[ 0 ].toString(),
            padStart( parseInt( entry.starttime[1], 10 ) ).toString(),
            padStart( parseInt( entry.starttime[2], 10 ) ).toString()
        ].join('-') + 'T02:00:00.000Z';
        //
        return entry;
    });
    return formatted;
};
//
const format_id = resource => resource.map( entry => {
    if ( entry.id_defect ) {
        if ( [ 'ARG', 'DDS' ].includes( entry.id_defect ) ) {
            entry.type_organisation = entry.id_defect;
            entry.id_defect = null;
        }
    }
    if ( entry.id_defect ) entry.id_defect = parseInt( entry.id_defect, 10 );
    if ( entry.id_order ) entry.id_order = parseInt( entry.id_order, 10 );
    return entry;
});
//
const fetch_defects = resource => {
    return Promise.all( resource.map( entry => {
        if ( !entry.id_defect ) return Promise.resolve( entry );
        else {
            return PSION
                .read( createSQL( entry.id_defect ))
                .then( results => {
                    entry.place.FLOC = results.data;
                    return entry;
                });
        }
    }));
};
//
const format_floc = resource => resource.map(( entry, index ) => {
    if ( entry.place.FLOC && Array.isArray( entry.place.FLOC ) ) {
        if ( entry.place.area ) {
            const floc = entry.place.FLOC.find( defect => {
                if ( !defect.Flocation ) {
                    console.log( 'defect without location' );
                    console.log( defect );
                }
                const comparator = entry.place.area === 'APL' ? 'whs' : entry.place.area.toLowerCase();
                return defect.db === comparator;
            });
            if ( floc ) entry.place.FLOC = floc.Flocation;
            else {
                console.log( 'not found: ' + index );
                console.log( entry );
            }
        }
        else if ( entry.place.machine ) {
            const comparator = entry.place.machine.slice( 0, 3 ).toLowerCase();
            const floc = entry.place.FLOC.find( defect => {
                if ( !defect.Flocation ) {
                    console.log( 'defect without location' );
                    console.log( defect );
                }
                return defect.db === comparator;
            });
            if ( floc ) entry.place.FLOC = floc.Flocation;
            else {
                console.log( 'not found: ' + index );
                console.log( entry );
            }
        }
    }
    return entry;
});
//
const place_from_floc = resource => {
   return readFile( 'C:\\easyreporting_api\\resources\\places_hash.config.json' )
        .then( JSON.parse )
        .then( places => {
            return resource.map( entry => {
                let place = entry.place.FLOC
                    ? places[ entry.place.FLOC ]
                    : null;
                if ( place ) {
                    delete place.plant;
                    delete place.department;
                    delete entry.place;
                    Object.keys( place ).forEach( key => {
                        place[ key ] = place[ key ].trim();
                    });
                    return Object.assign( entry, place );
                }
                else return entry;
            });
        });
};
//
const place_from_lookup = resource => resource.map( entry => {
    if ( entry.place ) {
        let place = null;
        const combo = `${ entry.place.area }-${ entry.place.machine }`;
        switch( combo ) {
            case 'WHS-Buggywash' :
                place = {
                    line: 'off line systems',
                    area: 'scrap area',
                    location: 'waste buggy washing equipment'
                };
            break;
            case 'ULF-Case conveyor' :
            case 'ULF-ULF: Case conveyor' :
                place = {
                    line: lines[ entry.place.line ]
                };
                if ( place.line !== 'line4' ) {
                    place.area = 'palletizer';
                    place.location = 'case conveyors';
                }
                else {
                    if ( entry.place.line === 'Lijn 4.1' ) {
                        place.area = 'palletizers';
                        place.location = 'palletizer 41';
                        place.equipment = 'tray conveyors 41';
                    }
                    else {
                        place.area = 'palletizers';
                        place.location = 'palletizer 42';
                        place.equipment = 'tray conveyors 42';
                    }
                }
            break;
            case 'APL-WHS: APL' :
                place = {
                    line: 'off line systems',
                    area: 'pringles warehouse',
                    location: 'apl'
                };
            break;
            case 'ULF-ULF: Labeler' :
                place = {
                    line: lines[ entry.place.line ]
                };
                if ( place.line === 'line4' ) {
                    place.area = 'palletizers';
                    place.location = 'labeller';
                }
                else if ( place.line === 'spot' ) {
                    place.area = 'palletiser';
                    place.location = 'labeller';
                }
                else {
                    place.area = 'palletizer';
                    place.location = 'labeller';
                }
            break;
            case 'SPOT-SPT: Neervoerder' :
                place = {
                    line: 'spot',
                    area: 'spot',
                    location: 'downfeeder spot'
                };
            break;
            case 'SPOT-Flowwrapper' :
            case 'SPOT-SPT: Flowwrapper' :
                place = {
                    line: 'spot',
                    area: 'spot',
                    location: 'flowwrapper spot'
                };
            break;
            case 'SPOT-SPT: Casepacker' :
                place = {
                    line: 'spot',
                    area: 'spot',
                    location: 'case packer spot'
                };
            break;
            case 'SPOT-QA street 1-2':
            case 'SPOT-SPT: QA street 1' :
                place = {
                    line: 'spot',
                    area: 'spot',
                    location: 'q-street 1'
                };
            break;
            case 'SPOT-SPT: Vellenlegger' :
                place = {
                    line: 'spot',
                    area: 'palletiser',
                    location: 'clevertec',
                    equipment: 'clevertec tussenvellenleger'
                };
            break;
            case 'ULF-ULF: Palletizer' :
                place = {
                    line: lines[ entry.place.line ]
                };
                
                if ( place.line === 'line1' || place.line == 'line2' ) {
                    place.area = 'palletizer';
                    place.location = 'ulf clevertec';
                }
                else if ( place.line === 'line3' || place.line === 'line5' || place.line === 'line6' ) {
                    place.area = 'palletizer';
                    place.location = 'ulf';
                }
                else if ( place.line === 'line4' ) {
                    place.area = 'palletizers';
                }
                else if ( place.line === 'spot' ) {
                    place.area = 'palletiser';
                    place.location = 'clevertec';
                    place.equipment = 'clevertec tussenvellenleger';
                }
                else throw new Error( JSON.stringify( entry ));
            break;
            case 'SPOT-SPT: Devider' :
                place = {
                    line: 'spot',
                    area: 'spot',
                    location: 'lane dividers spot'
                };
            break;
            case 'SPOT-ULF: Palletizer' :
                place = {
                    line: 'spot',
                    area: 'palletiser',
                    location: 'clevertec'
                };
            break;
            case 'SPOT-SPT: Stretch wrapper' :
                place = {
                    line: 'spot',
                    area: 'spot',
                    location: 'stretch wrapper spot'
                };
            break;
            case 'ULF-ULF: EPD' :
                place = {
                    line: lines[ entry.place.line ]
                };
                if ( place.line !== 'line4' ) {
                    place.area = 'palletizer';
                    place.location = 'epd';
                }
                else throw new Error( JSON.stringify( entry ));
            break;
            case 'ULF-ULF: Pallet conveyor' :
                place = {
                    'line' : lines[ entry.place.line ],
                };
                if ( place.line !== 'spot' ) {
                    place.area = 'palletizer';
                    place.location = 'palletizer conveyors';
                }
                else {
                    place.area = 'palletiser';
                    place.location = 'outfeed palletconveyor';
                }
            break;
            case 'ULF-ULF: Vellenlegger' :
                 //all lines: non L4 L4 spot
                place = {
                    line: lines[ entry.place.line ],
                };
                if ( place.line === 'line1' || place.line == 'line2' ) {
                    place.area = 'palletizer';
                    place.location = 'ulf clevertec';
                    place.equipment = 'clev tussenvellenlegger';
                }
                else if ( place.line === 'line3' || place.line === 'line5' || place.line === 'line6' ) {
                    place.area = 'palletizer';
                    place.location = 'ulf';
                    place.equipment = 'ulf vellenlegger';
                }
                else if ( place.line === 'line4' ) {
                    place.area = 'palletizers';
                    place.location = 'palletizer 41';
                    place.equipment = 'ulf 41';
                    place.equipment = 'ulf vellenlegger 41';
                }
                else if ( place.line === 'spot' ) {
                    place.area = 'palletiser';
                    place.location = 'clevertec';
                    place.equipment = 'clevertec tussenvellenleger';
                }
            break;
        }
        if ( place ) {
            delete entry.place;
            return Object.assign( entry, place );
        }
        else {
            console.log( 'failed casting place' );
            console.log( entry );
            return entry;
        }
    }
    else return entry;
});
//
// WORKFLOW
/* CREATE OK*/
/*
read_json( 'idd_cns_source' )
    .then( JSON.parse )
    .then( remove_NAs )
    .then( cast_properties )
    .then( create_starttimes )
    .then( format_dates )
    .then( format_id )
    .then( fetch_defects )
    .then( format_floc )
    .then( place_from_floc )
    .then( write_json( 'idd_cns' ) )
    .catch( err => console.error( err ));
//*/
/*
read_json( 'idd_cns' )
    .then( JSON.parse )
    .then( place_from_lookup )
    .then( write_json( 'idd_cns' ) )
    .catch( err => console.error( err ));
*/

/*
read_json( 'idd_cns' )
    .then( JSON.parse )
    .then( resource => resource[ 0 ] )
    .then( entry => fetch( `http://localhost/api/idd_dev`, { 'body' : JSON.stringify( entry ), 'method' : 'POST' } ))
    .then( response => {
        console.log( response );
    })
    .catch( error => console.log( error ));
    */
    
    alert( 'ready' );
    
    

    