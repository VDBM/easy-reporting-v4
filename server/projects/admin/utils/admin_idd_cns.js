const { fetch, root } = require( '../../../modules/fetch.js' );

fetch( '/resources/idd_cns.json' )
    .then( resource => Promise.all( resource.map( entry => fetch( `http://mecvm21/api/idd_dev`, { 'body' : JSON.stringify( entry ), 'method' : 'POST' } ))))
    .then( responses => {
        responses.forEach( response => console.log( response ));
    })
    .catch( error => console.log( error ));