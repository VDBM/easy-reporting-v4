
1) Duid aan wat waar is:

Een Food Safety Risk Assessment (FSRA) is een risico analyse om de voedselveiligheidsrisico’s van een activiteit in te schatten en te elimineren, reduceren of te mitigeren 
true => p3

Een FSRA voor taken, werken en projecten moet steeds gemaakt worden VOOR de start van de activiteit 
true => p4

Alle types FSRA formulieren moeten altijd goedgekeurd worden door iemand van het Food Safety team 
false => p14. bvb een FRSA voor een taak moet Niet door food safety goedgekeurd worden.

Er moet geen FSRA gemaakt worden voor het uitvoeren van een activiteit op een stilstaande lijn – aangezien er dan geen direct risico is tot productcontaminatie 
false => p???

De te nemen maatregelen vastgelegd in de FSRA moeten besproken worden met elke betrokken uitvoerder  
true => p???

Een FSRA kan een onderdeel zijn van het werkvergunningssysteem 
true => p15

Als er na de activiteit nog een Class A uitgevoerd wordt, moet er nooit een FSRA gemaakt worden.
false => p???

-----

2) Welke soorten risico’s moeten bij het opstellen van een FSRA bekeken worden (meerdere antwoorden mogelijk) 

=> p3

true  Fysische contaminatie
true  Chemische contaminatie
true  Microbiologische contaminatie
true  Allergenen 

-----

3) Multiple choice: In welke situaties moet er een FSRA gebruikt worden (Meerdere antwoorden mogelijk)

=> p4-p5-p6

true  voorafgaand op taken, werken en projecten
true   bij het opstellen van een werkinstructie
false als voorbereiding op een externe audit
true  voor elke hold / alert waarbij er een mogelijk food safety risico is en een release beslissing genomen moet worden

-----

4) Multiple choice: Duid aan wat er zeker op elke FSRA voorafgaand op activiteiten vermeld moet worden (meerdere antwoorden mogelijk)

=> p16-p17

true  de area en zone waarin de activiteit uitgevoerd zal worden
true  of de activiteit op een draaiende of stilstaande lijn uitgevoerd zal worden
false of er een werkinstructie bestaat voor de activiteit
true  welke de specifieke risico’s zijn op contaminatie verbonden aan de activiteit
true  preventiemaatregelen die genomen moeten worden om risico te elimineren, reduceren of mitigeren

-----

5) Duid aan wat nodig is om de voedselveiligheidsrisico’s voldoende in te dekken (slechts 1 antwoord mogelijk) : Er moet een nieuw stuk drain rooster gelegd worden in RAW area.

=> p15

false geen FSRA nodig, taak volgens werkinstructie
false QRA / FSRA formulier
true  FSRA als onderdeel van werkvergunning
false FSRA document

-----

6) Duid aan wat nodig is om de voedselveiligheidsrisico’s voldoende in te dekken (slechts 1 antwoord mogelijk) : Ik ben C&T operator en moet tijdens pit stop als wederkerende taak de vertikal belt (zone 1, RAW area) controleren op beschadiging

=> p24

true  geen FSRA nodig, taak volgens werkinstructie
false QRA / FSRA formulier
false FSRA als onderdeel van werkvergunning
false FSRA document

-----

7) Duid aan wat nodig is om de voedselveiligheidsrisico’s voldoende in te dekken (slechts 1 antwoord mogelijk): Als onderdeel van een project voor Utilities op het dak moet een buitendeur voor langere tijd open blijven staan. 

=> p27

false geen FSRA nodig, taak volgens werkinstructie
false QRA / FSRA formulier
false FSRA als onderdeel van werkvergunning
true  FSRA document

-----

8) Duid aan wat nodig is om de voedselveiligheidsrisico’s voldoende in te dekken (slechts 1 antwoord mogelijk): Als gevolg van een breakdown moet er een dringend koud werk uitgevoerd worden in zone 3 RTE waarvoor geen werkinstructie bestaat. 

=> p26

false geen FSRA nodig, taak volgens werkinstructie
true  QRA / FSRA formulier
false FSRA als onderdeel van werkvergunning
false FSRA document