class Model {
  constructor () {
    this.listeners = new Set();
  }

  get (property) {
    this.notifyAll('get', property, this[property]);
    return this[property];
  }

  set (property, value) {
    this.notifyAll('set', property, value);
    return this[property] = value;
  }

  addListener (listener) {
    this.listeners.add(listener);
  }

  deleteListener (listener) {
    this.listeners.delete(listener);
  }

  notifyAll (message, ...args) {
    for (let listener of this.listeners) {
      listener.notify(this, message, ...args);
    }
  }
}

class Person extends Model {
  constructor (first, last) {
    super();
    this.set('first', first);
    this.set('last', last);
  }

  fullName () {
    return `${this.get('first')} ${this.get('last')}`;
  }
};

class View {
  constructor (model) {
    this.model = model;
    model.addListener(this);
  }
}

class PersonView extends View {
  // ...

  notify(notifier, method, ...args) {
    if (notifier === this.model && method === 'set') this.redraw();
  }

  redraw () {
    document
      .querySelector(`person-${this.model.id}`)
      .text(this.model.fullName())
  }
}