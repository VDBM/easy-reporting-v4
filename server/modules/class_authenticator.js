/*  class_authenticator.mjs         => 
*   @language:  ES6
*   @version:   v1.0.0
*   @user:      VDBM
*   @date:      2018-06-14
*/
// TODO: Once we switch to usint the API version of user_profile, we can change the team names in the active_directory connector instead of having check and change the response from the server.
////////////////////
/*  API
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { fetch } = require( './fetch.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Authenticator
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Authenticator {
    // Create an instance.
    constructor( use_init = true ) {
        this.use_init = use_init;
        this.user_profile = null;
    }
    get() {
        return fetch( 'http://mecvm21/ER_service/ER_service.js/user_profile', { 'headers' : { 'Cache-Control' : 'private' } } )
            .then( response => {
                const valid = response.info.status === 'OK';
                const user_profile = valid
                    ? response.value[ 0 ]
                    : { "account" : null, "team" : null };
                console.assert( valid, 'Login service response is invalid.' );
                if ( user_profile && user_profile.team === 'dag' ) user_profile.team = 'team_day';
                this.user_profile = user_profile;
                return user_profile;
            } )
            .catch( error => {
                console.error( 'failed checking user profile' );
                console.error( error );
                return null;
            } );
    }
    init() {
        if ( this.use_init ) this.get();
    }
    login() {
        return this.user_profile
            ? this.user_profile
            : this.get();
    }
}
// Static type.
Authenticator.prototype.type = Object.freeze( 'authenticator' );
////////////////////
module.exports = Authenticator;