/*  component_dropdown.mjs   =>
*   @language:  ES6
*   @version:   1.0.0
*   @creator:      VDBM
*   @date:      2018-06-15
*/
////////////////////
/*  API
*
*   const dropdown = new Dropdown( { "event": "function", "name": "string", "style": "string" } );
*   dropdown.html();
*   dropdown.html( [] );
*   dropdown.render();
*   dropdown.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { capitalize, isFn, isObj, propSort, translate } = require( './util.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [ "default" ];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Dropdown
*   @param      Object
*                   Function event
*                   String labels
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               - Since we need to add event handlers to the select nodes, we kind of need a mechanism that will actually render the nodes and add event handlers to them.
*                 So we can check the 'event' property of the instance. If it's a function, we need to attach the event handlers ourselves, if it's a string, we can just add it as an event attribute  to the select node.
*                 => OK: instance.bind()
*               - We can choose between clearing each select fully and re-adding the default, or we could not remove the default value and hide the entire select.
*/
class Dropdown {
    // Create an instance.
    constructor( { event = null, name, style = '' } = {} ) {
        this.data = null;
        this.event = event;
        this.i18n = null;
        this.name = name;
        this.root = `#dropdown_${ name }`;
        this.state = null;
        this.style = style;
    }
    // Add an event handler to a target, usually the container of the component.
    // Either bind the function, or add the data attributes that a mediator will look for.
    bind( target ) {
        const node = target || document.querySelector( this.root );
        if ( !node ) throw new Error( `component dropdown_${ this.name } - root undefined, can't bind event` );
        else {
            node.addEventListener( 'change', this.onchange.bind( this ) );
        }
        return this;
    }
    // Return the html, after populating it with array data. If no data is provided, the data property or DEFAULT_DATA will be used.
    // The operand, relation, expectation parameters provide compatibility with loops in our our template engine.
    // For other functionality, `html( data )` usually suffices.
    html( data, operand, relation, expectation ) {
        // If we have a data parameter inserted and it's not an array, we made a design error OR we're inside a template loop of an unrelated template.
        // So we have to ignore the data parameter sent by the template sicne the loop is not related to our component.
        // If the template was aprt of our component, we would actually define it here instead of having it inserted by the template engine.
        // We keep the distinction between this.data and data to provide for future extension where we have a case that a component does need access to whatever template data scope this component is part of.
        const is_template = ( data && isObj( data ) && data.hasOwnProperty( 'collection' ) && data.hasOwnProperty( 'source' ) );
        const data_source = !is_template && data
            ? data
            : this.data || DEFAULT_DATA;
        // Dirty hack, since our dropdown now has implementation details of the tempalte engine. We ened to fix this inside the tempalte itsself. 
        const resource_name = is_template
            ? Object.keys( data ).filter( key => key !== 'collection' && key !== 'source' )
            : null;
        const id = is_template
            ? `${ this.type }_${ this.name }_entry_${ data[ resource_name ].id }`
            : `${ this.type }_${ this.name }`;
        console.assert( Array.isArray( data_source ), `${ this.root }.html: data is not an array.` );
        const translation = data_source
            .map( item => ( { "value" : item, "label" : capitalize( translate( item, this.i18n ) ) } ) )
            .sort( propSort( 'lexical', 'label' ) );
        // Only add the event as an attribute if it exists AND it's not a function.
        const events = !isFn( this.event ) && this.event !== null;
        // Create all dropdowns.
        const dropdown = `<select id="${ id }" class="${ this.style }" ${ events ? this.event : '' }><option value="default" selected="true">Select</option></select>`;
        const options = translation.map( ( { value, label } ) => `<option value="${ value }">${ label }</option>` );
        // Add the options to the select tag . 
        return dropdown.replace( '</select>', `${ options.join( '' ) }</select>` );
    }
    onchange( event ) {
        this.update( { "state": event.target.value } );
        if ( isFn( this.event ) ) this.event( event );
        return this;
    }
    // Render the component to the root element and bind the event handler.
    render() {
        const node = document.querySelector( this.root );
        if ( !node ) throw new Error( `component dropdown_${ this.name } - root undefined, can't render` );
        else {
            node.innerHTML = this.html();
            this.bind( node );
        }
        return this;
    }
    /*scope() {
    }*/
    // Remove an event handler from a target, usually the container of the component.    
    unbind( target ) {
        const node = target || document.querySelector( this.root );
        if ( !node ) throw new Error( `component dropdown_${ this.name } - root undefined, can't bind event` );
        else {
            if ( !this.event || isFn( this.event ) ) node.removeEventListener( 'change', this.onchange.bind( this ) );
            else if ( isObj( this.event ) ) Object.entries( this.event ).forEach( ( [ attribute, value ] ) => node.removeAttribute( attribute ) );
        }
        return this;
    }
    // Update our properties.    
    update( source ) {
        if ( source.data ) this.data = source.data;
        if ( source.i18n ) this.i18n = source.i18n;
        if ( source.root ) this.root = source.root;
        if ( source.state ) this.state = source.state;
        if ( source.style ) this.style = source.style;
        return this;
    }
}
// Static type.
Dropdown.prototype.type = Object.freeze( 'dropdown' );
////////////////////
module.exports = Dropdown;