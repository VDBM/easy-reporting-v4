/*  component_downtimebar.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-12
*/
////////////////////
/*  API
*
*           - Proficy downtime events are passed as UTC time strings representing local time. So we ahve to use 'getUTC...()' methods to get the correct local times.
*           - For ongoing downtimes that started before the shift started, the width automatically is 100%, since we don't have an endtime yet.
*           - Since we work with a tempalte that ahs to compeltely be rerendered, our render function and our update function are the same.
*           - TODO: To make this more efficient, we have to split this into two templates, or ahve the update function replace a certain node. +- 1 hour work, but low prio atm.
*           - BUG: still takes local time to calculate the sundial. We have to sue UTC time here, since atm between 14:15 and 15:15 we overflow the bar due to wrong hour.
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const CONSTANTS = require( './constants.js' );
const UTIL = require( './util.js' );
const { Template } = require( './class_template.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = (() => {
    // 1 hour UDT, 30 min UT, 30 min PDT, 15 min UT, 15 min UDT, 4 hours UT, 1 hour 30min ongoing
    const shift  = UTIL.shiftCurrent();
    const date = shift.toJSON().slice( 0, 11 );
    const hour = shift.getUTCHours(); // 6 14 22
    const hour_60min = ( hour + 1 ).toString().padStart( 2, '0' ); // 07 15 23
    const hour_120min = hour + 2 === 24 ? '00' : ( hour + 2 ).toString().padStart( 2, '0' ); // 08 16 00
    const hour_360min = hour + 6 === 28 ? '05' : ( hour + 6 ).toString().padStart( 2, '0' ) // 12 20 04
    /*  Fixed
    */
    return UTIL.updateTimeframe( [
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 1 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : shift.toJSON(), 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 2 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 1 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 3 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 2 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'planned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 4 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 3 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 5 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 4 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'planned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 6 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 5 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 7 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 6 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'planned' },
        { 'duration' : null, 'endtime' : null, 'starttime' : `${ date }${ ( hour + 7 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'ongoing' }
    ] );
    /*  Variant
    return UTIL.updateTimeframe( [
        { 'duration' : 60.00, 'endtime' : `${ date }${ hour_60min }:15:00.000Z`, 'starttime' : shift.toJSON(), 'type' : 'unplanned' },
        { 'duration' : 30.00, 'endtime' : `${ date }${ hour_120min }:15:00.000Z`, 'starttime' : `${ date }${ hour_60min }:45:00.000Z`, 'type' : 'planned' },
        { 'duration' : 15.00, 'endtime' : `${ date }${ hour_120min }:45:00.000Z`, 'starttime' : `${ date }${ hour_120min }:30:00.000Z`, 'type' : 'unplanned' },
        { 'duration' : null, 'endtime' : null, 'starttime' : `${ date }${ hour_360min }:45:00.000Z`, 'type' : 'ongoing' }
    ] );
    */
})();
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
const calculateWidth = duration_ms => duration_ms > CONSTANTS.MILISECONDS_SHIFT ? 100.00 : parseFloat( ( duration_ms * 100 / ( CONSTANTS.MILISECONDS_SHIFT - 1000 ) ).toFixed( 2 ), 10 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Downtimebar {
    constructor( name ) {
        this.name = name;
        this.root = `#er_downtimebar_${ name }`;
        this.template = new Template( 'component_downtimebar' );
    }
    render() {
        this.update( [] );
        return this;
    }
    update( data = DEFAULT_DATA ) {
        // Update the bar to show the downtimes
        // Entire bar = 1 shift = 8 hours - 1sec = 100% width .
        // start = current time => cast to shift => cast to date .
        const bar_start = UTIL.shiftCurrent();
        const hour_start = bar_start.getUTCHours();
        // end = start + duration shift - 1 second to get x:14:59.000 .
        const bar_end = new Date( bar_start.getTime() + CONSTANTS.MILISECONDS_SHIFT - 1000 );
        // If there aren't any downtime records, we can create one record spanning the entire shift .
        // Else we create an array of durations, filling in any time gaps and creating the entry object as we go .
        const durations = !data.length
            ? [ { 'label' : '&nbsp;', 'type' : 'running', 'width' : 100.00 } ]
            : data
                .reduce( ( summary, entry, index, data ) => {
                    let width;
                    let label;
                    // We could do this with a mapping, but might as well fold it back into the redution .
                    // Create a 'running' type chunk before the entry starttime if needed .
                    // We actually need the difference between the two to be bigger than 1 second, else we just consider the downtimes to be adjacent.
                    if ( entry.starttime.getTime() - summary.last_ms > 1000 ) {
                        width = calculateWidth( entry.starttime.getTime() - summary.last_ms );
                        summary.sum += width;
                        summary.durations.push( { 'label' : '&nbsp;', 'type' : 'running', width } );
                    }
                    // Create a chunk for the current entry and update the summary .
                    // If the downtime is ongoing, the endtime is null and we use the endtime of the shift so we fill the remainder width of the bar with ongoing .
                    summary.last_ms = entry.endtime ? entry.endtime.getTime() : bar_end.getTime();
                    width = calculateWidth( summary.last_ms - entry.starttime.getTime() );
                    summary.sum += width;
                    // Determine label
                    if ( !entry.duration ) {
                        if ( width > 4 && entry.type == 'ongoing' ) label = 'ongoing';
                        else label = '&nbsp;';
                    }
                    else if ( width > 2.5 ) label = parseFloat( entry.duration, 10 ).toFixed( 2 );
                    else if ( width > 1 ) label = parseInt( entry.duration, 10 ).toString();
                    // Dont't show any label if the chunk isn't wide enough to render anything without overflowing. 
                    else label = '&nbsp;';
                    summary.durations.push( { label, 'type' : entry.type,  width } );
                    // If this is the last entry
                    if ( index === data.length - 1 ) {
                       // Check if we need to create a 'running' type chunk to fill the last part of the bar .
                       // Since we have the sum of all widths, this width is just the remaining width .
                       // This also satisfies the width < 100.00
                       if ( summary.last_ms < bar_end.getTime() ) summary.durations.push( { 'label' : '&nbsp;', 'type' : 'running', 'width' : 99.99 - summary.sum } );
                       // Satisfy the width > 100.00 by decreasing the length of the last chunk .
                       else if ( summary.sum > 100 ) summary.durations[ summary.durations.length - 1 ].width = ( summary.durations[ summary.durations.length - 1 ].width + 99.99 - summary.sum ).toFixed( 2 );
                    }
                    return summary;
                }, { 'durations' : [], 'last_ms' : bar_start.getTime(), 'sum' : 0.00 } )
                .durations;
        const hour_now = new Date().getHours();
        const sundial = {
            'offset' : calculateWidth( UTIL.dateUTC() - bar_start.getTime() ),
            'symbol' : hour_now > 8 && hour_now < 19 ? '\u263C' : '\u263E'
        };
        const timings = Array.from( { 'length' : 9 }, ( _ , index ) => `${ ( hour_start + index ).toString().padStart( 2, '0' ) }:15` );
        // Insert the current time, splitting the timeframe into located in, into two.
        const node = document.querySelector( this.root );
        if ( node ) node.innerHTML = this.template.html( { durations, sundial, timings } );
        else throw new Error( 'component_downtimebar: root node not found.' );
        return this;
    }
}
/*
*/
Downtimebar.prototype.type = Object.freeze( 'downtimebar' );
////////////////////
module.exports = Downtimebar;