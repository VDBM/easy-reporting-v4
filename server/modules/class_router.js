/*  class_router.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-19
*/
////////////////////
/*  API
*
*               - All routes start with a / , differentiating it from an event .
*               - Each fixed part is unique, but can have a variable number of routes associated with it .
*               - Do we want .call() and send a hash as a single parameter, or do we want .apply() and send multiple parameters?
*               - If we provide a context, the callback has to use the function keyword instead of an arrow, since we'[ll be using .apply() on it .
*               - We need to include the leading / in the routes to be able to use routes like /:line inside a report with its own namespace .
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
// No imports.
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Router {
    constructor() {
        this.history = [];
        this.routes = {};
    }
    checkHash() {
		let path;
		if ( process && process.browser && window ) {
			// There is no difference between route server/page and server/page/ . The browser will automatically route to server/page/ so we don't have to check for this condition.
			// Routing to /#/ directly will trigger hashchange and hence our route method.
			// Routing to anything shorter will not trigger hashchange and hence not the route method.
			// So if we want consistency, we need to remove any this.rotue() calls that will be detected by the hashchange event.
			// So we changed this function from routing to the path if it exists to replacing the state and triggering the route method manually if no hash exists.
			// The end result is that route / will always be triggered and that we will always have the hash /#/ if no hash exists.
			// We still want to return the path, so any other application logic that has to happen upon route / can be bound to either the / route handler OR by handlign the return value of this method.
			// This way a module has a choice, since the checkInitial can be called as a method while the hashchange is only trigegred by changing the hash some way.
			if ( window.location.hash && window.location.hash.length > 2 ) path = window.location.hash;
			else {
				path = '#/';
				window.history.replaceState( window.history.state, '', `${ window.location.origin }${ window.location.pathname }${ path }` );
			}
			this.route( path );
			return path;
		}
		else return null;
    }
    init() {
        if ( process && process.browser && window ) window.addEventListener( 'hashchange', event => this.checkHash() );
        return { 'router' : true };
    }
    register( route, callback, context = null ) {
        // 1) Split the route into fixed and variable parts.
        const first_parameter = route.indexOf( ':' );
        // If the index of the first occurence of ':' equals -1, there are no parameters inside the route .
        const base_route = first_parameter > 0
            ? route.slice( 0, first_parameter )
            : route;
        // Likewise, no need to search for parameters, if there aren't any .
        const variables = first_parameter > 0
            ? route
                .slice( first_parameter )
                .split( '/' )
                .map( variable => variable.slice( 1 ) )
            : [];
        // 2) Check if the route namespace already exists or not . If not, create it .
        if ( !this.routes[ base_route ] ) this.routes[ base_route ] = {};
        // 3) Register the fixed part of the route, using the length of the variable parts as the key, the variables themselves and callback as the value .
        this.routes[ base_route ][ variables.length ] = { callback, context, variables };
        return { 'route' : base_route };
    }
	/*
	*	The uri_hash will always start with #/
	*/
    route( uri_hash ) {
		let match = null;
        let parameters = [];
		let chunked_route = uri_hash.slice( 1 );
		// Update our own history
		this.history.push( uri_hash );
		// Find the route handler, if any.
        // We start off with the entire cloned uri .
        // If it immediately matches a route, we directly found the correct route with 0 parameters .
        // Else we chop off the last / delimited part of the route and up the parameter count .
        // This continues until we have found the right base_route with the right amount of parameters, or we run out of chunks, signifying the route is not registered .
        // This logic checks the routes from more specific to less specific, so we can have specialized and general routes:
        // Eg. we can have two routes: '/report/line1' and '/report/:line' . The first will only match '/report/line1' and the other will match any other route starting with '/report/' .
        while ( chunked_route && !match ) {
            if ( this.routes.hasOwnProperty( chunked_route ) && this.routes[ chunked_route ].hasOwnProperty( parameters.length ) ) match = this.routes[ chunked_route ][ parameters.length ];
            else {
                const index = chunked_route.lastIndexOf( '/' );
                const parameter = chunked_route.slice( index + 1 );
                parameters.push( parameter );
                // If the last occurence of / is the very start of the route and we haven't found a match yet, try a final time to detect a global match on / with the correct amount of parameters .
                // So route '/:line' will be triggered by uri_hash '#/line1', but not by uri '#/line1/cf' .
                chunked_route = !index && parameter
                    ? '/'
                    : chunked_route.slice( 0, index );
            }
        }
        if ( match ) {
            parameters = parameters.reverse();
            const values = match
                .variables
                .reduce( ( hash, variable, index ) => { hash[ variable ] = parameters[ index ]; return hash; }, {} );
            if ( match.context ) match.callback.apply( match.context, [ values ] );
            else match.callback( values );
        }
    }
    unregister( route ) {
        // TBI
    }
}
/*
*/
Router.prototype.type = Object.freeze( 'router' );
////////////////////
module.exports = Router;