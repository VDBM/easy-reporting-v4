/*  component_marquee.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-15
*/
////////////////////
/*  API
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
// No imports.
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = 'Loading...';
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Marquee {
    constructor( name ) {
        this.name = name;
        this.root = `#er_marquee_${ name }`;
    }
    render( data = DEFAULT_DATA ) {
        const node = document.querySelector( this.root );
        if ( node ) {
            node.innerHTML = `<div>${ data }</div>`;
            node.classList.add( 'comp-marquee-active' );
        }
        else throw new Error( 'component_marquee: root node not found.' );
        return this;
    }
    update( data = DEFAULT_DATA ) {
        const node = document.querySelector( `${ this.root } > div` );
        if ( node ) node.textContent = data;
        else {
            console.log( 'component_marquee: root node not found.' );
            console.error( error );
            throw error;
        }
        return this;
    }
}
// Static type .
Marquee.prototype.type = Object.freeze( 'marquee' );
////////////////////
module.exports = Marquee;