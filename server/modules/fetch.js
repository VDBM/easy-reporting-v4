/*  fetch.mjs   => 
*   @language: ES6
*   @version:  v1.0.0
*   @creator:    VDBM
*   @date:   2018-03-12
*/
////////////////////
/*  fetch
*/
const fetch = ( function() {
    if ( process ) {
        // Browser
        if ( process.browser ) {
            // Browser supports fetch, return it .
            if ( window.fetch ) {
                return window.fetch;
            }
            // No fetch, mimic the API through an AJAX call .
            else {
                return ( url, { body = null, headers = {}, method = 'GET' } = {} ) => new Promise( ( resolve, reject ) => {
                    const request = new XMLHttpRequest();
                    /*
                    if ( Object.keys( headers ).length ) {
                        Object
                            .entries( headers )
                            .forEach( ( [ header, value ] ) => request.setRequestHeader( header, value ) );
                    }
                    */
                    request.navigationAbort = false;
                    request.onload = () => {
                        const type = request.getResponseHeader( 'Content-Type' );
                        try {
                            const resource = type === 'application/json' && request.responseText
                                ? JSON.parse( request.responseText )
                                : request.responseXML || request.responseText;
                            resolve( resource );
                        }
                        catch( error ) {
                            if ( type ) reject( { error, 'text' : `fetch.onload: failed to parse response - ${ url }` } );
                            else reject( { error, 'text' : `fetch.onload: invalid response - ${ type }` } );
                        }
                    };
                    request.onerror = error => reject( { error, 'text' : 'fetch onerror handler' } );
                    request.open( method, url );
                    // CORS and IE10-IE11 withCredentials fix: the object state has to be OPEN to set the credentials flag in IE10-IE11
                    request.withCredentials = true;
                    // IE7-9 domain request premature abort fix.
                    // http://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
                    // xhr.timeout = 2000;
                    request.ontimeout = error => reject( { error, 'text' : 'fetch ontimeout handler' } );
                    request.onabort = error => reject( { error, 'text' : 'fetch onabort handler' } );
                    request.onprogress = () => void( 0 );
                    if ( body ) request.send( body );
                    else request.send();
                } );
            }
        }
        else if ( process.release && process.release.name === 'node' ) {
            // If we put all our links relative
            const HTTP = require( 'http' );
            return uri => new Promise( ( resolve, reject ) => {
                const chunks = [];
                HTTP
                    .get( uri, response => {
                        response.on( 'data', data => chunks.push( data ) );
                        response.on( 'end', () => resolve( Buffer.concat( chunks ).toString() ) );
                    } )
                    .on( 'error', error => reject( error ) );
            } );
        }
    }
    else throw new Error( 'enviroment not supported' );
} () );
/*  root
*/
const root = 'http://mecvm21/api';
/*  root_resources
*/
const root_resources = 'http://mecvm21/resources';
////////////////////
module.exports = { fetch, root_resources, root };