/*  transformations.mjs   =>
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-23
*/
////////////////////
// ${ imports }
const { isObj } = require( './util.js' );
////////////////////
/*  cache               =>
*/
const cache = {};
/*  transform           =>
*   @notes:
*                       - Each resource will have multiple transformations that get resolved in order:
*                         1) MODEL: The base transformation defined inside the .model file.
*                            This transformation will perform extra calculations on the resultset to put it into a standardized format.
*                            Each resource can have a seperate transformation.
*                         2) SORT: If we have a timespan over a month, we'll have gathered the data in multiple chunks, thereby invalidating the ORDER BY clause.
*                            So resort the resultset if needed.
*                            We need to sort before doing any other operation, so any groupings will automatically be sorted as well.
*                         3) GROUPING: the used group parameter.
*                            Divide the resource entries into distinct data parts. Each part will be handled seperately in the following transformations, as if they were different resources.
*                            We mainly use this to group entries into timebased groups, so we can calculate statistics per shift / day / month / ...
*                            This has to be extended further to allow for grouping on a certain property as well.
*                         4) AGGREGATION: the used aggregation parameter.
*                            Calculate a derived value from the resource or groups of resources. These are you standard statistical values and specific aggregations.
*                            Eg: raw: full entry representatrion / count: amount of records / avg: average per grouping over timeframe
*                         5) OUTPUT: We support different ways to visually represent  or package the resource response.
*                            Eg: json / text / table / table without headers / table with grouping aggregation / multiple tables.
*                            By offering multiple table formats, we make it easier to import the data correctly into excel.
*/
const transform = ( resource, parameters, queryModel, dependencies ) => {
    const transformation_exceptions = queryModel.transformation_exceptions || [];
    const transformation_names = {
        'model' : queryModel.transformation,
        'sort' : parameters.hasOwnProperty( 'sort' ) ? parameters.sort : 'sort_line_date',
        'group' : parameters.group,
        'aggregation' : parameters.aggregation,
        'output' : parameters.output
    };
    // If the transformation exists and is not cached yet, load it.
    const transformations = Object.values( transformation_names )
        .filter( transformation_name => !!transformation_name )
        .map( transformation_name => /* !cache[ name ] ? ( cache[ name ] = */ require( `../transformations/${ transformation_name }` )/* ) && cache[ name ] : cache[ name ]*/ );
    return transformations.reduce( ( resource, transformation ) => transformation( resource, parameters, dependencies ), resource );
};
////////////////////
module.exports = { transform };