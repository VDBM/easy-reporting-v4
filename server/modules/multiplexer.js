/*  multiplexer.mjs         =>
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-02
*/
////////////////////
//import
////////////////////
/*
    1s = 1000ms
    1m = 60s = 60,000ms
    1h = 60m = 3,600s = 3,600,000ms
    1d = 24h = 1,440m = 86,400s = 86,400,000ms
    1M = 30d = 720h = 43,200m = 2,592,000s = 2,592,000,000ms
*/
const chunkTimes = ( parameters, chunkSize ) => {
    if ( !parameters.starttime || !parameters.endtime ) return [ parameters ];
    else {
        const ms = chunkSize || 2592000000;
        const startpoint = new Date( parameters.starttime ).getTime();
        const endpoint = new Date( parameters.endtime ).getTime();
        // If the requested timings are shorter than 30 days, we don't need to chunk and just return the original parameters inside an array.
        if ( startpoint + ms > endpoint ) return [ parameters ];
        else {
            const chunkedTimes = [];
            let next = startpoint;
            while ( next < endpoint ) {
                let nextStart = next;
                let nextEnd = null;
                next += ms;
                if ( next < endpoint ) {
                    nextEnd = next;
                    // Add a milisecond
                    next += 1;
                }
                else nextEnd = endpoint;
                chunkedTimes.push( {
                    'starttime' : new Date( nextStart ).toJSON(),
                    'endtime' : new Date( nextEnd ).toJSON()
                } );
            }
            return chunkedTimes.map( update => Object.assign( {}, parameters, update ) );
        }
    }
};
/*
*/
const merge = resourceChunks => {
    const reference_headers = JSON.stringify( resourceChunks[ 0 ].headers );
    const reference_codes = JSON.stringify( resourceChunks[ 0 ].http_status_code );
    const all_valid = resourceChunks.every( chunk => JSON.stringify( chunk.headers ) === reference_headers && JSON.stringify( chunk.http_status_code ) === reference_codes );
    if ( all_valid ) return Object.assign( resourceChunks[ 0 ], { "data" : resourceChunks.reduce( ( resource, chunk ) => resource.concat( chunk.data ), [] ) } );
    else throw new Error( { "data" : "failed merging resource", "headers" : {}, "http_status_code" : 500 } );
};

////////////////////
module.exports = { chunkTimes, merge };