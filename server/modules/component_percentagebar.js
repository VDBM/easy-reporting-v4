/*  component_percentagebar.mjs =>
*   @language:  ES6
*   @version:   1.0.0
*   @creator:      VDBM
*   @date:      2018-03-27
*/
////////////////////
/*  API
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
// No imports.
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Percentagebar {
    constructor( name ) {
        this.name = name;
        this.root = `#er_percentagebar_${ name }`;
    }
    render() {
        // We actually have to create the nodes in advance adn then update their style property if we want ot be able to use css transitions for animation .
        const node = document.querySelector( this.root );
        if ( node ) node.innerHTML = '<div class="comp-percbar"><div class="">0</div></div>';
        return this;
    }
    update( data, min = 0, max, limit ) {
        const node = document.querySelector( `${ this.root } > div.comp-percbar > div` );
        const height = 100 / ( max - min ) * data;
        const cls = data >= limit
            ? 'comp-percbar-ool'
            : '';
        if ( node ) {
            node.setAttribute( 'class', cls );
            node.style.height = `${ height }%`;
            node.innerHTML = data;
        }
        else throw new Error( `component_percentagebar - ${ this.root }: root node not found.` );
        return this;
    }
}
/*
*/
Percentagebar.prototype.type = Object.freeze( 'percentagebar' );
////////////////////
module.exports = Percentagebar;