            'progressbar' : {
                element : null,
                timer : null,
                html( resource ) {
                    return `<h3 class="mar-b-16 mar-t-perc10 ta-c">${ $.format.capitalize( resource ) }</h3><progress id="dex_progressbar" max="100" value="0" class="bor-grey bor-round h-p-24 mar-autocenter w-50"></progress>`;
                },
                // This can propably be merged into start() stop(), but very low prio refactoring.
                reset() {
                    $.util.removeCls( $( 'body' ), 'cursor-busy' );
                    if ( this.timer ) {
                        clearInterval( this.timer );
                        this.timer = null;
                    }
                    if ( this.element ) this.element = null;
                },
                restart() {
                    this.element.value = 0;
                    this.stop();
                    // Defer so we can render the empty bar first. Else our timer will tick so fast that we'll already render 15% by the time we reset the value to 0
                    setTimeout( function() {
                        this.start();
                    }.bind( this ), 1000 );
                },
                start() {
                    $.util.setCls( $( 'body' ), 'cursor-busy' );
                    this.timer = setInterval( function() {
                        if ( !this.element ) this.element = $( '#dex_progressbar' );
                        if ( this.element ) {
                            $.util.removeCls( this.element, 'hidden' );
                            if ( this.element.value < 100 ) this.element.value += 1;
                            else this.restart();
                        }
                    }.bind( this ), 50 );
                },
                stop() {
                    $.util.removeCls( $( 'body' ), 'cursor-busy' );
                    if ( this.element ) $.util.setCls( this.element, 'hidden' );
                    clearInterval( this.timer );
                    this.timer = null;
                    this.element = null;
                }
            },