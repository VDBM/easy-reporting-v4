/*  component_menu_list.mjs     =>
*   @language:      ES6
*   @version:       v1.0.0
*   @creator:          VDBM
*   @date:          2018-05-09
*/
////////////////////
/*  API
*
*   const menu_list = new Menu_list( { "event": "function", "name": "string", "style": "string" } );
*   menu_list.html();
*   menu_list.html( [] );
*   menu_list.render();
*   menu_list.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { nodeParent } = require( './util.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const PATH_CSS = './resources/component_menu_list.css';
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [
    { "content": "primary" },
    { "content": "secondary" },
    { "content": "tertiary" },
    { "content": "quaternary" },
    { "content": "quinary" }
];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Menu_list
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Menu_list {
    // Create an instance.
    constructor( { event = null, name, style = '' } = {} ) {
        this.data = null;
        this.event = event;
        this.name = name;
        this.root = `#menu_list_${ name }`;
        this.state = null;
        this.style = style;
    }
    // Add an event handler to a target, usually the container of the component.
    // Either bind the function, or add the data attributes that a mediator will look for .
    bind( target ) {
        const node = target || document.querySelector( this.root );
        if ( !node ) throw new Error( `component menu_list_${ this.name } - root undefined, can't bind event` );
        else {
            if ( !this.event || isFn( this.event ) ) node.addEventListener( 'click', this.onclick.bind( this ) );
            else if ( isObj( this.event ) ) Object.entries( this.event ).forEach( ( [ attribute, value ] ) => node.setAttribute( attribute, value ) );
        }
        return this;
    }
	highlight( item ) {
		// Activate the new item, if it exists. So sending null === resetting
		if ( item ) item.classList.toggle( 'active' );
		// Deactivate the previous active item
		if ( this.state ) this.state.classList.toggle( 'active' );
		// Save the new state.
		this.update( { "state": item } );
	}
    // Return the html, after populating it with array data. If no data is provided, the DEFAULT_DATA will be used.
    // The operand, relation, expectation parameters provide compatibility with loops in our our template engine.
    // For other functionality, `html( data )` usually suffices.
    html( data, operand, relation, expectation ) {
		const data_source = data || this.data || DEFAULT_DATA;
        const list = `<ul id="menu_list_${ this.name }" class="comp-menu-list ${ this.style }">`;
        const items = data_source.map( item => `<li${ item.attribute || '' }>${ item.content }</li>` );
        return `${ list }${ items.join( '' ) }</ul>`;
    }
    // Its possible that the content also contains HTML nodes that we click on, so we need to find the actual list item, if the target isnt a list item.
    onclick( event ) {
        const item = nodeParent( event.target, 'li' );
        if ( item ) this.highlight( item );
        if ( this.event ) this.event( item );
        return this;
    }
    // Render the component to the root element.
    render( outer ) {
        const node = document.querySelector( this.root );
        if ( !node ) throw new Error( `component menu_list_${ this.name } - root undefined, can't render.` );
        else {
			node.innerHTML = this.html();
            this.bind( node );
        }
        return this;
    }
    //
    route( hash ) {
        window.location.hash = hash;
        this.onclick( { 'target' : document.querySelector( `#menu_list_${ this.name } a[href="${ hash }"]` ) } );         
    }
    // Update our properties.
    update( source ) {
        if ( source.hasOwnProperty( 'data' ) ) this.data = source.data;
        if ( source.hasOwnProperty( 'root' ) ) this.root = source.root;
        if ( source.hasOwnProperty( 'state' ) ) this.state = source.state;
        if ( source.hasOwnProperty( 'style' ) ) this.style = source.style;
        return this;
    }
	updateLinks() {
		const items = this.data.map( item => item.content );
		const nodes = document.querySelectorAll( `${ this.root } > li` );
		Array.from( nodes ).forEach( ( node, index ) => { node.innerHTML = items[ index ] } );
		return this;
	}
}
// Static type.
Menu_list.prototype.type = Object.freeze( 'menu_list' );
////////////////////
module.exports = Menu_list;