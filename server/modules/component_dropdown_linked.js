/*  component_dropdown_linked.mjs   =>
*   @language:  ES6
*   @version:   1.1.0
*   @creator:      VDBM
*   @date:      2018-05-09
*/
//      TODO: include the internationalization, as used in component_dropdown.
////////////////////
/*  API
*
*   const dropdown_linked = new Dropdown_linked( { "event": "function", "name": "string", "style": "string" } );
*   dropdown_linked.html();
*   dropdown_linked.html( [] );
*   dropdown_linked.render();
*   dropdown_linked.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { capitalize, isFn, isObj, nodeClear, translate } = require( './util.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const PATH_CSS = './resources/component_dropdown_linked.css';
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Dropdown_linked
*   @param      Object
*                   Function event
*                   Array labels
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               - Since we need to add event handlers to the select nodes, we kind of need a mechanism that will actually render the nodes and add event handlers to them.
*                 So we can check the 'event' property of the instance. If it's a function, we need to attach the event handlers ourselves, if it's a string, we can just add it as an event attribute  to the select node.
*                 => OK: instance.bind()
*               - We can choose between clearing each select fully and re-adding the default, or we could not remove the default value and hide the entire select.
*/
class Dropdown_linked {
    // Create an instance.
    constructor( { event = null, labels = [], name, style = '' } = {} ) {
        this.data = null;
        this.event = event;
        this.i18n = null;
        this.labels = labels;
        this.name = name;
        this.root = `#dropdown_linked_${ name }`;
        this.state = labels.reduce( ( obj, link ) => { obj[ link ] = null; return obj; }, {} );
        this.style = style;
    }
    // Add an event handler to a target, usually the container of the component.
    bind( target ) {
        const node = target || document.querySelector( this.root );
        if ( !node ) throw new Error( `component dropdown_linked_${ this.name } - root undefined, can't bind event` );
        else {
            const dropdowns = Array.from( node.querySelectorAll( `select[id=^"dropdown_linked_${ this.name }_]` ) );
            dropdowns.forEach( dropdown => dropdown.addEventListener( 'change', this.onchange.bind( this ) ) );
        }
        return this;
    }
    // Return the html, after populating it with array data. If no data is provided, the data property or DEFAULT_DATA will be used.
    // The operand, relation, expectation parameters provide compatibility with loops in our our template engine.
    // For other functionality, `html( data )` usually suffices.  
    html( data, operand, relation, expectation ) {
        const data_source = data || this.data || DEFAULT_DATA;
        const events = this.event
            ? this.labels.map( () => Object
                .entries( this.event )
                .map( ( [ attribute, value ] ) => `${ attribute }="${ value }"` )
                .join( ' ' )
              )
            : null;
        // Create all dropdowns .
        const dropdowns = this.labels
            .map( ( label, index ) => `<label class="${ index ? 'dis-hidden' : '' } ib pad-4 ta-r">${ capitalize( label ) }:</label><select id="dropdown_linked_${ this.name }_${ label }" class="comp-dropdown-linked ${ this.style }${ index ? ' dis-hidden' : '' }" ${ events && events[ index ] }><option value="default" selected="true">Select</option></select>` );
        // Create the options for the initial dropdown .
        const option_type = this.labels[ 0 ];
        const options = data_source
            .filter( collection => collection.type === option_type )
            .map( collection => `<option value="${ collection.name.trim() }">${ collection.name.trim() }</option>` );
        // Add the options to the first select tag that will always be visible . 
        if ( dropdowns.length ) dropdowns[ 0 ] = dropdowns[ 0 ].replace( '</select>', `${ options.join( '' ) }</select>` );
        return dropdowns.join( '' );
    }
    // Since handleEvent() method of the EventListener interface only exposes one parameter, adding a second parameter to this 'onchange' will not break anything if this method is added as an event handler.
    onchange( event, node = null ) { 
        const type = event.target.id.split( '_' ).pop();
        this.updateState( type, event.target.value )
            .renderLink( type, event.target.value, node );
        if ( isFn( this.event ) ) this.event( event );
        return this;
    }
    // Render the component to the root element and bind the event handler.
    render() {
        const node = document.querySelector( this.root );
        if ( !node ) throw new Error( `component dropdown_linked_${ this.name } - node undefined, can't render` );
        else {
            node.innerHTML = this.html();
            this.bind( node );
        }
        return this;
    }
    renderLink( type, value, node ) {
        // By providing an extra parameter to add a node, we can send a document fragment containing the prepared HTML to update it Before actually rendering it.
        const dropdown = node || document;
        const next_index = this.labels.indexOf( type ) + 1;
        const next_type = this.labels[ next_index ];
        const data_options = Object
            .entries( this.state )
            .reduce( ( collection, [ state_type, state_value ] ) => {
                if ( state_value ) return ( collection.find( item => item.type === state_type && item.name.trim() === state_value ) || { "data" : [] } ).data;
                else return collection;
            }, this.data );
        const options = [ '<option value="default" selected="true">Select</option>', ...data_options.map( collection => `<option value="${ collection.name.trim() }">${ collection.name.trim() }</option>` ) ];
        // Find all the selects .
        const selects = Array.from( dropdown.querySelectorAll( `select[id^="dropdown_linked_${ this.name }"]` ) );
        selects.forEach( ( select, index ) => {
            const is_hidden = select.classList.contains( 'dis-hidden' );
            if ( index < next_index && is_hidden ) {
                select.previousSibling.classList.remove( 'dis-hidden' );
                select.classList.remove( 'dis-hidden' );
            }
            else if ( index === next_index ) {
                nodeClear( select );
                if ( options.length > 1 ) {
                    if ( is_hidden ) {
                        select.previousSibling.classList.remove( 'dis-hidden' );
                        select.classList.remove( 'dis-hidden' );
                    }
                    select.innerHTML = options.join( '' );
                }
                else if ( !is_hidden ) {
                    select.previousSibling.classList.add( 'dis-hidden' );
                    select.classList.add( 'dis-hidden' );
                }
            }
            else if ( index > next_index && !is_hidden ) {
                nodeClear( select );
                select.previousSibling.classList.add( 'dis-hidden' );
                select.classList.add( 'dis-hidden' );
            }
        } );
        return this;
    }
    setSelections( place ) {
		//const updateNode = function( node, value ) { return function() { console.log( node, ' => ', value ); node.value = value; }; };
		//const updates = [];
		Object.entries( place ).forEach(([ property, value ]) => {
			if ( this.state.hasOwnProperty( property ) ) {
				const node = document.querySelector( `${ this.root }_${ property }` );
				const updated_value = value.trim();
				this.updateState( property, updated_value )
					.renderLink( property, updated_value );
				node.value = updated_value;
				//updates.push( updateNode( node, value ) );
			}
		});
		//setTimeout( function() {updates.forEach( update => update() ); }, 1000 );
	}
    // Remove an event handler from a target, usually the container of the component.
    unbind( target ) {
        const node = target || document.querySelector( this.root );
        if ( !node ) throw new Error( `component dropdown_linked_${ this.name } - root undefined, can't bind event` );
        else {
            if ( !this.event || isFn( this.event ) ) node.removeEventListener( 'change', this.onchange.bind( this ) );
            else if ( isObj( this.event ) ) Object.entries( this.event ).forEach( ( [ attribute, value ] ) => node.removeAttribute( attribute ) );
        }
        return this;
    }
    // Update our properties.
    update( source ) {
        // Since state is defined by the labels, changing the labels should also change the state.
        // Changing state directly has been removed.
        if ( source.data ) this.data = source.data;
        if ( source.i18n ) this.i18n = source.i18n;
        if ( source.labels ) {
            this.labels = source.labels;
            this.state = source.labels.reduce( ( obj, link ) => { obj[ link ] = null; return obj; }, {} );
        }
        if ( source.root ) this.root = source.root;
        if ( source.style ) this.style = source.style;
        return this;
    }
    updateState( type, value ) {
        // We need to loop over the entire state, so we can reset all deeper levels if a higher level gets changed .
        let updated = false;
        Object.keys( this.state ).forEach( key => {
            if ( updated ) this.state[ key ] = null;
            if ( key === type ) {
                this.state[ key ] = value;
                updated = true;
            }
        } );
        return this;
    }
}
// Static type.
Dropdown_linked.prototype.type = Object.freeze( 'dropdown_linked' );
////////////////////
module.exports = Dropdown_linked;