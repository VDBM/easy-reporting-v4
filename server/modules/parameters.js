/*  parameters.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-02
*/
////////////////////
// import UTIL from './util.mjs';
const UTIL_ER = require( './util.js' );
// import CONSTANTS from './constants.mjs';
const CONSTANTS = require( './constants.js' );
////////////////////
/*  REGEX TEST
    var regex = /^\/api\/(\w+)(?:$|\/(\d+))?(?:\?(?=(.+)?))?/;
    var uris = [
        '/api/idd',
        '/api/idd/123',
        '/api/idd?output=json',
        '/api/idd/245?output=json&filter=test'
    ];
    console.log( '-----' );
    uris.forEach( function( uri ) {
        console.log( uri );
        console.log( regex.exec( uri ) );
        console.log( '-----' );
    });
*/
/*
slash api slash (resourceName) [ slash (id) ] [ question (parameters) ]
CASE INSENSITIVE !
Repaled the /id part with /any sowe can capture paths
*/
//const regex_uri = /^\/api\/(\w+)(?:\/(.+))?(?:\?(?=(.+)?))?/i;
const regex_uri = /^\/api\/(\w+)(?:\/([\w\/]+))?(?:\?(?=(.+)?))?/i;
/*
*/
const regex_parameters = /^(\w+?)(=|<>|><|<|>)(.+)$/;
/*
*   notes:
*               - Date.toJSON() will return null when an invalid date is provided. Date.toISOString() will vary in output dependeing on the browser. ( invalid date error, string, etc )
*               - Since we use the parameters as the scope of a new Function() call to easily populate a template string ( namely the SQL code ), we need a plain old javascript object. ( POJO )
*               - Tried replacing this by using: const URL = require( 'url' ); const parameters = new URL().searchParams;
*                 But this returns a Map, not an object. Since the default version of Maps don't have a method to turn them into a POJO, we'd have to do that ourselves.
*                 So in the end the code we'd have to add just to use a Map instead of a POJO, is larger and more complex than just reducing the string ourselves into a POJO.
*   required to standardize:
*               - id
*               - line
*               - date
*               - period
*               - text
*/
const parse = uri => { // TODO: FIX THIS MESS
    // If our URL still ontains a '/', we know that some of its parameters are statically defined, like the ID.
    // Atm we can assume that the first part is always the ID, sicne it's the only one we've implemented so far.
    // Match first, then destructure, else we'll get iterator errors once the destructuring fails.
    const uri_match = regex_uri.exec( uri );
    if ( !uri_match ) throw new Error( `parameters.js - failed parsing URI - ${ uri }` );
    const [ source_uri, resourceName, path, parameter_string ] = uri_match;
    const parameters = parameter_string
        ? parameter_string.split( '&' )
        : []
    const chunks = uri.split( '?' );
    // TODO: update the logic to support following syntax.
    // = equals 
    // < smaller then
    // > bigger then
    // <> between
    // >< not between
    const parsed_parameters = parameters
        .map( parameter => parameter.match( regex_parameters ) )
        // Hash index 1 as the key with index 2 as the value.
        .reduce( UTIL_ER.propHash( 1, true, null, next => next[ 3 ] ), {} );
    if ( parsed_parameters.timeframe ) {
        // Calculate starttime and endtime from the period .
        // As string, since we need to charAt and slice .
        const current_shift = UTIL_ER.dateShift( Date.now() ).toString();
        const start_day = new Date();
        start_day.setUTCHours( 0, 0, 0, 0 );
        switch ( parsed_parameters.timeframe ) {
            case 'shift' :
                const start_shift = UTIL_ER.shiftDate( current_shift );
                parsed_parameters.starttime = new Date( start_shift.getTime() ).toJSON(); // clone
                parsed_parameters.endtime = new Date( start_shift.getTime() + CONSTANTS.MILISECONDS_SHIFT - 1000 ).toJSON();
            break;
            case 'today' :
                // The shift will already do the calculation to check if we're still inside the night shift of yesterday, eg. hour < 06:15:00
                // So we can just replace the last two numbers with 06, so we always have the mornign shift of the day the current shift belongs to.
                const start_morning_shift = current_shift.charAt( 7 ) === '6'
                    ? UTIL_ER.shiftDate( current_shift )
                    : UTIL_ER.shiftDate( `${ current_shift.slice( 0, 6 ) }06` );
                parsed_parameters.starttime = new Date( start_morning_shift.getTime() ).toJSON();
                parsed_parameters.endtime = new Date( start_morning_shift.getTime() + CONSTANTS.MILISECONDS_DAY - 1000 ).toJSON();
            break;
            case 'week' :
            
            break;
            case 'month' :
                // .setDate( 0 ) = last day of prev month
                // .setDate( 1 ) = first of this month
                // .setDate( 32 ) = first day of next month
                // Since Date setters returns the datetime is ms, we can't chain them.
                const first_current_month = new Date( start_day.getTime() );
                first_current_month.setUTCDate( 1 );
                // Find the first day of next month, get the miliseconds, subtract 1 second, create new Date Object
                // => last day of this month with time set at 23:59:59
                const first_next_month =  new Date( start_day.getTime() );
                first_next_month.setUTCDate( 32 );
                const last_current_month = new Date( first_next_month.getTime() - 1000 );
                parsed_parameters.starttime = new Date( first_current_month.getTime() ).toJSON();
                parsed_parameters.endtime = new Date( last_current_month.getTime() ).toJSON();
            break;
            case 'last_month' :
                // .setDate( 0 ) = last day of prev month
                // .setDate( 1 ) = first of this month
                // .setDate( 32 ) = first day of next month
                // Since Date setters returns the datetime is ms, we can't chain them.
                start_day.setUTCMonth( start_day.getUTCMonth() - 1 );
                const first_last_month = new Date( start_day.getTime() );
                first_last_month.setUTCDate( 1 );
                // Find the first day of next month, get the miliseconds, subtract 1 second, create new Date Object
                // => last day of this month with time set at 23:59:59
                const first =  new Date( start_day.getTime() );
                first.setUTCDate( 32 );
                const last = new Date( first.getTime() - 1000 );
                parsed_parameters.starttime = new Date( first_last_month.getTime() ).toJSON();
                parsed_parameters.endtime = new Date( last.getTime() ).toJSON();
                
                console.log( parsed_parameters );
            break;
            case 'period' :
            
            break;
            default :
                throw new Error( `timeframe '${ parsed_parameters.timeframe }' is not supported` );
            break;
        }
    }
    if ( parsed_parameters.starttime && !parsed_parameters.starttime_shift ) parsed_parameters.starttime_shift = UTIL_ER.dateShift( parsed_parameters.starttime );
    if ( parsed_parameters.endtime && !parsed_parameters.endtime_shift ) parsed_parameters.endtime_shift = UTIL_ER.dateShift( parsed_parameters.endtime );
    if ( parsed_parameters.headers ) parsed_parameters.headers = JSON.parse( parsed_parameters.headers );
    if ( parsed_parameters.output ) parsed_parameters.output = `output_${ parsed_parameters.output }`;
    else parsed_parameters.output = 'output_table';
    if ( parsed_parameters.group ) parsed_parameters.group = `group_${ parsed_parameters.group }`;
    if ( parsed_parameters.aggregation )  parsed_parameters.aggregation = `aggregation_${ parsed_parameters.aggregation }`;
    if ( path ) parsed_parameters.path = path;
    return Object.assign( { resourceName }, parsed_parameters );
};
////////////////////
module.exports = { parse };