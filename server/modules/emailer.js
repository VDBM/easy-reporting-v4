/*  emailer.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-26
*/
// WATCHOUT:
////////////////////
// import
const MAIL = require( 'nodemailer' );
////////////////////
/*  emailer    =>
*   @type           public Function
*   @name           emailer
*   @param          Array resource                - 
*   @return         Object => "type", "data"
*   @notes
*                   -
*/
const emailer = ( message ) => {
    var connection = MAIL.createTransport( {
        'service' : 'gmail',
        'auth' : {
            'user' : 'mathias.vandenbroeck@gmail.com',
            'pass' : 'FreeLeague1'
        }
    } );
    const mail_options = {
        'from' : 'mathias.vandenbroeck@gmail.com',
        'to' : 'mathias.vandenbroeck@gmail.com',
        'subject' : 'mecvm21_mailer',
        'html' : 'Hello World!'
    };
    connection.verify( function( err, info ) {
        if ( err ) console.log( err );
        else console.log( info );
    } );
};
////////////////////
module.exports = emailer;