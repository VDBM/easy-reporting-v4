/*  rest.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.4
*   @entry_user:    VDBM
*   @update_date:   2017-12-12
*/
////////////////////
// import AD from '../connectors.active_directory.connector.js'
const ACTIVE_DIRECTORY = require( '../connectors/active_directory.connector.js' );
// import ADMIN from '';
const ADMIN = require( '../connectors/admin.connector.js' );
// import COCKPIT from '../connectors/cockpit.connector.mjs';
const COCKPIT = require( '../connectors/cockpit.connector.js' );
// import
const DEPAL = require( '../connectors/depal.connector.js' );
// import EASYREPORTING from '../connectors/easyreporting.connector.mjs';
const EASYREPORTING = require( '../connectors/easyreporting.connector.js' );
// import EXCEL from '../connectors/exce.connector.mjs';
const EXCEL = require( '../connectors/excel.connector.js' );
// import FS from 'fs';
const FS = require( 'fs' );
// import MECPRI02 from '../connectors/mecpri02.connector.mjs';
const LOGBOOK = require( '../connectors/logbook.connector.js' );
// import MUX from './multiplexer.mjs';
const MUX = require( './multiplexer.js' );
// import output_json from '';
const output_json = require( '../transformations/output_json.js' );
// import { propTransform } from './util.mjs';
const { propTransform } = require( './util.js' );
// import PHD from '../connectors/phd.connector.mjs';
const PHD = require( '../connectors/phd.connector.js' );
// import PROFICY from '../connectors/proficy.connector.mjs';
const PROFICY = require( '../connectors/proficy.connector.js' );
// import PSION from '../connectors/psion.connector.mjs';
const PSION = require( '../connectors/psion.connector.js' );
// import
const RANDOMIZER = require( '../connectors/randomizer.connector.js' );
// import RAW_MATERIALS from '../connectors/raw_materials.connector.mjs';
const RAW_MATERIALS = require( '../connectors/raw_materials.connector.js' );
// import STATICS from '../connectors/statics.connector.mjs';
const STATICS = require( '../connectors/statics.connector.js' );
// import TRANSFORM from './modules/transformations.mjs';
const TRANSFORMER = require( './transformations.js' );
//
const { promisify } = require( 'util' );
// import WEBSERVER from '../connectors/webserver.connector.mjs'
//const WEBSERVER = require( '../connectors/webserver.connector.js' );
////////////////////
const readFile = promisify( FS.readFile );
////////////////////
/*  DATA_SOURCES            =>
*   notes:
*                   - By keeping the import statements seperate from the DATA_SOURCES object, we can change the import code without the need to edit the import name everywhere.
*/
const DATA_SOURCES = { ACTIVE_DIRECTORY, ADMIN, COCKPIT, DEPAL, EASYREPORTING, EXCEL, LOGBOOK, PHD, PROFICY, PSION, RANDOMIZER, RAW_MATERIALS, STATICS };
/*  getModel                =>
*/
const getModel = resourceName => readFile( `./models/${ resourceName }.model.json` ).then( buffer => buffer.toString() ).then( model => JSON.parse( model ) );
/*  resourceCreate          => POST
// should return status code: 201 Created + Location of created resource or 409 Conflict if the resource already exists. Eg between checking if it exists before and actually creating the reosurce, another user might have done the same thing.
// can return 400 Bad Request
// can return 500 Internal Error
*/
const resourceCreate = ( resourceName, chunks_body ) => { 
return getModel( resourceName )
    // 1) Fetch the model first.
    .then( queryModel => [ DATA_SOURCES[ queryModel.servers[ 0 ] ], queryModel ] )
    .then( ( [ connector, queryModel ] ) => {
        // By providing json as the  output, we force a json response ( instead of having to do it after ). By settings sort to null, we skip the sorting .
        // The creation logic can be created by the connector transformation. We will have to extend this into using tempaltes as well for SQL based updates later on .
        return connector
            .create( queryModel, chunks_body )
            .then( resource => TRANSFORMER.transform( resource, { "output" : "output_json", "sort" : null }, queryModel, null ) );
    } );
};
/*  resourceDelete          => DELETE
*/
const resourceDelete = ( resourceName ) => { // 
return new Promise( ( resolve, reject ) => {
    resolve( `DELETE: ${ resourceName }` );
} );
};
/*  resourceRead - Alternative: This should be simplified this way, but shown code doesn't work.
const resourceRead2 = ( resoruceName, parameters ) => {
    const error = () => text => console.error( text );
    const queryModel = getModel( resourceName );
    const connector = queryModel.then(
        queryModel => DATA_SOURCES[ queryModel.servers[ 0 ] ],
        error( `Failed parsing model file: '${ resourceName }'. Typo in JSON?` )
    );
    const sqlTemplate = connector.then(
        connector => connector.template( queryModel.extending || resourceName ),
        error( `Failed finding connector for: '${ resourceName }'. Check the 'server' field in the corresponding .model file.` )
    );
    const dependencies = sqlTemplate.then(
        template => queryModel.dependencies.length
            ? Promise
                .all( queryModel.dependencies.map( dependency => resourceRead( dependency, Object.assign( {}, parameters, { 'resourceName' : dependency } ) ) ) )
                .then( dependencies => [ connector, queryModel, template, dependencies ] )
            : null,
        error( `Failed fetching dependencies for: '${ resourceName }'` )
    );
    
};
*/
/*  resourceRead            => GET
*/
const resourceRead = ( resourceName, parameters ) => { // should return status code: 200 OK
// Using this structure, we can add models, extending models, and transformations and use them as fixed parameters for an SQL query, hence creating a simple dynamic query pipeline without messing with overly convoluted SQL.
// There are cases where the .template() method of the connector used won't actually return any SQL code, since not all resource soruces will be databases.
// For example, in the excel connector, our template is the full path to the excel file. WE can reuse the model.extending field for this.
if ( resourceName === 'dev' ) {
    console.log( 'returning dev' );
    return Promise.resolve( TRANSFORMER.transform( [ {"id":1,"val":"tester"} ], parameters, {}, {} ) );
}
else {
    return getModel( resourceName )
        // 1) Fetch the model first.
        .then(
            queryModel => { return [ DATA_SOURCES[ queryModel.servers[ 0 ] ], queryModel ]; },
            error => { throw new Error( `Failed parsing model file: '${ resourceName }'. Typo in JSON?` ); }
        )
        // 2 )If the model extends an existing query, fetch the SQL ( or other type of template )of that query this model extends, else fetch the SQL with the same name as this resource .
        .then(
            // We need the nesting here to be able to return connector, queryModel AND the newly created template in one container .
            ( [ connector, queryModel ] ) => connector.fetchTemplate( queryModel.extending || resourceName ).then( template => [ connector, queryModel, template ] ),
            error => { throw new Error( `Failed finding connector for: '${ resourceName }'. Check the 'server' field in the corresponding .model file.` ); }
        )
        // 3) if the model contains any dependencies, fetch their result before creating the SQL for the main resource request and inject those dependency results into the population scope of the main resource SQL template .  
        //    Since we always want the dependencies as raw resources, we need to remove any aggregation / group / output parameters from the parameters object if we want to use the default TRANSFORMER .
        .then( ( [ connector, queryModel, template ] ) => queryModel.dependencies.length
            ? Promise
                .all( queryModel.dependencies.map( dependency => resourceRead( dependency, Object.assign( {}, parameters, { 'aggregation' : null, 'group' : null, 'resourceName' : dependency, 'output' : null } ) ) ) )
                .then( dependencies => [ connector, queryModel, template, dependencies ] )
            : [ connector, queryModel, template, null ]
        )
        // Once we have gathered the resource model, resource SQL template, any data we depend on and the source parameters, we can construct the query, run any transformations and return the result .
        .then( ( [ connector, queryModel, template, dependencies ] ) => {
            // Since both arrays and strings have length, length works for both.
            // Add the extra filters defined inside the model if appropriate.
            // Maybe 'injection' can be written in ES6 with spread syntax.
            const dependencyInjection = queryModel.dependencies.reduce( ( map, next, index ) => { map[ next ] = dependencies[ index ]; return map; }, {} );
            if ( ( !template || !template.length ) && queryModel.dependencies.length ) {
                const resource = queryModel.transformation ? [] : dependencyInjection[ queryModel.dependencies[ 0 ] ];
                // Due to the new structure of the transformator, we have to remove the output, group and aggregation parameters if they are inside the parameters object .
                // Else we'll create html / json or a grouping instead of raw data.
                return Promise.resolve( TRANSFORMER.transform( resource, Object.assign( {}, parameters, { 'aggregation' : null, 'group' : null, 'output' : null, 'sort' : null } ), queryModel, dependencyInjection ) );
            }
            else {
                // TODO: We want cleaner syntax here!!!
                const sourceData = Object.assign( {}, parameters, queryModel.filters.length ? { 'filters' : queryModel.filters } : { 'filters' : '' } );
                // Chunk the dates if needed.
                // Our SQL servers can handle the base implementation of 30 days each.
                // Our Illuminator PHD connector however, cannot handle a full 30 days. So we decided to use 7 day chunks until we find a better solution .
                const parameterChunks = MUX.chunkTimes( sourceData, connector.chunksize || null );
                // Create a seperate sql template for each chunk.
                // First listed server in the model is the default.
                // Some queries will just be aggregations of their respective dependencies, sent through their transformation.
                // So if this is the case, we can just hand the dependencies off to the transformation immediately to handle the dependencies inside the transformation.
                // If there's no transformation, we'll assume that the first dependency is the only one and that we can just return this dependency as the final results.
                // TODO: dobulecheck if we need to add the group and parameter/transform here as well.
                // TODO: triplecheck this now that we changed the transformation module.
                const sqlChunks = parameterChunks.map( parameterChunk => connector.populateTemplate( parameterChunk, dependencyInjection, template ) );
                // The transformer will sort our chunks byline and date again during the sort step.
                return Promise
                    .all( sqlChunks.map( connector.read ) )
                    .then( MUX.merge )
                    .then( resource => TRANSFORMER.transform( resource, parameters, queryModel, dependencyInjection ) );
            }
        } );
}
};
/*  resourceUpdate          => PUT
*/
const resourceUpdate = ( resourceName, parameters, chunks_body ) => {
	const id = parameters.path;
    if ( id ) {
        return getModel( resourceName )
            // 1) Fetch the model first.
            .then( queryModel => [ DATA_SOURCES[ queryModel.servers[ 0 ] ], queryModel ] )
            .then(([ connector, queryModel ]) => {
                // By providing json as the  output, we force a json response ( istead of having to do it after ). By settings sort to null, we skip the sorting .
                // The creation logic can be created by the connector transformation. We will have to extend this into using tempaltes as well for SQL based updates later on .
                return connector
                    .update( queryModel, id, chunks_body )
                    .then( resource => TRANSFORMER.transform( resource, { "output" : "output_json", "sort" : null }, queryModel, null ) );
            } );
    }
    else return Promise.reject( new Error( 'rest.resourceUpdate: no id specified' ) );
};
////////////////////
// export default {
//     'post' : createResource,
//     'delete' : deleteResource,
//     'get' : readResource,
//     'put' : updateResource
// };
module.exports = { 'delete' : resourceDelete, 'get' : resourceRead, 'post' : resourceCreate, 'put' : resourceUpdate };