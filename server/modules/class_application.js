/*  class_application.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.1
*   @entry_user:    VDBM
*   @update_date:   2018-06-04
*/
////////////////////
/*  API
*
*   ${ name_class }.create()
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const Cache = require( './class_cache.js' );
const Mediator = require( './class_mediator.js' );
const Router = require( './class_router.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Application
*   @param      Object
*                   Function init
*                   Function on
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Application {
    constructor( namespace, components_application, state ) {
        this.components_application = [
            new Cache(),
            new Router(),
            new Mediator(),
            ...components_application
        ];
        this.namespace = namespace;
        this.state = Object.assign( {}, state );
        // Alias it onto the namespace so we can address then directly through the pointer .
        this.components_application.forEach( component => {
            const reference = component.name
                ? `${ component.type }_${ component.name }`
                : component.type;
            this[ reference ] = component;
        } );
        
    }
    init( app_initialization, global = ( process && process.browser ? window : process ) ) {
        const initialized_components = this.components_application.map( component => component.init ? component.init() : null );
        const initializated_app = app_initialization
            ? app_initialization.call( this )
            : null;
        global[ this.namespace ] = this;
        return Promise.all( initializated_app ? [ initializated_app, ...initialized_components ] : initialized_components );
    }
    on( event, callback ) {
        const module = event.charAt( 0 ) === '/'
            ? 'router'
            : 'mediator';
        this[ module ].register( event, callback );
    }
}
/*
*/
Application.prototype.type = Object.freeze( 'application' );
////////////////////
module.exports = Application;