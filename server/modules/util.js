/*  util.mjs        => A collection of utility functions for both frontend and backend. These are exported as ES6 module methods.
*   @data_language: ES6
*   @data_version:  1.0.3
*   @entry_user:    VDBM
*   @update_date:   2018-01-31
*/
////////////////////
/*  callbackify
*   notes:
*               - We need scope here, so function instead of arrow.                         
*/
const callbackify = function callbackify( sourceFn ) {
    return ( ... parameters ) => {
        return sourceFn
            .apply( this, args.slice( 0, args.length - 1 ) )
            .then( resource => args[ args.length - 1 ]( null, resouce ) )
            .catch( error => args[ args.length - 1 ]( error ) );
    };
};
/*  colorRgbaHex: does not include alpha calculation, so only accurate for alpha = 1
const colorRgbaHex = rgba => {
    const { match, red, green, blue, alpha } = /rgba\(\s?(\d{1,3}),\s?(\d{1,3}),\s?(\d{1,3}),\s?(\d{1,3})\s?\)/.exec( rgba );
    return `#${ red.toString( 16 ) }${ blue.toString( 16 ) }${ green.toString( 16 ) }`; 
};
*/
/*  capitalize
*   notes:
*               - Returns an empty string if the source str is empty as well. This works due to charAt already returning an empty string if the source has length 0 .
*/
const capitalize = ( str = 'error' ) => `${ str.charAt( 0 ).toUpperCase() }${ str.slice( 1 ) }`;
/*  compose
*/
const compose = ( ...updateFunctions ) => sourceObj => updateFunctions.reduceRight( ( targetObj, updateFunction ) => updateFunction( targetObj ), sourceObj );
/*  dateDayOfYear
*/
const dateDayOfYear = datetimeStr => {
    const firstDayCurrentYear = new Date( datetimeStr );
    firstDayCurrentYear.setMonth( 0 );
    firstDayCurrentYear.setDate( 1 );
    firstDayCurrentYear.setHours( 0 );
    firstDayCurrentYear.setMinutes( 0 );
    firstDayCurrentYear.setSeconds( 0 );
    firstDayCurrentYear.setMilliseconds( 0 );
    return Math.ceil( ( new Date( datetimeStr ).getTime() - firstDayCurrentYear.getTime() ) / 86400000 );
};
/*
*   @notes:
*               - Since toJSON() returns UTC time and toLocaleString() returns the full text version, we need our own implementation to turn 2018-01-01T04:15:00.000Z into 2018-01-01 06:15:00
*/
const dateLocal = dateUTC => {
    const local_time = dateUTC.getTime() - ( new Date().getTimezoneOffset() * 60000 );
    return new Date( local_time ).toJSON();
};
/*  dateJulian
*/
const dateJulian = datetimeStr => {
    const chunk_year = new Date().getUTCFullYear().toString().charAt( 3 );
    let chunk_day = dateDayOfYear( datetimeStr ).toString();
    while ( chunk_day.length < 3 ) {
        chunk_day = `0${chunk_day}`;
    }
    return `${chunk_year}${chunk_day}`;
};
/*  dateSeries      =-> Calculate all dates between starttime and endtime according to an interval. This is mostly used to extrapolate the shifts / days / other interval that don't have events in them but still have to be reported.
*   @notes:
*               - Since we just want to create a series of shiftgroups, we can just use basic arithmetic. technically we could create actual dates and use dateShift() on each of them, but this seems way less efficient.
*               - If the future proves there's need for us to create actual date objects, we can change this later.
*               - Update 1: If we don't actually use dates, we dont really know when we pass a month border or something, so we can't do calcualtions on the shiftgroup alone, else we'd need to also calculate the last day of a month and such.
*/
const dateSeries = ( starttime, endtime, interval ) => {
    // Basic defense so we don't loop indefinately if no positive interval is provided.
    // We could expand the function to allow for negative intervals, but I can't think of any usecase immediately that can't be solved by using this method and basic math.
    // Maybe we should always calculate the startpoint according to the shift of the starttime so we can use non-shift-start dates?
    if ( !interval || interval < 1 ) return null;
    else {
        const date_starttime = starttime instanceof Date
            ? starttime
            : new Date( starttime );
        const date_endtime = endtime instanceof Date
            ? endtime
            : new Date( endtime );
        const endpoint = date_endtime.getTime();
        let next = date_starttime.getTime(); // Since we can do actual calculations with miliseconds, we can just use the number here, no need to clone yet .
        let series = [];
        while ( next < endpoint ) {
            series.push( new Date( next ) );
            next += interval;
        }
        return series;
    }
};
/*  dateShift
*   @notes
*           - Since our local shifts start at 06:15 local time, we have to change this function to look at 04:15, 12:15, 20:15 UTC as the real starttime of shifts .
*           - Hours before 06:15 are part of the night shift of the previous day.
*           - [ 2, 0, 1, 8, -, 0, 2, -, 0, 1, 'T', 0, 0, :, 0, 0, :, 0, 0 ]
*           - [ 0, 0, 1, 1, 0, 1, 1, 0, 1, 1 ]
*/
const dateShift = source => {
    const datetime = source instanceof Date ? source : new Date( source );
    const offset_dst = datetime.getTimezoneOffset() / 60 * 100;
    const timeInt = datetime.getUTCHours() * 100 + datetime.getUTCMinutes();
    // We assume that the shfit begins at 06:15 loal time, so if we use UTC time as the format, we ned to add/subtract the timezone difference to the shift start hour .
    const shift_early = 615 + offset_dst;
    const shift_late = 1415 + offset_dst;
    const shift_night = 2215 + offset_dst;
    let suffix;
    if ( timeInt < shift_early || timeInt >= shift_night ) suffix = '22';
    else if ( timeInt >= shift_early && timeInt < shift_late ) suffix = '06';
    else if ( timeInt >= shift_late && timeInt < shift_night ) suffix = '14';
    const shiftDay = timeInt < shift_early
        ? new Date( datetime.getTime() - 86400000 )
        : datetime;
    return parseInt( [
        shiftDay.getUTCFullYear().toString().slice( 2 ),
        ( shiftDay.getUTCMonth() + 1 ).toString().padStart( 2, '0' ),
        shiftDay.getUTCDate().toString().padStart( 2, '0' ),
        suffix
    ].join( '' ), 10 );
};
/*  dateUTC
*   - 1min = 60 sec = 60000 ms
*/
const dateUTC = () => Date.now() - ( new Date().getTimezoneOffset() * 60000 );
/*  debounce
*/
const debounce = () => { throw new Error( 'util/debounce is not implemented' ); };
/*  flattenCollection
*   notes:
*           - All collections will have at least a 'data' property on each object inside the collection. Extracting other info from the collection member can be done inside the transformation.
*             So this function will use the transformation to operate on the current item and will check if the current item contains nested data that also needs to be handled.
*/
const flattenCollection = transformation => ( collection, collection_entry ) => collection.concat( [ transformation( collection_entry ), ...collection_entry.data.reduce( flattenCollection( transformation ), [] ) ] );
/*  inspectError => public function extends util triggers console.log
* @name     inspectError
* @param    Object info
*               Object data
*               Error error
*               String source
*               String text
*/
const inspectError = function inspectError( info ) {
	if ( info.source ) console.log( `----- ERROR: ${new Date().toLocaleString()} @ ${info.source} -----` );
    if ( info.text ) console.log( info.text );
    if ( info.data ) isObj( info.data ) ? console.dir( info.data ) : console.log( info.data );
	if ( info.error ) console.log( info.error.stack || info.error );
	console.log( '--- End Of Error ---' );
};
/*  intRandom
*/
const intRandom = ( min, max ) => Math.floor( Math.random() * ( max - min + 1 ) ) + min;
/*  isFn            => Is the source a Function?
*   @type           public Function
*   @name           isFn
*   @param          Any source
*   @return         Boolean
*/
const isFn = source => Object.prototype.toString.call( source ) === '[object Function]';
/*  isNumber       => Is the source an Integer or a Float?
*   @type           public Function
*   @name           isNumber
*   @param          Any source
*   @return         Boolean
*/
const isNumber = source => !isNaN( parseFloat( source, 10 ) ) && isFinite( source );
/*  isObj           => Is the source an Object?
*   @type           public Function
*   @name           isObj
*   @param          Any source
*   @return         Boolean
*/
const isObj = source => Object.prototype.toString.call( source ) === '[object Object]';
/*
*/
const isStr = source => Object.prototype.toString.call( source ) === '[object String]';
/*  mapLog
*/
const mapLog = ( rows ) => {
    if ( rows && Array.isArray( rows ) ) {
        rows.forEach( row => {
            const keys = Object.keys( row );
            const values = Object.values( row );
            keys.forEach( ( key, index ) => {
                console.log( `${ key }: ${ values[ index ] }` );
            } );
            console.log( '-----' );
        } );
    }
    else console.log( `rows is not an array:\n${ rows }` );
    return rows;
};
/*  nodeClear
*/
const nodeClear = node => {
    while ( node.firstChild ) {
        node.removeChild( node.lastChild );
    }
    return null;
};
/*  nodeParent
*/
const nodeParent = ( node, type ) => {
    let currentNode = node;
    let currentType = node.nodeName;
    const typeToFind = type.toUpperCase();
    if ( currentType === typeToFind ) return node;
    else {
        while (true) {
            currentNode = currentNode.parentNode;
            currentType = currentNode.nodeName;
            if ( currentType === typeToFind ) return currentNode;
            else if ( currentType === 'HTML' ) return false;
        }
    }
};
/*  nodeUpdate      => Update nodes using DOM methods instead of string methods. Usefull to set default selected values on selects without the need to include it in HTML templates.
*/
const nodeUpdate = ( source, updates ) => {
    const fragment = document.createDocumentFragment();
    const node = isStr( source )
        ? document.createRange().createContextualFragment( source )
        : source;
    fragment.appendChild( node );
    Object
        .entries( updates )
        .forEach( ( [ target, value ] ) => {
            const node = fragment.querySelector( target );
            if ( node ) node.value = value;
            else console.error( `nodeUpdate failed - node ${ target } not found` );
        } );
    return fragment;
};
/*  NOOP            => Do nothing
*   @type           public function
*   @name           NOOP
*/
const NOOP = () => {};
/*  normalizeHTML   => Strip all symbols from a string that affect HTML layout when rendered.
*   @type           public Function
*   @name           normalizeHTML
*   @param          String sourceStr                - A string containing HTML syntax.
*   @return         String
*   @notes
*                   - Replaces: HTML comments, new lines, tabs, carriage returns, any whitespace left between both angle brackets < >.
*                   - This lets us write indented HTML templates for use with innerHTML without the cruft generating DOM nodes like empty text nodes.
*                   - By replacing the tabs and new lines first, our following comment removal regex will also work on strings from HTML template files, so we can funally use comments inside our templates.
*                   - Should introduce better error handling.
*/
const normalizeHTML = sourceStr => {
    let comments;
    let lineOperations;    
    let whitespace;
    if ( typeof sourceStr !== 'string' ) {
        throw new TypeError( `parameter sourceStr is not a string: ${ sourceStr }` );
        return null;
    }
    try {
        lineOperations = sourceStr.replace( /[\n\t\r]+/g, '' );
    }
    catch( err ) {
        throw `failed removing new lines, tabs or carriage returns from: ${ sourceStr }`;
    }
    try {
        comments = lineOperations.replace( /<!--.+?-->/gi, '' );
    }
    catch ( err ) {
        throw `failed removing HTML comments from ${ lineOperations }`;
    }
    try {
        whitespace = comments.replace(  /(?:>)\s+(?:<)/g, '><' );
        
    }
    catch( err ) {
        throw `failed removing whitespace between tags from: ${ comments }`;
    }
    return whitespace.trim();
};
/*  pathObj         => Transform a string representing a javascript object path into the actual value inside the 'sourceObj'.
*   @type           public Function
*   @name           pathObj
*   @param          Object sourceObj
*   @param          String path
*   @return         Any
*   @notes
*                   - By replacing brackets with dots, we also support traversing arrays: obj.propertyAry[0].value => obj.propertyAry.0.value => value
*/
const pathObj = ( sourceObj, path ) => path.replace( /\[(.*?)\]/g, '.$1' ).split( '.' ).reduce( ( reference, pathChunk ) => reference ? reference[ pathChunk ] : undefined, sourceObj );
/*  pipe           => Reduce: Reduce all functions inside 'updates' into the 'entry'.
*   @type           public Function
*   @name           pipe
*   @param          Array( Function ) updates
*   @returns        Function( sourceObj )
*/
const pipe = ( ...updateFunctions ) => sourceObj => updateFunctions.reduce( ( targetObj, updateFunction ) => updateFunction( targetObj ), sourceObj );
/*	print
*
*/
const printHTML = html => {
	const popup = window.open( '/resources/easyreporting_app_print.html' );
	const finish_printing = event => {
		popup.close();
		window.removeEventListener( 'message', finish_printing );
	};
	window.addEventListener( 'message', finish_printing );
	setTimeout( function() { popup.postMessage( html, '*' ); }, 100 );
};
/* propFilter
*/
/*
const propFilter = ( filters ) => {
    const required_values = Object
        .entries( filters )
        .filter( ( [ filter, value] ) => value !== 'default' && required_checks.includes( filter ) );
        
        
    return resource.filter( entry => required_values.every( ( [ filter, value ] ) => entry[ filter ] === value ) );
    
    
    
    return ( resource ) => {
        
    };
};
*/
/*  propHash        => Reduce: Create an object with 'propertyReference' after 'keyTransform' as its keys and aggregate all array entries into it. 
*   @type           public Function
*   @name           propHash
*   @param          String propertyReference        - The property of each element we want to use as the hash key.
*   @param          Boolean isUnique                - When true, will set each hash key to the next record. When false, will create an array as the value of the hash key and push the next record to this array.
*   @param          Function keyTransform           - An optional function to apply to the property name of the (next) source.
*   @param          Function valueTransform         - An optional function to apply to the property value of the (next) source.
*   @returns        Object                          - Object containing all the keys found inside the records. Each key value is either a unique value or an array of values that share the same key ( property value )
*   @notes          
*                   - By adding default keys to the object we're reducing into, we guarantee that those keys exists in the final result.
*/
//export
const propHash = ( propertyReference, outputIsUnique, keyTransform, valueTransform ) => {
    const generateKey = keyTransform
        ? ( next, index, source ) => keyTransform( next[ propertyReference ], index, source )
        : next => next[ propertyReference ];
    return ( hash, next, index, source ) => {
        const key = generateKey( next, index, source );
        if ( !hash.hasOwnProperty( key ) ) {
            if ( outputIsUnique ) hash[ key ] = valueTransform ? valueTransform( next, index, source ) : next;
            else hash[ key ] = [];
        }
        if ( !outputIsUnique ) hash[ key ].push( valueTransform ? valueTransform( next, index, source ): next );
        return hash;
    };
};
/*  propReplace     => Map: Update the key name 'propertyName' of a property to a different key name 'replacementName'.
*   @type           public Function
*   @name           propReplace
*   @param          String propertyName
*   @param          String replacementName
*   @return         Function( sourceObj )
*/
const propReplace = ( propertyName, replacementName ) => sourceObj => {
    const clone = Object.assign( {}, sourceObj );
    delete clone[ propertyName ];
    clone[ replacementName ] = sourceObj[ propertyName ];
    return clone;
};
/*  propSort
*/
const propSort = ( type, property ) => {
    if ( type === 'lexical' ) {
        return ( first, second ) => {
            const first_property = first[ property ];
            const second_property = second[ property ];
            if ( first_property === second_property ) return 0;
            else if ( first_property < second_property ) return -1;
            else if ( first_property > second_property ) return 1;
            else throw new Error( `util.propSort.${ type }: failed sorting ${ first_property } and ${ second_property }` );
        };
    }
    else if ( type === 'numerical' ) return ( first, second ) => first[ property ] - second[ property ];
};
/*  proptransform   => Update the 'sourceObj' property values with the value of the 'updateObj' property of the same name, if it exists. Adds the same property with suffix '_normalized' if 3rd parameter is true
*   @type           public Function
*   @name           propTransform
*   @param          Object sourceObj
*   @param          Object updateObj
*   @return         Object
*   @notes
*                   - Each property in the 'updateObj' is an object itsself containing several options, each option to a value the sourceObj can have.
*                   - Eg. if we want to update the 'line' property of the  'sourceObj', then the 'updateObj' has to have 'line' as well, containing the values 'line' cvan have in the 'sourceObj' and their mapping.
*                   - sourceObj.lineProperty = updateObj.line[ sourceObj.lineValue ]Value
*                   - If the value of the updateObj is a function, the result of executing this function, using the sourceObj value as input, is used as the new value.
*                   - TODO: This isnt' flexible enough. We could not use proptranform correctly inside the [connector_psion.js connector, since we would ahve top add 'lijn1' : 'lijn1' to the psion transfromation.
*/
const propTransform = ( sourceObj, updateObj, addNormalizedSource ) => {
    return Object.keys( sourceObj ).reduce( ( clone, key ) => {
        const value = sourceObj[ key ];
        if ( updateObj.hasOwnProperty( key ) ) {
            clone[ key ] = isFn( updateObj[ key ] ) ? updateObj[ key ]( value ) : updateObj[ key ][ value ];
            if ( addNormalizedSource ) clone[ `${ key }_normalized` ] = value;
        }
        else clone[ key ] = value;
        return clone;
    }, {} );
};
/*
*/
const propUnhash = ( sourceObj, keyName, valueName ) => Object.keys( sourceObj ).map( ( key, index ) => ( { [ keyName || 'key' ] : key, [ valueName || 'value' ] : sourceObj[ key ] } ) );
/*  propUpdate      => Map: Update the value of a 'propertyName' to the result of the 'transformation'.
*   @type           public Function
*   @name           propUpdate
*   @param          String propertyName
*   @param          Function transformation
*   @return         Function( sourceObj )
*   @notes
*                   - Replaced transformation.call( sourceObj, sourceObj[ propertyName ] ) by directly calling the transformation. Before we'd use the this context to refer to the sourceObj, but then we can't use arrow functions.
*/
const propUpdate = ( propertyName, transformation ) => sourceObj => Object.assign( sourceObj, { [ propertyName ] : transformation( sourceObj ) } );
/*  shiftCurrent
*/
const shiftCurrent = () => shiftDate( dateShift( new Date() ) );
/*  shiftDate
*   @notes:
*                   - Since we express our shifts in local time, we need to actually use the version without 'Z' at the end to end up with the correct UTC date .
*                   - This approach should also work in different parts of the world, since the Date constructor will be relative to their location as well .
*/
const shiftDate = shift => {
    const chunks = shift.toString().split( '' );
    const dateString = `20${ chunks[ 0 ] }${ chunks[ 1 ] }-${ chunks[ 2 ] }${ chunks[ 3 ] }-${ chunks[ 4 ] }${ chunks[ 5 ] }T${ chunks[ 6 ] }${ chunks[ 7 ] }:15:00`;
    return new Date( dateString );
};
/*  strExtractInt   => Parse the first Integer from a string 'sourceStr', no matter where its located in that string. ( As opposed to parseInt/parseFloat requiring the string to start with the Number.
*   @type           public Function
*   @name           strExtractInt
*   @param          String inputStr
*   @return         Number( Integer )
*/
const strExtractInt = sourceStr => { const integerMatch = sourceStr.match( /\d+/ ); return integerMatch && parseInt( integerMatch[ 0 ], 10 ) || null; };
/*  supportsES6
*/
const supportsES6 = () => {
    return [
        { 'name' : 'constants', 		'fn' : function() { const constant = true; return constant; } },
        { 'name' : 'promises',			'fn' : function() { return new Promise( function( resolve, reject ) { resolve( true ); } ); } },
        { 'name' : 'template_strings',	'fn' : function() { return new Function( 'injection', 'return `${ injection }`;' )( 'template_test' ); } }
    ].reduce( function( summary, test ) {
        try {
            var result = test.fn();
            if ( result ) summary[ test.name ] = true;
        }
        catch( error ) {
            summary[ test.name ] = false;
        }
        return summary;
    }, {} );
};
/*  templatePopulate    =>
*/
const templatePopulate = ( sqlTemplate, sourceObj, dependencies ) => new Function( "resources", "dependencies", `return \`${ sqlTemplate }\`;` )( sourceObj, dependencies );
/*  translate
*   @notes
*                   - If no language is sent as a parameter, we just return the source string.
*                     This is easier to debug than having to check for the existance of the language in each module using the translate function.
*                     We coudl also use a default parameter with an empty data property to make sure a language always exists, but that also comes down to just returning the original string.
*/
const translate = ( str, language, reverse_lookup ) => {
	/*
	if ( str === 'off line systems' ) {
		console.log( 'translating off line systems' );
		console.log( str );
		console.log( reverse_lookup );
		console.log( /(\D*)(\d*)/.exec( str ) );
		console.log( '---' );
		console.log( language );
		console.log( '---------' );
	}
	*/
	
    if ( language ) {
		if ( language.data ) {
			const [ source, root, number ] = /(\D*)(\d*)/.exec( str );
			if ( reverse_lookup ) {
				const word = Object.entries( language.data ).find( ( [ standard, translation ] ) => translation === root );
				if ( word ) return `${ word[0] }${ number }`;
				else throw new Error( `cannot find reverse translation of: ${ str }` );
			}
			else return `${ language.data[ root ] || root }${ number }`;
		}
		else throw new Error( 'language does not have a data property' );
    }
    else throw new Error( 'no language defined' );
};
/*  unique          => Filter: Remove all duplicate items from an array. Works with plain objects as well, since we stringify each array item.
*   @type           public Function
*   @name           unique
*   @return         Function( item )
*   @notes          
*                   - Usage: ary.filter( unique() );
*                   - Basically we just provide a closure around the seen object.
*                   - Our previous version would be called as unique( ary ) instead.
*                   - I think this version is better for consistency and is more readable when chaining array methods, esp in combination with propHash and other higher order utility we provide.
*/
const unique = () => {
    const seen = {};
    return item => {
        const json = JSON.stringify( item );
        return seen.hasOwnProperty( json )
            ? false
            : ( seen[ json ] = true );
    };
};
/*
*/
const updateTimeframe = entries => {
    if ( !entries.length ) return [];
    else {
        var res = entries[ 0 ].data
            ? entries.map( propUpdate( 'data', entry => updateTimeframe( entry.data ) ) )
            : entries.map( entry => Object.assign( entry, { 'starttime' : entry.starttime ? new Date( entry.starttime ) : null, 'endtime' : entry.endtime ? new Date( entry.endtime ) : null } ) );
           return res;
    }
};
////////////////////
module.exports = {
    callbackify,
    capitalize,
    compose,
    dateDayOfYear,
    dateLocal,
    dateJulian,
    dateSeries,
    dateShift,
    dateUTC,
    debounce,
    flattenCollection,
    inspectError,
    intRandom,
    isFn,
    isNumber,
    isObj,
    isStr,
    mapLog,
    nodeClear,
    nodeParent,
    nodeUpdate,
    NOOP,
    normalizeHTML,
    pathObj,
    pipe,
	printHTML,
    propHash,
    propReplace,
    propSort,
    propTransform,
    propUnhash,
    propUpdate,
    shiftCurrent,
    shiftDate,
    strExtractInt,
    supportsES6,
    templatePopulate,
    translate,
    unique,
    updateTimeframe
};
////////////////////