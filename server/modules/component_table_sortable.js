    <table>
        <thead>
            <tr data-tmpl="if headers_render = true">
                <th data-tmpl="foreach header in headers_table">${header}</th> 
            </tr>
        </thead>
        <tbody>
            <tr data-tmpl="if resource.length != 0, foreach entry in resource">
                <td data-tmpl="foreach header in source.headers_table">${source.entry[header]}</td>
            </tr>
            <tr data-tmpl="if resource.length = 0">
                <th colspan="${source.headers.length}">No data</th>
            </tr>
        </tbody>
    </table>
/*  component_table_sortable.mjs     =>
*   @language:      ES6
*   @version:       v1.0.0
*   @creator:       VDBM
*   @date:          2018-06-18
*/
////////////////////
/*  API
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { Template } = require( './class_template.js' );