/*  component_linechart.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-07
*/
////////////////////
/*  API
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*   new Linechart( {
*       'bottom' : Number optional,
*       'left' : Number optional,
*       'height' : Number ( usually 500 ),
*       'top' : Number optional,
*       'right' : Number optional,
*       'root' : String querySelector optional,
*       'width' : Number ( usually 960 )
*   }, {
*       'axis_x' : {
*           'label' : String,
*           'property' : String propertyNameOfData,
*           'scale' : String ( scaleTime, scaleLinear, usually scaleTime )
*       },
*       'axis_y' : {
*           'label' : String,
*           'property' : String propertyNameOfData,
*           'scale' : String ( scaleTime, scaleLinear, usually scaleLinear )
*       }
*   } );
*
*
*   D3 ISO timeparse = timeParse( '%Y-%m-%dT%H-%M-%S.%LZ' );
*   D3 strict ISO timeparse = utcParse("%Y-%m-%dT%H:%M:%S.%LZ");
*
*   https://github.com/d3/d3-time-format
*
*       %a - abbreviated weekday name.*
*       %A - full weekday name.*
*       %b - abbreviated month name.*
*       %B - full month name.*
*       %c - the locale’s date and time, such as %x, %X.*
*       %d - zero-padded day of the month as a decimal number [01,31].
*       %e - space-padded day of the month as a decimal number [ 1,31]; equivalent to %_d.
*       %f - microseconds as a decimal number [000000, 999999].
*       %H - hour (24-hour clock) as a decimal number [00,23].
*       %I - hour (12-hour clock) as a decimal number [01,12].
*       %j - day of the year as a decimal number [001,366].
*       %m - month as a decimal number [01,12].
*       %M - minute as a decimal number [00,59].
*       %L - milliseconds as a decimal number [000, 999].
*       %p - either AM or PM.*
*       %Q - milliseconds since UNIX epoch.
*       %s - seconds since UNIX epoch.
*       %S - second as a decimal number [00,61].
*       %u - Monday-based (ISO 8601) weekday as a decimal number [1,7].
*       %U - Sunday-based week of the year as a decimal number [00,53].
*       %V - ISO 8601 week of the year as a decimal number [01, 53].
*       %w - Sunday-based weekday as a decimal number [0,6].
*       %W - Monday-based week of the year as a decimal number [00,53].
*       %x - the locale’s date, such as %-m/%-d/%Y.*
*       %X - the locale’s time, such as %-I:%M:%S %p.*
*       %y - year without century as a decimal number [00,99].
*       %Y - year with century as a decimal number.
*       %Z - time zone offset, such as -0700, -07:00, -07, or Z.
*       %% - a literal percent sign (%).
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const CONSTANTS = require( './constants.js' );
const SVG = require( './class_svg.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [
    { 'date' : '01-Jan-18', 'data' : 1 },
    { 'date' : '02-Jan-18', 'data' : 15 },
    { 'date' : '02-Jan-18', 'data' : 23 },
    { 'date' : '03-Jan-18', 'data' : 9 },
    { 'date' : '03-Jan-18', 'data' : 7 },
    { 'date' : '03-Jan-18', 'data' : 3 },
    { 'date' : '04-Jan-18', 'data' : 21 },
    { 'date' : '05-Jan-18', 'data' : 23 },
    { 'date' : '05-Jan-18', 'data' : 23 },
    { 'date' : '06-Jan-18', 'data' : 18 },
];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Linechart extends SVG {
    constructor( { bottom = 25, height, left = 30, right = 30, name = null, top = 10, width } = {}, settings ) {
        super( { bottom, height, left, right, name, top, width }, settings );
        this.root = `#er_linechart_${ name }`;
        this.scales = {
            'x' : null,
            'y' : null
        };
    }
    render() {
        try {
        super.render();
        // Select & update svg node, then add a group that we'll use as the actualy rendering area. Size - margins.
        this.body = this.d3
            .select( this.root )
            .append( 'svg:svg' )
            .attr( 'height', this.dimensions.height )
            .attr( 'width', this.dimensions.width )
            .append( 'g' )
            .attr( 'transform', 'translate(' + this.dimensions.left + ',' + this.dimensions.top + ')' );
        // If we want to render a default scale, we also need to remove it before overwriting..
        }
        catch( error ) {
            console.log( 'component_linechart: render failed.' );
            console.error( error );
            throw error;
        }
        return this;
    }
    update( data ) {
        try {
            this.updateScales( data );
            this.updateData( data );
        }
        catch( error ) {
            console.log( 'component_linechart: update failed.' );
            console.error( error );
            throw error;
        }
        return this;
    }
    updateData( data ) {
        // accepts array of arrays of objects
        // define the line graph itsself as a function of the properties
        this.body
            .append( 'path' )
            .data( [ data ] ) 
            .attr( 'd', 
                this.d3 
                    .line()
                    .x( entry => this.scales.x( entry[ this.settings.axis_x.property ] ) )
                    .y( entry => this.scales.y( entry[ this.settings.axis_y.property ] ) )
            )
            .attr( 'fill', 'none' )
            .attr( 'stroke', CONSTANTS.COLOR_K_BLUE_FLUO.hex )
            .attr( 'stroke-linejoin', 'round' )
            .attr( 'stroke-linecap', 'round' )
            .attr( 'stroke-width', 1.5 );
        return this;
    }
    updateScales( data ) {
        // define x axis area as a function of time between 0 and the width
        this.scales.x = this.d3[ this.settings.axis_x.scale ]()
            .rangeRound( [ 0, this.dimensions.width - this.dimensions.left - this.dimensions.right ] )
            .domain(
                this.d3.extent( data, level => level[ this.settings.axis_x.property ] )
            );
        // define y axis area as a function of the value between the height and 0 ( x0y0 is top left )
        this.scales.y = this.d3[ this.settings.axis_y.scale ]()
            .rangeRound( [ this.dimensions.height - this.dimensions.top - this.dimensions.bottom, 0 ] )
            .domain(
                this.d3.extent( data, level => level[ this.settings.axis_y.property ] )
            );
        // draw x-axis
        this.body
            .append( 'g' )
            .call( this.d3.axisBottom( this.scales.x ) )
            .attr( 'transform', `translate( 0, ${ this.dimensions.height - this.dimensions.top - this.dimensions.bottom })` )
            .append( 'text' )
            .attr( 'fill', '#000' )
            .attr( 'x', this.dimensions.width - this.dimensions.left - this.dimensions.right )
            .attr( 'y', -7 )
            .attr( 'text-anchor', 'end' )
            .text( this.settings.axis_x.label );
        // draw y-axis
        this.body
            .append( 'g' )
            .call( this.d3.axisLeft( this.scales.y ) )
            .append( 'text' )
            .attr( 'fill', '#000' )
            .attr( 'transform', `rotate( -90 )` )
            .attr( 'y', 6 )
            .attr( 'dy', '0.71em' )
            .attr( 'text-anchor', 'end' )
            .text( this.settings.axis_y.label );
        return this;
    }
}
// Static type .
Linechart.prototype.type = Object.freeze( 'linechart' );
////////////////////
module.exports = Linechart;