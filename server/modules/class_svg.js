/*  class_svg.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-06
*/
////////////////////
/*  API
*
*           Destructure the dimensions, providing defaults.
*           Assign all the dimensions, trigering the setter.
*           The settings were already destructured inside the derived class.
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { isNumber, isObj } = require( './util.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class SVG {
    constructor( { bottom = 0, height, left = 0, right = 0, name = null, top = 0, width } = {}, settings ) {
        // Sanity check
        // 1) Error if we don't have a valid height and width.
        // 2) Error if the settings parameter isn't an object
        // 3) Error if the node isn't a DOM node and the we're not creating a new instance, since we'll init the root as null at the start
        //      todo: reevaluate this. We might want to add a selector or node to the config options.
        //      if ( !document.querySelector( queryString ) instanceof HTMLElement ) throw new TypeError( `class ${ this.name } - node not found : ${ queryString }` );
        if ( !isNumber( height ) || !isNumber( width ) ) throw new TypeError( `class ${ this.name } - height:width ${ height }:${ width }` );
        if ( !isObj( settings ) ) throw new TypeError( `class ${ this.name } - no settings` );
        ////////////////////
        this.body = null;
        this.dimensions = { bottom, height, left, right, top, width };
        this.name = name;
        this.settings = settings;
        this.state = null;
    }
    render() {
        if ( !this.root ) throw new Error( `class ${ this.name } - root undefined, can't render` );
    }
}
/* Provide access to the d3 lib as a property of any instances created. */
SVG.prototype.d3 = Object.freeze( require( '../resources/d3.min.js' ) );
/* Static type. */
SVG.prototype.type = Object.freeze( 'svg' );
////////////////////
module.exports = SVG;