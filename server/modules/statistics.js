/*  statistics.mjs        => A collection of utility functions containing common reductions
*   @data_language: ES6
*   @data_version:  1.0.1
*   @entry_user:    VDBM
*   @update_date:   2018-01-31
*/
////////////////////
const UTIL = require( './util.js' );
////////////////////
// We want to be able to use the 'logical syntax' resource.reduce( avg( propertyName ), 0 ) instead of avg( resource, propertyName )
// Reducing an empty array will throw an error if we dont provide an initial value, so it's better to make the rule to always provide a value.
// Sicne the loop body will not run if the array is empty, only the actual initial value can set the result to always return 0.
// Default parameters will only be used if we actually do the loop.
// So calculate the sum divided by the length and then just keep passing the same result.
// Current implementation is inefficient since we technically only need to loop once to get the result.
const avg = propertyName => ( aggregation, collection_entry, index, collection ) => index === 1 ? collection.reduce( sum( propertyName ), 0 ) / collection.length : aggregation;

// JS implementation is a bit flakey, due to the floating point numbers.
const avgGeometric = propertyName => ( aggregation, collection_entry, index, collection ) => {
    if ( index === 1 ) {
        const product_property = collection.reduce( multiply( propertyName ), 1 );
        return Math.pow( product_property, 1 / collection.length );
    }
    else return aggregation;
};
// This returns 0 if one of the values in null. Eg, proficy test that doesn't have the value yet.
const avgHarmonic = ( propertyName, weightName ) => ( aggregation, collection_entry, index, collection ) => {
    if ( index === 1 ) {
        const sum_property = collection
            .map( entry => ( { "weight" : entry[ weightName ] / entry[ propertyName ] } ) )
            .reduce( sum( 'weight' ), 0 );
        const sum_weight = collection.reduce( sum( weightName ), 0 );
        return sum_weight / sum_property;
    }
    else return aggregation;
};
// avg after the top 2% and bottom 2% of values have been removed.
const avgTrimmed = null;

const avgWeighted = ( propertyName, weightName ) => ( aggregation, collection_entry, index, collection ) => {
    if ( index === 1 ) {
        const sum_property = collection
            .map( entry => ( { "weight" : entry[ propertyName ] * entry[ weightName ] } ) )
            .reduce( sum( 'weight' ), 0 );
        const sum_weight = collection.reduce( sum( weightName ), 0 );
        return sum_property / sum_weight;
    }
    else return aggregation;
};

const median = propertyName => {
    let isEven = null;
    let middle = null;
    return ( aggregation, collection_entry, index, collection ) => {
        if ( isEven === null ) {
            isEven = !( collection.length % 2 );
            middle = isEven
                ? collection.length / 2
                : Math.ceil( collection.length / 2 );
        }
        if ( index + 1 === middle ) {
            const sorted = collection.sort( ( primary, secondary ) => primary[ propertyName ] > secondary[ propertyName ] );
            return isEven
                ? ( sorted[ index ][ propertyName ] + sorted[ index + 1 ][ propertyName ] ) / 2
                : sorted[ index ][ propertyName ];
        }
        else return aggregation;
    };
};    

const multiply = propertyName => ( aggregation, collection_entry ) => {
    if ( UTIL.isObj( aggregation ) && aggregation.hasOwnProperty( propertyName ) ) aggregation[ propertyName ] *= collection_entry[ propertyName ] || 1;
    else aggregation *= collection_entry[ propertyName ] || 1;
    return aggregation;
};     

// Need to specify the aggregation, else the first agg will always be the first entry and we'll reduce into an object instead of a value.
const sum = propertyName => ( aggregation, collection_entry ) => {
    if ( UTIL.isObj( aggregation ) && aggregation.hasOwnProperty( propertyName ) ) aggregation[ propertyName ] += collection_entry[ propertyName ] || 0;
    else aggregation += collection_entry[ propertyName ] || 0;
    return aggregation;
};    
////////////////////
module.exports = { avg, avgGeometric, avgHarmonic, avgTrimmed, avgWeighted, median, multiply, sum };
////////////////////
/*
const test_statistics = () => {
    
    const data = [
        { "id" : 1, "department" : "packing", "type" : "defect", "value" : 1 },
        { "id" : 2, "department" : "packing", "type" : "defect", "value" : 9 },
        { "id" : 3, "department" : "packing", "type" : "check", "value" : 4 },
        { "id" : 4, "department" : "process", "type" : "defect", "value" : 6 }
    ];
    //
    //const data_avg = data.reduce( avg( 'value' ), 0 );
    //return data_avg;
    //
    const data_median = data.reduce( median( 'value' ), 0 );
    
    return data_median;

};
*/