/*  class_mediator.mjs   => 
*   @language:  ES6
*   @version:   v1.0.2
*   @creator:      VDBM
*   @date:      2018-06-04
*/////////////////////
/*  API
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { nodeParent } = require(  './util.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Mediator
*   @param      Object
*                   Function attrBind
*                   Function attrEvent
*                   Function event
*                   Function init
*                   Function register
*                   Function unregister
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Mediator {
    constructor() {
        this.events = {};
        this.exceptions = [];
    }
    attrBind( node ) {
        const target = node || window;
        target.addEventListener( 'click', this.attrEvent.bind( this ) );
        target.addEventListener( 'change', this.attrEvent.bind( this ) );
        return this;
    }
    attrEvent( event ) {
        const node = event.target;
        const event_name = node
            ? node.getAttribute( 'data-event' )
            : null;
        const context = node && event_name
            ? node.getAttribute( 'data-context' )
            : null;
        if ( event_name ) {
            if ( event_name === 'BUBBLE' ) {
                const handler = nodeParent( node, context );
                if ( handler ) this.event( handler.getAttribute( 'data-event' ), handler.getAttribute( 'data-context' ), handler );
            }
            else this.event( event_name, context, node, event.type );
        }
    }
    event( name, data, source, type ) {
        if ( this.events.hasOwnProperty( name ) ) {
            const results = this.events[ name ].map( callback => callback( data, source, type ) );
            return Promise.resolve( results.length !== 1 ? results : results[ 0 ] );
        }
        else return Promise.reject( `Event ${ name } is not registered.` );
    }
    init() {
        return { 'mediator' : true };
    }
    register( name, callback ) {
        if ( !this.events.hasOwnProperty( name ) ) this.events[ name ] = [];
        this.events[ name ].push( callback );
        return this;
    }
    
    unregister( name, callback ) {
        this.events[ name ] = this.events[ name ].filter( registered => registered.name === callback.name );
        return this;
    }
}
/*
*/
Mediator.prototype.type = Object.freeze( 'mediator' );
////////////////////
module.exports = Mediator;