/*  component_gauge.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-03-06
*   @reference:     http://bl.ocks.org/tomerd/1499279 https://gist.github.com/1499279 
*/
////////////////////
/*  API
*
*   TODO: Should we force the min and max ranges isnide the report or inside this class? If data > max, data = max ?
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*   new Gauge( {
*       'bottom' : Number optional,
*       'left' : Number optional,
*       'height' : Number optional ( has to be the same as the width and the size )
*       'top' : Number optional,
*       'right' : Number optional,
*       'root' : String querySelector optional,
*       'width' : Number optional ( has to be the same as the height and the size )
*   }, {
*       'label' : String optional,
*       'max' : Number optional,
*       'min' : Number optional,
*       'precision' : Number optional,
*       'size' : Number optional
*   } );
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const CONSTANTS = require( './constants.js' );
const SVG = require( './class_svg.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
// const DEFAULT_DATA = [];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
const valueToDegrees = ( value, settings ) => value / settings.range * 270 - ( settings.min / settings.range * 270 + 45 );
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
const valueToFixed = {
    '50' : 1.5707963267948965,
    '75' : 2.748893571891069,
    '90' : 3.455751918948773,
    '100' : 3.9269908169872414
};
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
const valueToPath = ( value, settings ) => {
    const delta = settings.range / 13;
    const tail = value - ( settings.range * ( 1 / ( 270 / 360 ) ) / 2 );
    return [
        valueToPoint( value, settings, 0.85, settings.cx, settings.cy ),
        valueToPoint( value - delta, settings, 0.12, settings.cx, settings.cy ),
        valueToPoint( tail + delta, settings, 0.12, settings.cx, settings.cy ),
        valueToPoint( tail, settings, 0.28, settings.cx, settings.cy ),
        valueToPoint( tail - delta, settings, 0.12, settings.cx, settings.cy ),
        valueToPoint( value + delta, settings, 0.12, settings.cx, settings.cy ),
        valueToPoint( value, settings, 0.85, settings.cx, settings.cy )
    ];
};
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
const valueToPoint = ( value, settings, factor, min_x = 0, min_y = 0 ) => {
    return {
        'x' : ( settings.cx - settings.radius * factor * Math.cos( valueToRadians( value, settings ) ) ) - min_x,
        'y' : ( settings.cy - settings.radius * factor * Math.sin( valueToRadians( value, settings ) ) ) - min_y
    };
};
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
const valueToRadians = ( value, settings ) => valueToDegrees( value, settings ) * Math.PI / 180;
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Gauge extends SVG {
    constructor( { bottom, height = 200, left, right, name, top, width = 200 } = {}, { label = '', max = 100, min = 0, precision = 0, size = 200 } = {} ) {
        // Destructure the settings, providing defaults.
        // Assign all the settings, triggering supers setter. 
        // Use the settings.size property for both the dimensions.height and dimensions.width so we get a square.
        super( {
            bottom,
            height,
            left,
            right,
            name,
            top,
            width
        }, {
            'cx' : size / 2,
            'cy' : size / 2,
            'duration_transition' : 500,
            label,
            max,
            min,
            precision,
            'radius' : size * 0.97 / 2,
            'range' : max - min,
            'size' : size * 0.9,
            'ticks_major' : 5,
            'ticks_minor' : 2,
        } );
        this.root = `#er_gauge_${ name }`;
        this.state = {
            'data' : null,
            'rotation' : null
        };
        /* Original zones that try extrapolate the percentage. This does not work for this that don't start at 0.
        this.zones = {
            'green' : [ { 'from' : ( max / 2 ) - 0.09, 'to' : ( max * 0.75 ) + 0.09 } ],
            'orange' : [ { 'from' : ( max * 0.75 ) + 0.09, 'to' :  max * 0.9 } ],
            'red' : [ { 'from' : max * 0.9, 'to' : max } ]
        };
        */
        // Current implementation: fixed zones at the 50-75 75-90 90-100 intervals.
        this.zones = {
            'green' : [ { 'from' : 50, 'to' : 75 } ],
            'orange' : [ { 'from' : 75, 'to' : 90 } ],
            'red' : [ { 'from' : 90, 'to' : 100 } ]
        };
    }
    render() {
        try {
            super.render();
            // Select & update container
            this.body = this.d3
                .select( this.root )
                .append( 'svg:svg' )
                .attr( 'class', 'gauge' )
                .attr( 'height', this.dimensions.height )
                .attr( 'width', this.dimensions.width );
            //	Draw outer grey circle
            this.body
                .append( 'svg:circle' )
                .attr( 'cx', this.settings.cx )
                .attr( 'cy', this.settings.cy )
                .attr( 'r', this.settings.radius )
                .style( 'fill', '#ccc' )
                .style( 'stroke', '#000' )
                .style( 'stroke-width', '0.5px' );
            // Draw inner white circle
            this.body
                .append( 'svg:circle' )
                .attr( 'cx', this.settings.cx )
                .attr( 'cy', this.settings.cy )
                .attr( 'r', this.settings.radius  )
                .style( 'fill', '#fff' )
                .style( 'stroke', '#e0e0e0' )
                .style( 'stroke-width', '2px' );
            // Draw inner green, roange and red zones.
            this.zones.green.forEach( zone => this.renderZone( zone.from, zone.to, CONSTANTS.COLOR_K_GREEN_FLUO.hex ) );
            this.zones.orange.forEach( zone => this.renderZone( zone.from, zone.to, CONSTANTS.COLOR_K_ORANGE.hex ) );
            this.zones.red.forEach( zone => this.renderZone( zone.from, zone.to, CONSTANTS.COLOR_K_RED.hex ) );
            // Draw label
            this.body
                .append( 'svg:text' )
                .attr( 'x', this.settings.cx )
                .attr( 'y', this.settings.cy / 2 + Math.round( this.settings.size / 9 ) / 2 ) // fontSize
                .attr( 'dy', Math.round( this.settings.size / 9 ) / 2 ) // fontSize
                .attr( 'text-anchor', 'middle' )
                .text( this.settings.label )
                .style( 'font-size', ( Math.round( this.settings.size / 10 ) ).toString() + 'px' ) // fontSize
                .style( 'fill', '#333' )
                .style( 'stroke-width', '0px' );
            // Draw circle stripes every 15 degrees. Alternating small / large 
            const majorDelta = this.settings.range / ( this.settings.ticks_major - 1 );
            for ( let major = this.settings.min; major <= this.settings.max; major += majorDelta ) {
                let minorDelta = majorDelta / this.settings.ticks_minor;
                for ( let minor = major + minorDelta; minor < Math.min( major + majorDelta, this.settings.max ); minor += minorDelta ) {
                    let point1 = valueToPoint( minor, this.settings, 0.75 );
                    let point2 = valueToPoint( minor, this.settings, 0.85 );
                    let point3 = valueToPoint( major, this.settings, 0.7 );
                    let point4 = valueToPoint( major, this.settings, 0.85 );
                    this.body
                        .append( 'svg:line' )
                        .attr( 'x1', point1.x )
                        .attr( 'y1', point1.y )
                        .attr( 'x2', point2.x )
                        .attr( 'y2', point2.y )
                        .style( 'stroke', '#666' )
                        .style( 'stroke-width', '1px' );
                    this.body
                        .append( 'svg:line' )
                        .attr('x1', point3.x )
                        .attr( 'y1', point3.y )
                        .attr( 'x2', point4.x )
                        .attr( 'y2', point4.y )
                        .style( 'stroke', '#333' )
                        .style( 'stroke-width', '2px' );
                }
                // Final circle stripe.
                if ( major === this.settings.min || major === this.settings.max ) {
                    let range_label = valueToPoint( major, this.settings, 0.5 );
                    this.body
                        .append( 'svg:text' )
                        .attr( 'x', range_label.x + ( major === this.settings.min ? -8 : 8 ) ) // offset the label by 4px so there's room for floating points numbers.
                        .attr( 'y', range_label.y )
                        .attr( 'dy', Math.round( this.settings.size / 16 ) / 3 ) // fontSize
                        .attr( 'text-anchor', major === this.settings.min ? 'start' : 'end' )
                        //.text( ( major / this.settings.precision ).toFixed( this.settings.precision.toString().length - 1 ) )
                        .text( major.toFixed( this.settings.precision ) )
                        .style( 'font-size', Math.round( this.settings.size / 16 ).toString() + 'px' ) // fontSize
                        .style( 'fill', '#333' )
                        .style( 'stroke-width', '0px' );
                }
            }
            // Create arrow pointer element
            let arrow = this.body
                .append( 'svg:g' )
                .attr( 'class', 'pointerContainer' );
            // Draw actual arrow
            arrow
                .selectAll( 'path' )
                .data( [ valueToPath( ( this.settings.min + this.settings.max ) / 2, this.settings ) ] ) // midvalue
                .enter()
                .append( 'svg:path' )
                .attr( 'd',
                    this.d3
                        .line()
                        .x( point => point.x )
                        .y( point => point.y )
                        .curve( this.d3.curveBasis )
                )
                .style( 'fill', '#dc3912' )
                .style( 'stroke', '#c63310' )
                .style( 'fill-opacity', 0.7 );
            // Draw blue middle circle
            arrow
                .append( 'svg:circle' )
                .attr( 'cx', this.settings.cx )
                .attr( 'cy', this.settings.cy )
                .attr( 'r', 0.12 * this.settings.radius )
                .style( 'fill', CONSTANTS.COLOR_K_BLUE_FLUO.hex ) // '#4684EE'
                .style( 'stroke', '#666' )
                .style( 'opacity', 1 );
            // Draw arrow value as text
            arrow
                .selectAll( 'text' )
                .data( [ ( this.settings.min + this.settings.max ) / 2 ] )
                .enter()
                .append( 'svg:text' )
                .attr( 'x', this.settings.cx )
                .attr( 'y', this.settings.size - this.settings.cy / 4 - Math.round( this.settings.size / 10 ) ) // fontSize
                .attr( 'dy', Math.round( this.settings.size / 10 ) / 2 ) // fontSize
                .attr( 'text-anchor', 'middle' )
                .style( 'font-size', Math.round( this.settings.size / 10 ).toString() + 'px' )
                .style( 'fill', '#000' )
                .style( 'stroke-width', '0px' );
            // Set the arrow to the startpoint initially
            this.update( this.settings.min, 0 );
        }
        catch( error ) {
            console.log( 'component_gauge: render failed.' );
            console.error( error );
            throw error;
        }
        return this;
    }
    renderZone( start, end, color ) {
        // Current definition of the this.zones is fixed so we get the same 50-75 75-90 90-100 marks .
        if ( end - start <= 0 ) return this;
        this.body
            .append( 'svg:path' )
			.style( 'fill', color )
			.attr( 'd',
                this.d3
                    .arc()
                    //.startAngle( valueToRadians( start, this.settings ) )
                    .startAngle( valueToFixed[ start ] )
                    //.endAngle( valueToRadians( end, this.settings ) )
                    .endAngle( valueToFixed[ end ] )
                    .innerRadius( this.settings.radius * 0.65 )
                    .outerRadius( this.settings.radius * 0.85 )
            )
			.attr( 'transform', () => `translate(${ this.settings.cx }, ${ this.settings.cy }) rotate(270)` );
        return this;
    }
    update( data, duration ) {
        try {
            // Save Number data & Integer duration
            this.state.data = data;
            // Update data field
            this.body
                .select( '.pointerContainer' )
                .selectAll( 'text' )
                //.text( ( data / this.settings.precision ).toFixed( this.settings.precision.toString().length - 1 ) );
                .text( data.toFixed( this.settings.precision ) );
            // Move arrow
            this.body
                .select( '.pointerContainer' )
                .selectAll( 'path' )
                .transition()
                .duration( duration || this.settings.duration_transition )
                .attrTween( 'transform', () => {
                    let arrowValue = data;
                    if ( data  > this.settings.max ) arrowValue = this.settings.max; // this was in the original code, but makes the arrow overflow: this.settings.max + 0.02 * this.settings.range;
                    else if ( data < this.settings.min ) arrowvalue = this.settings.min; // this was in the original code, but makes the arrow overflow: this.settings.min - 0.02 * this.settings.range;
                    let targetRotation = ( valueToDegrees( arrowValue, this.settings ) - 90 );
                    let currentRotation = this.state.rotation || targetRotation;
                    this.state.rotation = targetRotation;
                    return step => `translate(${ this.settings.cx }, ${ this.settings.cy }) rotate(${ currentRotation + ( targetRotation - currentRotation ) * step })`;
                } );
        }
        catch( error ) {
            console.log( 'component_gauge: update failed' );
            console.error( error );
            throw error;
        }
        return this;
    }
}
/* Static type */
Gauge.prototype.type = Object.freeze( 'gauge' );
////////////////////
module.exports = Gauge;