/*  component_arrow_links.mjs   => 
*   @language: ES6
*   @version:  v1.0.0
*   @creator:    VDBM
*   @date:   2018-04-18
*/
/*  API
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
// No imports.
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const PATH_CSS = '/resources/component_arrow_links.css';
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [
    { 'label' : 'primary', 'url' : '#/primary' },
    { 'label' : 'secondary', 'url' : '#/secondary' },
    { 'label' : 'tertiary', 'url' : '#/tertiary' },
    { 'label' : 'quaternary', 'url' : '#/quaternary' },
    { 'label' : 'quinary', 'url' : '#/quinary' }
];
/*
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       ${ name_class }
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Arrow_links {
    constructor( name, border, colors ) {
        this.border = border || null;
        // No color,no array .
        if ( !colors ) this.colors = [];
        // Clone the array .
        else if ( Array.isArray( colors ) ) this.colors = [ ...colors ];
        // One color string, array of length 1 containing that string .
        else this.colors = [ colors ];
        this.name = name;
        this.root = `#er_arrow_list_${ name }`;
    }
    /*bind() {
        return this;
    }*/
    /*html() {
        return '';
    }*/
    /*onclick() {
    }*/
    render( data = DEFAULT_DATA ) {
        const node = document.querySelector( this.root );
        if ( !node ) throw new Error( 'component_arrow_links: root node not found.' );
        else {
            const cls_border = this.border
                ? `${ this.border } `
                : '';
            const cls_items = this.colors.length
                ? data.map( ( item, index ) => ` class="${ this.colors[ index % this.colors.length ] }"` )
                : new Array( data.length ).fill( '' );
            const list = [
                `<ul class="${ cls_border }comp-arrow-links">`,
                ...data.map( ( item, index ) => `<li${ cls_items[ index ] }><a href="${ item.url }">${ item.label }</a></li>` ),
                `</ul>`
            ];
            node.innerHTML = list.join( '' );
        }
        return this;
    }
    /*scope() {
        
    }*/
    update( data = DEFAULT_DATA ) {
        return this.render( data );
    }
}
/* Static type */
Arrow_links.prototype.type = Object.freeze( 'arrow_links' );
////////////////////
module.exports = Arrow_links;