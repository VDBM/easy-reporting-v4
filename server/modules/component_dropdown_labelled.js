/*  component_dropdown_labelled.mjs   =>
*   @language:  ES6
*   @version:   1.1.0
*   @creator:      VDBM
*   @date:      2018-05-09
*/
////////////////////
/*  API
*
*   const dropdown = new Dropdown_labelled( { "event": "function", "name": "string", "style": "string" } );
*   dropdown.html();
*   dropdown.html( [] );
*   dropdown.render();
*   dropdown.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { capitalize } = require( './util.js' );
const Dropdown = require( './component_dropdown.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const PATH_CSS = null;
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [ "labelled_default" ];
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
// const helper = () => void( 0 );
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name       Dropdown
*   @param      Object
*                   Function event
*                   String labels
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               - Since we need to add event handlers to the select nodes, we kind of need a mechanism that will actually render the nodes and add event handlers to them.
*                 So we can check the 'event' property of the instance. If it's a function, we need to attach the event handlers ourselves, if it's a string, we can just add it as an event attribute  to the select node.
*                 => OK: instance.bind()
*               - We can choose between clearing each select fully and re-adding the default, or we could not remove the default value and hide the entire select.
*/
class Dropdown_labelled extends Dropdown {
    // Create an instance.
    constructor( { event = null, labels, name, style = '' } = {} ) {
        super( { event, name, style } );
        this.labels = labels;
        this.root = `#dropdown_labelled_${ name }`;
    }
    // Adds a label in front of the HTML egnerated by the Dropdown.
    html( data, operand, relation, expectation ) {
        const html = super.html( data, operand, relation, expectation );
        // Add the label in front of the dropdown.
        return `<label class="ib pad-4 ta-r">${ capitalize( this.labels ) }:</label>${ html }`;
    }
    // Update our properties through super, then check for the labels. 
    update( source ) {
        super.update( source );
        if ( source.labels ) this.labels = source.labels; 
        return this;
    }
}
// Static type.
Dropdown_labelled.prototype.type = Object.freeze( 'dropdown_labelled' );
////////////////////
module.exports = Dropdown_labelled;