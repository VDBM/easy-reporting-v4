/*  component_picker_datetime.mjs     =>
*   @language:      ES6
*   @version:       v1.0.0
*   @creator:          ${ user }
*   @date:          ${ date }
*/
////////////////////
/*  API
*
*   const picker_datetime = new Picker_datetime( { "event": "function", "name": "string", "style": "string" } );
*   picker_datetime.html();
*   picker_datetime.html( [] );
*   picker_datetime.render();
*   picker_datetime.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
////////////////////
/*  IMPORTS             => Import dependencies.
*   @type       private Any
*   @name       IMPORTS
*   @param      null
*   @return     any
*   @notes
*               - ES5: const dependency = require( path ); => ES6: import dependency from 'path';
*               - ES5: const dependency_function = require( path ); => ES6: import { dependency_function } from 'path;
*/
const { Template } = require( './class_template.js' );
/*  DERIVATIONS         => Derivations from the imported dependencies.
*   @type       private Any
*   @name       DERIVATIONS
*   @param      null
*   @return     Any
*   @notes
*               - Eg. promisifying callbacks.
*               -
*/
// No derivations.
////////////////////
/*  CSS                 => Seperate CSS file for this component.
*   @type       public File
*   @name       PATH_CSS
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const PATH_CSS = './resources/component_picker_datetime.css';
/*  DEFAULT_DATA        => Default data used to populate the component if no data was provided.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
const DEFAULT_DATA = [];
const DEFAULT_NODES = {
    "root": null,
    "datetime": {
        "Date": null,
        "Month": null,
        "FullYear": null,
        "Hours": null,
        "Minutes": null
    }
};
////////////////////
/*  HELPERS             => Utility functions and values used by the class.
*   @type       static Array
*   @name       DEFAULT_DATA
*   @param      null
*   @return     null
*   @notes
*               -
*               -
*/
/*  helper              => Helper definition.
*   @type       private Any
*   @name       helper
*   @param      access Type
*   @param      access Type
*   @return     access Type
*   @notes
*               -
*               -
*/
////////////////////
/*  CLASS               => Component definition.
*   @type       public Class
*   @name      Picker_datetime
*   @param      Object
*                   Function event
*                   String name
*                   String style
*   @return     public Object
*   @notes      
*               -
*               -
*/
class Picker_datetime {
    // Create an instance.
    constructor( { event = null, name, style = '' } = {} ) {
        this.bound = false;
        this.data = new Date();
        this.event = event;
        this.name = name;
        this.nodes = JSON.parse( JSON.stringify( DEFAULT_NODES ) ); // deep clone
        this.root = `#picker_datetime_${ name }`;
        this.state = null;
        this.style = style;
        this.template = new Template( 'component_picker_datetime' );
    }
    // Add an event handler to a target, usually the container of the component.
    // Either bind the function, or add the data attributes that a mediator will look for.
    bind( target ) {
        // root
        this.nodes.root = document.querySelector( this.root );
        // click, change, keyup handlers for the inputs
        Object
            .keys( this.nodes.datetime )
            .forEach( reference => {
                this.nodes.datetime[ reference ] = document.querySelector( `#pdt_${ reference }_${ this.name }` );;
                this.nodes.datetime[ reference ].addEventListener( 'click', this.onclick.bind( this ) );
                this.nodes.datetime[ reference ].addEventListener( 'change', this.onchange.bind( this ) );
                if ( reference !== 'Minutes' ) {
                    this.nodes.datetime[ reference ].addEventListener( 'keyup', this.onkeyup.bind( this ) );
                }
            } );
        // border toggling due to no support for :focus-within
        // document.addEventListener( 'click', this.toggleBorder.bind( this ) );
        // Clear button
        document.querySelector( `#pdt_clear_${ this.name }` ).addEventListener( 'click', this.onclear.bind( this ) );
        // Show popup button
        document.querySelector( `#pdt_show_popup_${ this.name }` ).addEventListener( 'click', this.onpopup.bind( this ) );
        this.bound = true;
        return this;
    }
    // Return the html, after populating it with array data. If no data is provided, the DEFAULT_DATA will be used.
    // The operand, relation, expectation parameters provide compatibility with loops in our our template engine.
    // For other functionality, `html( data )` usually suffices.
    html( data, operand, relation, expectation ) {
        // const data_source = data || this.data || DEFAULT_DATA;
        // CODE
        return this.template.html( {
            "name": this.name,
            "values": {
                "Date": this.data.getDate().toString().padStart( 2, '0' ),
                "Month": ( this.data.getMonth() + 1 ).toString().padStart( 2, '0' ),
                "FullYear": this.data.getFullYear().toString(),
                "Hours": this.data.getHours().toString().padStart( 2, '0' ),
                "Minutes": this.data.getMinutes().toString().padStart( 2, '0' )
            }
        } );
    }
    // If the entered value is between the limits, change the state, else change the input value to the closest limit.
    onchange( event ) {
        const button_clear = document.querySelector( `#pdt_clear_${ this.name }` );
        const target = event.target;
        const context = target.id.split( '_' )[ 1 ];
        let value = parseInt( target.value, 10 );
        let adjustment = context && context === 'Month'
            ? 1
            : 0;
        const limit_max = target.getAttribute( 'max' );
        const limit_min = target.getAttribute( 'min' );
        const limit_length = parseInt( target.getAttribute( 'maxlength' ), 10 );
        button_clear.classList.add( 'active' );
        if ( context ) {
            if ( value >= limit_min && value <= limit_max ) {
                // If we type like 201 instead of 2018 into the year field, we just use the previous year.
                if ( context === 'FullYear' && value.length < limit_length ) {
                    if ( this.data ) {
                        value = this.data.getFullYear();
                        arget.value = value.toString();
                    }
                    else throw new Error( 'no year defined' );
                }
                else if ( value < 10 && target.value.length != 2 ) target.value = value.toString().padStart( 2, '0' );
                this.data[ 'set' + context ]( value - adjustment );
            }
            else if ( value < limit_min ) {
                const adjustment = context === 'Month' ? 1 : 0;
                target.value = limit_min;
                this.data[ 'set' + context ]( limit_min - adjustment );
            }
            else if ( value > limit_max ) {
                target.value = limit_max;
                this.data[ 'set' + context ]( limit_max - adjustment );
            }
        }
        if ( this.event ) this.event( { "component": this.root, "date": this.data } );
        return this;
    }
    onclick( event ) {
        event.target.select();
        return this;
    }
    onclear( event ) {
        this.update( { "data": new Date() } );
        event.target.classList.remove( 'active' );
        this.nodes.root.classList.add( 'active' );
        this.nodes.datetime.Date.select();
    }
    onkeyup( event ) {
        if ( event.target.value.length === parseInt( event.target.getAttribute( 'maxlength' ), 10 ) ) {
            var next = document.querySelector( '#' + event.target.id + ' ~ input' );
            if ( next ) next.select();
        }
        return this;
    }
    onpopup( event ) {
        // toggle popup visibility
    }
    register( scope, method ) {
        this.template.register( scope, method );
        return this;
    }
    // Render the component to the root element and bind the event handler. We can use the this.ndoes.root reference, since it'll only exist after binding.
    render() {
        const node = document.querySelector( this.root );
        if ( !node ) throw new Error( `component picker_datetime_${ this.name } - root undefined, can't render.` );
        else {
            node.innerHTML = this.html();
            this.bind( node );
        }
        return this;
    }
	render() {
		document.querySelector( this.root ).outerHTML = this.html();
		return this;
	}
    // Update our visualisation. Data should be a datetime object or null.
    update( source ) {
        if ( source.hasOwnProperty( 'data' ) ) {
            this.data = source.data;
            Object
                .entries( this.nodes.datetime )
                .forEach( ( [ reference, node ] ) => {
                    let value = this.data
                        ? this.data[ `get${ reference }` ]()
                        : null;
                    if ( value ) {
                        if ( reference === 'Month' ) value += 1;
                        node.value = reference === 'Fullyear'
                            ? reference.toString()
                            : reference.toString().padStart( 2, '0' );
                    }
                    else node.value = '';
                } );
        }
        if ( source.root ) {
            this.root = source.root;
            this.nodes.root = document.querySelector( source.root );
        }
        if ( source.state ) this.state = source.state;
        if ( source.style ) this.style = source.style;
        // CODE
        return this;
    }
    toggleBorder() {
        const root = this.nodes.root;
        if ( root.contains( event.target ) ) root.classList.add( 'active' );
        else root.classList.remove( 'active' );
    }
    unbind() {
        //The event handlers will be garbage collected when their nodes get garbage collected.
        // So we need to remove all references to these nodes.
        // Easiest way is probably just to completely overwrite the nodes property with a clean clone.
        // Warning: using Object.assign here would reference the datetime object, so once we add DOM nodes to the datetime fields, all pickers would refer to the same Date fields.
        this.nodes = JSON.parse( JSON.stringify( DEFAULT_NODES ) ); // deep clone
        
        document.removeEventListener( 'click', this.toggleBorder.bind( this ) );
        
    }
}
// Static type.
Picker_datetime.prototype.type = Object.freeze( 'picker_datetime' );
////////////////////
module.exports = Picker_datetime;