/*  aggregation_sum.mjs   => Count amount of records
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-08
*/
// WATCHOUT:
////////////////////
// import
const UTIL = require( '../modules/util.js' );
////////////////////
/*  aggregation_sum   => 
*   @type           public Function
*   @name           aggregation_sum
*   @param          Array resources || Object grouping
*   @return         Array
*   @notes
*                   - We could probably fold the aggregation_count back into this by changing the way we check for the grouping, cfr the implementation of avg .
*/
const aggregation_sum = ( resource, parameters, dependencies ) => {
    if ( !parameters.property ) throw new Error( 'aggregation_sum: no property defined' );
    else {
        const property = parameters.property;
        // Grouped resource is an array of group objects, each containing a data property that has a chunk of the orginal resource result set.
        if ( parameters.group ) {
            const groupName = parameters.group.slice( 6 );
            const counted_resource = resource.data.map( resource => ( { [ groupName ] : resource.data[ groupName ], 'sum' : resource.data.reduce( ( sum, next ) => sum += ( Math.round( next[ property ] * 100 ) || 0 ), 0 ) / 100 } ) );
            counted_resource.unshift( { [ groupName ] : 'total', 'sum' : counted_resource.reduce( ( sum, next ) => sum += next.sum, 0.00 ) } );
            return Object.assign( resource, { "data" : counted_resource } );
        }
        else return Object.assign( resource, { "data" : [ { 'sum' : resource.reduce( ( sum, resource ) => sum += ( Math.round( resource[ property ] * 100 ) || 0 ), 0 ) / 100 } ] } );
    }
};
////////////////////
module.exports = aggregation_sum;