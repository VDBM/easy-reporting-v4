/*  model_logbook_teamleaders.mjs   => Transformation logbook_teamleaders
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-01-16
*/
// WATCHOUT:
////////////////////
// import
const { propHash, propUpdate, unique } = require( '../modules/util.js' );
////////////////////
/*  logbook_teamleaders   => 
*   @type           public Function
*   @name           logbook_teamleaders
*   @param          Array resources
*   @param          Object dependencies
*   @return         Array
*   @notes
*                   - 
*/
const model_logbook_teamleaders = ( resource, parameters, dependencies ) => {
    const entries = dependencies.partial_logbook_teamleaders_easyreporting;
    const eventsById = resource.data.reduce( propHash( 'id', true ), {} );
    // Since not every logbook entry will have proficy events bound to it, we need to reduce the easyreporting entries array, not the proficy events array.
    return Object.assign( resource, { "data" : entries.reduce( ( resource, entry ) => {
        const bound_event_ids = entry
            .data_reference
            .split( ', ' );
        return bound_event_ids.reduce( ( resource, event_id ) => {
            resource.push( Object.assign( {}, entry, eventsById[ event_id ] || {} ) );
            return resource;
        }, resource );
    }, [] ) } );
    // To support table_merged output, we need to return `{ headers, 'resource' : sorted_merge }` instead of `[]`.
};
////////////////////
module.exports = model_logbook_teamleaders;