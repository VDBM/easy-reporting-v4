/*  model_unloading_trucks_citrix.mjs   => Transformation unloading_trucks
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-01-15
*/
// WATCHOUT:
////////////////////
// import
////////////////////
const padFloatingPoint = ( number, length ) => {
    // split into integer chunk and floating point chunk.
    const chunks = number.toString().split( '.' );
    if ( !chunks[ 1 ] ) chunks[ 1 ] = '0'.repeat( length );
    while( chunks[1].length < length ) chunks[ 1 ] = chunks[ 1] + '0';
    return chunks.join( '.' );
};
/*  unloading_trucks_citrix   => 
*   @type           public Function
*   @name           unloading_trucks_citrix
*   @param          Array resource                 -
*   @return         Array
*   @notes
*                   - 
*/
const model_unloading_trucks_citrix = ( resource, parameters, dependencies ) => {
    return Object.assign( resource, { "data" : resource.data.map( entry => {
        if ( entry.AmountDelivered != undefined ) entry.AmountDelivered = padFloatingPoint( entry.AmountDelivered, 2 );
        if ( entry.DestinLevelStart != undefined ) entry.DestinLevelStart = padFloatingPoint( entry.DestinLevelStart, 5 );
        if ( entry.DestinLevelStop != undefined ) entry.DestinLevelStop = padFloatingPoint( entry.DestinLevelStop, 5 );
        return entry;
    } ) } );
};
////////////////////
module.exports = model_unloading_trucks_citrix;