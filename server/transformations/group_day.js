/*  group_day.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-09
*/
// WATCHOUT:
////////////////////
// import
const CONSTANTS = require( '../modules/constants.js' );
const UTIL = require( '../modules/util.js' );
////////////////////
/*  group_day   => 
*   @type           public Function
*   @name           group_day
*   @param          Array resources
*   @return         Array
*   @notes
*                   - 
*/
const group_day = ( resource, parameters, dependencies ) => {
    const hash_entries = resource.data.reduce( UTIL.propHash( 'starttime', false, starttime => UTIL.dateShift( starttime ).slice( 0, 6 ) ), {} ); 
    if ( parameters.aggregation && parameters.starttime && parameters.endtime ) {
        const days_in_timeframe = UTIL
            .dateSeries( parameters.starttime, parameters.endtime, CONSTANTS.MILISECONDS_DAY )
            .map( starttime => UTIL.dateShift( starttime ).slice( 0, 6 ) )
            .map( day => parseInt( day, 10 ) );
        return Object.assign( resource, { "data" : days_in_timeframe.map( day => ( { day, "data" : hash_entries[ day ] || [] } ) ) } );
    }
    else return Object.assign( resource, { "data" : Object.keys( hash_entries ).map( day => parseInt( day, 10 ) ).map( day => ( { day, "data" : hash_entries[ day ] } ) ) } );
};
////////////////////
module.exports = group_day;