/*  output_html.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-01-23
*/
// WATCHOUT:
////////////////////
// import
////////////////////
/*  output_html     =>
*   @type           public Function
*   @name           output_html
*   @param          Array resource                - 
*   @return         Object => "type", "data"
*   @notes
*                   -
*/
const output_html = ( resource, parameters, dependencies ) => {
    resource.headers = Object.assign( resource.headers, { "Content_Type" : "text/html" } );
    resource.data = `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=Edge"/>${ resource.data.style || '' }<title>ER REST</title></head><body>${ resource.data.content || '' }</body></html>`;
    return resource;
};
////////////////////
module.exports = output_html;