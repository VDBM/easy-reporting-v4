/*  group_month.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-09
*/
// WATCHOUT:
////////////////////
// import
const CONSTANTS = require( '../modules/constants.js' );
const UTIL = require( '../modules/util.js' );
////////////////////
/*
*/
const MILISECONDS_MONTH = null; // Since not all month have the same amount of days, we can't just use a fixed number here. We might have to swicth back to basic math calculations or write it fullout.
/*  group_month   => 
*   @type           public Function
*   @name           group_month
*   @param          Array resources
*   @return         Array
*   @notes
*                   - 
*/
const group_month = ( resource, parameters, dependencies ) => {
    const hash_month = resource.data.reduce( UTIL.propHash( 'starttime', false, starttime => UTIL.dateShift( starttime ).slice( 0, 4 ) ), {} );
    return Object.assign( resource, { "data" : Object.keys( hash_month ).map( month => ( { "month" : month, "data" : hash_month[ month ] } ) ) } );
};
////////////////////
module.exports = group_month;