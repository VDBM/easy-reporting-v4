/*  model_idd_dev.mjs   => Transformation teams
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-08-03
*/
////////////////////
////////////////////
/*  model_idd_dev   => 
*   @type           public Function
*   @name           model_idd_dev
*   @param          Array resources
*   @param          Object dependencies
*   @return         Array
*   @notes
*                   - 
*/
const model_idd_dev = ( resource, parameters, dependencies ) => {
    resource.data = resource.data.map( entry => Object.assign( entry, { "endable": entry.endable ? true : false }));
    return resource;
};
////////////////////
module.exports = model_idd_dev;