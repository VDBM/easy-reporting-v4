/*  output_text.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-23
*/
// WATCHOUT:
////////////////////
// import
////////////////////
/*  output_text     =>
*   @type           public Function
*   @name           output_text
*   @param          Array resource                - 
*   @return         Object => "type", "data"
*   @notes
*                   -
*/
const output_text = ( resource, parameters, dependencies )  => {
    resource.headers = Object.assign( resource.headers, { "Content-Type" : "text/plain" } );
    resource.data = JSON.stringify( resource.data );
    return resource;
};
////////////////////
module.exports = output_text;