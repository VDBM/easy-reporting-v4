/*  aggregation_count.mjs   => Count amount of records
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-08
*/
// WATCHOUT:
////////////////////
// import
const UTIL = require( '../modules/util.js' );
////////////////////
/*  aggregation_count   => 
*   @type           public Function
*   @name           aggregation_count
*   @param          Array resources || Object grouping
*   @return         Array
*   @notes
*                   - 
*/
const aggregation_count = ( resource, parameters, dependencies ) => {
    // Grouped resource.esource is an array of group objects, each containing a data property that has a chunk of the orginal resource result set.
    if ( parameters.group ) {
        const groupName = parameters.group.slice( 6 );
        const counted_resource = resource.data.map( resource => ( { [ groupName ] : resource.data[ groupName ], 'count' : resource.data.length } ) );
        counted_resource.unshift( { [ groupName ] : 'total', 'count' : resource.data.reduce( ( sum, next ) => sum += next.data.length, 0 ) } );
        return Object.assign( resource, { "data" : counted_resource } );
    }
    else return Object.assign( resource, { "data" : [ { 'count' : resource.data.length } ] } );
};
////////////////////
module.exports = aggregation_count;