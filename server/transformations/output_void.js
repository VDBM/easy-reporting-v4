/*  output_void.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-04-04
*/
// WATCHOUT:
////////////////////
// import
////////////////////
/*  output_void     =>
*   @type           public Function
*   @name           output_htm
*   @param          Array resource                - 
*   @return         Object => "type", "data"
*   @notes
*                   -
*/
const output_void = ( resource, parameters, dependencies ) => {
    resource.headers = Object.assign( resource.headers, { "Content_Type" : "text/plain" } );
    resource.data = `${ parameters.resourceName } => void(0)`;
    return resource;
};
////////////////////
module.exports = output_void;