/*  aggregation_avg.mjs   => Avg of records
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-08
*/
// WATCHOUT:
////////////////////
// import
const STATS = require( '../modules/statistics.js' );
////////////////////
/*  aggregation_avg   => 
*   @type           public Function
*   @name           aggregation_avg
*   @param          Array grouped resource
*   @return         Array
*   @notes
*                   - & aggregation                         => resource.length / resource.length = 1 => does not make sense.
*                   - & aggregation & group                 => sum( group.length ) / count( group )
*                   - & aggregation & property              => sum( resource.property ) / resource.length
*                   - & aggregation & group     & property  => sum( group.property ) / group.length
*/
/*

    aggregation without grouping = length / length = 1 => does not make sense
    aggregation group = sum( group length ) / amount of groups
    aggregation property = sum( property ) / amount of entities
    aggregation group property = per group sum( property ) / amount of entities in group


*/
const aggregation_avg = ( resource, parameters, dependencies ) => {
    // Since it doesn't not make sense to take an average of a not-grouped resource, we'll only handle the grouped version for now.
    // Grouped resource is an array of group objects, each containing a data property that has a chunk of the orginal resource result set.
    const name = parameters.resourceName;
    const average_length = resource
        .data
        .map( entry => ( { 'length' : entry.data.length } ) )
        .reduce( STATS.avg( 'length' ), 0 );
    return Object.assign( resource, {
        "data" : [ { [ `avg ${ name } for ${ resource.data.length } ${ parameters.group.slice( 6 ) }s` ] : average_length } ]
    } );
};
////////////////////
module.exports = aggregation_avg;

/*

*  aggregation_avg.mjs   => Avg of records
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-08
*
// WATCHOUT:
////////////////////
// import
const STATS = require( '../modules/statistics.js' );
////////////////////
*  aggregation_avg   => 
*   @type           public Function
*   @name           aggregation_avg
*   @param          Array grouped resource
*   @return         Array
*   @notes
*                   - 
*
const aggregation_avg = ( resource, parameters, dependencies ) => {
    // Since it doesn't not make sense to take an average of a not-grouped resource, we'll only handle the grouped version for now.
    // Grouped resource is an array of group objects, each containing a data property that has a chunk of the orginal resource result set.
    const average_length = resource
        .map( entry => ( { 'length' : entry.data.length } ) )
        .reduce( STATS.avg( 'length' ), 0 );
    return [ { [ `avg ${ parameters.resourceName } for ${ resource.length ) ${ parameters.group.slice( 6 ) }s between ${ parameters.starttime } and ${ parameters.endtime }` ] : average_length } ];
};
////////////////////
module.exports = aggregation_avg;

*/