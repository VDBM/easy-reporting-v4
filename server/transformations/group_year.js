/*  group_year.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-09
*/
// WATCHOUT:
////////////////////
// import
const CONSTANTS = require( '../modules/constants.js' );
const UTIL = require( '../modules/util.js' );
////////////////////
/*
*/
const MILISECONDS_MONTH = null; // Is this possible?
/*  group_year   => 
*   @type           public Function
*   @name           group_year
*   @param          Array resources
*   @return         Array
*   @notes
*                   - 
*/
const group_year = ( resource, parameters, dependencies ) => {
    const hash_year = resource.data.reduce( UTIL.propHash( 'starttime', false, starttime => UTIL.dateShift( starttime ).slice( 0, 2 ) ), {} );
    return Object.assign( resource, { "data" : Object.keys( hash_year ).map( year => ( { "year" : year, "data" : hash_year[ year ] } ) ) } );
};
////////////////////
module.exports = group_year;