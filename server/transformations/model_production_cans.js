/*  model_production_cans.mjs   => Transformation overpack & rejects for production_cans
*   @data_language: ES6
*   @data_version:  v1.0.1
*   @entry_user:    VDBM
*   @update_date:   2018-01-31
*/
////////////////////
// import
const STATS = require( '../modules/statistics.js' );
const UTIL = require( '../modules/util.js' );
////////////////////
/*  overpack   => 
*   @type           public Function
*   @name           overpack
*   @param          Array resources
*   @return         Array
*   @notes
*                   - 
*/
const model_production_cans = ( resource, parameters, dependencies ) => {
    // { '18022706' : [ record, record, ... ], ... }
    // this basically works as if we had the aprameter group=shift .
    // Fix this asap .
    const shifts = resource
        .data
        .reduce( UTIL.propHash( 'shift' ), {} );
    // [ { 'shift' : '18022706', 'data' : [ record, record, ... ] }, ... ]
    const chunks = UTIL
        .propUnhash( shifts, 'shift', 'data' );
    // [ { 'shift' : '18022706', 'data' : [ record, record, ... ], 'statistics' : { ... } }, ... ]
    // Since we have our multiplexer combine records and then have our transformer sort them by line and date, we ened to make sure each record ahs either a starttime or a shift property .
    const addedStatistics = chunks.map( chunk => {
        // Due to hashing the resource, we change the shift number into a string, since it's used as a key .
        // So we have to reparse it to a number before sending it to the consumer .
        return {
            'shift' : parseInt( chunk.shift, 10 ),
            'total_cans' : chunk.data.reduce( STATS.sum( 'data_cans_accepted' ), 0 ),
            'avg_overpack' : parseFloat( ( chunk.data.reduce( STATS.avg( 'data_cans_overpack' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_overpack_weighted' : parseFloat( ( chunk.data.reduce( STATS.avgWeighted( 'data_cans_overpack', 'data_cans_accepted' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_overpack_harmonic' : parseFloat( ( chunk.data.reduce( STATS.avgHarmonic( 'data_cans_overpack', 'data_cans_accepted' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_overpack_geometric' : parseFloat( ( chunk.data.reduce( STATS.avgGeometric( 'data_cans_overpack' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_rejects' : parseFloat( ( chunk.data.reduce( STATS.avg( 'data_cans_rejects' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_rejects_weighted' : parseFloat( ( chunk.data.reduce( STATS.avgWeighted( 'data_cans_rejects', 'data_cans_accepted' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_rejects_harmonic' : parseFloat( ( chunk.data.reduce( STATS.avgHarmonic( 'data_cans_rejects', 'data_cans_accepted' ), 0 ) || 0 ).toFixed( 2 ), 10 ),
            'avg_rejects_geometric' : parseFloat( ( chunk.data.reduce( STATS.avgGeometric( 'data_cans_rejects' ), 0 ) || 0 ).toFixed( 2 ), 10 )
        };
    } );
    return Object.assign( resource, { "data" : addedStatistics } );
};
////////////////////
module.exports = model_production_cans;

/* GROUPED

"[{\"group\":\"18013006\",\"data\":[{\"line\":\"line1\",\"department\":\"packing
\",\"entry_date\":\"2018-01-30T06:23:01.000Z\",\"entry_shift\":18013006,\"entry_
user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7559,\"data
_cans_weight\":167.6199951171875,\"data_cans_overpack\":2.62,\"data_cans_rejects
\":2.5799999237060547,\"data_cans_target\":165},{\"line\":\"line1\",\"department
\":\"packing\",\"entry_date\":\"2018-01-30T06:52:57.000Z\",\"entry_shift\":18013
006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\"
:7667,\"data_cans_weight\":167.08999633789062,\"data_cans_overpack\":2.09,\"data
_cans_rejects\":2.440000057220459,\"data_cans_target\":165},{\"line\":\"line1\",
\"department\":\"packing\",\"entry_date\":\"2018-01-30T07:22:54.000Z\",\"entry_s
hift\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_can
s_accepted\":7395,\"data_cans_weight\":166.80999755859375,\"data_cans_overpack\"
:1.81,\"data_cans_rejects\":2.930000066757202,\"data_cans_target\":165},{\"line\
":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T07:52:49.000Z
\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":nul
l,\"data_cans_accepted\":7548,\"data_cans_weight\":166.86000061035156,\"data_can
s_overpack\":1.86,\"data_cans_rejects\":2.690000057220459,\"data_cans_target\":1
65},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T0
8:22:45.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,
\"text\":null,\"data_cans_accepted\":7515,\"data_cans_weight\":167.0700073242187
5,\"data_cans_overpack\":2.07,\"data_cans_rejects\":2.4800000190734863,\"data_ca
ns_target\":165},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\
"2018-01-30T08:52:41.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry
_team\":null,\"text\":null,\"data_cans_accepted\":7484,\"data_cans_weight\":167,
\"data_cans_overpack\":2,\"data_cans_rejects\":2.3499999046325684,\"data_cans_ta
rget\":165},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-30T09:22:37.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":7444,\"data_cans_weight\":167.08999
633789062,\"data_cans_overpack\":2.09,\"data_cans_rejects\":2.490000009536743,\"
data_cans_target\":165},{\"line\":\"line1\",\"department\":\"packing\",\"entry_d
ate\":\"2018-01-30T09:52:33.000Z\",\"entry_shift\":18013006,\"entry_user\":null,
\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7397,\"data_cans_weight
\":167.1999969482422,\"data_cans_overpack\":2.2,\"data_cans_rejects\":2.56999993
3242798,\"data_cans_target\":165},{\"line\":\"line1\",\"department\":\"packing\"
,\"entry_date\":\"2018-01-30T10:22:28.000Z\",\"entry_shift\":18013006,\"entry_us
er\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7390,\"data_c
ans_weight\":167.2899932861328,\"data_cans_overpack\":2.29,\"data_cans_rejects\"
:1.7200000286102295,\"data_cans_target\":165},{\"line\":\"line1\",\"department\"
:\"packing\",\"entry_date\":\"2018-01-30T10:52:25.000Z\",\"entry_shift\":1801300
6,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7
412,\"data_cans_weight\":167.5399932861328,\"data_cans_overpack\":2.54,\"data_ca
ns_rejects\":1.4500000476837158,\"data_cans_target\":165},{\"line\":\"line1\",\"
department\":\"packing\",\"entry_date\":\"2018-01-30T11:22:21.000Z\",\"entry_shi
ft\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_
accepted\":7461,\"data_cans_weight\":167.2100067138672,\"data_cans_overpack\":2.
21,\"data_cans_rejects\":1.850000023841858,\"data_cans_target\":165},{\"line\":\
"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T11:52:16.000Z\",
\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\
"data_cans_accepted\":7186,\"data_cans_weight\":166.77000427246094,\"data_cans_o
verpack\":1.77,\"data_cans_rejects\":2.5399999618530273,\"data_cans_target\":165
},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T12:
04:21.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"
text\":null,\"data_cans_accepted\":2975,\"data_cans_weight\":167.5,\"data_cans_o
verpack\":2.5,\"data_cans_rejects\":2.069999933242798,\"data_cans_target\":165},
{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T13:04
:17.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"te
xt\":null,\"data_cans_accepted\":3648,\"data_cans_weight\":205.97000122070312,\"
data_cans_overpack\":5.97,\"data_cans_rejects\":4.349999904632568,\"data_cans_ta
rget\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-30T13:34:13.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":6059,\"data_cans_weight\":205.25,\"
data_cans_overpack\":5.25,\"data_cans_rejects\":2.069999933242798,\"data_cans_ta
rget\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-30T14:04:10.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":6696,\"data_cans_weight\":201.74000
549316406,\"data_cans_overpack\":1.74,\"data_cans_rejects\":2.0899999141693115,\
"data_cans_target\":200}]},{\"group\":\"18013014\",\"data\":[{\"line\":\"line1\"
,\"department\":\"packing\",\"entry_date\":\"2018-01-30T14:34:05.000Z\",\"entry_
shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_ca
ns_accepted\":6638,\"data_cans_weight\":201.94000244140625,\"data_cans_overpack\
":1.94,\"data_cans_rejects\":1.909999966621399,\"data_cans_target\":200},{\"line
\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T15:04:01.000
Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":nu
ll,\"data_cans_accepted\":6411,\"data_cans_weight\":202.1300048828125,\"data_can
s_overpack\":2.13,\"data_cans_rejects\":2.700000047683716,\"data_cans_target\":2
00},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T1
5:33:57.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,
\"text\":null,\"data_cans_accepted\":6573,\"data_cans_weight\":201.6600036621093
8,\"data_cans_overpack\":1.66,\"data_cans_rejects\":2.559999942779541,\"data_can
s_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"
2018-01-30T16:04:36.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_
team\":null,\"text\":null,\"data_cans_accepted\":6504,\"data_cans_weight\":201.5
0999450683594,\"data_cans_overpack\":1.51,\"data_cans_rejects\":2.55999994277954
1,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"ent
ry_date\":\"2018-01-30T16:34:33.000Z\",\"entry_shift\":18013014,\"entry_user\":n
ull,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6554,\"data_cans_we
ight\":201.00999450683594,\"data_cans_overpack\":1.01,\"data_cans_rejects\":2.53
99999618530273,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"pa
cking\",\"entry_date\":\"2018-01-30T17:04:28.000Z\",\"entry_shift\":18013014,\"e
ntry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6373,\
"data_cans_weight\":201.97000122070312,\"data_cans_overpack\":1.97,\"data_cans_r
ejects\":1.9700000286102295,\"data_cans_target\":200},{\"line\":\"line1\",\"depa
rtment\":\"packing\",\"entry_date\":\"2018-01-30T17:34:24.000Z\",\"entry_shift\"
:18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_acce
pted\":6342,\"data_cans_weight\":201.38999938964844,\"data_cans_overpack\":1.39,
\"data_cans_rejects\":1.7400000095367432,\"data_cans_target\":200},{\"line\":\"l
ine1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T18:04:20.000Z\",\"
entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"d
ata_cans_accepted\":6409,\"data_cans_weight\":201.89999389648438,\"data_cans_ove
rpack\":1.9,\"data_cans_rejects\":2.119999885559082,\"data_cans_target\":200},{\
"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T18:34:1
5.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text
\":null,\"data_cans_accepted\":6499,\"data_cans_weight\":201.72999572753906,\"da
ta_cans_overpack\":1.73,\"data_cans_rejects\":2.059999942779541,\"data_cans_targ
et\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-0
1-30T19:04:12.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\"
:null,\"text\":null,\"data_cans_accepted\":6604,\"data_cans_weight\":201.8399963
3789062,\"data_cans_overpack\":1.84,\"data_cans_rejects\":2.1500000953674316,\"d
ata_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_da
te\":\"2018-01-30T19:34:07.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\
"entry_team\":null,\"text\":null,\"data_cans_accepted\":6470,\"data_cans_weight\
":201.92999267578125,\"data_cans_overpack\":1.93,\"data_cans_rejects\":2.1600000
858306885,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing
\",\"entry_date\":\"2018-01-30T20:04:04.000Z\",\"entry_shift\":18013014,\"entry_
user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6479,\"data
_cans_weight\":201.67999267578125,\"data_cans_overpack\":1.68,\"data_cans_reject
s\":1.2200000286102295,\"data_cans_target\":200},{\"line\":\"line1\",\"departmen
t\":\"packing\",\"entry_date\":\"2018-01-30T20:34:00.000Z\",\"entry_shift\":1801
3014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\
":6474,\"data_cans_weight\":201.6999969482422,\"data_cans_overpack\":1.7,\"data_
cans_rejects\":1.8799999952316284,\"data_cans_target\":200},{\"line\":\"line1\",
\"department\":\"packing\",\"entry_date\":\"2018-01-30T21:03:57.000Z\",\"entry_s
hift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_can
s_accepted\":6479,\"data_cans_weight\":201.77000427246094,\"data_cans_overpack\"
:1.77,\"data_cans_rejects\":1.6200000047683716,\"data_cans_target\":200},{\"line
\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T21:33:53.000
Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":nu
ll,\"data_cans_accepted\":6403,\"data_cans_weight\":202.14999389648438,\"data_ca
ns_overpack\":2.15,\"data_cans_rejects\":2.180000066757202,\"data_cans_target\":
200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T
22:03:48.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null
,\"text\":null,\"data_cans_accepted\":6393,\"data_cans_weight\":201.889999389648
44,\"data_cans_overpack\":1.89,\"data_cans_rejects\":2.069999933242798,\"data_ca
ns_target\":200}]},{\"group\":\"18013022\",\"data\":[{\"line\":\"line1\",\"depar
tment\":\"packing\",\"entry_date\":\"2018-01-30T22:33:45.000Z\",\"entry_shift\":
18013022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accep
ted\":6201,\"data_cans_weight\":201.8300018310547,\"data_cans_overpack\":1.83,\"
data_cans_rejects\":3,\"data_cans_target\":200},{\"line\":\"line1\",\"department
\":\"packing\",\"entry_date\":\"2018-01-30T23:03:41.000Z\",\"entry_shift\":18013
022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\"
:6454,\"data_cans_weight\":201.6199951171875,\"data_cans_overpack\":1.62,\"data_
cans_rejects\":2.200000047683716,\"data_cans_target\":200},{\"line\":\"line1\",\
"department\":\"packing\",\"entry_date\":\"2018-01-30T23:33:38.000Z\",\"entry_sh
ift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans
_accepted\":6569,\"data_cans_weight\":202.52000427246094,\"data_cans_overpack\":
2.52,\"data_cans_rejects\":1.5399999618530273,\"data_cans_target\":200},{\"line\
":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T00:03:34.000Z
\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":nul
l,\"data_cans_accepted\":6500,\"data_cans_weight\":203.1999969482422,\"data_cans
_overpack\":3.2,\"data_cans_rejects\":2.869999885559082,\"data_cans_target\":200
},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T00:
33:29.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"
text\":null,\"data_cans_accepted\":6605,\"data_cans_weight\":202.1699981689453,\
"data_cans_overpack\":2.17,\"data_cans_rejects\":1.6200000047683716,\"data_cans_
target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"20
18-01-31T01:03:26.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_te
am\":null,\"text\":null,\"data_cans_accepted\":6520,\"data_cans_weight\":201.899
99389648438,\"data_cans_overpack\":1.9,\"data_cans_rejects\":2.25,\"data_cans_ta
rget\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-31T01:33:22.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":6347,\"data_cans_weight\":202.00999
450683594,\"data_cans_overpack\":2.01,\"data_cans_rejects\":2.0399999618530273,\
"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_
date\":\"2018-01-31T02:03:18.000Z\",\"entry_shift\":18013022,\"entry_user\":null
,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6580,\"data_cans_weigh
t\":201.99000549316406,\"data_cans_overpack\":1.99,\"data_cans_rejects\":1.72000
00286102295,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packi
ng\",\"entry_date\":\"2018-01-31T02:33:13.000Z\",\"entry_shift\":18013022,\"entr
y_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6453,\"da
ta_cans_weight\":202.3000030517578,\"data_cans_overpack\":2.3,\"data_cans_reject
s\":2.2899999618530273,\"data_cans_target\":200},{\"line\":\"line1\",\"departmen
t\":\"packing\",\"entry_date\":\"2018-01-31T03:03:10.000Z\",\"entry_shift\":1801
3022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\
":6534,\"data_cans_weight\":201.97999572753906,\"data_cans_overpack\":1.98,\"dat
a_cans_rejects\":2.8499999046325684,\"data_cans_target\":200},{\"line\":\"line1\
",\"department\":\"packing\",\"entry_date\":\"2018-01-31T03:33:06.000Z\",\"entry
_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_c
ans_accepted\":6579,\"data_cans_weight\":202.00999450683594,\"data_cans_overpack
\":2.01,\"data_cans_rejects\":1.809999942779541,\"data_cans_target\":200},{\"lin
e\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T04:03:01.00
0Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":n
ull,\"data_cans_accepted\":6638,\"data_cans_weight\":201.5,\"data_cans_overpack\
":1.5,\"data_cans_rejects\":2.430000066757202,\"data_cans_target\":200},{\"line\
":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T04:32:58.000Z
\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":nul
l,\"data_cans_accepted\":6366,\"data_cans_weight\":202.5399932861328,\"data_cans
_overpack\":2.54,\"data_cans_rejects\":5.349999904632568,\"data_cans_target\":20
0},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T05
:00:51.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\
"text\":null,\"data_cans_accepted\":4777,\"data_cans_weight\":201.6699981689453,
\"data_cans_overpack\":1.67,\"data_cans_rejects\":2.309999942779541,\"data_cans_
target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"20
18-01-31T05:31:14.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_te
am\":null,\"text\":null,\"data_cans_accepted\":345,\"data_cans_weight\":170.1799
9267578125,\"data_cans_overpack\":9.18,\"data_cans_rejects\":3.630000114440918,\
"data_cans_target\":161},{\"line\":\"line1\",\"department\":\"packing\",\"entry_
date\":\"2018-01-31T06:01:43.000Z\",\"entry_shift\":18013022,\"entry_user\":null
,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7428,\"data_cans_weigh
t\":164.00999450683594,\"data_cans_overpack\":3.01,\"data_cans_rejects\":2.59999
99046325684,\"data_cans_target\":161}]}]"

*/
/* NOT GROUPED

"[{\"group\":\"18013006\",\"data\":[{\"line\":\"line1\",\"department\":\"packing
\",\"entry_date\":\"2018-01-30T06:23:01.000Z\",\"entry_shift\":18013006,\"entry_
user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7559,\"data
_cans_weight\":167.6199951171875,\"data_cans_overpack\":2.62,\"data_cans_rejects
\":2.5799999237060547,\"data_cans_target\":165},{\"line\":\"line1\",\"department
\":\"packing\",\"entry_date\":\"2018-01-30T06:52:57.000Z\",\"entry_shift\":18013
006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\"
:7667,\"data_cans_weight\":167.08999633789062,\"data_cans_overpack\":2.09,\"data
_cans_rejects\":2.440000057220459,\"data_cans_target\":165},{\"line\":\"line1\",
\"department\":\"packing\",\"entry_date\":\"2018-01-30T07:22:54.000Z\",\"entry_s
hift\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_can
s_accepted\":7395,\"data_cans_weight\":166.80999755859375,\"data_cans_overpack\"
:1.81,\"data_cans_rejects\":2.930000066757202,\"data_cans_target\":165},{\"line\
":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T07:52:49.000Z
\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":nul
l,\"data_cans_accepted\":7548,\"data_cans_weight\":166.86000061035156,\"data_can
s_overpack\":1.86,\"data_cans_rejects\":2.690000057220459,\"data_cans_target\":1
65},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T0
8:22:45.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,
\"text\":null,\"data_cans_accepted\":7515,\"data_cans_weight\":167.0700073242187
5,\"data_cans_overpack\":2.07,\"data_cans_rejects\":2.4800000190734863,\"data_ca
ns_target\":165},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\
"2018-01-30T08:52:41.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry
_team\":null,\"text\":null,\"data_cans_accepted\":7484,\"data_cans_weight\":167,
\"data_cans_overpack\":2,\"data_cans_rejects\":2.3499999046325684,\"data_cans_ta
rget\":165},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-30T09:22:37.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":7444,\"data_cans_weight\":167.08999
633789062,\"data_cans_overpack\":2.09,\"data_cans_rejects\":2.490000009536743,\"
data_cans_target\":165},{\"line\":\"line1\",\"department\":\"packing\",\"entry_d
ate\":\"2018-01-30T09:52:33.000Z\",\"entry_shift\":18013006,\"entry_user\":null,
\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7397,\"data_cans_weight
\":167.1999969482422,\"data_cans_overpack\":2.2,\"data_cans_rejects\":2.56999993
3242798,\"data_cans_target\":165},{\"line\":\"line1\",\"department\":\"packing\"
,\"entry_date\":\"2018-01-30T10:22:28.000Z\",\"entry_shift\":18013006,\"entry_us
er\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7390,\"data_c
ans_weight\":167.2899932861328,\"data_cans_overpack\":2.29,\"data_cans_rejects\"
:1.7200000286102295,\"data_cans_target\":165},{\"line\":\"line1\",\"department\"
:\"packing\",\"entry_date\":\"2018-01-30T10:52:25.000Z\",\"entry_shift\":1801300
6,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7
412,\"data_cans_weight\":167.5399932861328,\"data_cans_overpack\":2.54,\"data_ca
ns_rejects\":1.4500000476837158,\"data_cans_target\":165},{\"line\":\"line1\",\"
department\":\"packing\",\"entry_date\":\"2018-01-30T11:22:21.000Z\",\"entry_shi
ft\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_
accepted\":7461,\"data_cans_weight\":167.2100067138672,\"data_cans_overpack\":2.
21,\"data_cans_rejects\":1.850000023841858,\"data_cans_target\":165},{\"line\":\
"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T11:52:16.000Z\",
\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"text\":null,\
"data_cans_accepted\":7186,\"data_cans_weight\":166.77000427246094,\"data_cans_o
verpack\":1.77,\"data_cans_rejects\":2.5399999618530273,\"data_cans_target\":165
},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T12:
04:21.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"
text\":null,\"data_cans_accepted\":2975,\"data_cans_weight\":167.5,\"data_cans_o
verpack\":2.5,\"data_cans_rejects\":2.069999933242798,\"data_cans_target\":165},
{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T13:04
:17.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team\":null,\"te
xt\":null,\"data_cans_accepted\":3648,\"data_cans_weight\":205.97000122070312,\"
data_cans_overpack\":5.97,\"data_cans_rejects\":4.349999904632568,\"data_cans_ta
rget\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-30T13:34:13.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":6059,\"data_cans_weight\":205.25,\"
data_cans_overpack\":5.25,\"data_cans_rejects\":2.069999933242798,\"data_cans_ta
rget\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-30T14:04:10.000Z\",\"entry_shift\":18013006,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":6696,\"data_cans_weight\":201.74000
549316406,\"data_cans_overpack\":1.74,\"data_cans_rejects\":2.0899999141693115,\
"data_cans_target\":200}]},{\"group\":\"18013014\",\"data\":[{\"line\":\"line1\"
,\"department\":\"packing\",\"entry_date\":\"2018-01-30T14:34:05.000Z\",\"entry_
shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_ca
ns_accepted\":6638,\"data_cans_weight\":201.94000244140625,\"data_cans_overpack\
":1.94,\"data_cans_rejects\":1.909999966621399,\"data_cans_target\":200},{\"line
\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T15:04:01.000
Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":nu
ll,\"data_cans_accepted\":6411,\"data_cans_weight\":202.1300048828125,\"data_can
s_overpack\":2.13,\"data_cans_rejects\":2.700000047683716,\"data_cans_target\":2
00},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T1
5:33:57.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,
\"text\":null,\"data_cans_accepted\":6573,\"data_cans_weight\":201.6600036621093
8,\"data_cans_overpack\":1.66,\"data_cans_rejects\":2.559999942779541,\"data_can
s_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"
2018-01-30T16:04:36.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_
team\":null,\"text\":null,\"data_cans_accepted\":6504,\"data_cans_weight\":201.5
0999450683594,\"data_cans_overpack\":1.51,\"data_cans_rejects\":2.55999994277954
1,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"ent
ry_date\":\"2018-01-30T16:34:33.000Z\",\"entry_shift\":18013014,\"entry_user\":n
ull,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6554,\"data_cans_we
ight\":201.00999450683594,\"data_cans_overpack\":1.01,\"data_cans_rejects\":2.53
99999618530273,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"pa
cking\",\"entry_date\":\"2018-01-30T17:04:28.000Z\",\"entry_shift\":18013014,\"e
ntry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6373,\
"data_cans_weight\":201.97000122070312,\"data_cans_overpack\":1.97,\"data_cans_r
ejects\":1.9700000286102295,\"data_cans_target\":200},{\"line\":\"line1\",\"depa
rtment\":\"packing\",\"entry_date\":\"2018-01-30T17:34:24.000Z\",\"entry_shift\"
:18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_acce
pted\":6342,\"data_cans_weight\":201.38999938964844,\"data_cans_overpack\":1.39,
\"data_cans_rejects\":1.7400000095367432,\"data_cans_target\":200},{\"line\":\"l
ine1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T18:04:20.000Z\",\"
entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"d
ata_cans_accepted\":6409,\"data_cans_weight\":201.89999389648438,\"data_cans_ove
rpack\":1.9,\"data_cans_rejects\":2.119999885559082,\"data_cans_target\":200},{\
"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T18:34:1
5.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text
\":null,\"data_cans_accepted\":6499,\"data_cans_weight\":201.72999572753906,\"da
ta_cans_overpack\":1.73,\"data_cans_rejects\":2.059999942779541,\"data_cans_targ
et\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-0
1-30T19:04:12.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\"
:null,\"text\":null,\"data_cans_accepted\":6604,\"data_cans_weight\":201.8399963
3789062,\"data_cans_overpack\":1.84,\"data_cans_rejects\":2.1500000953674316,\"d
ata_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_da
te\":\"2018-01-30T19:34:07.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\
"entry_team\":null,\"text\":null,\"data_cans_accepted\":6470,\"data_cans_weight\
":201.92999267578125,\"data_cans_overpack\":1.93,\"data_cans_rejects\":2.1600000
858306885,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing
\",\"entry_date\":\"2018-01-30T20:04:04.000Z\",\"entry_shift\":18013014,\"entry_
user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6479,\"data
_cans_weight\":201.67999267578125,\"data_cans_overpack\":1.68,\"data_cans_reject
s\":1.2200000286102295,\"data_cans_target\":200},{\"line\":\"line1\",\"departmen
t\":\"packing\",\"entry_date\":\"2018-01-30T20:34:00.000Z\",\"entry_shift\":1801
3014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\
":6474,\"data_cans_weight\":201.6999969482422,\"data_cans_overpack\":1.7,\"data_
cans_rejects\":1.8799999952316284,\"data_cans_target\":200},{\"line\":\"line1\",
\"department\":\"packing\",\"entry_date\":\"2018-01-30T21:03:57.000Z\",\"entry_s
hift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_can
s_accepted\":6479,\"data_cans_weight\":201.77000427246094,\"data_cans_overpack\"
:1.77,\"data_cans_rejects\":1.6200000047683716,\"data_cans_target\":200},{\"line
\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T21:33:53.000
Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null,\"text\":nu
ll,\"data_cans_accepted\":6403,\"data_cans_weight\":202.14999389648438,\"data_ca
ns_overpack\":2.15,\"data_cans_rejects\":2.180000066757202,\"data_cans_target\":
200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-30T
22:03:48.000Z\",\"entry_shift\":18013014,\"entry_user\":null,\"entry_team\":null
,\"text\":null,\"data_cans_accepted\":6393,\"data_cans_weight\":201.889999389648
44,\"data_cans_overpack\":1.89,\"data_cans_rejects\":2.069999933242798,\"data_ca
ns_target\":200}]},{\"group\":\"18013022\",\"data\":[{\"line\":\"line1\",\"depar
tment\":\"packing\",\"entry_date\":\"2018-01-30T22:33:45.000Z\",\"entry_shift\":
18013022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accep
ted\":6201,\"data_cans_weight\":201.8300018310547,\"data_cans_overpack\":1.83,\"
data_cans_rejects\":3,\"data_cans_target\":200},{\"line\":\"line1\",\"department
\":\"packing\",\"entry_date\":\"2018-01-30T23:03:41.000Z\",\"entry_shift\":18013
022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\"
:6454,\"data_cans_weight\":201.6199951171875,\"data_cans_overpack\":1.62,\"data_
cans_rejects\":2.200000047683716,\"data_cans_target\":200},{\"line\":\"line1\",\
"department\":\"packing\",\"entry_date\":\"2018-01-30T23:33:38.000Z\",\"entry_sh
ift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans
_accepted\":6569,\"data_cans_weight\":202.52000427246094,\"data_cans_overpack\":
2.52,\"data_cans_rejects\":1.5399999618530273,\"data_cans_target\":200},{\"line\
":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T00:03:34.000Z
\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":nul
l,\"data_cans_accepted\":6500,\"data_cans_weight\":203.1999969482422,\"data_cans
_overpack\":3.2,\"data_cans_rejects\":2.869999885559082,\"data_cans_target\":200
},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T00:
33:29.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"
text\":null,\"data_cans_accepted\":6605,\"data_cans_weight\":202.1699981689453,\
"data_cans_overpack\":2.17,\"data_cans_rejects\":1.6200000047683716,\"data_cans_
target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"20
18-01-31T01:03:26.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_te
am\":null,\"text\":null,\"data_cans_accepted\":6520,\"data_cans_weight\":201.899
99389648438,\"data_cans_overpack\":1.9,\"data_cans_rejects\":2.25,\"data_cans_ta
rget\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018
-01-31T01:33:22.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team
\":null,\"text\":null,\"data_cans_accepted\":6347,\"data_cans_weight\":202.00999
450683594,\"data_cans_overpack\":2.01,\"data_cans_rejects\":2.0399999618530273,\
"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_
date\":\"2018-01-31T02:03:18.000Z\",\"entry_shift\":18013022,\"entry_user\":null
,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6580,\"data_cans_weigh
t\":201.99000549316406,\"data_cans_overpack\":1.99,\"data_cans_rejects\":1.72000
00286102295,\"data_cans_target\":200},{\"line\":\"line1\",\"department\":\"packi
ng\",\"entry_date\":\"2018-01-31T02:33:13.000Z\",\"entry_shift\":18013022,\"entr
y_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":6453,\"da
ta_cans_weight\":202.3000030517578,\"data_cans_overpack\":2.3,\"data_cans_reject
s\":2.2899999618530273,\"data_cans_target\":200},{\"line\":\"line1\",\"departmen
t\":\"packing\",\"entry_date\":\"2018-01-31T03:03:10.000Z\",\"entry_shift\":1801
3022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_cans_accepted\
":6534,\"data_cans_weight\":201.97999572753906,\"data_cans_overpack\":1.98,\"dat
a_cans_rejects\":2.8499999046325684,\"data_cans_target\":200},{\"line\":\"line1\
",\"department\":\"packing\",\"entry_date\":\"2018-01-31T03:33:06.000Z\",\"entry
_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":null,\"data_c
ans_accepted\":6579,\"data_cans_weight\":202.00999450683594,\"data_cans_overpack
\":2.01,\"data_cans_rejects\":1.809999942779541,\"data_cans_target\":200},{\"lin
e\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T04:03:01.00
0Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":n
ull,\"data_cans_accepted\":6638,\"data_cans_weight\":201.5,\"data_cans_overpack\
":1.5,\"data_cans_rejects\":2.430000066757202,\"data_cans_target\":200},{\"line\
":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T04:32:58.000Z
\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\"text\":nul
l,\"data_cans_accepted\":6366,\"data_cans_weight\":202.5399932861328,\"data_cans
_overpack\":2.54,\"data_cans_rejects\":5.349999904632568,\"data_cans_target\":20
0},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"2018-01-31T05
:00:51.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_team\":null,\
"text\":null,\"data_cans_accepted\":4777,\"data_cans_weight\":201.6699981689453,
\"data_cans_overpack\":1.67,\"data_cans_rejects\":2.309999942779541,\"data_cans_
target\":200},{\"line\":\"line1\",\"department\":\"packing\",\"entry_date\":\"20
18-01-31T05:31:14.000Z\",\"entry_shift\":18013022,\"entry_user\":null,\"entry_te
am\":null,\"text\":null,\"data_cans_accepted\":345,\"data_cans_weight\":170.1799
9267578125,\"data_cans_overpack\":9.18,\"data_cans_rejects\":3.630000114440918,\
"data_cans_target\":161},{\"line\":\"line1\",\"department\":\"packing\",\"entry_
date\":\"2018-01-31T06:01:43.000Z\",\"entry_shift\":18013022,\"entry_user\":null
,\"entry_team\":null,\"text\":null,\"data_cans_accepted\":7428,\"data_cans_weigh
t\":164.00999450683594,\"data_cans_overpack\":3.01,\"data_cans_rejects\":2.59999
99046325684,\"data_cans_target\":161}]}]"

*/