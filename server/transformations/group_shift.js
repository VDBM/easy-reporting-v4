/*  group_shift.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-09
*/
// WATCHOUT:
////////////////////
// import
const CONSTANTS = require( '../modules/constants.js' );
const UTIL = require( '../modules/util.js' );
////////////////////
/*  group_shift   => 
*   @type           public Function
*   @name           group_shift
*   @param          Array resources
*   @return         Array
*   @notes
*                   - If there's an aggregation attached to the request, we need to fill in all the timechunks than don't have an event associated with it so we actually report all the timechunks in the interval.
*                   - In version 1 we just always sent the timechunks without data with the results, but this caused queries that group, but not aggregate to show empty rows for the timechunsk that ahd no events.
*                   - SIcne this is not desirable, we added the explicit check for an aggegation in the parameters.
*/
const group_shift = ( resource, parameters, dependencies ) => {
    const hash_entries = resource.data.reduce( UTIL.propHash( 'starttime', false, UTIL.dateShift ), {} );
    if ( parameters.aggregation && parameters.starttime && parameters.endtime ) {
        const shifts_in_timeframe = UTIL
            .dateSeries( parameters.starttime, parameters.endtime, CONSTANTS.MILISECONDS_SHIFT )
            .map( UTIL.dateShift )
            .map( shift => parseInt( shift, 10 ) );
        return Object.assign( resource, { "data" : shifts_in_timeframe.map( shift => ( { shift, "data" : hash_entries[ shift ] || [] } ) ) } );
    }
    else return Object.assign( resource, { "data" : Object.keys( hash_entries ).map( shift => parseInt( shift, 10 ) ).map( shift => ( { shift, "data" : hash_entries[ shift ] } ) ) } );
};
////////////////////
module.exports = group_shift;