/*  output_table.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-23
*/
// WATCHOUT: Changed the output parameter to check if the object is actuall;y an array. If not, we'll assume that the resource itsself contains the headers and resource properties.
//           We changed this to be able to implement table merging into transformations instead of defining a new output type for it, where we'd have to complciate the transformation to output passing too much.
//            Might still change in the future if this architecture doesn't fit our needs.
//            We should check inside the tempalte if a value is an array. If we check it in advance, we need to pass the name of the proeprty that is a aprameetrs as well, further complicating the whole template.
//              ${exec(JSON.stringify(source.entry[ header ]))} WORKS, ${exec(JSON.stringify(source.entry[header]))} DOES NOT!!!
////////////////////
const { Template } = require( '../modules/class_template.js' );
////////////////////
/*
*/
const template_page = '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=Edge"/>${style}<title>ER REST</title></head><body>${content}</body></html>';
/*
*/
const template_style_table = '<style>table { border-collapse: collapse; } th, td { border: 1px solid black; padding: 4px; text-align: center; } .grouping { font-weight: bold; vertical-align: top; }</style>';
/*  output_table     =>
*   @type           public Function
*   @name           output_table
*   @param          Array resource                - 
*   @return        
*   @notes
*/
const output_table = ( resource, parameters, dependencies ) => {
    // resource.length confirms resource[ 0 ] exists.
    // parameters.group confirms resource[ 0 ] has a property data.
    // Array.isArray() confirms that the data property is an array.
    // If the data property is an array, it'll always be an object, either the raw resource entries or some transformation result.
    const data = {
        // If we have a grouped resource, we need to unwrap it .
        'headers_data' : resource.data.length && parameters.group && Array.isArray( resource.data[ 0 ].data )
            ? Object.keys( resource.data[ 0 ].data[ 0 ] )
            : [],
        'headers_render' : !( parameters.headers === false ),
        'headers_table' : Object.keys( resource.data[ 0 ] || {} ),
        "resource" : resource.data
    };
    const templated_table = new Template( data.headers_data.length ? 'output_table_groups' : 'output_table' );
    const templated_page = template_page
        .replace( '${style}', template_style_table )
        .replace( '${content}',  templated_table.html( data ) );
    resource.headers = Object.assign( resource.headers, { "Content-Type" : "text/html" } );
    resource.data = templated_page;
    return resource;
};
////////////////////
module.exports = output_table;