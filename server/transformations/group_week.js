/*  group_week.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-12
*/
// WATCHOUT:
////////////////////
// import
const CONSTANTS = require( '../modules/constants.js' );
const UTIL = require( '../modules/util.js' );
////////////////////
/*
*/

// TODO: This doersn't extract the correct week, so we probably need to use the starttime of the shift of the even to actually determine the correct week.
// TODO: When we add shfit to each query, this just becomes a slice of a property instead of a calculation?


const MILISECONDS_WEEK = null; // Is this possible? Weeks are always 7 days, but doublecheck if it holds out using miliseconds when crossing month / year borders
/*  group_week   => 
*   @type           public Function
*   @name           group_shift
*   @param          Array resources
*   @return         Array
*   @notes
*                   - 
*/
const group_week = ( resource, parameters, dependencies ) => {
    const hash_week = resource.data.reduce( UTIL.propHash( 'starttime', false, starttime => {
        const year = new Date( starttime ).getFullYear().toString().slice( 2 );
        let day = Math.ceil( UTIL.dateDayOfYear( starttime ) / 7 ).toString();
        if ( day.length < 2 ) day = `0${ day }`;
        return Object.assign( resource, { "data" : `${ year }${ day }` } );
    } ), {} );
    return Object.assign( resource, { "data" : Object.keys( hash_week ).map( week => ( { "week" : week, "data" : hash_week[ week ] } ) ) } );
};
////////////////////
module.exports = group_week;