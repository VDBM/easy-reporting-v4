/*  output_json.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2017-11-23
*/
// WATCHOUT:
////////////////////
// import
////////////////////
/*  output_json     =>
*   @type           public Function
*   @name           output_json
*   @param          Array resource                - 
*   @return         Object => "type", "data"
*   @notes
*                   -
*/
const output_json = ( resource, parameters, dependencies ) => {
    resource.headers = Object.assign( resource.headers, { "Content-Type" : "application/json" } );
    resource.data = JSON.stringify( resource.data );
    return resource;
};
////////////////////
module.exports = output_json;