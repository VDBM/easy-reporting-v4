/*  sort_line_date.mjs   => 
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-02-12
*/
// WATCHOUT:
////////////////////
// import
const UTIL = require( '../modules/util.js' );
////////////////////
/*  sort_line_date   => 
*   @type           public Function
*   @name           sort_line_date
*   @param          Array resources
*   @return         Array resources
*   @notes
*                   - TODO: we can make this more eeficient by checking for starttime and shift property first, so that we don't loop eneedelssly if the resource can't be sorted.
*/
const sort_line_date = ( resource, parameters, dependencies ) => {
    
   // console.log( 'sorting' );
    
    resource.data = resource.data.sort( ( first, second ) => {
        if ( first.line < second.line ) return -1;
        else if ( first.line > second.line ) return 1;
        else if ( first.line === second.line ) {
            // check starttime first, so that records in the same shift get sorted as well
            if ( first.starttime ) {
                const date_first = new Date( first.starttime );
                const date_second = new Date( second.starttime );
                return date_first.getTime() - date_second.getTime();
            }
            // Else if there's not datetime tosort on, check for the shift property
            else if ( first.shift ) return first.shift - second.shift;
            // If no starttime or shift is available, we can't actually sort this array and just return the original order.
            else return 0;
        }
    } );
    
  //  console.log( resource.data );
    
    
    return resource;
};
////////////////////
module.exports = sort_line_date;