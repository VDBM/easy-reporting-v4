/*  model_teams.mjs   => Transformation teams
*   @data_language: ES6
*   @data_version:  v1.0.0
*   @entry_user:    VDBM
*   @update_date:   2018-05-22
*/
////////////////////
// import
const { dateShift } = require( '../modules/util.js' );
const CONFIG = require( '../resources/teams.config.json' );
////////////////////
/*  model_teams   => 
*   @type           public Function
*   @name           model_teams
*   @param          Array resources
*   @param          Object dependencies
*   @return         Array
*   @notes
*                   - 
*/
const model_teams = ( resource, parameters, dependencies ) => {
    const datetime = new Date( parameters.starttime );
    const year = datetime.getFullYear().toString();
    const shiftCharacter = { "06" : "v", "14": "l", "22": "n" }[ dateShift( datetime ).toString().slice( 6, 8 ) ];
    const day_of_month = datetime.getDate() - 1;
    const month_index = datetime.getMonth() * 5;
    const team_index = CONFIG
        .find( collection => collection.name === year )
        .data
        .findIndex( ( row, index ) => ( index >= month_index ) && ( index < month_index + 5 ) && ( row.charAt( day_of_month ) === shiftCharacter ) );
    resource.data = [ { "team": `team${ ( team_index % 5 ) + 1}` } ];
    return resource;
};
////////////////////
module.exports = model_teams;