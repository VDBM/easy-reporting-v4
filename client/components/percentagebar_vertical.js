/*  percentagebar_vertical.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-08-06
*/
/*  API
*      
*
*/
/* TODO
*
*/
//  DEPENDENCIES
////    IMPORT
import Component from '../core/component.js';
import { isStr } from '../core/util.js';
////    Helpers
//  COMPONENTS
class Percentagebar_vertical extends Component {
    constructor({ config, name, root, state = 0, template } = {}) {
        template = template || [
            '<div class="comp-pbar_v" id="${ id }">',
                '<div class="${exec( state.data >= config.limit ? "ool" : "" )}" style="height: ${exec( this.height() )}%;">${ state }</div></div>',
            '</div>'
        ].join('');
        super({
            config: {
                max: 100,
                min: 0,
                limit: 100
            },
            name,
            root,
            state,
            template
        });
        this.event_types = [];
        this.interval = null;
    }
    autorun() {
        this.interval = setInterval(() => {
            this.update({
                state: Math.floor( Math.random() * ( this.config.max - this.config.min + 1 ) ) + this.config.min
            });
        }, 1500 );
    }
    height() {
        return `${ 100 / ( this.config.max - this.config.min ) * this.state }%`;
    }
    remove() {
        if ( this.interval ) clearInterval( this.interval );
        super.remove();
    }
    // Since we have an animation, we can't actually rerender upon updating.
    update({ render = true, state } = {}) {
        this.state = state;
        if ( render && !this.is_rendered ) this.render();
        const root = this.root();
        const bar = root.querySelector( `#${ this.id } > div` );
        bar.style.height = this.height();
        bar.innerHTML = this.state;
        return this;
    }
}
Percentagebar_vertical.prototype.module_type = 'percentagebar_vertical';
//
export default Percentagebar_vertical;