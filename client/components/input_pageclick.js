/*  input_pageclick.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-25
*/
/*  API
*       Since we only need events, we might as well inherit from Mediator instead of Component and just reimplement the few component methods we need.
*       The API isn't the same anymore as other elements though.
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORT
import { DOM_EVENTS } from '../core/util.js';
import Mediator from '../core/mediator.js';
////    Helpers
//  COMPONENTS
class Input_pageclick extends Mediator {
    constructor( handler ) {
        super();
        this.handler = handler;
        this.root = () => document;
    }
    init() {
        this.on( 'click', { selector: 'html' }, this.handler );
        DOM_EVENTS( "add", this, [ "click" ] );
        return this;
    }
    remove() {
        DOM_EVENTS( "remove", this, [ "click" ] );
        this.trigger( 'remove' );
        return this;
    }
}
Input_pageclick.prototype.module_type = 'input_pageclick';
//
export default Input_pageclick;