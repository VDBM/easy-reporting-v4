/*  datatable_edit.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-17
*/
/*  API
*
*/
/*  TODO
*
*       After editting a value, the sorting is not correct anymore if the column was sorted before.
*
*/
//  DEPENDENCIES
////    IMPORTS
import Datatable_sort from './datatable_sort.js';
import Input_date from './input_date.js';
import Input_number from './input_number.js';
import Input_string from './input_string.js';
import { nodeParent } from '../core/util.js';
import Popover from './popover.js';
////    Helpers
const inputs_factory = {
    "[object Date]": Input_date,
    "[object Number]": Input_number,
    "[object String]": Input_string
};
const default_template = [
    '<table class="comp-datatable" id="${ id }">',
        '<thead>',
            '<tr>',
                '<th data-tmpl="foreach header in state[0]" class="${exec( source.sort_reverse && source.sort_property === header.key ? ' + '"arrow-up"' + ' : "arrow-down"' + ')}">${ header.key }</th>',
            '</tr>',
        '</thead>',
        '<tbody class="striped">',
            '<tr data-tmpl="foreach entry in state" id="dte_${ entry.id }">',
                '<td data-tmpl="foreach property in entry">${ property.value }</td>',
            '</tr>',
        '</tbody>',
    '</table>'
].join('');
//  COMPONENTS
class Datatable_edit extends Datatable_sort {
    constructor({ config, name, root, state, template = default_template } = {}) {
        //if ( !state.length ) throw new Error( `cannot create datatable_edit ${ name }: no default state specified.` );
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.active_edit = null;
    }
    init() {
        // When a data cell is clicked, we want to edit the cell content.
        // Actually rendering the input element inside the cells just means we need to write more event handler stopPropagation() BS.
        // So we opted to just use a popover instead, we only edit one value at the time anyway.
        this.on( 'click', { selector: `#${ this.id } tbody > tr` }, event => {
            // The event target gives us the row.
            const row = nodeParent( event.target, 'tr' );
            // The row gives us an id to parse.
            const id = parseInt( row.id.replace( /\D/g, '' ), 10 );
            // The id gives us the entry from the state.
            const entry = this.state.find( entry => entry.id === id );
            // The cell index finds us the property.
            const property = Object.keys( this.config )[ event.target.cellIndex ];
            // The config gives us the data type of the property.
            const type = this.config[ property ];
            // Create the popover. Inject the input element as the state of the popover.
            // The popover will then init and render the component once it has rendered itsself.
            // So we only have to initand render the popover and let the lifecycle management of the popover do the rest.
            const input_component = new inputs_factory[ type ]({ "name": "editor", "root": "#popover_content", "state": entry[ property ] });
            input_component.on( 'change', event => this.trigger( 'edit', event, input_component, id, property ));
            const popover = new Popover({ "name": "editor", "root": "#hook_popover", "state": input_component });
            popover.on( 'remove', () => {
                this.active_edit.state.remove();
                // The popover will .remove() itsself, so no need to explictly do that again here.
                this.active_edit = null;
            });
            popover.init().then( instance => instance.render());
            this.active_edit = popover;
        });
        return super.init();
    }
}
Datatable_edit.prototype.module_type = 'datatable_edit';
//
export default Datatable_edit;