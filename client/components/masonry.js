/*  API
*
*       - This version only suppports 1, 2, 3, 6
*
*
*
*/
/*  TODO
*
*       - Write a 1, 2, 4, 8 version
*       - Write a 1, 5, 10 version
*
*/
////    DEPENDENCIES    ////
//  IMPORT
import Component from '../core/component.js';
//  STATIC
/*
const default_template = [
    '<ul class="comp-masonry color-11-bg list-clean">',
        '<template data-tmpl="foreach row in config">',
            '<li class="',
            'grid-${exec( ( source.collection.index + 1 ) * 2 )}-${exec( 2 + ( collection.index * source.source.grid_span / source.row * 2 - 1 ) + 1 )} ',
            'grid-sc-${exec( source.source.grid_span / source.row * 2 - 1 )} ',
            'color-7-bg" data-tmpl="iterate row">${ collection.index }</li>',
        '</template>',
    '</ul>'
].join('');
*/
const default_data = [
    { "starttime": "2018-08-01T02:00:00.000Z" },
    { "starttime": "2018-08-02T02:00:00.000Z" },
    { "starttime": "2018-08-03T02:00:00.000Z" },
    { "starttime": "2018-08-04T02:00:00.000Z" },
    { "starttime": "2018-08-05T02:00:00.000Z" },
    { "starttime": "2018-08-06T02:00:00.000Z" },
    { "starttime": "2018-08-07T02:00:00.000Z" }/*,
    { "starttime": "2018-08-08T02:00:00.000Z" },
    { "starttime": "2018-08-09T02:00:00.000Z" },
    { "starttime": "2018-08-10T02:00:00.000Z" },
    { "starttime": "2018-08-11T02:00:00.000Z" },
    { "starttime": "2018-08-12T02:00:00.000Z" },
    { "starttime": "2018-08-13T02:00:00.000Z" },
    { "starttime": "2018-08-14T02:00:00.000Z" },
    { "starttime": "2018-08-15T02:00:00.000Z" },
    { "starttime": "2018-08-16T02:00:00.000Z" },
    { "starttime": "2018-08-17T02:00:00.000Z" },
    { "starttime": "2018-08-18T02:00:00.000Z" },
    { "starttime": "2018-08-19T02:00:00.000Z" },
    { "starttime": "2018-08-20T02:00:00.000Z" }/*,
    { "starttime": "2018-08-21T02:00:00.000Z" },
    { "starttime": "2018-08-22T02:00:00.000Z" },
    { "starttime": "2018-08-23T02:00:00.000Z" },
    { "starttime": "2018-08-24T02:00:00.000Z" },
    { "starttime": "2018-08-25T02:00:00.000Z" },
    { "starttime": "2018-08-26T02:00:00.000Z" },
    { "starttime": "2018-08-27T02:00:00.000Z" },
    { "starttime": "2018-08-28T02:00:00.000Z" },
    { "starttime": "2018-08-29T02:00:00.000Z" },
    { "starttime": "2018-08-30T02:00:00.000Z" },
    { "starttime": "2018-08-31T02:00:00.000Z" },
    { "starttime": "2018-09-01T02:00:00.000Z" },
    { "starttime": "2018-09-02T02:00:00.000Z" },*/
];
const default_template = [
    '<section class="comp-masonry">',
        '<ul class="list-clean">',
            '<li class="grid-1-1 grid-sr-2 spacer"></li>',
            '<template data-tmpl="foreach item in state">',
                '<li class="grid-1-${exec( collection.index * 3 + 2 )} grid-sc-3 ${exec( item.highlight ? " active" : "" )}">${exec( item.label )}</li>',
                '<li class="grid-2-${exec( collection.index * 3 + 2 )}${exec( item.highlight ? " active" : "" )}">V</li>',
                '<li class="grid-2-${exec( collection.index * 3 + 3 )}${exec( item.highlight ? " active" : "" )}">L</li>',
                '<li class="grid-2-${exec( collection.index * 3 + 4 )}${exec( item.highlight ? " active" : "" )}">N</li>',
            '</template>',
            '<li class="grid-1-23 grid-sr-2 spacer"></li>',
        '</ul>',
    '</section>'
].join( '' );
////    COMPONENTS    ////
class Masonry extends Component {
    constructor({ config, name, root, state, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
        const label_today = new Date().toJSON().split( 'T' )[ 0 ].split( '-' ).reverse().slice( 0, 2 ).join( '-' );
        this.event_types = [ "click", "keydown", "keyup" ];
        this.state = default_data.map( source => {
            const item = {
                date: new Date( source.starttime ),
                highlight: null,
                label: null,
                starttime: source.starttime,
            };
            const [ chunk_date, chunk_time ] = item.date.toJSON().split( 'T' );
            item.label = chunk_date.split( '-' ).reverse().slice( 0, 2 ).join( '-' );
            item.highlight = item.label === label_today;
            return item;
        });
    }
}
Masonry.prototype.module_type = 'masonry';
//
export default Masonry;