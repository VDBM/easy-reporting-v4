/*  menu_links.mjs       =>
*   @language:  ES6
*   @version:   v1.0.0
*   @creator:   VDBM
*   @date:      2018-08-02
*/
/*  API
*/
/*  TODO
*/
//  DEPENDENCIES
////    Import
import Component from '../core/component.js';
import { nodeParent } from '../core/util.js';
////    Helpers
const default_template = [
    '<ul class="comp-menu-links" id="${ id }">',
        '<li data-tmpl="foreach link in config" class="${exec( link.uri === source.state.uri ? "active" : "" )}">',
            '<a href="${ link.uri }">${ link.label }</a>',
        '</li>',
    '</ul>',
    '<article id="${ name }_content"></article>'
].join('');
////    Data
//  COMPONENTS
class Menu_links extends Component {
    // A state property cannot be created through the constructor since we don't want to be able to overwrite the default state object without breaking functionality.
    // By including the highlight check in the template, we make rerouting and automation way easier. Else we'd have to hook up a render event that will highlight the correct node.
    constructor({ config, name, root, template = default_template }) {
        super({
            config,
            name,
            root,
            state: {
                highlighted: null,
                uri: null
            },
            template
        });
        this.interval = null;
    }
    autorun( ms ) {
        const cleanup = event => {
            if ( this.interval ) {
                clearInterval( this.interval );
                this.off( 'remove', cleanup );
            }
        };
        const reroute = (function() {
            let active_route = 0;
            return function() {
                const root = this.root();
                const next_route = this.config[ active_route ].uri;
                this.highlight( root.querySelector( `a[href="${ next_route }"]` ));
                window.location = next_route;
                active_route += 1;
                if ( active_route >= this.config.length ) active_route = 0;
            }.bind( this );
        }.bind( this )());
        this.on( 'remove', cleanup );
        reroute();
        this.interval = setInterval( reroute, ms );
        return this;
    }
    autostop() {
        clearInterval( this.interval );
        return this;
    } 
    highlight( target ) {
        //  If we call .highlight( null ) , list_item will be false due to nodeParent.
        //  Since our state will never be 'false', the if clause is triggered, removing the last highlight.
        //  If we then also set our uri to null, neither highlighting the new node, nor the lsit_item will trigger.
        //  Our state will both be reset to null, brining us into abse condition without a rerender.
        const list_item = nodeParent( target, 'li' );
        if ( this.state.highlighted !== list_item ) {
            if ( this.state.highlighted ) this.state.highlighted.classList.remove( 'active' );
            else if ( this.state.uri ) {
                const root = this.root();
                const highlight = root.querySelector( '.active' );
                if ( highlight ) highlight.classList.remove( 'active' );
            }
            if ( list_item ) list_item.classList.add( 'active' );
        }
        this.state.highlighted = list_item;
        this.state.uri = target
            ? target.getAttribute( 'href' )
            : null;
        return this;
    }
    init() {
        this.on( 'click', { selector: 'a' }, event => this.highlight( event.target ));
        super.init();
        return this;
    }
    inject( selector ) {
        if ( !this.is_rendered ) throw new Error( 'Cannot inject menu elements in an unrendered menu.' );
        else {
            const root = this.root();
            const list = root.querySelector( 'ul.comp-menu-links' );
            const item = document.createElement( 'li' );
            // Slice off the leading hash or dot.
            item.id = selector;
            item.setAttribute( 'data-injection', true );
            list.appendChild( item );
        }
        return this;
    }
    remove() {
        if ( this.interval ) clearInterval( this.interval );
        super.remove();
    }
}
Menu_links.prototype.module_type = 'menu_links';
//
export default Menu_links;