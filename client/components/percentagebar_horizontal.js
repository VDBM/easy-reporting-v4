/*  Percentagebar_horizontal.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-08-08
*/
/*  REQUIREMENTS
*
*       1) 
*       2) 
*       3) Method that will fill the bar from 0 to 100 in around ${ integer } seconds.
*       4) Method that adds ${ integer } to the current percentage.
*       5) Method that fills the bar to ${ integer } percentage.
*       6) Method that resets the bar to 0.
*       7) remove() should reset.
*       8) 
*
*
*/
/*  API
*
*       Progressbar({ any config, string name, string || DOMnode root, integer state, template_string template })
*
*
*
*/
/*  TODO
*
*       This animate function completely screws with the timings of things. Find a better way to make it work both on autorun and on manual run.
*
*/
//  DEPENDENCIES
////    IMPORT
import Component from '../core/component.js';
////    Helpers
const default_template = '<div class="comp-pbar_h"><div data-scale="${ state }"></div></div>';
//  COMPONETS
class Percentagebar_horizontal extends Component {
    constructor({ config, name, root, state, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [];
        this.interval = null;
    }
    // Should automatically run the progression to 100% in ms miliseconds.
    autorun( ms ) {
        this.config = ms / 1000;
        this.interval = setInterval(() => {
            if ( this.state < 100 ) this.tick();
            else this.reset();
        }, 1000 );
        return this;
    }
    // Should run the progression to 100% on the next tick.
    finish() {
        this.update({ state: 100 });
        return this;
    }
    // Should stop any active autoruns. Should reset state to 0.
    remove() {
        this.reset();
        super.remove();
        return this;
    }
    // Should clear any active interval. Should reset state to 0.
    reset() {
        if ( this.interval ) clearInterval( this.interval );
        this.state = 0;
        return this;
    }
    tick() {
        this.update({ state: Math.round( 100 / this.config ) + this.state });
    }
    // Should run the progression to the integer specified in state.
    // Since we want to see a transition, we can't actually rerender the bar without losing this animation.
    // So we have to update the original bar.
    // Should not throw an error if the bar is not found, since a last update could still be queued in memory.
    // This would usually happen if we navigate away from the page with the bar that is animating.
    update({ state } = {}) {
        // constrain pecentage to be between 0 and 100.
        if ( state < 0 ) this.state = 0;
        else if ( state > 100 ) this.state = 100;
        else this.state = state;
        const root = this.root();
        const bar = root.querySelector( '.comp-pbar_h > *[data-scale]' );
        if ( bar ) {
            bar.setAttribute( 'data-scale', this.state );
            bar.style.width = `${ this.state }%`;
        }
        return this;
    }
}
Percentagebar_horizontal.prototype.module_type = 'percentagebar_horizontal';
//
export default Percentagebar_horizontal;