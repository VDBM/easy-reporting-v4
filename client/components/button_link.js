/*  button_link.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-09-17
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    Import
import Button from '../components/button.js';
////    Data
////    Helpers
const default_template = '<a class="comp-button-${ config.color } comp-button-${ config.size }" href="${ config.uri }" id="${ id }" >${ config.label }</a>';
//  COMPONENTS
class Button_link extends Button {
    constructor({ config = { color: 'orange', label: 'No config', size: 'small', uri: '#/' }, name, root, state, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
    }
}
Button_link.prototype.module_type = 'button_link';
//  WORKFLOW
////    Routes
////    Application Events
////    Delegated Events
//  EXPORT
export default Button_link;