/*  gauge.mjs   => 
*   @language: ES6
*   @version:  v1.0.0
*   @creator:    VDBM
*   @date:   2018-08-28
*/
/*  API
*
*   http://bl.ocks.org/tomerd/1499279
*   https://gist.github.com/1499279 
*
*/
/*  TODO
*       - Should we force the min and max ranges isnide the report or inside this class? If data > max, data = max ?
*       - Refactor this into a better structure so it resembles the other components better.
*
*
*/
//  DEPENDENCIES
//      WE HAVE A FULL DEPENDENCY ON d3.js being a global variable! The implementation of the Component_svg will add the global d3 to its prototype.
////    IMPORTS
import Component_svg from '../core/component_svg.js';
import { cloneJSON, isStr, nodeClear } from '../core/util.js';
//import {} from '../core/constants.js';
////    Helpers
const default_config = {
    bottom: 0,
    cx: 200 / 2, // size / 2
    cy: 200 / 2, // size / 2
    height: 200,
    label: '',
    left: 0,
    max: 100,
    min: 0,
    precision: 0,
    radius: 200 * 0.97 / 2, // size * 0.97 / 2
    range: 100 - 0, // max - min
    right: 0,
    size: 200 * 0.9, // size * 0.9
    ticks_major: 5,
    ticks_minor: 2,
    top: 0,
    transition: 500,
    width: 200
};
const fixed_angles = {
    "50": 1.5707963267948965,
    "75": 2.748893571891069,
    "90": 3.455751918948773,
    "100": 3.9269908169872414
};
const zones = {
    "low": [{
        "from": 50,
        "to": 75
    }],
    "medium": [{
        "from": 75,
        "to": 90
    }],
    "high": [{
        "from": 90,
        "to": 100
    }]
};
//
const create_degrees = ( value, config ) => value / config.range * 270 - ( config.min / config.range * 270 + 45 );
const create_path = ( value, config ) => {
    const delta = config.range / 13;
    const tail = value - ( config.range * ( 1 / ( 270 / 360 ) ) / 2 );
    return [
        create_point( value, config, 0.85, config.cx, config.cy ),
        create_point( value - delta, config, 0.12, config.cx, config.cy ),
        create_point( tail + delta, config, 0.12, config.cx, config.cy ),
        create_point( tail, config, 0.28, config.cx, config.cy ),
        create_point( tail - delta, config, 0.12, config.cx, config.cy ),
        create_point( value + delta, config, 0.12, config.cx, config.cy ),
        create_point( value, config, 0.85, config.cx, config.cy )
    ];
};
const create_point = ( value, config, factor, min_x = 0, min_y = 0) => ({
    "x": ( config.cx - config.radius * factor * Math.cos( create_radians( value, config ))) - min_x,
    "y": ( config.cy - config.radius * factor * Math.sin( create_radians( value, config ))) - min_y
});
const create_radians = ( value, config ) => create_degrees( value, config ) * Math.PI / 180;
//  COMPONENTS
class Gauge extends Component_svg {
    constructor({ config = {}, name = 'default_gauge_name', root, state = 0, template = null } = {}) {
        super({
            config: Object.assign( cloneJSON( default_config ), config ),
            name,
            root,
            state,
            template
        });
        this.event_types = [];
        this.interval = null;
        this.rotation = null;
    }
    autorun() {
        this.interval = setInterval(() => {
            this.update({
                state: Math.floor( Math.random() * ( this.config.max - this.config.min + 1 ) ) + this.config.min
            });
        }, 1500 );
    }
    remove() {
        if ( this.interval ) clearInterval( this.interval );
        super.remove();
    }    
    render() {
        const root = this.root();
        nodeClear( root );
        if ( !this.is_rendered ) this.render_gauge();
        this.is_rendered = true;
        // Bind the event handlers to the newly rendered version.
        this.bind();
        // Trigger any registered rendering callbacks.
        this.trigger( 'render' );
        // Doublecheck that our root actually contains rendered content.
        if ( !root.hasChildNodes() ) throw new Error( `failed rendering ${ this.id }` );
        return this;
    }
    render_gauge() {
        try {
            const root = this.root();
            // Select & update container
            this.svg_node = this.d3
                .select( root )
                .append( 'svg:svg' )
                .attr( 'class', 'gauge' )
                .attr( 'height', this.config.height )
                .attr( 'width', this.config.width );
            //	Draw outer grey circle    
            this.svg_node
                .append( 'svg:circle' )
                .attr( 'cx', this.config.cx )
                .attr( 'cy', this.config.cy )
                .attr( 'r', this.config.radius )
                .style( 'fill', '#ccc' )
                .style( 'stroke', '#000' )
                .style( 'stroke-width', '0.5px' );
            // Draw inner white circle
            this.svg_node
                .append( 'svg:circle' )
                .attr( 'cx', this.config.cx )
                .attr( 'cy', this.config.cy )
                .attr( 'r', this.config.radius  )
                .style( 'fill', '#fff' )
                .style( 'stroke', '#e0e0e0' )
                .style( 'stroke-width', '2px' );
            // Draw inner green, roange and red zones.
            //      const COLOR_K_GREEN_FLUO = { 'rgba' : 'rgba( 190, 219, 0, 1 )', 'hex' : '#BEDB00' };
            //      const COLOR_K_ORANGE = { 'rgba' : 'rgba( 238, 175, 48, 1 )', 'hex' : '#EEAF30' };
            //      const COLOR_K_RED = { 'rgba' : 'rgba( 181, 24, 71, 1 )', 'hex' : '#B51847' };
            zones.low.forEach( zone => this.render_zone( zone.from, zone.to, '#BEDB00' ));
            zones.medium.forEach( zone => this.render_zone( zone.from, zone.to, '#EEAF30' ));
            zones.high.forEach( zone => this.render_zone( zone.from, zone.to, '#B51847' ));
            // Draw label
            this.svg_node
                .append( 'svg:text' )
                .attr( 'x', this.config.cx )
                .attr( 'y', this.config.cy / 2 + Math.round( this.config.size / 9 ) / 2 ) // fontSize
                .attr( 'dy', Math.round( this.config.size / 9 ) / 2 ) // fontSize
                .attr( 'text-anchor', 'middle' )
                .text( this.config.label )
                .style( 'font-size', ( Math.round( this.config.size / 10 ) ).toString() + 'px' ) // fontSize
                .style( 'fill', '#333' )
                .style( 'stroke-width', '0px' );
            // Draw circle stripes every 15 degrees. Alternating small / large 
            const majorDelta = this.config.range / ( this.config.ticks_major - 1 );
            for ( let major = this.config.min; major <= this.config.max; major += majorDelta ) {
                let minorDelta = majorDelta / this.config.ticks_minor;
                for ( let minor = major + minorDelta; minor < Math.min( major + majorDelta, this.config.max ); minor += minorDelta ) {
                    const point1 = create_point( minor, this.config, 0.75 );
                    const point2 = create_point( minor, this.config, 0.85 );
                    const point3 = create_point( major, this.config, 0.7 );
                    const point4 = create_point( major, this.config, 0.85 );
                    this.svg_node
                        .append( 'svg:line' )
                        .attr( 'x1', point1.x )
                        .attr( 'y1', point1.y )
                        .attr( 'x2', point2.x )
                        .attr( 'y2', point2.y )
                        .style( 'stroke', '#666' )
                        .style( 'stroke-width', '1px' );
                    this.svg_node
                        .append( 'svg:line' )
                        .attr('x1', point3.x )
                        .attr( 'y1', point3.y )
                        .attr( 'x2', point4.x )
                        .attr( 'y2', point4.y )
                        .style( 'stroke', '#333' )
                        .style( 'stroke-width', '2px' );
                }
                // Final circle stripe.
                if ( major === this.config.min || major === this.config.max ) {
                    let range_label = create_point( major, this.config, 0.5 );
                    this.svg_node
                        .append( 'svg:text' )
                        .attr( 'x', range_label.x + ( major === this.config.min ? -8 : 8 ) ) // offset the label by 4px so there's room for floating points numbers.
                        .attr( 'y', range_label.y )
                        .attr( 'dy', Math.round( this.config.size / 16 ) / 3 ) // fontSize
                        .attr( 'text-anchor', major === this.config.min ? 'start' : 'end' )
                        //.text( ( major / this.config.precision ).toFixed( this.config.precision.toString().length - 1 ) )
                        .text( major.toFixed( this.config.precision ) )
                        .style( 'font-size', Math.round( this.config.size / 16 ).toString() + 'px' ) // fontSize
                        .style( 'fill', '#333' )
                        .style( 'stroke-width', '0px' );
                }
            }
            // Create arrow pointer element
            const arrow = this.svg_node
                .append( 'svg:g' )
                .attr( 'class', 'pointerContainer' );
            // Draw actual arrow
            arrow
                .selectAll( 'path' )
                .data( [ create_path( ( this.config.min + this.config.max ) / 2, this.config ) ] ) // midvalue
                .enter()
                .append( 'svg:path' )
                .attr( 'd',
                    this.d3
                        .line()
                        .x( point => point.x )
                        .y( point => point.y )
                        .curve( this.d3.curveBasis )
                )
                .style( 'fill', '#dc3912' )
                .style( 'stroke', '#c63310' )
                .style( 'fill-opacity', 0.7 );
            // Draw blue middle circle
            //      const COLOR_K_BLUE_FLUO = { 'rgba' : 'rgba( 0, 169, 244, 1 )', 'hex' : '#00A9F4' };
            arrow
                .append( 'svg:circle' )
                .attr( 'cx', this.config.cx )
                .attr( 'cy', this.config.cy )
                .attr( 'r', 0.12 * this.config.radius )
                .style( 'fill', '#00A9F4' ) // '#4684EE'
                .style( 'stroke', '#666' )
                .style( 'opacity', 1 );
            // Draw arrow value as text
            arrow
                .selectAll( 'text' )
                .data( [ ( this.config.min + this.config.max ) / 2 ] )
                .enter()
                .append( 'svg:text' )
                .attr( 'x', this.config.cx )
                .attr( 'y', this.config.size - this.config.cy / 4 - Math.round( this.config.size / 10 ) ) // fontSize
                .attr( 'dy', Math.round( this.config.size / 10 ) / 2 ) // fontSize
                .attr( 'text-anchor', 'middle' )
                .style( 'font-size', Math.round( this.config.size / 10 ).toString() + 'px' )
                .style( 'fill', '#000' )
                .style( 'stroke-width', '0px' );
            // Set the arrow to the startpoint initially
            this.update({ state: this.config.min });
        }
        catch( error ) {
            console.error( error );
            console.error( error.stack );
        }
        return this;
    }
    render_zone( start, end, color ) {
        // Current definition of the this.zones is fixed so we get the same 50-75 75-90 90-100 marks .
        if ( end - start <= 0 ) return this;
        this.svg_node
            .append( 'svg:path' )
			.style( 'fill', color )
			.attr( 'd',
                this.d3
                    .arc()
                    //.startAngle( create_radians( start, this.config ) )
                    .startAngle( fixed_angles[ start ])
                    //.endAngle( create_radians( end, this.config ) )
                    .endAngle( fixed_angles[ end ])
                    .innerRadius( this.config.radius * 0.65 )
                    .outerRadius( this.config.radius * 0.85 )
            )
			.attr( 'transform', () => `translate(${ this.config.cx }, ${ this.config.cy }) rotate(270)` );
        return this;
    }
    rotate_arrow() {
        this.svg_node
            .select( '.pointerContainer' )
            .selectAll( 'text' )
            //.text( ( this.state / this.config.precision ).toFixed( this.config.precision.toString().length - 1 ) );
            .text( this.state.toFixed( this.config.precision ) );
        // Move arrow
        this.svg_node
            .select( '.pointerContainer' )
            .selectAll( 'path' )
            .transition()
            .duration( this.config.transition )
            .attrTween( 'transform', () => {
                let arrowValue = this.state;
                if ( this.state  > this.config.max ) arrowValue = this.config.max; // this was in the original code, but makes the arrow overflow: this.config.max + 0.02 * this.config.range;
                else if ( this.state < this.config.min ) arrowvalue = this.config.min; // this was in the original code, but makes the arrow overflow: this.config.min - 0.02 * this.config.range;
                const targetRotation = ( create_degrees( arrowValue, this.config ) - 90 );
                const currentRotation = this.rotation || targetRotation;
                this.rotation = targetRotation;
                return step => `translate(${ this.config.cx }, ${ this.config.cy }) rotate(${ currentRotation + ( targetRotation - currentRotation ) * step })`;
            } );
        return this;
    }
    update( options ) {
        //  Since our render function will only render the gauge if our is_rendered property is false, calling update here will not rerender the gauge.
        //  So we can trigger render_arrow() safely.
        super.update( Object.assign( options, { render: false } ) );
        this.rotate_arrow();
        return this;
    }
}
Gauge.prototype.module_type = 'gauge';
//
export default Gauge;