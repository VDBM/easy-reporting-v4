/*  hook_list.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-09-12
*/
/*  API
*
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    Import
import Component from '../core/component.js';
////    Data
////    Helpers
const default_template = [
    '<ul class="list-clean pad-8" id="${ id }">',
        '<li class="mar-b-8" data-tmpl="iterate config.amount" id="hook_${ source.config.hook }_${exec( collection.index + 1 )}"></li>',
    '</ul>'
].join( '' );
//  COMPONENTS
class Hook_list extends Component {
    constructor({ config = {}, name, root = () => null, state = null, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
    }
}
Hook_list.prototype.module_type = 'hook_list';
//  WORKFLOW
////    Routes
////    Application Events
////    Delegated Events
//  EXPORT
export default Hook_list;