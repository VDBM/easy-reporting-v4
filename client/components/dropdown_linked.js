/*  dropdown_linked.mjs
*   @language:      ES6
*   @version:       1.0.0
*   @creator:       VDBM
*   @date:          2018-09-18
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Component from '../core/component.js';
////    Data
const default_template = [
    '<section id="${ id }">',
    '<template data-tmpl="foreach level in state.index">',
        '<label>${ level }</label>',
        '<select id="">',
            '${exec( Object.keys( source ) )}',
        '</select>',
    '</template>',
    '</section>'
].join( '' );
////    Helpers
//  COMPONENTS
class Dropdown_linked extends Component {
    constructor({ config, name, root, state, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [ 'change' ];
    }
    init() {
        this.on( 'change', event => this.onchange( event ));
    }
    onchange( event ) {
        console.log( 'CHANGE EVENT HANDLER' );
        console.log( this );
    }
    /*
    render() {
        const collection = this.state;
        
        console.log( 'collection' );
        console.log( collection );
        

        
        console.log( 'places' );
        console.log( places );
        
        const dropdown_count = places.index.length;
        
        console.log( `We need to create ${ dropdown_count } dropdowns.` );
        
        const selects = Array
            .from({ length: dropdown_count })
            .map(( _, index ) => `<label>${ places.index[ index ] }</label><select id="${ this.id }_${ index }"></select>` );
        const primary_options = places
            .data
            .map( department => `<option value="${ department.name }">${ department.name }</option>` )
            .join( '' );
        selects[ 0 ] = selects[ 0 ].replace( '</select>', `${ primary_options }</select>` );
        this.root().innerHTML = selects.join( '' );
        this.bind();
    }
    */
}
Dropdown_linked.prototype.module_type = 'dropdown_linked';
//  WORKFLOW
//  EXPORT
export default Dropdown_linked;
