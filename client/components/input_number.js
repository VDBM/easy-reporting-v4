/*  input_number.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-24
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORT
import Component from '../core/component.js';
////    Helpers
//  COMPONENTS
class Input_number extends Component {
    constructor({ config, name, root, state, template } = {}) {
        template = template || '<input class="comp-input" type="number" value="${ state }">';
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [ "change" ];
    }
}
Input_number.prototype.module_type = 'input_number';
//
export default Input_number;