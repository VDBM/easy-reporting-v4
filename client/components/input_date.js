/*  input_date.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-08-01
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORTS
import Component from '../core/component.js';
////    Helpers
//  COMPONENTS
class Input_date extends Component {
    constructor({ config, name, root, state, template } = {}) {
        template = template || '<input class="comp-input" type="datetime" value="${ state }">';
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [ "change" ];
    }
}
Input_date.prototype.module_type = 'input_date';
//
export default Input_date;