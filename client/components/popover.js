/*  popover.mjs
*   @language:  ES6
*   @version:   1.0.1
*   @creator:   VDBM
*   @date:      2018-08-30
*/
/*  API
*
*       Set state to an object that has .render() and .remove() methods
*       We can listen for the responses by using the events .on( 'confirmed' ) and .on( 'cancelled' )
*       
*/
/*  TODO
*
*
*/
//  DEPENDENCIES
////    Import
import Component from '../core/component.js';
import { nodeClear } from '../core/util.js';
////    Data
////    Helpers
const default_template = [
    '<section class="comp-popover" id="${ id }">',
        '<div>',
            '<p class="mar-b-16" id="hook_popover_content"></p>',
            '<nav>',
                '<button class="comp-button-orange comp-button-small" data-type="confirm">Ok</button>',
                '<button class="comp-button-orange comp-button-small" data-type="cancel">Cancel</button>',
            '</nav>',
        '</div>',
    '</section>'
].join( '' );
//  COMPONENTS
class Popover extends Component {
    // State is another component that we want to render inside the popover.
    constructor({ config, name, state, template = default_template } = {}) {
        const root = (function() {
            const body = document.querySelector( 'body' );
            return () => body;
        }());
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [ "click", "keypress" ];
        this.interval = null;
    }
    autorun() {
        this.interval = true;
        const root = this.root();
        const rendered_button = root.querySelector( 'button[data-type="confirm"]' );
        const create_interval = () => Math.random() * ( 800 - 500 ) + 500;
        const run = () => {
            if ( this.interval ) {
                rendered_button.classList.add( 'autohover' );
                setTimeout(() => {
                    rendered_button.classList.remove( 'autohover' );
                    rendered_button.classList.add( 'autoactive' );
                    setTimeout(() => {
                        this.remove();
                        setTimeout( run, create_interval() );
                    }, create_interval() );
                }, create_interval() );
            }
        };
        run();
    }
    init() {
        this.on( 'click', { selector: '.comp-popover' }, event => {
            event.stopPropagation();
            const root = this.root();
            const section = root.querySelector( `#${ this.id } > div` );
            if ( section ) {
                if ( !section.contains( event.target )) this.remove();
            }
            else throw new Error( 'popover not found' );
        });
        this.on( 'click', { selector: 'button[data-type="confirm"]' }, event => {
            event.stopPropagation();
            this.remove();
            this.trigger( 'confirmed' );
        });
        this.on( 'click', { selector: 'button[data-type="cancel"]' }, event => {
            event.stopPropagation();
            this.remove();
            this.trigger( 'cancelled' );
        });
        this.on( 'render', event => {
            const root = this.root();
            root.querySelector( `#${ this.id }` ).classList.add( 'faded' );
        });
        return super.init();
    }
    remove() {
        if ( this.state ) this.state.remove();
        if ( this.interval ) this.interval = null;
        if ( this.is_rendered ) {
            const root = this.root();
            const node = root.querySelector( `#${ this.id }` );
            node.parentNode.removeChild( node );
        }
        super.remove();
    }
    render() {
        //  We need to ovewrite the rendered ehre so we explicitly prevent the node from clearing.
        //console.log( `Rendering ${ this.id } => is_rendered = ${ this.is_rendered }` );
        //  Fetch the root we ned to append to.
        const root = this.root();
        // Generate the HTML from the template or error if no template is available.
        if ( !this.template ) throw new Error( `Failed rendering ${ this.id } - no template` );
        if ( !root ) throw new Error( `Failed rendering ${ this.id } - no root` );
        // Since we call .html() using 'this' as the data, we don't actually have to use the template.register() method unless we want to.
        const html = this.template.html( this );
        const fragment = document.createDocumentFragment();
        const wrapper = document.createElement( 'div' );
        wrapper.innerHTML = html;
        Array
            .from( wrapper.childNodes )
            .forEach( node => fragment.appendChild( node ));
        // Either render the HTML to our root node, or replace the currently rendered version.
        if ( this.is_rendered ) {
            const node = root.querySelector( `#${ this.id }` );
            node.parentNode.removeChild( node );
        }
        root.appendChild( fragment );
        //console.log( `${ this.id } => setting is_rendered to true. Previous value => ${ this.is_rendered }` );
        this.is_rendered = true;
        // Bind the event handlers to the newly rendered version.
        this.bind();
        // Trigger any registered rendering callbacks.
        this.trigger( 'render' );
        // Doublecheck that our root actually contains rendered content.
        if ( !root.hasChildNodes() ) {
            console.log( '-----   root   -----' );
            console.log( root );
            console.log( '----- fragment -----' );
            console.log( fragment );
            throw new Error( `Root node is empty after rendering.` );
        }
        return this;        
    }
    show() {
        this.state.render();
    }
}
Popover.prototype.module_type = 'popover';
//
export default Popover;