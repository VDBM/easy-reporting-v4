/*  input_string.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-24
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORT
import Component from '../core/component.js';
////    Helpers
//  COMPONENTS
class Input_string extends Component {
    constructor({ config, name, root, state, template } = {}) {
        template = template || '<input class="comp-input" tabindex="1" type="text" value="${ state }">';
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [ "change" ];
    }
    init() {
        this.on( 'change', { selector: 'input' }, event => {
            this.state = event.target.value;
        });
        this.on( 'render', () => {
            const root = this.root();
            const input = root.querySelector( 'input' );
            input.focus();
            input.select();
        });
        return super.init();
    }
}
Input_string.prototype.module_type = 'input_string';
//
export default Input_string;