/*  downtimebar.mjs   => 
*   @language: ES6
*   @version:  v1.0.0
*   @creator:    VDBM
*   @date:   2018-08-22
*/
/*  API
*
*           - Proficy downtime events are passed as UTC time strings representing local time. So we ahve to use 'getUTC...()' methods to get the correct local times.
*           - For ongoing downtimes that started before the shift started, the width automatically is 100%, since we don't have an endtime yet.
*           - Since we work with a tempalte that ahs to compeltely be rerendered, our render function and our update function are the same.
*           - TODO: To make this more efficient, we have to split this into two templates, or ahve the update function replace a certain node. +- 1 hour work, but low prio atm.
*           - BUG: still takes local time to calculate the sundial. We have to sue UTC time here, since atm between 14:15 and 15:15 we overflow the bar due to wrong hour.
*
*   const ${ name_component } = new ${ name_class }( { "event": "function", "name": "string", "style": "string" } );
*   ${ name_component }.html();
*   ${ name_component }.html( [] );
*   ${ name_component }.render();
*   ${ name_component }.update( { "data": "array', "root": "string", "state": "any", "style": "string" } );
*
*/
/*  TODO
*       Refactor this into a better structure so it resembles the other modules better.
*
*
*/
//  DEPENDENCIES
////    IMPORTS
import Component from '../core/component.js';
import { MILISECONDS_SHIFT } from '../core/constants.js';
////    Helpers
const calculateWidth = duration_ms => duration_ms > MILISECONDS_SHIFT ? 100.00 : parseFloat( ( duration_ms * 100 / ( MILISECONDS_SHIFT - 1000 ) ).toFixed( 2 ), 10 );
/*  dateShift
*   @notes
*           - Since our local shifts start at 06:15 local time, we have to change this function to look at 04:15, 12:15, 20:15 UTC as the real starttime of shifts .
*           - Hours before 06:15 are part of the night shift of the previous day.
*           - [ 2, 0, 1, 8, -, 0, 2, -, 0, 1, 'T', 0, 0, :, 0, 0, :, 0, 0 ]
*           - [ 0, 0, 1, 1, 0, 1, 1, 0, 1, 1 ]
*/
const dateShift = source => {
    const datetime = source instanceof Date ? source : new Date( source );
    const offset_dst = datetime.getTimezoneOffset() / 60 * 100;
    const timeInt = datetime.getUTCHours() * 100 + datetime.getUTCMinutes();
    // We assume that the shfit begins at 06:15 loal time, so if we use UTC time as the format, we ned to add/subtract the timezone difference to the shift start hour .
    const shift_early = 615 + offset_dst;
    const shift_late = 1415 + offset_dst;
    const shift_night = 2215 + offset_dst;
    let suffix;
    if ( timeInt < shift_early || timeInt >= shift_night ) suffix = '22';
    else if ( timeInt >= shift_early && timeInt < shift_late ) suffix = '06';
    else if ( timeInt >= shift_late && timeInt < shift_night ) suffix = '14';
    const shiftDay = timeInt < shift_early
        ? new Date( datetime.getTime() - 86400000 )
        : datetime;
    return parseInt( [
        shiftDay.getUTCFullYear().toString().slice( 2 ),
        ( shiftDay.getUTCMonth() + 1 ).toString().padStart( 2, '0' ),
        shiftDay.getUTCDate().toString().padStart( 2, '0' ),
        suffix
    ].join( '' ), 10 );
};
/*  dateUTC
*   - 1min = 60 sec = 60000 ms
*/
const dateUTC = () => Date.now() - ( new Date().getTimezoneOffset() * 60000 );
/*  propUpdate      => Map: Update the value of a 'propertyName' to the result of the 'transformation'.
*   @type           public Function
*   @name           propUpdate
*   @param          String propertyName
*   @param          Function transformation
*   @return         Function( sourceObj )
*   @notes
*                   - Replaced transformation.call( sourceObj, sourceObj[ propertyName ] ) by directly calling the transformation. Before we'd use the this context to refer to the sourceObj, but then we can't use arrow functions.
*/
const propUpdate = ( propertyName, transformation ) => sourceObj => Object.assign( sourceObj, { [ propertyName ] : transformation( sourceObj ) } );
/*  shiftDate
*   @notes:
*                   - Since we express our shifts in local time, we need to actually use the version without 'Z' at the end to end up with the correct UTC date .
*                   - This approach should also work in different parts of the world, since the Date constructor will be relative to their location as well .
*/
const shiftDate = shift => {
    const chunks = shift.toString().split( '' );
    const dateString = `20${ chunks[ 0 ] }${ chunks[ 1 ] }-${ chunks[ 2 ] }${ chunks[ 3 ] }-${ chunks[ 4 ] }${ chunks[ 5 ] }T${ chunks[ 6 ] }${ chunks[ 7 ] }:15:00`;
    return new Date( dateString );
};
/*
*/
const shiftCurrent = () => shiftDate( dateShift( new Date() ) );
/*
*/
const updateTimeframe = entries => {
    if ( !entries.length ) return [];
    else {
        var res = entries[ 0 ].data
            ? entries.map( propUpdate( 'data', entry => updateTimeframe( entry.data ) ) )
            : entries.map( entry => Object.assign( entry, { 'starttime' : entry.starttime ? new Date( entry.starttime ) : null, 'endtime' : entry.endtime ? new Date( entry.endtime ) : null } ) );
           return res;
    }
};
//
const default_data = (() => {
    // 1 hour UDT, 30 min UT, 30 min PDT, 15 min UT, 15 min UDT, 4 hours UT, 1 hour 30min ongoing
    const shift  = shiftCurrent();
    const date = shift.toJSON().slice( 0, 11 );
    const hour = shift.getUTCHours(); // 6 14 22
    const hour_60min = ( hour + 1 ).toString().padStart( 2, '0' ); // 07 15 23
    const hour_120min = hour + 2 === 24 ? '00' : ( hour + 2 ).toString().padStart( 2, '0' ); // 08 16 00
    const hour_360min = hour + 6 === 28 ? '05' : ( hour + 6 ).toString().padStart( 2, '0' ) // 12 20 04
    const timings = [
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 1 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : shift.toJSON(), 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 2 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 1 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 3 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 2 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'planned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 4 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 3 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 5 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 4 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'planned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 6 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 5 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'unplanned' },
        { 'duration' : 60.00, 'endtime' : `${ date }${ ( hour + 7 ).toString().padStart( 2, '0' ) }:15:00.000Z`, 'starttime' : `${ date }${ ( hour + 6 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'planned' },
        { 'duration' : null, 'endtime' : null, 'starttime' : `${ date }${ ( hour + 7 ).toString().padStart( 2, '0' ) }:15:01.000Z`, 'type' : 'ongoing' }
    ];
    /*  Fixed
    */
    return updateTimeframe( timings );
    /*  Variant
    return UTIL.updateTimeframe( [
        { 'duration' : 60.00, 'endtime' : `${ date }${ hour_60min }:15:00.000Z`, 'starttime' : shift.toJSON(), 'type' : 'unplanned' },
        { 'duration' : 30.00, 'endtime' : `${ date }${ hour_120min }:15:00.000Z`, 'starttime' : `${ date }${ hour_60min }:45:00.000Z`, 'type' : 'planned' },
        { 'duration' : 15.00, 'endtime' : `${ date }${ hour_120min }:45:00.000Z`, 'starttime' : `${ date }${ hour_120min }:30:00.000Z`, 'type' : 'unplanned' },
        { 'duration' : null, 'endtime' : null, 'starttime' : `${ date }${ hour_360min }:45:00.000Z`, 'type' : 'ongoing' }
    ] );
    */
})();
//
const default_template = [
    '<div class="comp-dtbar-bar">',
        '<label class="comp-dtbar-sundial" data-symbol="${ state.sundial.symbol }" style="left: ${ state.sundial.offset }%;"></label>',
        '<span data-tmpl="foreach duration in state.durations" class="comp-dtbar-duration comp-dtbar-${ duration.type }" style="width: ${ duration.width }%;">${ duration.label }</span>',
    '</div>',
    '<div class="comp-dtbar-timings">',
        '<label data-tmpl="foreach timing in state.timings" class="comp-dtbar-timing">${ timing }</label>',
    '</div>'
].join('');
//  COMPONENTS
class Downtimebar extends Component {
    constructor({ config, name = 'default_downtimebar_name', root, state, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
    }
    update( options ) {
        // Update the bar to show the downtimes
        // Entire bar = 1 shift = 8 hours - 1sec = 100% width .
        // start = current time => cast to shift => cast to date .
        const data = default_data;
        const bar_start = shiftCurrent();
        // end = start + duration shift - 1 second to get x:14:59.000 .
        const bar_end = new Date( bar_start.getTime() + MILISECONDS_SHIFT - 1000 );
        // If there aren't any downtime records, we can create one record spanning the entire shift .
        // Else we create an array of durations, filling in any time gaps and creating the entry object as we go .
        const durations = !data.length
            ? [ { 'label' : '&nbsp;', 'type' : 'running', 'width' : 100.00 } ]
            : data
                .reduce( ( summary, entry, index, data ) => {
                    let width;
                    let label;
                    // We could do this with a mapping, but might as well fold it back into the redution .
                    // Create a 'running' type chunk before the entry starttime if needed .
                    // We actually need the difference between the two to be bigger than 1 second, else we just consider the downtimes to be adjacent.
                    if ( entry.starttime.getTime() - summary.last_ms > 1000 ) {
                        width = calculateWidth( entry.starttime.getTime() - summary.last_ms );
                        summary.sum += width;
                        summary.durations.push( { 'label' : '&nbsp;', 'type' : 'running', width } );
                    }
                    // Create a chunk for the current entry and update the summary .
                    // If the downtime is ongoing, the endtime is null and we use the endtime of the shift so we fill the remainder width of the bar with ongoing .
                    summary.last_ms = entry.endtime ? entry.endtime.getTime() : bar_end.getTime();
                    width = calculateWidth( summary.last_ms - entry.starttime.getTime() );
                    summary.sum += width;
                    // Determine label
                    if ( !entry.duration ) {
                        if ( width > 4 && entry.type == 'ongoing' ) label = 'ongoing';
                        else label = '&nbsp;';
                    }
                    else if ( width > 2.5 ) label = parseFloat( entry.duration, 10 ).toFixed( 2 );
                    else if ( width > 1 ) label = parseInt( entry.duration, 10 ).toString();
                    // Dont't show any label if the chunk isn't wide enough to render anything without overflowing. 
                    else label = '&nbsp;';
                    summary.durations.push( { label, 'type' : entry.type,  width } );
                    // If this is the last entry
                    if ( index === data.length - 1 ) {
                       // Check if we need to create a 'running' type chunk to fill the last part of the bar .
                       // Since we have the sum of all widths, this width is just the remaining width .
                       // This also satisfies the width < 100.00
                       if ( summary.last_ms < bar_end.getTime() ) summary.durations.push( { 'label' : '&nbsp;', 'type' : 'running', 'width' : 99.99 - summary.sum } );
                       // Satisfy the width > 100.00 by decreasing the length of the last chunk .
                       else if ( summary.sum > 100 ) summary.durations[ summary.durations.length - 1 ].width = ( summary.durations[ summary.durations.length - 1 ].width + 99.99 - summary.sum ).toFixed( 2 );
                    }
                    return summary;
                }, { 'durations' : [], 'last_ms' : bar_start.getTime(), 'sum' : 0.00 } )
                .durations;
        const hour_now = new Date().getHours();
        const sundial = {
            'offset' : calculateWidth( dateUTC() - bar_start.getTime() ),
            'symbol' : hour_now > 8 && hour_now < 19 ? '\u263C' : '\u263E'
        };
        //  Since our dates are in UTC and we stil want to render the local dates, we don't have to use getUTCHours() as in the previous version, but the local hour, so the label is correct.
        //  hour_start is not used in different calculations, so this is ok.
        const hour_start = bar_start.getHours();
        const timings = Array.from( { 'length' : 9 }, ( _ , index ) => `${ ( hour_start + index ).toString().padStart( 2, '0' ) }:15` );
        
        /*
        // Insert the current time, splitting the timeframe into located in, into two.
        const node = document.querySelector( this.root );
        if ( node ) node.innerHTML = this.template.html( { durations, sundial, timings } );
        else throw new Error( 'component_downtimebar: root node not found.' );
        */
        this.state = { durations, sundial, timings };
        return super.update( options );
    }    
}
Downtimebar.prototype.module_type = 'downtimebar';
//
export default Downtimebar;