/*  datatable_sort.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-17
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORTS
import Datatable from './datatable.js';
////    Helpers
//  COMPONENTS
class Datatable_sort extends Datatable {
    constructor({ config, name, root, state, template } = {}) {
        //if ( !state.length ) throw new Error( `cannot create datatable_sort ${ name }: no default state specified.` );
        template = template || [
            '<table class="comp-datatable" id="${ id }">',
                '<thead>',
                    '<tr>',
                        '<th data-tmpl="foreach header in state[0]" class="${exec( source.sort_reverse && source.sort_property === header.key ? ' + '"arrow-up"' + ' : "arrow-down"' + ')}">${ header.key }</th>',
                    '</tr>',
                '</thead>',
                '<tbody class="striped">',
                    '<tr data-tmpl="foreach entry in state" id="dts_${ entry.id }">',
                        '<td data-tmpl="foreach property in entry">${ property.value }</td>',
                    '</tr>',
                '</tbody>',
            '</table>'
        ].join('');
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.sort_property = null;
        this.sort_reverse = false;
    }
    init() {
        // sorting
        this.on( 'click', { selector: `#${ this.id } th` }, event => {
            const property = event.target.innerHTML.trim();
            if ( property ) {
                // nothing sorted yet
                // previous == next ==> reverse
                // previous !+= next ==> sort next
                if ( !this.sort_property || this.sort_property !== property ) {
                    const sorted_state = this.state.sort(( a, b ) => {
                        if ( a[ property ] < b[ property ] ) return -1;
                        else if ( a[ property ] > b[ property ] ) return 1;
                        else return 0;
                    });
                    this.sort_property = property;
                    this.sort_reverse = false;
                    this.update({ state: sorted_state });
                }
                else if ( this.sort_property === property ) {
                    this.sort_reverse = !this.sort_reverse;
                    this.update({ state: this.state.reverse() });
                }
                else console.log( `this should not happen: ${ this.sort_property } => ${ this.sort_reverse } => ${ property }` );
            }
        });
        this.on( 'remove', event => {
            this.sort_property = null;
            this.sort_reverse = false;
        });
        return super.init();
    }
}
Datatable_sort.prototype.module_type = 'datatable_sort';
//
export default Datatable_sort;