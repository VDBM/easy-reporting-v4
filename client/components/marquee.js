/*  marquee.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-08-30
*/
/*  API
*/
/*  TODO
*
*
*/
//  DEPENDENCIES
////    Import
import Component from '../core/component.js';
////    Helpers
const default_state = 'Loading...';
const default_template = '<figure class="comp-marquee active" id="${ id }"><div>${ state }</div></figure>';
//  COMPONENTS
class Marquee extends Component {
    constructor({ config, name, root, state = default_state, template = default_template }) {
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [];
        this.interval = null;
    }
}
Marquee.prototype.module_type = 'marquee';
//
export default Marquee;