/*  button.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-08-09
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    Import
import Component from '../core/component.js';
////    Data
////    Helpers
const default_template = '<button class="comp-button-${ config.color } comp-button-${ config.size }" id="${ id }">${ config.label }</button>';
//  COMPONENTS
class Button extends Component {
    constructor({ config = { color: 'orange', label: 'No config', size: 'small' }, name, root, state, template = default_template } = {}) {
        super({
            config,
            name,
            root,
            state,
            template
        });
        this.event_types = [ "click" ];
        this.interval = false;
    }
    autorun() {
        this.interval = true;
        const root = this.root();
        const rendered_button = root.querySelector( `#${ this.id }` );
        const create_interval = () => Math.random() * ( 800 - 500 ) + 500;
        const run = () => {
            if ( this.interval ) {
                rendered_button.classList.remove( 'autoactive' );
                setTimeout(() => {
                    rendered_button.classList.add( 'autohover' );
                    setTimeout(() => {
                        rendered_button.classList.remove( 'autohover' );
                        rendered_button.classList.add( 'autoactive' );
                        setTimeout( run, create_interval() );
                    }, create_interval() );
                }, create_interval() );
            }
        };
        run();
    }
    init() {
        this.on( 'click', event => {
            if ( this.state ) this.state();
        });
        super.init();
        return this;
    }
    remove() {
        this.interval = false;
        super.remove();
    }
}
Button.prototype.module_type = 'button';
//  WORKFLOW
////    Routes
////    Application Events
////    Delegated Events
//  EXPORT
export default Button;