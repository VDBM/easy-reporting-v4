/*  datatable.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-24
*/
/*  API
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORTS
import Component from '../core/component.js';
////    Helpers
const config_from_state = state => Object.keys(( state || [{}] )[0] ).reduce(( hash, key ) => { hash[ key ] = Object.prototype.toString.call( state[0][ key ] ); return hash; }, {} );
const default_template = [
    '<table class="comp-datatable" id="${ id }">',
        '<thead>',
            '<tr>',
                '<th data-tmpl="foreach header in state[0]">${ header.key }</th>',
            '</tr>',
        '</thead>',
        '<tbody class="striped">',
            '<tr data-tmpl="foreach entry in state" id="dt_${ entry.id }">',
                '<td data-tmpl="foreach property in entry">${ property.value }</td>',
            '</tr>',
        '</tbody>',
    '</table>'
].join('');
//  COMPONENTS
class Datatable extends Component {
    constructor({ config, name = 'default_datatable_name', root, state, template = default_template } = {}) {
        super({
            config: config_from_state( state ),
            name,
            root,
            state,
            template
        });
    }
    update( data ) {
        const { config, state } = data;
        // We need to reconstruct the model if we update the state to a different type.
        if ( config && state ) this.config = config_from_state( state );
        super.update( data );
    }
}
Datatable.prototype.module_type = 'datatable';
//
export default Datatable;