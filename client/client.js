/*  client.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-10
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application_manager from './core/application_manager.js';
import admin from './reports/admin.js';
import bridges from './reports/bridges.js';
import buggies from './reports/buggies.js';
import explorer from './reports/explorer.js';
import idd from './reports/idd.js';
import linestatus from './reports/linestatus.js';
import logbooks from './reports/logbooks.js';
import pcs from './reports/pcs.js';
import silos from './reports/silos.js';
import sto from './reports/sto.js';
import tests from './reports/tests.js';
import triggers from './reports/triggers.js';
import { nodeClear, log_error } from './core/util.js';
////    Helpers
////    Data
//  COMPONENTS
const app_manager = new Application_manager( 'easyreporting' );
//  WORKFLOW
app_manager
    .install( admin )
    .install( bridges )
    .install( buggies )
    .install( explorer )
    .install( idd )
    .install( linestatus )
    .install( logbooks )
    .install( pcs )
    .install( silos )
    .install( sto )
    .install( tests )
    .install( triggers )
    .init()
    .catch( error => {
        console.error( error.message );
        console.log( error.stack );
        debugger;
    });
////    Routes
app_manager.router.on( '/', null, () => {
    //  Since this is a path without parameters, we don't have access to the path here, since we know the path is '#/'
    //  Due to route cascading, explicitly check the active hash and only clear the main content when needed.
    //  We could technically implement this in each app as well, but since this is a 'global level' event, it makes sense to implement it once in this location.
    //  Check the rotuer instead of the window so we don't rely on being inside a browser.
    const root = document.querySelector( '#app_main' );
    const history_count = app_manager.router.history.length;
    const active_hash = app_manager.router.history[  history_count - 1 ];
    //  If the root HTML exists, and if we have rendered something before, and if we return to the screen without a route, clear all the content from the main field.
    if ( root && history_count > 1 && active_hash === '#/' ) nodeClear( root );
    if ( active_hash === '#/' ) root.innerHTML = '<h2 class="grid-1-1 grid-sc-5 mar-8 ta-c">Easy Reporting Mechelen</h2>';
});
////    Application Events
////    Delegated Events
//  EXPORT
window.ER = app_manager;