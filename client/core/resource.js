/*  resource.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-17
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import { fetch, root } from './fetch.js';
////    Data
////    Helpers
//  COMPONENTS
class Resource {
    constructor({ manager, name, type } = {}) {
        this.file = null;
        this.id = `${ this.module_type }_${ name }`;
        this.manager = manager;
        this.name = name;
        this.type = type;
    }
    fetch() {
        return fetch( `${ root }/${ this.manager }/${ this.name }.${ this.type }` )
            .then( response => {
                if ( response.status === 200 && response.body ) {
                    this.file = this.type === 'json'
                        ?   response.json()
                        :   response.body.slice();
                    return this;
                }
                else throw new Error( `Failed fetching resource ${ this.id }` );
            });
    }
    get() {
        if ( this.file ) return Promise.resolve( this.file );
        else return this.fetch().then( resource => resource.file );
    }
    init() {
        //  Do we need to return this.get() here?
        this.get();
        return this;
    }
    remove() {
        return this;
    }
}
Resource.prototype.module_type = 'resource';
////
//  WORKFLOW
//  EXPORT
export default Resource;