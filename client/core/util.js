/*  cloneJSON
*/
const cloneJSON = source => JSON.parse( JSON.stringify( source ));
/*  DOM_EVENTS
*/
const DOM_EVENTS = ( event_method, instance, event_types ) => {
    event_types.forEach(function( event_type ) {
        if ( instance.events[ event_type ] ) {
            instance.events[ event_type ].forEach( event => {
                // If the event selector is a string, it's a reference to a HTML node inside our rendered content.
                // If the selector is a html node, we use that HTML node.
                // All other cases we use the component root.
                const bind_targets = event.selector
                    ? isStr( event.selector )
                    ? instance.root().querySelectorAll( event.selector )
                    : [ event.selector ]
                    : [ instance.root() ];
                Array.from( bind_targets ).forEach( target => target[ `${ event_method }EventListener` ]( event_type, event.handler ));
            });
        }
    });
};
/*  inspectError => public function extends util triggers console.log
* @name     inspectError
* @param    Object info
*               Object data
*               Error error
*               String source
*               String text
*/
const inspectError = function inspectError( options ) {
	if ( options.source ) console.log( `----- ERROR: ${new Date().toLocaleString()} @ ${options.source} -----` );
    if ( options.text ) console.log( options.text );
    if ( options.data ) isObj( options.data ) ? console.dir( options.data ) : console.log( options.data );
	if ( options.error ) console.log( options.error.stack || options.error );
	console.log( '--- End Of Error ---' );
};
/*  isFn            => Is the source a Function?
*   @type           public Function
*   @name           isFn
*   @param          Any source
*   @return         Boolean
*/
const isFn = source => {
    return Object.prototype.toString.call( source ) === '[object Function]';
};
/*  isNumber       => Is the source an Integer or a Float?
*   @type           public Function
*   @name           isNumber
*   @param          Any source
*   @return         Boolean
*/
const isNumber = source => {
    return !isNaN( parseFloat( source, 10 ) ) && isFinite( source );
};
/*  isObj           => Is the source an Object?
*   @type           public Function
*   @name           isObj
*   @param          Any source
*   @return         Boolean
*/
const isObj = source => {
    return Object.prototype.toString.call( source ) === '[object Object]';
};
/*  isStr
*/
const isStr = source => {
    return Object.prototype.toString.call( source ) === '[object String]';
};
/*  nodeClear
*/
const nodeClear = node => {
    while ( node.lastChild ) {
        node.removeChild( node.lastChild );
    }
};
/*  nodeParent
*/
const nodeParent = ( node, type ) => {
    if ( !node ) return false;
    let currentNode = node;
    let currentType = node.nodeName;
    const typeToFind = type.toUpperCase();
    if ( currentType === typeToFind ) return node;
    else {
        while (true) {
            currentNode = currentNode.parentNode;
            currentType = currentNode.nodeName;
            if ( currentType === typeToFind ) return currentNode;
            else if ( currentType === 'HTML' ) return false;
        }
    }
};
/*  normalizeHTML   => Strip all symbols from a string that affect HTML layout when rendered.
*   @type           public Function
*   @name           normalizeHTML
*   @param          String sourceStr                - A string containing HTML syntax.
*   @return         String
*   @notes
*                   - Replaces: HTML comments, new lines, tabs, carriage returns, any whitespace left between both angle brackets < >.
*                   - This lets us write indented HTML templates for use with innerHTML without the cruft generating DOM nodes like empty text nodes.
*                   - By replacing the tabs and new lines first, our following comment removal regex will also work on strings from HTML template files, so we can funally use comments inside our templates.
*                   - Should introduce better error handling.
*/
const normalizeHTML = html => {
    let comments;
    let lineOperations;    
    let whitespace;
    if ( typeof html !== 'string' ) {
        throw new TypeError( `parameter html is not a string: ${ html }` );
        return null;
    }
    try {
        lineOperations = html.replace( /[\n\t\r]+/g, '' );
    }
    catch( err ) {
        throw `failed removing new lines, tabs or carriage returns from: ${ html }`;
    }
    try {
        comments = lineOperations.replace( /<!--.+?-->/gi, '' );
    }
    catch ( err ) {
        throw `failed removing HTML comments from ${ lineOperations }`;
    }
    try {
        whitespace = comments.replace(  /(?:>)\s+(?:<)/g, '><' );
        
    }
    catch( err ) {
        throw `failed removing whitespace between tags from: ${ comments }`;
    }
    return whitespace.trim();
};
/*  pathObj         => Transform a string representing a javascript object path into the actual value inside the 'sourceObj'.
*   @type           public Function
*   @name           pathObj
*   @param          Object sourceObj
*   @param          String path
*   @return         Any
*   @notes
*                   - By replacing brackets with dots, we also support traversing arrays: obj.propertyAry[0].value => obj.propertyAry.0.value => value
*/
const pathObj = ( source, path ) => {
    return path.replace( /\[(.*?)\]/g, '.$1' ).split( '.' ).reduce( ( reference, pathChunk ) => reference ? reference[ pathChunk ] : undefined, source );
};
/*  propHash        => Reduce: Create an object with 'propertyReference' after 'keyTransform' as its keys and aggregate all array entries into it. 
*   @type           public Function
*   @name           propHash
*   @param          String property_reference        - The property of each element we want to use as the hash key.
*   @param          Boolean is_unique                - When true, will set each hash key to the next item record. When false, will create an array as the value of the hash key and push the next item record to this array.
*   @param          Function transformation_key      - An optional function to apply to the property name of the (item) source.
*   @param          Function transformation_value    - An optional function to apply to the property value of the (item) source.
*   @returns        Object                           - Object containing all the keys found inside the records. Each key value is either a unique value or an array of values that share the same key ( property value )
*   @notes          
*                   - By adding default keys to the object we're reducing into, we guarantee that those keys exists in the final result.
*/
const propHash = ( property_reference, is_unique, transformation_key, transformation_value ) => {
    const generateKey = transformation_key
        ? ( item, index, source ) => transformation_key( item, index, source )
        : item => item[ property_reference ];
    return ( hash, item, index, source ) => {
        const key = generateKey( item, index, source );
        if ( !hash.hasOwnProperty( key ) ) {
            if ( is_unique ) hash[ key ] = transformation_value ? transformation_value( item, index, source ) : item;
            else hash[ key ] = [];
        }
        if ( !is_unique ) hash[ key ].push( transformation_value ? transformation_value( item, index, source ): item );
        return hash;
    };
};
/*  propSort
*/
const propSort = ( type, property ) => {
    if ( type === 'lexical' ) {
        return ( first, second ) => {
            const first_property = first[ property ];
            const second_property = second[ property ];
            if ( first_property === second_property ) return 0;
            else if ( first_property < second_property ) return -1;
            else if ( first_property > second_property ) return 1;
            else throw new Error( `util.propSort.${ type }: failed sorting ${ first_property } and ${ second_property }` );
        };
    }
    else if ( type === 'numerical' ) return ( first, second ) => first[ property ] - second[ property ];
};
/*  unique          => Filter: Remove all duplicate items from an array. Works with plain objects as well, since we stringify each array item.
*   @type           public Function
*   @name           unique
*   @return         Function( item )
*   @notes          
*                   - Usage: ary.filter( unique() );
*                   - Basically we just provide a closure around the seen object.
*                   - Our previous version would be called as unique( ary ) instead.
*                   - I think this version is better for consistency and is more readable when chaining array methods, esp in combination with propHash and other higher order utility we provide.
*/
const unique = () => {
    const seen = {};
    return item => {
        const json = JSON.stringify( item );
        return seen.hasOwnProperty( json )
            ? false
            : ( seen[ json ] = true );
    };
};
////////////////////
export {
    cloneJSON,
    DOM_EVENTS,
    inspectError,
    isFn,
    isNumber,
    isObj,
    isStr,
    nodeClear,
    nodeParent,
    normalizeHTML,
    pathObj,
    propHash,
    propSort,
    unique
};