/*  component.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-17
*/
/*  API
*
*       If we don't use an object as the parameter for the update function, we need to add alot of extra methods or parameters to account of rall possible combinations of:
*           new root element, new state, need to reconfigure this.config accodring to state, explicit rerender
*
*       Event added through this.on() that include a selector, will still bubble up. So we ened to stopPropagation() in the event handler if we don't want it to bubble any further.
*
*       Component({ object config, string name, string || DOMnode root, any state, template_string template });
*       Component.init();
*       Component.bind();
*       Component.off( string event_name, function handler );
*       Component.on( string event_name, object options, function handler );
*           .on( 'change', ... );
*           .on( 'click', ... );
*           .on( 'init', ... );
*           .on( 'remove', ... );
*           .on( 'render', ... );
*           .on( string, ... );
*       Component.remove();
*       Component.render({ boolean append });
*       Component.update({ boolean append, boolean config, boolean render, string || DOMnode root, any state, });
*/
/*  TODO
*
*       Clean up all the code that has been commented away.
*
*/
//  DEPENDENCIES
////    Import
import Mediator from './mediator.js';
import Template from './template.js';
import { DOM_EVENTS, isStr, nodeClear, normalizeHTML } from './util.js';
//
class Component extends Mediator {
	constructor({ config = {}, name, root = () => null, state = null, template } = {}) {
        super();
		//  Any data required by the component upon creation.
		this.config = config;
        // Event types as a proeprty so we can overwrite this in extensions.
        this.event_types = [ "click" ];
        //  Unique ID.
        this.id = `${ this.module_type }_${ name }`;
        //  Flag for signifying we have an async HTML template to fetch
        this.is_async = false;
        //  Rendered flag used for lifecycle management.
        this.is_rendered = false;
        //  Static string to signify the component does not have to be removed until their namespace is no longer the active hash.
        //  We can then render any namespaced wrappers, like menus and have them automatically get removed and their root cleared out.
        this.is_static = false;
        //  Component name.
        this.name = name;
        //  Root node where the component will be rendered to.
        this.root = root;
		//	state			=> Current state of the component.
		this.state = state;
        this.template = null;
        if ( template && template.module_type && template.module_type === 'resource' ) {
            this.is_async = true;
            this.on( 'init', () => {
                return template
                    .get()
                    .then( html => {
                        this.template = new Template({
                            name: template.id,
                            raw: normalizeHTML( html )
                        });
                    })
                    .catch( error => { throw error; });
            });
        }
        else {
            if ( name && template ) this.template = new Template({ "name": this.id, "raw": template });
            else if ( !name && template ) this.template = new Template({ "name": "default_component_name", "raw": template });
            else if ( name && !template ) this.template = new Template( this.id );
            else this.template = new Template({ "name": "default_component_name", "raw": "no template defined" });
        }
	}  
    bind() {
        DOM_EVENTS( "add", this, this.event_types );
        return this;
    }
    //  Should do any housekeeping needed before the component can be used.
    //  Should return a promise.
    init() {
        if ( this.is_async ) return this.trigger_async( 'init' );
        else return Promise.resolve( this );
    }
    // Trigger the default off method inherited from the Mediator class.
    // Then unbind the respective event handler if it's bound in the DOM at that moment.
    off( event_name, handler ) {
        if ( this.is_rendered ) DOM_EVENTS( "remove", this, [ event_name ] );
        super.off( event_name, handler );
        return this;
    }
    //  Should remove all bound event handlers from the DOM, as part of component lifecycle management, to help prevent memory leaks.
    //  Should be triggered by the application once the app state no longer contains the component.
    //  Since static components will only get their remove() event trigegred when not in their namesapce anymore, we always want to clear their root content when they get removed instead of relying on their render function.
    remove() {
        if ( this.is_rendered ) DOM_EVENTS( "remove", this, this.event_types );
        this.is_rendered = false;
        this.trigger( 'remove' );
        return this;
    }
    //  Should always get its data from state so the workflow stays simple. We tried adding the data parameter in the previous version and it served no purpose.
    //  Should always insert a unique indentifier for the component.
    //  Should only render if not rendered already. The update function can then call remove before
    render() {
        //console.log( `Rendering ${ this.id } => is_rendered = ${ this.is_rendered }` );
        //  Fetch the root we ned to append to.
        const root = this.root();
        // Generate the HTML from the template or error if no template is available.
        if ( !this.template ) throw new Error( `Failed rendering ${ this.id } - no template` );
        if ( !root ) throw new Error( `Failed rendering ${ this.id } - no root` );
        // Since we call .html() using 'this' as the data, we don't actually have to use the template.register() method unless we want to.
        const html = this.template.html( this );
        const fragment = document.createDocumentFragment();
        const wrapper = document.createElement( 'div' );
        wrapper.innerHTML = html;
        Array
            .from( wrapper.childNodes )
            .forEach( node => fragment.appendChild( node ));
        // Either render the HTML to our root node, or replace the currently rendered version.
        if ( this.is_rendered ) {
            //console.log( `Component ${ this.id } was already rendered, replacing predecessor with the new content.` );
            const predecessor = root.querySelector( `#${ this.id }` );
            if ( predecessor ) predecessor.parentNode.replaceChild( fragment, predecessor );
            else throw new Error( `Cannot find rendered component ${ this.id } => ${ this.is_rendered } => ${ predecessor }. Check if the component is installed into the application.` );
        }
        else {
            nodeClear( root );
            root.appendChild( fragment );
        }
        //console.log( `${ this.id } => setting is_rendered to true. Previous value => ${ this.is_rendered }` );
        this.is_rendered = true;
        // Bind the event handlers to the newly rendered version.
        this.bind();
        // Trigger any registered rendering callbacks.
        this.trigger( 'render' );
        // Doublecheck that our root actually contains rendered content.
        if ( !root.hasChildNodes() ) {
            console.log( '-----   root   -----' );
            console.log( root );
            console.log( '----- fragment -----' );
            console.log( fragment );
            throw new Error( `Root node is empty after rendering.` );
        }
        return this;
    }
    toString() {
        return this.id;
    }
    //  Should update the component config, state or template.
    //  Should always rerender unless the render options has explicitly set to false.
    update({ config, render = true, state, template } = {}) {
        //console.log( `Updating state for ${ this.id }` );
        if ( config ) this.config = config;
        if ( state !== undefined ) this.state = state;
        if ( template ) this.template = new Template({ "name": this.id, "raw": template });
        if ( render ) this.render();
        return this;
    }
}
Component.prototype.module_type = 'component';
//
export default Component;