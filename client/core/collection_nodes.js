/*  collection.mjs     =>
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          ${ starttime }
*/
/*  API
*
*
*
*/
/*  TODO
*
*       Is this a WeakMap? If so, we can replace this entire class with that.
*
*/
//  DEPENDENCIES
////    Import
////    Data
////    Helpers
//  COMPONENTS
////    Component
class Collection_nodes {
    constructor( name ) {
        this.collection = {};
        this.id = `${ this.module_type }_${ name }`;
    }
    get( node_name ) {
        return () => this.collection[ node_name ];
    }
    init() {
        return this;
    }
    remove() {
        return this;
    }
    update( node_id, node ) {
        if ( node ) this.collection[ node_id ] = node;
        else {
            const selection = document.querySelector( `#${ node_id }` );
            if ( selection ) this.collection[ node_id ] = selection;
            else throw new Error( `${ this.id } - Cannot find node #${ node_id } in the DOM` );
        }
        return this;
    }
}
Collection_nodes.prototype.module_type = 'collection_nodes';
//  WORKFLOW
////    Routes
////    Application Events
////    Delegated Events
//  EXPORT
export default Collection_nodes;