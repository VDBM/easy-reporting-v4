/*  application.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-17
*/
/*  API
*
*           Hashchange listeners can be implemented ad hoc when creatign an application, since not all apps will have multiple router states.
*           This allows us to just ignore anything considered routing when it does not aplly to the app.
*           The base hashchange
*
*       Application( string name, array modules );
*       Application.init();
*       Application.install( instance module_or_component );
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    Import
class Application {
    constructor({ name = 'default_name', modules = [] } = {}) {
        this.events = [];
        this.id = `${ this.module_type }_${ name }`;
        this.is_running = false;
        this.modules = {};
        this.name = name;
        this.routes = [];
        this.state = {
            namespaces: []
        };
        modules.forEach( module => this.install( module ));
    }
    //  Should return the results of all module inits, all events to register and all routes to register.
    init() {
        this.is_running = true;
        const module_initializations = Object.values( this.modules ).map( module => module.init ? module.init() : module.id );
        //  A hash change will change the rendered state of everything that isn't the menu and its components. So we need to .remove() any modules that are active when the hash changes.
        //  Dont use the remove_statics flag so any static components stay rendered.
        this.on( 'hashchange', ( state, path ) => {
            this.state.namespaces = path.slice( 2 ).split( '/' );
            this.remove();
        });
        return Promise
            .all( module_initializations )
            .then( modules => ({ events: this.events, modules, routes: this.routes }))
    }
    //  Should add a module to the modules array.
    //  Should immediately init() the module if the app is already running.
    install( module ) {
        const id = module.id || module.module_type;
        if ( this.modules[ id ] ) throw new Error( `cannot install module "${ id }". A module with this id already exists.` );
        else this.modules[ module.id || module.module_type ] = module;
        if ( this.is_running ) return module.init();
        else return this;
    }
    on( event_name, options_or_callback, callback ) {
        const stringify = any => Object.prototype.toString.call( any );
        let handler;
        let options;
        //  We have to use arrows here instead of .bind to preserve the this context of the handlers.
        if ( Object.prototype.toString.call( options_or_callback ) === '[object Function]' ) {
            handler = ( ...values ) => options_or_callback( this.state, ...values );
            options = null;
        }
        else {
            handler = ( ...values ) => callback( this.state, ...values );
            options = options_or_callback;
        }
        //  Route registration. Push to the rotue array.
        if ( event_name.charAt( 0 ) == '/' ) this.routes.push({ event_name, options, handler });
        //  Component or module event. We wrap the event so we inject our own state as the first parameter of the event.
        else if ( event_name.includes( '.' )) {
            const [ module_name, delegated_event_name ] = event_name.split( '.' );
            if ( this.modules.hasOwnProperty( module_name )) this.modules[ module_name ].on( delegated_event_name, options, handler );
            else throw new Error( `Cannot bind event ${ delegated_event_name }: module ${ module_name } cannot be found.` );
        }
        //  Upper scope event listener.
        else this.events.push({ event_name, options, handler });
        return this;
    }
    remove({ remove_statics = false } = {}) {
        Object
            .values( this.modules )
            .forEach( module => {
                if ( remove_statics ) module.remove();
                else if ( module.is_rendered && !module.is_static ) {
                    if ( module.remove ) module.remove();
                    else throw new Error( `${ this.id }: cannot remove module ${ module.id }. Module does not have a .remove() method.` );
                }
            });
        return this;
    }
    toString() {
        return this.id;
    }
}
Application.prototype.module_type = 'application';
//
export default Application;