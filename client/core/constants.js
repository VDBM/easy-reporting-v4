/*  constants.mjs   => 
*   @language:  ES6
*   @version:   v1.0.0
*   @creator:   VDBM
*   @date:      2018-08-22
*/
////////////////////
/*  API
*    - kellogg oldred: rgba(211,17,69,1);
*    - kellogg red: rgba(181,24,71,1);
*    - kellogg grey: rgba(224,224,224,1);
*    - kellogg orange: rgba(238,175,48,1);
*    - fluo green: rgba(190,219,0,1);
*    - dark green: rgba(0,115,99,1);
*    - fluo blue : rgba(0,169,244,1);
*    - ET light blue: rgba(153,153,255,1);
*    - ET dark blue: rgba(0,51,102,0.9);
*    - ET white: rgba(238,238,238,1);
*/
const COLOR_ER_WHITE = { 'rgba' : 'rgba( 238, 238, 238, 1 )', 'hex' : '#EEEEEE' };
const COLOR_K_BLUE = { 'rgba' : 'rgba( 80, 153, 219, 1 )', 'hex' : '#5099DB' };
const COLOR_K_BLUE_FLUO = { 'rgba' : 'rgba( 0, 169, 244, 1 )', 'hex' : '#00A9F4' };
const COLOR_K_GREEN = { 'rgba' : 'rgba( 0, 115, 99, 1 )', 'hex' : '#007363' };
const COLOR_K_GREEN_FLUO = { 'rgba' : 'rgba( 190, 219, 0, 1 )', 'hex' : '#BEDB00' };
const COLOR_K_GREY = { 'rgba' : 'rgba( 224, 224, 224, 1 )', 'hex' : '#E0E0E0' };
const COLOR_K_ORANGE = { 'rgba' : 'rgba( 238, 175, 48, 1 )', 'hex' : '#EEAF30' };
const COLOR_K_RED = { 'rgba' : 'rgba( 181, 24, 71, 1 )', 'hex' : '#B51847' };
const COLOR_K_YELLOW = { 'rgba' : 'rgba( x, x, x, 1 )', 'hex' : '' };
const MILISECONDS_SHIFT = 28800000; // 8 hours
const MILISECONDS_DAY = 86400000; // 24 hours = MILISECONDS_SHIFT * 3
const MILISECONDS_WEEK = 604800000; // 7 days = MILISECONDS_DAY * 7
const MILISECONDS_WEEKS_4 = 2419200000; // 4 weeks = MILISECONDS_WEEK * 4   // for calculating the kellogg period
const MILISECONDS_WEEKS_5 = 3024000000; // 5 weeks = MILISECONDS_WEEK * 5   // for calculating the kellogg period
////////////////////
export {
    COLOR_ER_WHITE,
    COLOR_K_BLUE,
    COLOR_K_BLUE_FLUO,
    COLOR_K_GREEN,
    COLOR_K_GREEN_FLUO,
    COLOR_K_GREY,
    COLOR_K_ORANGE,
    COLOR_K_RED,
    COLOR_K_YELLOW,
    MILISECONDS_DAY,
    MILISECONDS_SHIFT,
    MILISECONDS_WEEK,
    MILISECONDS_WEEKS_4,
    MILISECONDS_WEEKS_5
};