/*  router.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-17
*/
/*  API
*       Router.on( string route, function handler, object scope );
*       Router.off( string route );
*/
/*  TODO
*
*/
//  Parameters inside a route string will start with the double point : symbol.
const regex_parameters = /(\/:.+?)(?=\/|$)/g;
const regex_slashes = /\//g;
class Router {
    constructor( hashchange_handler ) {
        this.onhashchange = hashchange_handler;
        this.history = [];
        this.routes = {};
    }
    checkHash() {
		let path;
		if ( process && process.browser && window ) {
			// There is no difference between route server/page and server/page/ . The browser will automatically route to server/page/ so we don't have to check for this condition.
			// Routing to /#/ directly will trigger hashchange and hence our route method.
			// Routing to anything shorter will not trigger hashchange and hence not the route method.
			// So if we want consistency, we need to remove any this.rotue() calls that will be detected by the hashchange event.
			// So we changed this function from routing to the path if it exists to replacing the state and triggering the route method manually if no hash exists.
			// The end result is that route / will always be triggered and that we will always have the hash /#/ if no hash exists.
			// We still want to return the path, so any other application logic that has to happen upon route / can be bound to either the / route handler OR by handlign the return value of this method.
			// This way a module has a choice, since the checkInitial can be called as a method while the hashchange is only trigegred by changing the hash some way.
			if ( window.location.hash && window.location.hash.length > 2 ) path = window.location.hash;
			else {
				path = '#/';
				window.history.replaceState( window.history.state, '', `${ window.location.origin }${ window.location.pathname }${ path }` );
			}
            if ( this.onhashchange ) this.onhashchange( path );
            this.route( path );
			return path;
		}
		else return null;
    }
    init() {
        if ( process && process.browser && window ) window.addEventListener( 'hashchange', event => this.checkHash() );
        return { 'router' : true };
    }
    off( route ) {
        throw new Error( 'router.off() has not been implemented yet.' );
        return this;
    }
    on( route, options, callback ) {
        //  We want to use an object for direct reference so we can use the route as object property keys, but still need all the data inside each property value to contain the route as well.
        const route_parameters = route.match( regex_parameters ) || [];
        //  Format: start-of-string => static part not preceded by /: => all dynamic parts starting with /: where this /: is replaced by /:(+?) => end-of-string
        const regex = new RegExp( `^${ route_parameters.reduce(( path, parameter ) => path.replace( parameter, '/([A-Za-z0-9_-]+?)' ), route ) }$` );
        const scope = options && options.scope || null;
        const parameters = route_parameters.map( parameter => parameter.slice( 2 ));
        if ( !this.routes[ route ] ) {
            //  Handlers are the combinations of a callback with its scope that need to be called when a route is triggered.
            //  Namespaces is the amount of subroutes or route chunks inside the route sting.
            //      Since route chunks are seperated by a slash, this comes down to the amount of slashes inside the route.
            //      This also implies we ahve tod ocument that routes cannot end in a slash, or we have to change the code to check for this explicitly.
            //  Parameters is the array of dynamic namespaces inside the route string. These names wil be used to populate the aprameters array before calling a handler.
            //  Regex is the regular expressio nboject craetd from the route string. This will be used on hash changes to check if the active browser hash is served by this route.
            //  Route is a copy/reference? of the route string.
            this.routes[ route ] = {
                handlers: [],
                namespaces: ( route.match( regex_slashes ) || [] ).length,
                parameters,
                regex,
                route
            };
        }
        this.routes[ route ].handlers.push({ callback, scope });
        return { route };
    }
	//  The uri_hash will always start with #/
    route( uri_hash ) {
        this.history.push( uri_hash );
        //  If the uri_hash is the root, namely #/ , we just want to keep that root, since splitting it will still return two empty strings.
        //  Else we do want to split since the second namespace will not be empty.
        const paths = uri_hash === '#/'
            ? [ '/' ]
            : [ '/', ...uri_hash.slice( 2 ).split( '/' ).map(( namespace, index, source ) => '/' + source.slice( 0, index + 1 ).join( '/' )) ];
        //  All the available routes we have to check.
        const available_routes = Object.values( this.routes );
        //  For each namespace we have to check, find the first route whos regex matches the namespace.
        //  There should only be one namespace matching that route, preferring.
        const routes = paths
            .map( path => {
                //  Both dynamic routes and static routes can be found here./ So if we return the one with the least amount of parameters, we have the route with the most matching static part.
                //  Update: the description above fails for routes with multiple parameters, since there will be two matches.
                //  So we have to change the logic:
                //      If multiple routes are found, we want to select the one that has no parameters.
                //      If no route exists that has no parameters, we weant to take the route with the most amount of parameters? namespaces?
                const matches = available_routes
                    .filter( route_entry => route_entry.regex.test( path ))
                    .sort(( first_match, second_match ) => first_match.parameters.length - second_match.parameters.length );
                return matches.length
                    ? matches[ 0 ]
                    : null;
            })
            // Since some possible paths will not have a handler assigned to it, we have to check for existance.
            .filter( Boolean )
            //  Explicitly sort the routes so that we will trigger the handlers representing the shortest namespaces first.
            .sort(( first_route, second_route ) => first_route.namespaces - second_route.namespaces );
        if ( routes.length ) {
            // The last route will always be the one with the full list of parameters.
            const last_route = routes[ routes.length - 1 ];
            const parameter_keys = last_route
                ? last_route.parameters.slice().reverse()
                : [];
            const parameter_values = uri_hash.split( '/' ).reverse();
            const parameters = parameter_keys.reduce(( parameters, key, index ) => {
                parameters[ key ] = parameter_values[ index ];
                return parameters;
            }, {});
            routes.forEach( route_entry => {
                route_entry.handlers.forEach( handler => {
                    if ( handler.scope ) handler.callback.apply( handler.scope, [ parameters ] );
                    else handler.callback( parameters );
                });
            });
        }
        return this;
    }
}
Router.prototype.module_type = 'router';
//
export default Router;