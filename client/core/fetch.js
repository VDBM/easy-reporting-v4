/*  fetch.mjs   => 
*   @language: ES6
*   @version:  v1.0.0
*   @creator:    VDBM
*   @date:   2018-03-12
*/
////////////////////
/*  Response
*/
class Headers {
    constructor( header_string ) {
        this.headers = header_string
            .split( /\r\n/ )
            .map( header => {
                const index = header.indexOf( ':' )
                return [
                    header.substring( 0, index ),
                    header.substring( index + 2, header.length )
                ];
            })
            .reduce(( result, [ key, value ])=> {
                if ( value === 'true' ) result[ key ] = true;
                else if ( value === 'false' ) result[ key ] = false;
                else result[ key ] = value;
                return result;
            }, {});
    }
    entries() {
        return Object.entries( this.headers );
    }
    forEach( transformation ) {
        Object
            .values( this.headers )
            .forEach( transformation );
        return this;
    }
    get( header_name ) {
        return header_name
            ?   this.headers[ header_name ]
            :   this.headers;
    }
    keys() {
        return Object.keys( this.headers );
    }
    values() {
        return Object.values( this.headers );
    }
}
class Response {
    constructor( body, header_string, status_code, status_text, url ) {
        this.body = body;
        this.headers = new Headers( header_string );
        this.ok = ( status_code >= 200 && status_code <= 299 );
        this.status = status_code;
        this.statusText = status_text;
        this.url = url;
    }
    blob() {
        return Promise.resolve( this.body );
    }
    json() {
        return Promise.resolve( JSON.parse( this.body ));
    }
    text() {
        return Promise.resolve( this.body );
    }
}
/*  fetch
*/
const fetch = (function() {
    if ( process ) {
        // Browser
        if ( process.browser ) {
            // Browser supports fetch, return it .
            if ( window.fetch ) {
                return window.fetch;
            }
            // No fetch, mimic the API through an AJAX call .
            else {
                return ( url, { body = null, headers = {}, method = 'GET' } = {}) => new Promise(( resolve, reject ) => {
                    const request = new XMLHttpRequest();
                    if ( Object.keys( headers ).length ) {
                        Object
                            .entries( headers )
                            .forEach( ( [ header, value ] ) => request.setRequestHeader( header, value ) );
                    }
                    request.navigationAbort = false;
                    request.onload = () => {
                        const response = new Response(
                            request.responseXML || request.responseText, // body
                            request.getAllResponseHeaders(), // headers
                            request.status, // status_code
                            request.statusText, // status_text
                            url// url 
                        );
                        if ( response.body ) resolve( response );
                        else reject( response );
                    };
                    request.onerror = error => reject( { error, 'text' : 'fetch onerror handler' } );
                    request.open( method, url );
                    // CORS and IE10-IE11 withCredentials fix: the object state has to be OPEN to set the credentials flag in IE10-IE11
                    request.withCredentials = true;
                    // IE7-9 domain request premature abort fix.
                    // http://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
                    // xhr.timeout = 2000;
                    request.ontimeout = error => reject( { error, 'text' : 'fetch ontimeout handler' } );
                    request.onabort = error => reject( { error, 'text' : 'fetch onabort handler' } );
                    request.onprogress = () => void( 0 );
                    if ( body ) request.send( body );
                    else request.send();
                } );
            }
        }
        else if ( process.release && process.release.name === 'node' ) {
            // If we put all our links relative
            const HTTP = require( 'http' );
            return uri => new Promise(( resolve, reject ) => {
                const chunks = [];
                HTTP
                    .get( uri, response => {
                        response.on( 'data', data => chunks.push( data ) );
                        response.on( 'end', () => resolve( Buffer.concat( chunks ).toString() ) );
                    } )
                    .on( 'error', error => reject( error ) );
            });
        }
    }
    else throw new Error( 'enviroment not supported' );
}());
/*  root
*/
const root = 'http://mecvm21';
/*  root_services
*/
const root_services = `${ root }/api`;
/*  root_resources
*/
const root_resources = `${ root }/resources`;
////////////////////
export { fetch, root, root_resources, root_services };