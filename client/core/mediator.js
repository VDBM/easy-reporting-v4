/*  mediator.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-07-17
*/
/*  API
*       Mediator.on( string event_name, function handler );
*       Mediator.off( string event_name, function handler );
*/
/*  TODO
*       We could save some bytes if we replace the string based API to methods: onchange(), onclick(), onrender() etc so that thw whole bundle minifies better.
*/
class Mediator {
    constructor( scope = null ) {
        //  Event handler storage.
        this.events = {
            change: [],
            click: [],
            init: [],
            remove: [],
            render: []
        };
        this.scope = scope;
    }
    //  Should remove an event handler from the component.
    off( event_name, handler ) {
        if ( handler ) {
            const index = this.events[ event_name ].findIndex( event => event.handler === handler );
            this.events[ event_name ].splice( index, 1 );
        }
        else this.events[ event_name ] = [];
        return this;
    }
    //  Should add an event handler to the component.
    on( event_name, options, handler ) {
        try {
            let scope = undefined;
            let selector = undefined;
            if ( !handler ) {
                handler = options;
                options = null;
            }
            else {
                scope = options && options.scope || null;
                selector = options && options.selector || null;
            }
            if ( !this.events[ event_name ] ) this.events[ event_name ] = [];
            this.events[ event_name ].push({
                handler: handler.bind( scope || this.scope || this ),
                selector
            });
        }
        catch( error ) {
            console.error( `mediator.js - ${ this.id || 'base' }` );
            console.error( error.message );
            console.log( error.stack );
        }
        return this;
    }
    trigger( event_name, ...values ) {
        //  The higher level application will decide to wrap the handler or not in its own overwriting of the .on() call to the mediator.
        //  So we don't need to handle any injection of scope here, as we did in the previous version.
        if ( this.events[ event_name ] && this.events[ event_name ].length ) this.events[ event_name ].forEach( event => event.handler( ...values ));
        return this;
    }
    trigger_async( event_name, ...values ) {
        if ( this.events[ event_name ] && this.events[ event_name ].length ) {
            return Promise
                .all(
                    this.events[ event_name ].map( event => {
                        const result = event.handler( ...values );
                        if ( result && !!result.then ) return result;
                        else return Promise.resolve( result );
                    })
                )
                .then(() => this );
        }
        else return Promise.resolve( this );
    }
}
Mediator.prototype.module_type = 'mediator';
//
export default Mediator;