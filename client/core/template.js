// Implements ER_Next (3.1) templates into ER3.0
/* 	MODULE - TEMPLATE ENGINE
* 	DEPENDENCIES:
*	-	$
*	-	$.log
*	-	$.error
*	-	$.util2.trimHTML
*	-	$.util2.pathObj
*   -   ajax
*   -   creator
*   -   promise
* 	REQUIREMENTS: 
*	-	Easier syntax.
*	-	Preservation of static elements.
*	-	More extensible.
*	-	Less dependencies.
*	-	No shenanigans with scope injection as we had in templates version 1 @ ER 3.0.
*	-	Option to switch to template strings if they ever implement dynamic template strings.
* 	REALISATIONS:
*	-	Template syntax improved dramatically: No more wrapper elements unless you want to use a wrapper. data-tmpl attributes and ${...} variables are all what's needed to run a html snippet through the engine.
*	-	HTML comments can use any style or position now, so the amount of whitespace used before, after or inside the HTML comment doesn't matter anymore.
*       Updated trimHTML to remove the tabs and new lines first, then removing anything between <!-- and -->
*       Although we didn't expect it, the previous version didn't allows unlimited indentation inside templates.
*	-	Updated pathObj with a regex to transform brackets into points
*		This allows syntax like object[index] (object.index) or object[propertyName] to work with it as well, simplifying our getValue function into a single reduction.
*   -   We can use a <template> tag as a wrapper to hook a rule to an element that should not be rendered.
*   -   Fixed findTemplates to support infinite nesting of same tag types.
*   -   Extended the foreach rule to include COLLECTION and SOURCE inside its data scope. This makes collection and source reserved words inside foreach rules, so the original data used to populate should not have collection or source properties or its top level.
*   -   Replaced the 'this' mechanism for calculations with the explicit use of the word calculate() so it's more clear how the syntax works. This also allows us to extend the engine further with other type of variable transforms.
*   -   Replace calculate() with exec() and added support for global functionsinside exec(). So we can now use things like JSON.stringify() inside exec()
*   -   When using a exec() transformation, all data keys inside the source data will be rereferenced, so we should not use source data names that correspond with builtin functions.
*       EG. we can't use the Math.ceil() function inside a exec(), if the data source contains an object named Math. 
*	NOTES:
*   -   When using gulp or other concat tools, make sure that the generated templates file ONLY contains compatible tempaltes. We get an assignment error when the tempaltes.html file contains extra html not wrapped into a template tag.
*       We'd have to extend the cacheTemplate code (probably the regex used to detect a full template ) to make this  possible.
*       Since the 'print.html' and 'details.html' pages are resources that the app needs to be able to fetch, it makes sense to not have them inside the templates.html code.
*       So there hasn't been a need yet to implement this.
*	-	ELSE equals IF !=  . So all data used to populate should be structured to support this. We should not write very complex if/else structures into the template.
*       Instead we should structure the data so it supports this or make a seperate component for it.
*   -   In development, we have to disable all the try/catch error handling to actually get referred to the correct position when using source maps.
*       If we keep the try/catch handlers, the error will be traced to the try/catch part of the source maps instead of the actual place the error originated.
*       This can be solved by starting to use something like stacktrace.js, but we decided against importing even more modules in this stage of the project.
*	-	Removed the VAR_START and END constants from the _CONFIG, since this forces us to properly escape the entire constant for usage inside a regex.
*		The idea was to have the ${ and } tokens used inside the html rule variable as well, so devs can choose their own tokens to use.
*		Edit parseTemplate and populateTemplate regex' if we decide to implement this.
*       This could be beneficial if we want to use the same syntax inside the data-tmpl attributes as we do inside the variables.
*       We might be able to leverage this to have ${} inside rule content mean the encapsulated scope ( like in foreach loops using each record inside the array as the scope of a content chunk )
*       Then have non-${} rules be performed on the base data used to populate the template.
*	-	Recheck if there's ANY case were we'll have significant whitespace at the end of a rule.
*		We trim the rule string before parsing it to allow for a space after the comma inside multiple definition rules: "if x, if y" instead of "if x,if y"
*	-	Splitted the component based template into its own module. This was easier than passing around the entire report scope through all the functions.
*       Update: removed this again. A component definition is now just another type of rule. We do explicitly pass around scope now when needed.
*       This proved to be much simpler to implement. Anything can be used as a component now as long as it has a method that returns a HTML string by registering the components and their methods.
*	-	populateTemplate for components is still a bit weird.
*	-	Templates in different files can be nested by defining the external template used as a component. Or by manually adding the result of the inner template somewhere in the view code.
*       This slightly differs from inner templates written inline into the raw template string, since both templates can't be parsed at the same time without merging them soon.
*   -   Since access to the template class scope is only needed for components, we can just propagate the same this reference through the entire application.
*       Only in the ruleOperators, this is not defined, but the instance is passed into the function. We could replace this by not using arrow functions.
*   -   Added 'exists' and 'missing' as alternatives to " = true " and " != true " to check if the operand is not null or undefined.
*       So we can save using != and = to actually compare value instead of overloading it to check for existence as well.
*   -   Multipe ways to add an or rule: 'or operand = value, or operand = value' , 'if operand = value or if operand = value'. We can probably reuse the if rule some way.
*       Either edit the aplyRules rules.every() check or create a new 'or' type in ruleOperators
*   -   Added a loop check to 'getTemplateVariable' before calling pathObj. If we're in a loop and the loop variable gets used inside the path, we can replace that loop variable with its value
*       ( from the object created by the loop ) so we get autoamtic injection without wrapping everything in a exec() statement.
*   -   Sicne this class is shared between both frontend and backend, we had to redefine the readFile method. Added a check to see if FS.readFile exists.
*/
/*  API
*
*    - All rules can be combined by seperating them with a , inside the data-attribute used for the definition
*    - Paths can be expressed both with dots and brackets: records.options[0] = records[options][0] = records.options.0
*      This all follows from using the pathObj function. 
*    - 'collection' and 'source' are reserved words inside the foreach scope used to populate variables with.
*      Use x.collection and x.source instead. foreach x in collection will fail and foreach x in source will refer to the top non-looped data scope.
*    - We can do calculations on the collection properties by using calculate() wrapping the variable: ${calculate(index + 5)} === function() { return this.index + 5; }()
*    - We can refer to collection and source directly as well.
*    - data-tmpl="foreach x in y"    ( repeat the tag for each index in the source array)
*      ${x.propertyName}             ( the the reference is accessed by ${} wrapping
*                                      these can be nested inside a tag: <tagname>${source.property} other text in between ${source.otherProperty} and other text</tagname>
*                                    )
*    - if x.y = valueLiteral         ( check if source or property of source equals a value )
*    - if x.y != valueLiteral        ( check if source or property of source does not equal a value )
*    - if x.y                        ( check if source or property of source resolves to true )
*    - else => if !=
*    - component                     ( delegate creating the html to a registered component, using the source data to populate the component )
*    
*    - USAGE in controllers
*    
*    // View components mockup
*    const components = {
*        'primary' : {
*            'render' : ( data, operand, relation, expectation ) => '<label>PRIMARY HTML</label>', // Most components will only take one argument, data, but if needed, the other parts of the rule are also passed into the component.
*            'submodule' : {
*                'render' : () => '<label>SUBMODULE HTML</label>'
*            }
*        }
*    };
*    // Test the template
*    const template = $.template( $( '#template' ).innerHTML ); // create a new template from a string, either from a file or grabbed from elsewhere
*    template
*        .register( components, 'render' ) // Register our components so we can use the component rule. The 'render' part is the name of the method of the component that return the html.
*        .populate( data ) // populate the template using the data. Any components will used a prev registered method name.
*        .render( '#output' ); // render to a specific target. Alternatively, access the template.cache to get the populated html itsself.
*        
*        
*    Sample workflow:
*    
*    .populate( data )
*    => populateTemplate
*    => applyRules
*        => getTemplateVariable: replace all variables with data
*            => return value of variable
*            => return populateTemplate on inner template
*        => rule content result: replace the hook with the rule result
*            => COMPONENT: return component html
*            =>        IF: return empty string if rule fails, else return rule result
*            =>   FOREACH: return result of call getTemplateVariable on each item
*    
*/
/*  TODOS:
*
*   - REDO this with passing around definitions, we want to eliminate any instance of this and just have the class run the instance references.
*   - Refactor the caching mechanism into its own module so we can keep this module clean.
*   - Check if there's any use cases where merging external templates before parsing is an advantage over using them as a component.
*   - Check if we have to extend the 'if' rule operator with type checks, since the expectation will always be a string inside the rule.
*   - Implement the recursion rule. We want to repeat an entire foreach structure for a specific array used inside the foreach.
*   - Check if we have to include the original source data inside the foreach loop. Atm, I 'think' that we can't refer to the 'global' data object used to populate inside a foreach loop.
*     So we should extend the { [operand] : data } with the rest of the data if needed.
*   - Double check if this will work for our other visualisations. It might be that we need a more general calc() function, but for nwo we'll try to avoid it to keep the template syntax short.
*   - Once we update the MVC view structure, we can get rid of the INIT kludge to fix declarations of templates as components inside the view, since they might rely on data being fetched.
*     Currently we have issues with the current view implementation, since views will call the init() method on each of their components upon startup.
*   - Check if the register method for scope injection is still needed.
*   - Collection and source properties only seem to work on the tag that has the tmpl loop.
*   - Update the exec()transform so we can also use it on paths like collection.index
*   - There's still an issue with our promise implementation where the scope seems to be wrong, so we can't pass promise.resolve or promise.reject as a parameter without binding it to itsself.
*   - We think we fixed the scope issues with components thata re tempaltes themselves. Need tor egister methods onto the parent tempalte, not the ensted one. Assumed to work, verify this later.
*   - Decide if nested templates should be a seperate rule or not. Atm, it's defined as a component. BUt since we'd still need to register the this.components of the view as the scope, there's only a few letters difference between the eventual component and template code, so making syntactic sugar for templates seems unneeded unless we can find a way to get the scope definition out of the init function.
*   - Can't reuse data scope ames inside exec() functions. So if the part inside the exec() call contains keys also contained in the data, the template fails.
*/
'use strict';
////////////////////
// import
const { inspectError, isNumber, isObj, normalizeHTML, pathObj } = require( './util.js' );
/*  CACHE => private object
*       save the templates
*/
const _CACHE = {};
/*  _CONFIG => private final object 
* @name	_CONFIG		- Configuration object for the module.
* @key  ATTRIBUTE   - The data attribute on html tags that we want to use to indicate that element is a template. 
* @key  FILENAME
* @key  FILEPATH
*/
const _CONFIG = {
	"ATTRIBUTE" : "data-tmpl",
    "FILENAME"  : "templates.html",
    "FILEPATH"  : "./"
};
/*
*/
const _SELF_CLOSING = [ 'AREA', 'BASE', 'BR', 'COL', 'COMMAND', 'EMBED', 'HR', 'IMG', 'INPUT', 'KEYGEN', 'LINK', 'META', 'PARAM', 'SOURCE', 'TRACK', 'WBR' ];
/*  cacheTemplate => private function updates _CACHE
*/
const cacheTemplate = function cacheTemplate( template, name ) {
    let sanitized;
    let next;
    let count = 0;
    let rawTemplate = template;
    if ( rawTemplate ) {
        sanitized = normalizeHTML( rawTemplate );
        if ( sanitized ) {
            do {
                next = reTemplateRaw.exec( sanitized );
                if ( next ) {
                    _CACHE[ next[ 1 ] ] = next[ 2 ];
                    count += 1;
                }
            }
            while (next !== null);
            if ( !count ) alert( 'Template Failure.\nThe following string did not generate any valid templates. String probably does not contain <template id="">' );
        }
        else {
            inspectError( {
                'data' : raw,
                'error' : null,
                'source' : 'cacheTemplate.sanitized',
                'text' : `template ${name} cannot be sanitized correctly.`
            } );
        }
    }
    else {
        inspectError( {
            'data' : template,
            'error' : null,
            'source' : 'cacheTemplate.rawTemplate',
            'text' : `template ${name} cannot be found inside the cache.`
        } );
    }
};
/*  reTemplateRaw => private regex helps cacheTemplate
*/
const reTemplateRaw = /\<template id="(.*?)"\>([\s\S]*?)\<\/template\>(?=\<template id|$)/g;
/* 	parseTemplate => Sanitize the raw template, extract inner templates and rules
* @name    	parseTemplate	- public function triggers findRules
* @param   	String raw    	- A string representing a HTML template.
* @returns 	Object        	- 
* 		String content   	- Source string with all the template code replaced by placeholders.
*		String raw       	- Source string.
*		Array  templates 	- Nested templates.
*/
const parseTemplate = function parseTemplate( raw ) {
	let sanitized;
    let chunked;
    let findTemplates;
    let template;
    let templates = [];
    if ( raw ) {
        try {
            // 1) Sanitize the template from comments and non-meaningful whitespace.
            sanitized = normalizeHTML( raw );
            if ( sanitized ) {
                // 2) Split the template into static parts and templates, replacing the templates with placeholders.
                findTemplates = true;
                chunked = sanitized.slice();
                // We start with the full string. If we detect the template attribute somewhere, we use findRules to parse out the first template.
                // Then we replace that part of the string, so that the remaining string doesn't contain the template attribute anymore for that template.
                // We loop until no more templates are found.
                while ( findTemplates ) {
                    if ( chunked.indexOf( _CONFIG.ATTRIBUTE ) !== -1 ) {
                        template = findRules( chunked );
                        if ( template ) {
                            chunked = chunked.replace( template.raw, '${_template_chunk_' + templates.length + '}' ); // We have to escape ${ if we want to use template strings. 
                            templates.push( template );
                        }
                        else findTemplates = false;
                    }
                    else findTemplates = false;
                }
                return {
                    "content" : chunked,
                    "raw" : raw,
                    "rule" : '',
                    "templates" : templates
                };
            }
            else {
                inspectError( {
                    'data' : raw,
                    'error' : new Error(),
                    'source' : 'parseTemplate',
                    'text' : `template cannot be sanitized correctly.`
                } );
                return null;
            }
        }
        catch( error ) {
            inspectError( {
                'data' : raw,
                'error' : error,
                'source' : 'parseTemplate',
                'text' : 'parsing failed after sanitization.'
            } );
            return null;
        }
    }
    else {
        inspectError( {
            'data' : raw,
            'error' : new Error(),
            'source' : 'parseTemplate',
            'text' : 'raw template string not found'
        } );
    }
};
/* 	findRules => private function helps parseTemplate
* @name    	findRules		- Find the rules and inner templates of a template. Save everything in the definition and replace all the inners with a variable we can replace with populateTemplate.
*							  This way an inner template just becomes another variable to replace inside the content string of the template.
* @param   	String raw    	- A string representing a HTML template.
* @returns 	Object       	- 
*		String content   	- Source string with all the template code replaced by placeholders.
*		String raw       	- Source string.
*		String rule		 	- Text of the rule inside the template attribute.
*		Array  templates	- Nested templates.
*/
const findRules = function findRules( raw ) {
	let ruleStart;
    let ruleDelimiter;
    let templateStart;
    let tagName;
    let tagCount;
    let findEnd;
    let indexEnd;
    let character;
    let template;
    let content;
    let source;
    let prerule;
    let rule;
    let postrule;
    let innerTemplates;
    let includeTag;
    let selfClosing;
	try {
        // Find the start of the rule inside a tag that contains a rule attribute. ( data-tmpl )
		ruleStart = raw.indexOf( _CONFIG.ATTRIBUTE );
        // The end is the length of the used attribute + 1 for the equals sign ( = ), so we get either " or ' as the character.
        // We need to know this so we can start searching for end tags After the rule ends. This allows us to use the names of tags inside the rule.
        ruleDelimiter = raw.charAt( ruleStart + _CONFIG.ATTRIBUTE.length + 1 );
        // The tag itsself starts at the first occurrence of < before the found start of the rule attribute, so count backwards starting at the rule attribute. ( data-tmpl )
		templateStart = raw.lastIndexOf( '<', ruleStart );
        // Since we're parsing a tag with data-tmpl, there will always be a space between the tagName and the first attribute.
		tagName = raw.slice( templateStart + 1, raw.indexOf( ' ', templateStart ) );
        // Normally <template> tags aren't rendered by the browser.
        // Since this basic <template> tag functionality isn't completely supported yet, we can reuse them to group a bunch of elements we want to add a rule to.
        // The template tag itsself won't get added to the content property of the template.
        includeTag = tagName.toUpperCase() !== 'TEMPLATE';
        selfClosing = _SELF_CLOSING.includes( tagName.toUpperCase() );
		findEnd = true;
        // Since we start at the rule, the first instance of /> will be the end of the self closing tag. Self-closing tags can never have inner content.
        // So that point will automatically be the end of the template.
        if ( selfClosing ) indexEnd = raw.indexOf( '>', ruleStart ) + 1;
        // Else we need a start point beyond the tag that contains the rule, so we don't immediately find for the starttag itsself instead of its corresponding end tag.
        // Then we loop over the raw string until we find the proper end tag, ignoring inner content if needed.
        else {
            // Start searching after the tag containing the rule.
            indexEnd = ruleStart;
            tagCount = 1;
            while ( findEnd ) {
                // Find the next occurrence of the tag name. We want to skip any tags we already saw, so start looking from the previous indexEnd + the tagName length.
                // We want to search for the tagName, but we don't want to match any occurence of the tagName used inside the rule.
                // eg. <option data-tmpl="foreach option in options">...</option> will match foreach option instead of </option>
                // We tried searching for tagName + '>' but this didn't capture any start tags, so we lost nesting.
                // We could use a regexp to solve this as well, or just start searching after the rule end.
                // Since the rule will be a data-attribute, the next instance of " or ' will be the end of the rule
                indexEnd = raw.indexOf( tagName, indexEnd + tagName.length );
                if ( indexEnd !== -1 ) {
                    character = raw.charAt( indexEnd - 1 );
                    // The next found tag is already an end tag. If our tagCount equals zero, we found the proper end tag.
                    if ( character === '/' ) {
                        tagCount -= 1;
                        if ( !tagCount ) findEnd = false;
                    }
                    // Else we found the opening tag of a tag with the same name inside the content, so we up the count so the next iteration
                    else if ( character === '<' ) tagCount += 1;
                    // Else there's something wrong with the raw string, since only / and < are characters that should be in front of the tag name.
                    // Other instances of tagName were found, but none seem to close the templated tag.
                    else if ( character === undefined ) {
                        findEnd = false;
                        inspectError( {
                            'data' : raw,
                            'error' : new SyntaxError( 'character undefined' ),
                            'source' : 'templates.findRules',
                            'text' : `Template malformed.\nCan't find the correct end tag of type ${tagName}.\n${raw}` 
                        } );
                    }
                    // We skip any other characters in front of the tagName.
                    // This allows us to use the name of the tag inside the rule of the tag.
                }
                // No other instances of tagName were found. Big syntax error, since will always be invalid HTML.-
                else {
                    findEnd = false;
                    inspectError( {
                        'data' : raw,
                        'error' : new SyntaxError(),
                        'source' : 'templates.findRules',
                        'text' : `Template malformed.\nCan't find any end tag of type ${tagName}.\n${raw}`
                    } );
                }
            }
            if ( indexEnd !== undefined ) indexEnd += tagName.length + 1;
        }
        if ( templateStart !== undefined && indexEnd !== undefined ) {
            template = raw.slice( templateStart, indexEnd );
            // We don't want to join postrule and content into one capturing group since we want to check the content for inner templates.
            // Self-closing tags won't have content they enclose.
            // So by adding the empty capture group at the end, we force the regular expression to find an empty string.
            [ source, prerule, rule, postrule, content ] = selfClosing
                ? new RegExp( '(<' + tagName + '.*?)data-tmpl="(.+?)"\s?(.*?>)()', 'gi' ).exec( template )
                : new RegExp( '(<' + tagName + '.*?)data-tmpl="(.+?)"\s?(.*?>)(.*)(?:<\/' + tagName + '>)', 'gi' ).exec( template );
            // Source will be null and the others undefined if the regexp fails.
            if ( source ) {
                // Check if we have an nested template inside the content.
                // If so, parse it as well and overwrite the current templates content with the chunked version that includes the hooks for the inner templates.
                // This makes it possible to just loop over the innerTemplates array and match every template hook with the corresponding rules using its index in the innerTemplate array. 
                innerTemplates = content.indexOf( _CONFIG.ATTRIBUTE ) !== -1 ? parseTemplate( content ) : null;
                if ( innerTemplates ) {
                    return {
                        "content" : includeTag ? `${prerule.trim()}${postrule}${innerTemplates.content}</${tagName}>` : innerTemplates.content,
                        "raw" : template,
                        "rule" : rule,
                        "templates" : innerTemplates.templates
                    };
                }
                else {
                    return {
                        "content" : includeTag ? `${prerule.trim()}${postrule}${content}</${tagName}>` : content,
                        "raw" : template,
                        "rule" : rule,
                        "templates" : null
                    };
                }
            }
            else {
                inspectError( {
                    'data' : raw,
                    'error' : new SyntaxError(),
                    'source' : 'templates.findRules',
                    'text' : `Template malformed.\nCan't find the rule and content.\n${raw}` 
                } );
            }
        }
	}
	catch ( error ) {
        inspectError( {
            'data' : raw,
            'error' : error,
            'source' : 'templates.findRules',
            'text' : 'undefined error'
        } );
		return null;
	}
};
/* 	populateTemplate => public function triggers applyRules, getTemplateVariable
* @name     populateTemplate
* @param    Object definition 	- 
*       String content		 	-
*		String raw			 	- 
*		String rule			 	-
*		Array  templates	 	-
* @param	Object data 		-
*		Any any
* @returns	String HTML			-
*/
const populateTemplate = function populateTemplate( definition, data ) {
	try {
        let content;
		// 1) Check if we have rules to apply.
		const rules = applyRules.call( this, definition, data );
		// 2) Check if the rules all validate, returning an empty string otherwise.
		if ( !rules.every( rule => rule.valid ) ) content = '';
		// 3) Return the rule result if all rules are valid. This allows us to use multiple rules inside one rule definition. and hence mix if/foreach/component
        //      todo: we might be abel to just repalce this with alwasy unpacking the content of the rules array.
		else if ( definition.rule.includes( 'component' ) ) content = rules.find( rule => rule.type === 'component' ).content;
		else if ( definition.rule.includes( 'foreach' ) ) content = rules.find( rule => rule.type === 'foreach' ).content;
        else if ( definition.rule.includes( 'iterate' ) ) content = rules.find( rule => rule.type === 'iterate' ).content;
		// No rule
        // Using replace with a regex and a function will automatically loop over all the found regex results.
        // Equivalent with exec'ing the regex, replacing each variable as we match it, until no more matches are found.
        // Partial application of this, definition and data.
		else content = definition.content.replace( reTemplateVariable, getTemplateVariable.bind( this, definition, data ));
        return content;
	}
	catch( error ) {
        inspectError( {
            'data' : {
                'data' : data,
                'definition' : definition,
				'name' : this.name
            },
            'error' : error,
            'source' : 'populateTemplate',
            'text' : null
        } );
		return 'missing template';
	}
};
/* 	reTemplateVariable => private regex helps populateTemplate, ruleOperators.foreach
* @name	reTemplateVariable	- Regular expression to parse the contents between ${ and } tokens.
*/
const reTemplateVariable = /\$\{(.+?)\}/g;
/* 	getTemplateVariable => private function helps populateTemplate
* @name		getTemplateVariable
* @param	String match	- The full match of the regex.
* @param	String variable	- The actual content between ${ and }
* @param	Integer index	-
* @param    String string   - The full string we executed the regex on.
* @returns	Any any			- Template variables will return the populated content, data variables will return the value of the data expression.
*/
const getTemplateVariable = function getTemplateVariable( definition, data, match, variable, index, string ) {
    variable = variable.trim();
	// If our variable is an inner template, we can replace it with the return value of populating that inner template.
    if ( variable.includes( '_template_chunk_' ) ) return populateTemplate.apply( this, [ definition.templates[ variable.match( /\d+/ )[0] ], data ] );
    // Slightly more secure eval for calculated operations.
    else if ( variable.indexOf( 'exec(' ) !== -1 ) return variableTransforms.exec( variable, data );
    // Basic variable inside our data scope.
    else {
        // If we're inside a loop, there'll be a collection property.
        // Check if the loop variable is contained inside the variable we're trying to pathObj to.
        // If that loop variable is used inside the path we're following as a computed property name, we can replace that computed proeprty name with its actual value (name) so we get injection without the use of exec().
        if ( data && data.collection ) {
            // Inside loops, data will be { loopVariable, collection, source }
            const injection = Object.keys( data ).filter( key => key !== 'collection' && key !== 'source' )[ 0 ];
            // If the injection is the same as the path, we have a normal for loop and can just return the value.
            if ( injection !== variable ) {
                if ( variable.includes( `[${ injection }]` ) ) return pathObj( data, variable.replace( injection, data[ injection ] ) );
                else return pathObj( data, variable );
            }
            else return data[ variable ];
        }
        else return pathObj( data, variable );
    }
};
/*  variableTransforms => private object helps getTemplateVariable
* @name     variableOperators
* @param    String variable
* @returns  Any any
*/
const variableTransforms = {
    'exec' : function exec( variable, data ) {
        const sliced = variable.slice( 5, variable.length - 1 );
        // Do explicitly check for keys followed by . ( [ endOfInput or a space, else we can use keys that start with the same string as one of their properties.
        const referenced = Object.keys( data ).reduce( ( reference, name ) => reference.replace( new RegExp( `(^| )${name}(?=$|\\.|\\[|\\(| )`, 'g' ), `$1this.${name}` ), sliced );
        let executor;
        let output;
        try {
            executor = new Function( `return ${referenced};` );
            output = executor.call( data );
           // Changed to undefined so we can render null values without complicated template exec()
               // != null === ( !== null && !== undefined )
            if ( output !== undefined ) return output;
            else {
                inspectError( {
                    'data' : data,
                    'error' : new Error(),
                    'source' : 'variableTransforms.exec',
                    'text' : `no output:\n${variable}\n${executor.toString()}`
                } );
                return 'error';
            }
        }
        catch( error ) {
            inspectError( {
                'data' : data,
                'error' : error,
                'source' : 'variableTransforms.exec',
                'text' : `error:\n${variable}\n${executor.toString()}`
            } );
            return 'error';
        }
    }
};
/* 	applyRules => private function helps populateTemplate triggers ruleOperators.method
* @name    applyRules
* @param   Object definition
*       String content		 	-
*		String raw			 	- 
*		String rule			 	-
*		Array  templates	 	-
* @returns String html			- Populated inner template chunk.
*/
const applyRules = function applyRules( definition, data ) {
	return !definition.rule
		? []
		: definition
			.rule
			.split( ',' )
			.map( rule => {
                // for each rule, apply the correct transform
				const [ operator, operand, relation, ...expectation ] = rule.trim().split(' ');
				return ruleOperators[ operator ]( this, definition, data, operand, relation, expectation.join(' '));
			});
};
/* 	ruleOperators => private object helps applyRules
* @name     ruleOperators
* @param    Object instance     - The template instance (this) we're operating on. See the class definition for more info
*       String cache
*       Object definition
*               String content
*               String raw
*               String rule
*               Array templates
*       String method
*       String raw
*       Object scope
* @param    Object data         - Source data used in all rule operators. In components, passed as first argument to the registered component method.
* @param    String operand      - Name expressing the data we're applying the rule to. Usage differs.
*                                 component: Mostly unused. Passed as second argument to the registered component method.
*                                   foreach: Iteration name given to each data source we loop over: 'person' is the operand in the rule expression 'foreach person in department'.
*                                        if: Variable name that we want to compare: someFlag is the operand in the rule expression 'if someFlag = 15'.
* @param    String relation     - Defines the relation between the operand and the expectation. Usage differs.
*                                 component: Mostly unused. Passed as third argument to the registered component method.
*                                   foreach: 'in' is the default relation used by loops: for each operand INside expectation. Can be extended later to include 'of' or other syntax.
*                                        if: '=' or '!=' : Expressed equality or inequality.
* @param    String expectation  - Usage differs.
*                                 component: Mostly unused. Passed as fourth (last) argument to the registered component method.
*                                   foreach: The target of the loop: 'department' is the expectation in the rule expression 'foreach person in department'.
*                                        if: The expected result of the comparison. The rule will be valid, and hence will be applied, if the operand equals the expectation.
* @param    String content      - Everything inside the html tag using the rule. This seperates the templated tag from its content, making things like loops easier to implement.
*                                 Note that any inner templates will already be replaced by a string hook representing that template.
*                                 So any template inside this content can be handled like any other variable.
*/
const ruleOperators = {
    // Since we use arrow functions here, this = the whole module templates (with only its dependencies visible)
    // If the component is an (augmented inner) template comapred to a custom coponent, we need to register the scope and method onto the parent template.
	'component' : ( instance, definition, data, operand, relation, expectation ) => {
		// pathObj should return a function here
        // Update: since we defined the component rule as being <template data-tmpl="component">componentName</template>, the content property will only ever contain the full path we need to use.
        // const component = definition.content.match( /.+?\>(.+?)\</ )[1];
        const componentName = definition.content;
		const scope = pathObj( instance.scope, componentName );
		if ( scope && scope[ instance.method ] ) {
            // we inject data, operand, relation, expectation a aprameters into the component method.
            // It is up to the component itself to handle these.
            // Most components we have atm (2017) only require the data object.
            // The instance.scope will refer to a view or a namespace inside that view, so we can grab the component from anywhere inside the view.
            // The scope we want to apply the method on to, is the actual component scope though, not the view scope.
            // This preserves 'this' functionality. Might be able to be repalced by: `scope[ instance.method ]( data, operand, relation, expectation );`
            // But we kept the apply call to force the 'this; reference to always refer to the component.
            // Revalidate this later, we might ned the view scope as well for certain components. Find out how this affect component coupling.
			return {
				'content' : scope[ instance.method ].apply( scope, [ data, operand, relation, expectation ] ),
				'type' : 'component',
				'valid' : true
			};
		}
		else {
            let explanation = null;
            if ( instance.scope ) {
                if ( scope ) {
                    if ( scope[ componentName ] ) explanation = `method ${ instance.method } cannot be found on the component.`;
                    else explanation = 'component cannot be found in the scope.';
                }
                else explanation = 'the path to the component cannot be found inside the instance scope.';
            }
            else explanation = 'no scope object registered on the instance. Check the template object of this component and any templates this is part of.';
            inspectError( {
                'data' : { instance, data },
                'error' : new Error(),
                'source' : 'ruleOperators.component',
                'text' : `Failed rendering component ${ componentName }: ${ explanation }`
            } );
			return {
				'content' : 'missing component',
				'type' : 'component',
				'valid' : false
			};
		}
	},
	'foreach' : ( instance, definition, data, operand, relation, expectation ) => {
		const list = pathObj( data, expectation );
        if ( !list ) {
            inspectError({
                data,
                "error": new Error(),
                "source": "ruleOperators.foreach",
                "text": `No valid iteratable found: ${operand} ${relation} ${expectation}`
            });
            return {
                "content": "ERROR",
                "type": "foreach",
                "valid": false
            };
        }
        else {
            return {
                'content' : ( Array.isArray( list ) ? list : Object.entries( list ).map(([ key, value ]) => ({ "key": key, "value": value }))).map( function( item, index ) {
                    // We can bind the getTemplateVariable, so the bound version gets used to replace inside the string.
                    // The this reference gets bound to the current template instance, definition gets bound to the definition of that instance and the resolved operand is passed in as the new data source.
                    // The regex will add the rest of the parameters: match, variable, index, string.
                    // Match will be the result of the regex we captured between ()
                    // variable will be the string matched by the regex
                    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace#Specifying_a_function_as_a_parameter
                    // If we nest other rules inside a foreach rule, we can access the upper stages of context through the source and collection properties of the data object:
                    // Consider following structure:
                    // foreach b in a => sees the original data object containing 'a'
                    //     foreach c in b.x => sees b; sees collection (.index, .length )info about the array 'a' we're looping over; sees the original data (containing 'a' ) as source
                    //         if b.y then c.z => sees c; sees collection about 'b.x'; sees source containing collection info 'a',  sees 'b', sees source containing the original data
                    // So the correct version of the last rule should be 'if source.b.y then c.z'
                    // And a can be reached using source.source.a
                    return definition.content.replace( reTemplateVariable, getTemplateVariable.bind( instance, definition, { [operand] : item, collection : { index , length : list.length }, source : data } ) );
                }).join( '' ),
                'type' : 'foreach',
                'valid' : true
            };
        }
	},
	'if' : ( instance, definition, data, operand, relation, expectation ) => {
        // instance         => the template instance { _cache, definition, method, name, raw, scope, type }
        // definition       => the rule { content, raw, rule, templates }
        // data             => this instance { any } OR foreach loop { collection, [name], source }
        // operand          => variable name
        // relation         => exists, missing, = != 
        // expectation      => 
        // The operand will be expressing a boolean: truthiness or comparison of value
		const booleanMatch = expectation.match( /true|false/ );
        const comparator = (function( expectation ) {
            if ( booleanMatch && booleanMatch.length ) return JSON.parse( expectation );
            else if ( isNumber( expectation ) ) return parseFloat( expectation, 10 );
            else {
                const variable = pathObj( data, expectation );
                if ( variable !== undefined ) return variable;
                else return expectation;
            }
        }( expectation ));
		const reference = pathObj( data, operand );
		let valid = false;
		// if x = y
        // The only values that are == null are null and undefined.
        // so equivalent to valid === null || valid === undefined
        if ( relation === 'exists' ) valid = reference != null;
        else if ( relation === 'missing' ) valid = reference == null;
        else if ( relation === '=' ) valid = ( reference === comparator );
		// if x != y
		else if ( relation === '!=' ) valid = ( reference !== comparator );
		// if x
		else if ( relation === undefined && reference !== undefined ) valid = !!reference;
		else {
            inspectError( {
                'data' : data,
                'error' : new Error(),
                'source' : 'ruleOperators.if',
                'text' : `failed applying rule operator.
                         \"if ${operand} ${relation || ''} ${expectation || ''}\"` 
            } );
		}
        
        console.log( `if ${ operand } ${ relation } ${ expectation }` );
        console.log( `${ reference } ${ relation } ${ comparator } => ${ valid }` );
        
		return {
			'content' : null,
			'type' : 'if',
			'valid' : valid
		};
	},
    'iterate': ( instance, definition, data, operand, relation, expectation ) => {
        // instance = template instance
        // definition = the rule { content, raw, rule, templates }
        // data = this instance { any } OR foreach loop { collection, [name], source }
        // operand = the name of the variable inside data
        // relation = null
        //  expectation = null
        const length = pathObj( data, operand );
        const content = Array.from({ length }).map(( cell, index ) => {
            return definition.content.replace(
                reTemplateVariable,
                getTemplateVariable.bind( instance, definition, { [operand] : cell, collection : { index , length }, source : data })
            );
        }).join('');
        return {
            content,
            type: 'iterate',
            valid: true
        };
    }
};
/*  Class Template
*       String this.cache           - After populating the raw template, the result is saved in this property. Can be used to rerender without repopulating.
*       Object this.definition      - Contains the results of parseTemplate() onto the raw template string.
*           String content          - Contains the chunked template: All the instances of our template tag inside the raw template string are replaced by placeholder strings so we can do a simple string.replace on each inner template, if any.
*           String raw              - The raw (inner) template string. Where Class.template.raw contains the entire tempalte as read from the file, this property only contains a isolated (inner) template.
*           String rule             - The rule(s) associated with this template.
*           Array  templates        - Contains all inner tempaltes of this template.
*       String this.method          - After using register() method, this will contain the name of the method that components use to return their html string.
*       String this.raw             - Raw full template string as it was upon construction of this template instance.
*       Object this.scope           - Scope that will be used during the population method. Any fixed variables that are used inside a template, but aren't part of the dataset used, can be placed here.
*                                     EG, we use this to bind the view.components object to the template so any components used in the template will search inside this.scope for their data.
*                                     So after using template.register( view.components, 'render' ), any template that refers to a componentName, will use view.components[ componentName ][ method ]( data ) to grab the html.
*                                     By convention we use 'render' as the method that return populated HTML but this can be changed accordingly by using register( objRef, otherMethodName );
*                                     This also means we can use different methods for different templates.
*/
class Template {
	constructor( source ) {
		this._cache = null;
		this.definition = null;
		this.method = null;
        this.name = null;
		this.raw = null;
		this.scope = null;
        // Source contains the template. Check the cache for existance and onyl cache the tempalte if it does not exists yet.
        // Throw an error is we try to overwrite the template
        if ( isObj( source ) ) {
            if ( source.name && source.raw ) {
                if ( _CACHE[ source.name ] ) {
                    if ( _CACHE[ source.name ] !== source.raw ) throw new Error( `template ${ source.name } already exists: ${ _CACHE[ source.name ] }` );
                }
                else _CACHE[ source.name ] = source.raw.slice();
            }
            this.name = source.name;
            this.raw = _CACHE[ source.name ];
            this.definition = parseTemplate( this.raw );
        }
        // External template.
        else {
            // External template is already installed.
            if ( _CACHE.hasOwnProperty( source )) {
                this.name = source;
                this.raw = _CACHE[ source ];
                this.definition = parseTemplate( this.raw );
            }
            // External template should be fetched on init.
            else {
                this.name = source;
                this.raw = source;
                //INIT_CALLBACKS.push( this );
            }
        }
	}
    static inspectCache() {
        return _CACHE;
    }
    html( data ) {
        // By providing a method that returns the HTML or an empty string, we can use tempaltes as components by just registering the 'html' method of the inner component template into the template using the component.
        return data || !this._cache
            ? this.populate( data )._cache
            : this._cache || '';
    }
	populate( data ) {
		this._cache = populateTemplate.call( this, this.definition, data );
		return this;
	}
    // We need to register a scope and a method to support scope injection into components we want to use inside the template .
	register( scope, method ) {
		this.scope = scope;
		this.method = method || null;
		return this;
	}
	render( node ) {
		const target = document.querySelector( node )
        if ( !target ) inspectError( 'templates.render', `Node not found: ${node}` );
        else if ( !this._cache ) inspectError( 'templates.render', `template ${ this.name } has not been populated yet.` );
		else target.innerHTML = this._cache;
		return this;
	}
}
/*
*/
Template.prototype.type = Object.freeze( 'template' );
////////////////////
// Added the 'type' property to the class export so other modules can detect this wrapper as the templates engine .
// Since we export more than just the Template class, we don't have access to an instanced tempaltes' prototypal 'type' property .
//export { 'cache' : _CACHE, 'init' : INIT, Template, 'type' : 'templates' };
export default Template;