/*  component_svg.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-08-28
*/
/*  API
*       The diea is that we only want to import d3.min.js once and provide a namespace to add additional shared SVG options to.
*/
/*  TODO
*
*/
//  DEPENDENCIES
////    IMPORT
import Component from './component.js';
////    Helpers
//  COMPONENTS
class Component_svg extends Component {
    constructor({ config, name, root, state, template } = {}) {
        super({ config, name, root, state, template });
        this.svg_node = null;
    }
}
//  Including d3 with require() crashes the IE11 dev tools.
//  So we're forced to include it as a global and then reference that global here.
Component.prototype.d3 = d3;
Component_svg.prototype.module_type = 'component_svg';
//
export default Component_svg;