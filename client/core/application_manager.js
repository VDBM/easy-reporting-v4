/*  application_manager.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-10
*/
/*  API
*
*       This is basically a reimplementation of the application class with some changes:
*           naming scheme more suited to managing applications instead of modules.
*           integrated mediator
*           integrated router
*           .init() method steering the installed applications
*           no .on() method as this is not an extension of the mediator class.
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Mediator from './mediator.js';
import Router from './router.js';
////    Helpers
////    Data
//  COMPONENTS
////    Application_manager
class Application_manager {
    constructor( name ) {
        this.active_app = null;
        this.applications = {};
        this.id = `${ this.module_type }_${ name }`;
        this.mediator = new Mediator();
        this.router = new Router( path => this.hashchange( path ));
    }
    hashchange( path ) {
        // The very first namespace in the path is the application.
        const app_from_path = `application_${ path.slice( 2 ).split( '/' )[ 0 ] }`;
        const change_active_app = this.active_app && this.active_app.id !== app_from_path;
        //  Remove the active app the new path points to a different app.
        //  Use the true flag so all static content gets removed.
        if ( change_active_app ) this.active_app.remove({ remove_statics: true });
        //  We always have to update the active app if either no app is active yet, or the apps aren't the same.
        if ( !this.active_app || change_active_app ) this.active_app = Object.values( this.applications ).find( application => application.id === app_from_path );
        //  Always trigger the hashchange event so any handlers registered by the actvie app get triggered.
        this.mediator.trigger( 'hashchange', path );
    }
    init() {
        //  Init the router first so our page gets a hashchange handler. If the rotuer picks up extra init() funcionality, we might have to move this call.
        this.router.init();
        return Promise
            //  Init all the installed applications
            .all( Object.values( this.applications ).map( application => application.init()))
            //  Each application will return its list of application-level events, its installed submodules' and components' init() results and all the routes.
            //  Register all these callbacks.
            .then( app_initializations => {
                app_initializations.forEach(({ events, modules, routes }) => {
                    events.forEach(({ event_name, options, handler }) => this.mediator.on( event_name, options, handler ));
                    // modules.forEach();
                    routes.forEach(({ event_name, options, handler }) => this.router.on( event_name, options, handler ));
                });
            })
            //  Trigger our own init event so any application listening to the app init will get triggered.
            .then(() => this.mediator.trigger( 'init' ))
            //  Check the router for any initial hashes.
            .then(() => this.router.checkHash());
    }
    install( module ) {
        if ( !module.name ) throw new Error( 'Cannot install a module without a name.' );
        this.applications[ module.name ] = module;
        return this;
    }
    toString() {
        return this.id;
    }
}
//  WORKFLOW
Application_manager.prototype.module_type = 'application_manager';
////    Routes
////    Application Events
////    Delegated Events
//  EXPORT
export default Application_manager;
