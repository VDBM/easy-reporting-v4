
Notes:

1)
Changed the event handler of button components to a function triggering their state instead of binding that function directly.
This allows us to update the state of the buttons to also update the event that will get triggered by those buttons.
Combined with using the relative references to nodes we are trying to create inside the collection_nodes,
this allows us to fully update the rendering of components and the triggering of events at runtime, while still allowing us to keep defining initial state upion component creation.
In the previous version we had to change the whole root node structure used inside our components to accomodate for nodes that haven't been rendered yet.
By changing to relative reference, we avoid this.

2)
Changed component update function to always rerender unless a render flag set to false is passed into the function.





Standardized:

core/collection_nodes.js
reports/admin.js


Todo:

1)
Evaluate the use of the collection_nodes.js class when passing around node references to render to.

2)
Change the util.DOM_EVENTS code and the component.js code so it no longer relies on the inner structure of the component definition.
Any component implementation details should stay out of the util namespace.
So no more use of this or isntacne inside those functions, the component itsself should pass all the correct aprameters based on its own state.
This will make the DOM_EVENTS function reusable as well.



--------------------------
Button test:

setup:      One button, two event handlers.
            The first handler will replace the button state with the second handler.
            The second event handler will remove the button, update the root node, rerender the button to the new root node, replace the button state with the first handler again and finally update the root node back to the original root node.
        
result:     The first click succeeds in updating the root node. This triggers a rerender due to update being called when rendered.
            First issue is that we render too many times due to us calling render directly.
            The second click succeeds in updating
            The 3rd click will then crash because the rendered button is no longer found in the DOM_EVENTS
        
resolution: 1)  Removed all the direct triggers of render so we can completely rely on the update function triggering the render.
                This should remove all the unneeded renders and keep the nodes consistent.
            2)  After updating the logic used inside the component.update() method, we were able to get the exact result without unneeded renders by using the following structure:
                component.remove() => nodeClear( component.root() ) => root_nodes.update( new root ) => component.update( new state );
                
lessons:    We need to call remove() on a component before we change its event handlers or its root. Else the is_rendered flag will still be true and we will not clear the new root. This also means we will not rerender automatically
            How much can we rely on the nodeClear being triggered when the component has not been rendered yet?
            Do we need a flag to use with the remove() method to clear the node or can we continue doing this manually?
        