// Don't forget that the package needs to mention the babel compile version!
// Wrap our CommonJS modules into an AMD-style declaration so we can use the basic require.js library by leveraging the gulp-header and gulp-footer plugins.
// TODO: refactor the tempaltes and other resoruces files into its own resoruces call that will get called by every app . Add the grid css and such to it as well .
const babelify = require( 'babelify' );
const browserify = require( 'browserify' );
const buffer = require( 'vinyl-buffer' );
const concat = require( 'gulp-concat' );
const gulp = require( 'gulp' );
const rename = require( 'gulp-rename' );
const source = require( 'vinyl-source-stream' );
const sourcemaps = require( 'gulp-sourcemaps' );
gulp.task( 'client_css', () => {
    gulp
        //  Force the global.css file to be written at the top of the concatenated file, followed by all other files ordered by name
        .src([ 'css/global.css', 'css/*.css' ])
        .pipe( concat( 'client.min.css' ))
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/client' ));
});
gulp.task( 'client_html', () => {
    gulp
        .src( 'html/client.html' )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/client' ));
    gulp
        .src([ '!html/client.html', 'html/*.html' ])
        //.pipe( concat( 'templates.min.html' ))
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/client' ));
});
gulp.task( 'client_js', () => {
    browserify( 'client.js' )
        .transform( babelify, { 'presets' : [ 'es2015' ]})
        .bundle()
        .pipe( source( 'client.js' ) )
        .pipe( buffer() )
        .pipe( rename( 'client.min.js' ) )
        //.pipe( sourcemaps.init( { 'loadmaps' : true } ))
        //.pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/client' ));
});
gulp.task( 'client_resources', () => {
    gulp
        .src([ '!resources/d3.min.js', 'resources/*.*' ])
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/client' ));
    gulp
        .src([ 'resources/d3.min.js' ])
        .pipe( gulp.dest( 'C:/inetpub/wwwroot/resources' ));
        
});
gulp.task( 'client', [ 'client_css', 'client_html', 'client_js', 'client_resources' ], () => {} );