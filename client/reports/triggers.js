/*  triggers.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_triggers' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    triggers
const triggers = new Application({
    name: 'triggers',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
triggers
////    Routes
    .on( '/triggers', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = '<a href="#/triggers/packing">Triggers - Packing</a>';
    })
    .on( '/triggers/:organisation', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = `${ triggers.id } => organisation: ${ parameters.organisation }<br><br><a href="#/triggers">Triggers - MAIN</a>`;
    });
////    Application events
////    Delegated events
//  EXPORT
export default triggers;