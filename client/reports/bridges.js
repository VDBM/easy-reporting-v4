/*  bridges.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_bridges' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    bridges
const bridges = new Application({
    name: 'bridges',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
bridges
////    Routes
    .on( '/bridges', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = bridges.id;
    });
////    Application events
////    Delegated events
//  EXPORT
export default bridges;