/*  admin.mjs
*   @language:      ES6
*   @version:       v1.0.2
*   @user:          VDBM
*   @date:          2018-09-13
*/
/*  API
*
*
*
*/
/*  TODO
*
*       1) Create a grid module that can render grids of arbitrary size so we can replace the hook_list module.
*       2) Check if we can move the parts of the hashchange handler concerning the admin_navgiation into the /admin route. Base issue is getting access to the current path without contacting the router or window directly.
*       OK => fixed grid 3) The downtimebar label positioning does not work correctly anymore. Our time labels udnerneath it are not in position anymore.
*       OK => changed getUTCHours() into getHours() 4) The downtimebar is not adjusted yet to using UTC yet.
*       OK => readded 5) We have to finish up to grid_30 to make the masonry component and the downtimebar component render correctly again. Atm we're missing some grid classes.
*       OK => changed the logic we used and removed init() call away from the popover 6) Find a better way to inject a component into the popover. Where do we want to call the init() of that component?
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Button from '../components/button.js';
import Collection_nodes from '../core/collection_nodes.js';
import Component from '../core/component.js';
import Datatable from '../components/datatable.js';
import Datatable_sort from '../components/datatable_sort.js';
import Datatable_edit from '../components/datatable_edit.js';
import Downtimebar from '../components/downtimebar.js';
import Dropdown_linked from '../components/dropdown_linked.js';
import { fetch, root_services, root_resources } from '../core/fetch.js';
import Gauge from '../components/gauge.js';
import Hook_list from '../components/hook_list.js';
import Input_datetime from '../components/input_datetime';
import Input_number from '../components/input_number';
import Input_pageclick from '../components/input_pageclick';
import Input_string from '../components/input_string';
import Marquee from '../components/marquee.js';
import Masonry from '../components/masonry.js';
import Menu_links from '../components/menu_links.js';
import Percentagebar_horizontal from '../components/percentagebar_horizontal.js';
import Percentagebar_vertical from '../components/percentagebar_vertical.js';
import Popover from '../components/popover.js';
import Resource from '../core/resource.js';
import { cloneJSON, log_error, nodeClear, nodeParent, propHash, propSort } from '../core/util.js';
////    Data
const data = {
    "datatable": [
        { "id": 1337,   "date": new Date( '2018-03-05T14:15:12.000Z' ),     "value": "lorem"            },
        { "id": 42,     "date": new Date( '2018-04-01T17:15:12.000Z' ),     "value": "ipsum"            },
        { "id": 7,      "date": new Date( '2018-05-21T19:15:12.000Z' ),     "value": "dolores"          },
        { "id": 13,     "date": new Date( '2018-06-23T11:15:12.000Z' ),     "value": "magnum"           },
        { "id": 666,    "date": new Date( '2018-02-17T12:15:12.000Z' ),     "value": "misericordiam"    }
    ],    
    "navigation_links": [
        { "label": "API", "uri": "#/admin/api" },
        { "label": "Buttons", "uri": "#/admin/buttons" },
        { "label": "Datatables", "uri": "#/admin/datatables" },
        { "label": "Downtimebar", "uri": "#/admin/downtimebar" },
        { "label": "Dropdowns", "uri": "#/admin/dropdowns" },
        { "label": "Fieldset", "uri": "#/admin/fieldset" },
        { "label": "Gauge", "uri": "#/admin/gauge" },
        { "label": "Inputs", "uri": "#/admin/inputs" },
        { "label": "Marquee", "uri": "#/admin/marquee" },
        { "label": "Masonry", "uri": "#/admin/masonry" },
        { "label": "Percentagebar", "uri": "#/admin/percentagebar/vertical" },
        { "label": "Popover", "uri": "#/admin/popover" },
        { "label": "Progressbar", "uri": "#/admin/percentagebar/horizontal" }
    ]
};
////    Helpers
const create_api_view = modules => {
    const modules_hash = modules.reduce( propHash( 'name', true ), {});
    const find_recursive = ( property, module ) => module.extending
        ? module[ property ].concat( find_recursive( property, modules_hash[ module.extending ]))
        : module[ property ];
    return modules
        .filter( module => module.extending !== '!!!' )
        .map( module => {
            const creator_parameters = module
                .constructor
                .map( parameter => Array.isArray( parameter.type ) ? parameter.type.join( " or " ) : parameter.type )
                .join( ', ' );
            const view = {
                header: module.extending
                    ? `<span class="bold">${ module.name }</span> extends <span class="bold">${ module.extending }</span>`
                    : `<span class="bold">${ module.name }</span>`,
                creator: `new ${ module.name.charAt(0).toUpperCase() }${ module.name.substring( 1,  module.name.length - 3 ) }( ${ creator_parameters } )`,
                methods: find_recursive( 'methods', module )
                    .sort( propSort( 'lexical', 'name' )),
                properties: find_recursive( 'properties', module )
                    .sort( propSort( 'lexical', 'name' ))
            };
            return Object.assign( module, { view });
        });
};
//  COMPONENTS
////    collection_nodes_root. Both of the updates are located in the main client HTML and hence, are available immediately.
const node_manager = new Collection_nodes( 'app_admin' );
////    api_content
//
const api_config = new Resource({
    manager: 'client',
    name: 'api',
    type: 'json'
});
//
const api_content = new Component({
    config: {
        is_ready: false
    },
    name: 'api',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: new Resource({
        manager: 'client',
        name: 'api',
        type: 'html'
    })
});
////    button_content
const hook_list_buttons = new Hook_list({
    config: {
        amount: 6,
        hook: 'buttons'
    },
    name: 'buttons',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    button_blue_l
const button_blue_l = new Button({
    config: {
        color: 'blue',
        label: 'Large',
        size: 'large'
    },
    name: 'blue_l',
    root: node_manager.get( 'hook_buttons_1' ),
    state: () => button_blue_l.autorun(),
    template: undefined
});
////    button_blue_m
const button_blue_m = new Button({
    config: {
        color: 'blue',
        label: 'Medium',
        size: 'medium'
    },
    name: 'blue_m',
    root: node_manager.get( 'hook_buttons_2' ),
    state: () => button_blue_m.autorun(),
    template: undefined
});
////    button_blue_s
const button_blue_s = new Button({
    config: {
        color: 'blue',
        label: 'Small',
        size: 'small'
    },
    name: 'blue_s',
    root: node_manager.get( 'hook_buttons_3' ),
    state: () => button_blue_s.autorun(),
    template: undefined
});
////    button_orange_l
const button_orange_l = new Button({
    config: {
        color: 'orange',
        label: 'Large',
        size: 'large'
    },
    name: 'orange_l',
    root: node_manager.get( 'hook_buttons_4' ),
    state: () => button_orange_l.autorun(),
    template: undefined
});
////    button_orange_m
const button_orange_m = new Button({
    config: {
        color: 'orange',
        label: 'Medium',
        size: 'medium'
    },
    name: 'orange_m',
    root: node_manager.get( 'hook_buttons_5' ),
    state: () => button_orange_m.autorun(),
    template: undefined
});
////    button_orange_s
const button_orange_s = new Button({
    config: {
        color: 'orange',
        label: 'Small',
        size: 'small'
    },
    name: 'orange_s',
    root: node_manager.get( 'hook_buttons_6' ),
    state: () => button_orange_s.autorun(),
    template: undefined
});
////    hook_list_datatables
const hook_list_datatables = new Hook_list({
    config: {
        amount: 3,
        hook: 'datatables'
    },
    name: 'datatables',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    datatable_basic
const datatable_basic = new Datatable({
    config: undefined,
    name: 'basic',
    root: node_manager.get( 'hook_datatables_1' ),
    state: undefined,
    template: undefined
});
////    datatable_sortable
const datatable_sort_sortable = new Datatable_sort({
    config: undefined,
    name: 'sortable',
    root: node_manager.get( 'hook_datatables_2' ),
    state: undefined,
    template: undefined
});
////    datatable_editable
const datatable_edit_editable = new Datatable_edit({
    config: undefined,
    name: 'editable',
    root: node_manager.get( 'hook_datatables_3' ),
    state: undefined,
    template: undefined
});
////    hook_list-dtbar
const hook_list_dtbar = new Hook_list({
    config: {
        amount: 1,
        hook: 'dtbar'
    },
    name: 'dtbar',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    downtimebar_admin
const downtimebar_admin = new Downtimebar({
    config: undefined,
    name: 'admin',
    root: node_manager.get( 'hook_dtbar_1' ),
    state: undefined,
    template: undefined
});
////    places_mechelen_config
const places_mechelen_config = new Resource({
    manager: 'client',
    name: 'places_mechelen',
    type: 'json'
});
////    places_mechelen_hash
const places_mechelen_hash = new Resource({
    manager: 'client',
    name: 'places_mechelen_hash',
    type: 'json'
});
////    dropdown_linked_places
const dropdown_linked_places = new Dropdown_linked({
    config: undefined,
    name: 'places',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////     fieldset
//  TODO
////    guage_admin
const gauge_admin = new Gauge({
    config: {
        label: 'Pringles',
        max: 100,
        precision: 0
    },
    name: 'admin',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    hook_list_inputs
const hook_list_inputs = new Hook_list({
    config: {
        amount: 3,
        hook: 'inputs'
    },
    name: 'inputs',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    input_datetime_admin
const input_datetime_admin = new Input_datetime({
    config: undefined,
    name: 'admin',
    root: node_manager.get( 'hook_inputs_1' ),
    state: new Date().toJSON(),
    template: undefined
});
////    input_number_admin
const input_number_admin = new Input_number({
    config: undefined,
    name: 'admin',
    root: node_manager.get( 'hook_inputs_2' ),
    state: 1337,
    template: undefined
});
////    input_string_admin
const input_string_admin = new Input_string({
    config: undefined,
    name: 'admin',
    root: node_manager.get( 'hook_inputs_3' ),
    state: 'Easy Reporting',
    template: undefined
});
////    marquee_admin
const marquee_admin = new Marquee({
    config: undefined,
    name: 'admin',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    masonry_admin
const masonry_admin = new Masonry({
    config: undefined,
    name: 'admin',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    hook_list_percentagebars
const hook_list_percentagebars = new Hook_list({
    config: {
        amount: 5,
        hook: 'percentagebars'
    },
    name: 'percentagebars',
    root: node_manager.get( 'admin_content' ),
    state: undefined,
    template: undefined
});
////    percentagebar_primary
const percentagebar_primary = new Percentagebar_vertical({ 
    config: undefined,
    name: 'primary',
    root: node_manager.get( 'hook_percentagebars_1' ),
    state: undefined,
    template: undefined
});
////    percentagebar_secondary
const percentagebar_secondary = new Percentagebar_vertical({
    config: undefined,
    name: 'secondary',
    root: node_manager.get( 'hook_percentagebars_2' ),
    state: undefined,
    template: undefined
});
////    percentagebar_tertiary
const percentagebar_tertiary = new Percentagebar_vertical({
    config: undefined,
    name: 'tertiary',
    root: node_manager.get( 'hook_percentagebars_3' ),
    state: undefined,
    template: undefined
});
////    percentagebar_quaternary
const percentagebar_quaternary = new Percentagebar_vertical({
    config: undefined,
    name: 'quaternary',
    root: node_manager.get( 'hook_percentagebars_4' ),
    state: undefined,
    template: undefined
});
////    percentagebar_quinary
const percentagebar_quinary = new Percentagebar_vertical({
    config: undefined,
    name: 'quinary',
    root: node_manager.get( 'hook_percentagebars_5' ),
    state: undefined,
    template: undefined
});
////    progressbar_admin
const progressbar = new Percentagebar_horizontal({
    config: 100,
    name: 'progressbar',
    root: node_manager.get( 'admin_content' ),
    state: 0,
    template: undefined
});
/*//    popover_admin
*
*           The render event listeners ahve to be inthe exact order, sicne that's the order they'll get executed in. Eg, dont show the state before updating the root that state has to render to.
*
*/
const popover_admin = new Popover({
    config: undefined,
    name: 'admin',
    root: undefined,
    state: undefined,
    template: undefined
});
////    
const popover_admin_content = new Component({
    config: undefined,
    name: 'popover_admin_content',
    root: node_manager.get( 'hook_popover_content' ),
    state: 'Easy Reporting @ Kellogg Mechelen!',
    template: '<h1>${ state }</h1>'
});
////    navigation_admin
const navigation_admin = new Menu_links({
    config: cloneJSON( data.navigation_links ),
    name: 'admin',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: undefined
});
navigation_admin.is_static = true;
////    navigation_autorun
const navigation_autorun = new Button({
    config: {
        color: 'orange',
        label: 'Run',
        size: 'small'
    },
    name: 'navigation_autorun',
    root: node_manager.get( 'navigation_autorun' ),
    state: event => {
        admin.state.autorun = true;
        navigation_admin.autorun( 6500 );
    },
    template: undefined
});
navigation_autorun.is_static = true;
////    navigation_autostop
const navigation_autostop = new Button({
    config: {
        color: 'orange',
        label: 'Stop',
        size: 'small'
    },
    name: 'navigation_autostop',
    root: node_manager.get( 'navigation_autostop' ),
    state: event => {
        admin.state.autorun = false;
        navigation_admin.autostop();
    },
    template: undefined
});
navigation_autostop.is_static = true;
//  APPLICATION
const admin = new Application({
    name: 'admin',
    modules: [
        node_manager,
        api_config,
        api_content,
        hook_list_buttons,
        button_blue_l,
        button_blue_m,
        button_blue_s,
        button_orange_l,
        button_orange_m,
        button_orange_s,
        hook_list_datatables,
        datatable_basic,
        datatable_sort_sortable,
        datatable_edit_editable,
        hook_list_dtbar,
        downtimebar_admin,
        places_mechelen_config,
        places_mechelen_hash,
        dropdown_linked_places,
        gauge_admin,
        hook_list_inputs,
        input_datetime_admin,
        input_number_admin,
        input_string_admin,
        marquee_admin,
        masonry_admin,
        hook_list_percentagebars,
        percentagebar_primary,
        percentagebar_secondary,
        percentagebar_tertiary,
        percentagebar_quaternary,
        percentagebar_quinary,
        progressbar,
        popover_admin,
        popover_admin_content,
        navigation_admin,
        navigation_autorun,
        navigation_autostop
    ]
});
admin.state.autorun = false;
//  WORKFLOW
////    Initial nodes update
node_manager
    .update( 'app_main' )
    .update( 'app_footer' );
////        
api_content.on( 'click', { selector: '#api_content > ul > li' }, event => {
    nodeParent( event.target, 'li' ).classList.toggle( 'active' ); 
})
////
popover_admin.on( 'render', () => {
    nodeClear( node_manager.get( 'admin_content' )() );
    node_manager.update( 'hook_popover_content' );
    popover_admin.show();
});
popover_admin.on( 'confirmed', () => {
    node_manager.get( 'admin_content' )().innerHTML = '<div style="background-color: green;">User confirmed the action.</div>';
});
popover_admin.on( 'cancelled', () => {
    node_manager.get( 'admin_content' )().innerHTML = '<div style="background-color: red; color: white;">User cancelled the action.</div>';
});
////    Routes
admin
    .on( '/admin', ( state, parameters ) => {
        if ( !navigation_admin.is_rendered ) {
            const main = node_manager.get( 'app_main' )();
            main.classList.remove( 'b' );
            main.classList.add( 'grid-specific' );
            //  Render the navigation and inject two hooks for the buttons.
            navigation_admin
                .render()
                .inject( 'navigation_autorun' )
                .inject( 'navigation_autostop' );
            //  Register the nodes representing the two injected hooks and the content atatched to the menu.
            node_manager
                .update( 'admin_content' )
                .update( 'navigation_autorun' )
                .update( 'navigation_autostop' );
            //  Render the buttons to those hooks. Their root was already defined to refer to those hooks.
            navigation_autorun.render();
            navigation_autostop.render();
        }
    })
    .on( '/admin/autorun', ( state, parameters ) => {
        admin.state.autorun = true;
        navigation_admin.autorun( 6500 );
    })
    .on( '/admin/api', ( state, parameters ) => {
        api_config
            .get()
            .then( config => api_content.update({ state: create_api_view( config ) }));
    })
    .on( '/admin/buttons', ( state, parameters ) => {
        //  Render the button wrapper template.
        hook_list_buttons.render();
        //  Update all the hooks now that they are rendered.
        const hooks = [
            'hook_buttons_1',
            'hook_buttons_2',
            'hook_buttons_3',
            'hook_buttons_4',
            'hook_buttons_5',
            'hook_buttons_6',
        ];
        hooks.forEach( hook => node_manager.update( hook ));
        //  Render all the buttons, activating autorun if the app state has it set to true.
        const buttons = [
            button_blue_l,
            button_blue_m,
            button_blue_s,
            button_orange_l,
            button_orange_m,
            button_orange_s
        ];
        buttons.forEach( button_component => {
            button_component.render();
            if ( state.autorun ) button_component.autorun();
        });
    })
    .on( '/admin/datatables', ( state, parameters ) => {
        hook_list_datatables.render();
        node_manager
            .update( 'hook_datatables_1' )
            .update( 'hook_datatables_2' )
            .update( 'hook_datatables_3' );
        //  Updating the state of the datatable will automatically render it.
        datatable_basic.update({ state: data.datatable });
        datatable_sort_sortable.update({ state: data.datatable });
        datatable_edit_editable.update({ state: data.datatable });
    })
    .on( '/admin/downtimebar', ( state, parameters ) => {
        //  The downtimebar needs to be inside a different container to correctly calculate its width and positioning.
        hook_list_dtbar.render();
        node_manager.update( 'hook_dtbar_1' );
        // Since the update function will calculate the correct state, we need to actually call that instead of render until we fix its structure.
        downtimebar_admin.update();
    })
    .on( '/admin/dropdowns', ( state, parameters ) => {
        places_mechelen_config
            .get()
            .then( collection => {
                const mechelen = collection.find( plant => plant.name === 'mechelen' );
                const places = mechelen.data.find( config => config.name === 'places' );
                return dropdown_linked_places.update({ render: false, state: places });
            })
            .then( dropdown => dropdown.render());
    })
    .on( '/admin/fieldset', ( state, parameters ) => {
        node_manager.get( 'admin_content' )().innerHTML = '<h3 class="bold">The fieldset module is not yet completed.</h3>';
    })
    .on( '/admin/gauge', ( state, parameters ) => {
        gauge_admin
            .render()
            .autorun();
    })
    .on( '/admin/inputs', ( state, parameters ) => {
        hook_list_inputs.render();
        node_manager
            .update( 'hook_inputs_1' )
            .update( 'hook_inputs_2' )
            .update( 'hook_inputs_3' );
        input_datetime_admin.render();
        input_number_admin.render();
        input_string_admin.render();
    })
    .on( '/admin/marquee', ( state, parameters ) => {
        marquee_admin.update({ state: 'Easy Reporting @ Kellogg Mechelen!' });
    })
    .on( '/admin/masonry', ( state, parameters ) => {
        masonry_admin.render();
    })
    .on( '/admin/percentagebar/horizontal', ( state, parameters ) => {
        //  Render the bar and let it fill to 100% automatically in 5000 miliseconds.
        progressbar
            .render()
            .autorun( 5000 );
    })
    .on( '/admin/percentagebar/vertical', ( state, parameters ) => {
        hook_list_percentagebars.render();
        node_manager
            .update( 'hook_percentagebars_1' )
            .update( 'hook_percentagebars_2' )
            .update( 'hook_percentagebars_3' )
            .update( 'hook_percentagebars_4' )
            .update( 'hook_percentagebars_5' );
        const bars = [
            percentagebar_primary,
            percentagebar_secondary,
            percentagebar_tertiary,
            percentagebar_quaternary,
            percentagebar_quinary
        ];
        bars.forEach( bar => bar.update({ state: 0 }).autorun() );
    })
    .on( '/admin/popover', ( state, parameters ) => {
        //  Updating will render, render will trigger the node_manager update, render will trigger show() .
        node_manager.update( 'menu_links_admin' );
        popover_admin.update({ state: popover_admin_content });
        if ( state.autorun ) popover_admin.autorun();
    });
////    Application Events
admin
    .on( 'hashchange', ( state, path ) => {
        //  Hashchange should update the menu state, so that refreshing or pasting a link will also highlight the correct menu item as if it was clicked.
        navigation_admin.state.uri = path;
        //  Shoudl the following be moved inside the /admin route? How wil we inject the path then? App state? Add path to route parameters? Direct link to the app_manager?
        if ( path === '#/admin' && navigation_admin.is_rendered ) {
            nodeClear( node_manager.get( 'admin_content' )() );
            navigation_admin.highlight( null );
        }
    });
////    Delegated Events
admin
    .on( 'datatable_edit_editable.edit', ( state, event, input_component, id, property ) => {        
        // The edit event is defined by this component and implemented as this.trigger( 'edit', event, input, id, property )); to pass the values.
        // TODO : WHy does this ahve to be a delegated event again?
        const updated_datatable = data.datatable.map( entry => {
            if ( entry.id === id ) return Object.assign( entry, {[ property ]: input_component.state });
            else return entry;
        });
        data.datatable = updated_datatable;
        // TODO: this should be changed to posting to the database and either render first, then reconcile the two records after, or post first and render in the callback.
        datatable_basic.update({ state: updated_datatable });
        datatable_sortable.update({ state: updated_datatable });
        datatable_editable.update({ state: updated_datatable });
    });
//  EXPORT
export default admin;