/*  logbooks.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Button from '../components/button.js';
import Button_link from '../components/button_link.js';
import Component from '../core/component.js';
import Collection_nodes from '../core/collection_nodes.js';
import Resource from'../core/resource.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_logbooks' );
////    buttton_back
const button_main = new Button_link({
    config: {
        color: 'orange',
        label: '&#8678; Back',
        size: 'large',
        uri: '#/'
    },
    name: 'button_main',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: undefined
});
////    hook_list_logbooks
const link_list_logbooks = new Component({
    config: undefined,
    name: 'link_list_logbooks',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: [
        '<ul class="bor bor-round list-clean list-inline mar-8 w-100">',
            '<li id="hook_button_main"></li>',
            '<li id="hook_logbook_link_eni"></li>',
            '<li id="hook_logbook_link_packing"></li>',
            '<li id="hook_logbook_link_process"></li>',
            '<li id="hook_logbook_link_teamleaders"></li>',
        '</ul>'
    ].join( '' )
});
////    button_selection_eni
const button_selection_eni = new Button_link({
    config: {
        color: 'orange',
        label: 'E&amp;I',
        size: 'large',
        uri: '#/logbooks/eni'
    },
    name: 'selection_eni',
    root: node_manager.get( 'hook_logbook_link_eni' ),
    state: undefined,
    template: undefined
});
////    button_selection_packing
const button_selection_packing = new Button_link({
    config: {
        color: 'orange',
        label: 'Packing',
        size: 'large',
        uri: '#/logbooks/packing'
    },
    name: 'selection_packing',
    root: node_manager.get( 'hook_logbook_link_packing' ),
    state: undefined,
    template: undefined
});
////    button_selection_process
const button_selection_process = new Button_link({
    config: {
        color: 'orange',
        label: 'Process',
        size: 'large',
        uri: '#/logbooks/process'
    },
    name: 'selection_process',
    root: node_manager.get( 'hook_logbook_link_process' ),
    state: undefined,
    template: undefined
});
////    button_selection_tl
const button_selection_teamleaders = new Button_link({
    config: {
        color: 'orange',
        label: 'Team Leaders',
        size: 'large',
        uri: '#/logbooks/teamleaders'
    },
    name: 'selection_teamleaders',
    root: node_manager.get( 'hook_logbook_link_teamleaders' ),
    state: undefined,
    template: undefined
});
////    hook_list_paking
const link_list_packing = new Component({
    config: undefined,
    name: 'link_list_packing',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: [
        '<ul class="bor bor-round list-clean list-inline mar-8 w-100">',
            '<li id="hook_button_organisations"></li>',
            '<li id="hook_logbook_link_dm"></li>',
            '<li id="hook_logbook_link_gs"></li>',
            '<li id="hook_logbook_link_cf"></li>',
            '<li id="hook_logbook_link_cp"></li>',
        '</ul>'
    ].join( '' )
});
////
const button_organisations = new Button_link({
    config: {
        color: 'orange',
        label: '&#8678; Organisations',
        size: 'large',
        uri: '#/logbooks'
    },
    name: 'button_logbooks_organisations',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: undefined
});
////    button_selection_cf
const button_selection_cf = new Button_link({
    config: {
        color: 'orange',
        label: 'Can Filler',
        size: 'large',
        uri: '#/logbooks/packing/cf'
    },
    name: 'selection_cf',
    root: node_manager.get( 'hook_logbook_link_cf' ),
    state: undefined,
    template: undefined
});
////    button_selection_cp
const button_selection_cp = new Button_link({
    config: {
        color: 'orange',
        label: 'Case Packer',
        size: 'large',
        uri: '#/logbooks/packing/cp'
    },
    name: 'selection_cp',
    root: node_manager.get( 'hook_logbook_link_cp' ),
    state: undefined,
    template: undefined
});
////    button_selection_dm
const button_selection_dm = new Button_link({
    config: {
        color: 'orange',
        label: 'Doughmaking',
        size: 'large',
        uri: '#/logbooks/packing/dm'
    },
    name: 'selection_dm',
    root: node_manager.get( 'hook_logbook_link_dm' ),
    state: undefined,
    template: undefined
});
////    button_selection_gs
const button_selection_gs = new Button_link({
    config: {
        color: 'orange',
        label: 'Gasser Seamer',
        size: 'large',
        uri: '#/logbooks/packing/gs'
    },
    name: 'selection_gs',
    root: node_manager.get( 'hook_logbook_link_gs' ),
    state: undefined,
    template: undefined
});
////    button_packing
const button_packing = new Button_link({
    config: {
        color: 'orange',
        label: '&#8678; Packing',
        size: 'large',
        uri: '#/logbooks/packing'
    },
    name: 'button_packing',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: undefined
});
////    logbook_canfiller
const logbook_canfiller = new Component({
    config: undefined,
    name: 'logbook_canfiller',
    root: node_manager.get( 'app_main' ),
    state: undefined,
    template: new Resource({
        manager: 'client',
        name: 'logbook_canfiller',
        type: 'html'
    })
});
//  APPLICATION
const logbooks = new Application({
    name: 'logbooks',
    modules: [
        node_manager,
        button_main,
        link_list_logbooks,
        button_selection_eni,
        button_selection_packing,
        button_selection_process,
        button_selection_teamleaders,
        link_list_packing,
        button_organisations,
        button_selection_cf,
        button_selection_cp,
        button_selection_dm,
        button_selection_gs,
        button_packing,
        logbook_canfiller
    ]
});
//  WORKFLOW
////    Initial node updates.
node_manager
    .update( 'app_main', document.querySelector( '#app_main' ));
////
link_list_logbooks.on( 'remove', () => {
    button_main.root = node_manager.get( 'app_main' );
});
link_list_logbooks.on( 'render', () => {
    node_manager
        .update( 'hook_button_main' )
        .update( 'hook_logbook_link_eni' )
        .update( 'hook_logbook_link_packing' )
        .update( 'hook_logbook_link_process' )
        .update( 'hook_logbook_link_teamleaders' );
    button_main.root = node_manager.get( 'hook_button_main' );
    button_main.render();
    button_selection_eni.render();
    button_selection_packing.render();
    button_selection_process.render();
    button_selection_teamleaders.render();
});
link_list_packing.on( 'remove', () => {
    button_organisations.root = node_manager.get( 'app_main' );
});
link_list_packing.on( 'render', () => {
    node_manager
        .update( 'hook_button_organisations' )
        .update( 'hook_logbook_link_cf' )
        .update( 'hook_logbook_link_cp' )
        .update( 'hook_logbook_link_dm' )
        .update( 'hook_logbook_link_gs' );
    button_organisations.root = node_manager.get( 'hook_button_organisations' );
    button_organisations.render();
    button_selection_cf.render();
    button_selection_cp.render();
    button_selection_dm.render();
    button_selection_gs.render();
});
////    Routes
logbooks
    .on( '/logbooks', ( state, parameters ) => {
        const main = node_manager.get( 'app_main' )();
        main.classList.add( 'b' );
        main.classList.remove( 'grid-specific' );
        if ( state.namespaces.length === 1 ) link_list_logbooks.render();
    })
    .on( '/logbooks/:organisation', ( state, parameters ) => {
        if ( state.namespaces.length === 2 ) {
            if ( parameters.organisation === 'packing' ) link_list_packing.render();
            else {
                button_organisations.root = node_manager.get( 'app_main' );
                button_organisations.render();
            }
        }
    })
    .on( '/logbooks/:organisation/:type', ( state, parameters ) => {
        if ( parameters.type === 'cf' ) logbook_canfiller.render();
        else {
            node_manager.get( 'app_main' )().innerHTML = `Organisation: ${ parameters.organisation }`;
            button_packing.render();
        }
    });
////    Application events
////    Delegated events
//  EXPORT
export default logbooks;