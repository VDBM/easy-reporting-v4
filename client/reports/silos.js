/*  silos.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_silos' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    silos
const silos = new Application({
    name: 'silos',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
silos
////    Routes
    .on( '/silos', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = silos.id;
    });
////    Application events
////    Delegated events
//  EXPORT
export default silos;