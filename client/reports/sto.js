/*  sto.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
const menu_options = [
    { "kpi": "Triggersystem",          "link": "" },
    { "kpi": "BOS Observations",         "link": "" },
    { "kpi": "Incidents Safety",       "link": "" },
    { "kpi": "Defects Safety",         "link": "" },
    { "kpi": "Incidents Quality",    "link": "" },
    { "kpi": "Maffus",                  "link": "" },
    { "kpi": "Missed Tests",          "link": "" },
    { "kpi": "Defects Quality",      "link": "" },
    { "kpi": "Golden 6",                "link": "" },
    { "kpi": "MSS",                     "link": "" },
    { "kpi": "CIL Completion",           "link": "" },
    { "kpi": "CIL Out Of Limit",       "link": "" },
    { "kpi": "CL Completion",           "link": "" },
    { "kpi": "CL Out Of Limit",        "link": "" },
    { "kpi": "Defects Created",     "link": "" },
    { "kpi": "Defects Solved",       "link": "" },
    { "kpi": "Breakdowns",              "link": "" },
    { "kpi": "PM cards",              "link": "" },
    { "kpi": "Linespeed End Of Shift",     "link": "" },
    { "kpi": "Stuck Chips End Of Shift", "link": "" },
    { "kpi": "Stuck Chips Average", "link": "" },
    { "kpi": "Stops Shingling", "link": "" },
    { "kpi": "PQM", "link": "" },
    { "kpi": "Overpack", "link": "" },
    { "kpi": "Netweight Rejects", "link": "" },
    { "kpi": "Stops Can Filler", "link": "" },
    { "kpi": "Stops Gasser Seamer", "link": "" },
    { "kpi": "Stops Case Packer", "link": "" },
    { "kpi": "Downtime M&P", "link": "" },
    { "kpi": "Downtime Packing", "link": "" },
    { "kpi": "Downtime Planned", "link": "" },
    { "kpi": "Downtime Process", "link": "" },
    { "kpi": "Downtime Other", "link": "" },
    { "kpi": "Scrap M&P", "link": "" },
    { "kpi": "Scrap Packing", "link": "" },
    { "kpi": "Scrap Planned", "link": "" },
    { "kpi": "Scrap Process", "link": "" },
    { "kpi": "Scrap Other", "link": "" }
];
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_sto' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    sto
const sto = new Application({
    name: 'sto',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
sto
////    Routes
    .on( '/sto', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = '<a href="#/sto/packing">STO - Packing</a>';
    })
    .on( '/sto/:organisation', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = `${ sto.id } => organisation: ${ parameters.organisation }`;
    });
////    Application events
////    Delegated events
//  EXPORT
export default sto;