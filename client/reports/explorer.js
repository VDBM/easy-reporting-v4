/*  explorer.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_explorer' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    explorer
const explorer = new Application({
    name: 'explorer',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
explorer
////    Routes
    .on( '/explorer', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = explorer.id;
    })
    .on( '/explorer/:resource', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = `${ explorer.id } => resource: ${ parameters.resource }`;
    })
    .on( '/explorer/:resource/:organisation', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = `${ explorer.id } => resource: ${ parameters.resource } => organisation: ${ parameters.organisation }`;
    });
////    Application events
////    Delegated events
//  EXPORT
export default explorer;