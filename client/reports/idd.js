/*  idd.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*       header for this file
*       authenticator.js
*       dropdown.js
*       dropdown_labelled.js  DO we Really need this??? Seems kinda useless long term.
*       dropdown_linked.js
*       input_datetime.js
*       sort_date_time.js
*       fogure out if we need the templates or if we'll define these inside components.
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
//import Authenticator from '../core/authenticator.js';
//import Dropdown from '../components/dropdown.js';
//import Dropdown_linked from '../components/dropdown_linked.js';
import Input_datetime from '../components/input_datetime.js';
import Menu_links from '../components/menu_links.js';
import { fetch, root_services } from '../core/util.js';
//import sort_datet_time from '../components/sort_date_time.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_idd' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    authenticator
//const authenticator = new Authenticator();
////    dropdowns
/*
const dropdown_actor = new Dropdown({
    config: undefined,
    name: 'actor',
    root: undefined,
    state: undefined,
    template: undefined
});
const dropdown_assignee = new Dropdown({
    config: undefined,
    name: 'assignee',
    root: undefined,
    state: undefined,
    template: undefined    
});
const dropdown_id_order = new Dropdown({
    config: undefined,
    name: 'id_order',
    root: undefined,
    state: undefined,
    template: undefined
});
*//*  Place data
*   places.mechelen.department, 
*   places.mechelen.line,
*   places.mechelen.area,
*   places.mechelen.location,
*   places.mechelen.equipment,
*   places.mechelen.part
*/
/*const dropdown_linked_place = new Dropdown_linked({
    config: undefined,
    name: 'place',
    root: undefined,
    state: undefined,
    template: undefined
});
const dropdown_organisation = new Dropdown({
    config: undefined,
    name: 'organisation',
    root: undefined,
    state: undefined,
    template: undefined 
});
const dropdown_plant = new Dropdown({
    config: undefined,
    name: 'plant',
    root: undefined,
    state: undefined,
    template: undefined    
});
const dropdown_staging = new Dropdown({
    config: undefined,
    name: 'staging',
    root: undefined,
    state: undefined,
    template: undefined
});
const dropdown_type = new Dropdown({
    config: undefined,
    name: 'type',
    root: undefined,
    state: undefined,
    template: undefined
});
*/const input_datetime_duetime = new Input_datetime({
    config: undefined,
    name: 'duetime',
    root: undefined,
    state: undefined,
    template: undefined
});
////    input_datetime_endtime
const input_datetime_endtime = new Input_datetime({
    config: undefined,
    name: 'endtime',
    root: undefined,
    state: undefined,
    template: undefined
});
////    input_datetime_starttime
const input_datetime_starttime = new Input_datetime({
    config: undefined,
    name: 'starttime',
    root: undefined,
    state: undefined,
    template: undefined    
});
////    idd
const idd = new Application({
    name: 'idd',
    modules: [
        input_datetime_endtime,
        input_datetime_starttime,
        node_manager
    ]
});
//  WORKFLOW
idd
////    Routes
    .on( '/idd', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = '<a href="#/idd/eni">IDD - ENI</a>';
    })
    .on( '/idd/:organisation', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = `${ idd.id } => organisation: ${ parameters.organisation }<br><br><a href="#/idd">IDD - MAIN</a>`;
    });
////    Application events
////    Delegated events
//  EXPORT
export default idd;