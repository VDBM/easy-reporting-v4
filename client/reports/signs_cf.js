/*  report_signs_cf.mjs
*   @language:  ES6
*   @version:   1.0.0
*   @creator:   VDBM
*   @date:      2018-09-11
*/
/*  API
*    
*      
*       
*/
/*  TODO
*
*       1)  Is het citrix toestel dat ER in bezit heeft nog bruikbaar.
*               JA
*
*       2)  Is IE11 al beschikbaar? Indien niet, is de Chrome die eruitziet als IE11 beschikbaar?
*               Chrome versie
*
*       3)  Hoe moeten we de citrix configureren? Kunnen we dit zelf of moet Steve dit doen?
                    request cant find what im looking for. 7 tot 20 dagen
                    eens je ziet ie er assigned is, mail je die mens om te vragen wanneer dit kalar is wegens dringend nodig.
*               Steve Woods.
*
*       4)  Ticket met serienummer en MAC adres: Te vinden op het kaartje dat je uit de citrix kan trekken.
*           Automatische opstart en login en navigeren naar de juiste webpagina en fullscreen zetten:
*               vragen aan william hoe we de account voor de schermen hebben opgezet en zulke account aanvragen:
*               BEKSCF03 nAAM IS CAN FILLLER
*               Eerst account aanvragen en dan ticket maken voor de citrix.
*           
*       5)  dirk of cedric vargen naar ladder. Cedric vragen voor. Cedric vragen of er FSRA nodig is.
*       6)  kabels, via local it proberen.
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_signs_cf' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    signs_cf
const signs_cf = new Application({
    name: 'signs_cf',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
signs_cf
////    Routes
    .on( '/signs/:machine/:line', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = `${ signs_cf.id } => machine: ${ parameters.machine } => line: ${ parameters.line }`;
    });
////    Application events
////    Delegated events
//  EXPORT
export default signs_cf;