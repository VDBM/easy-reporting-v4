/*  pcs.mjs
*   @language:      ES6
*   @version:       v1.0.0
*   @user:          VDBM
*   @date:          2018-09-11
*/
/*  API
*
*
*
*/
/*  TODO
*
*
*
*/
//  DEPENDENCIES
////    Import
import Application from '../core/application.js';
import Collection_nodes from '../core/collection_nodes.js';
////    Helpers
////    Data
//  COMPONENTS
////    collection_nodes_root
const node_manager = new Collection_nodes( 'app_pcs' );
node_manager.update( 'app_main', document.querySelector( '#app_main' ));
////    pcs
const pcs = new Application({
    name: 'pcs',
    modules: [
        node_manager
    ]
});
//  WORKFLOW
pcs
////    Routes
    .on( '/pcs', ( state, parameters ) => {
        node_manager.get( 'app_main' )().innerHTML = pcs.id;
    });
////    Application events
////    Delegated events
//  EXPORT
export default pcs;